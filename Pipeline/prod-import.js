const settings = require("../account-config.json");
const fs = require('fs');

const importFolder = "Production";
const netsuiteFolder = "\"/SuiteScripts\"";
const email = settings.Production.email;
const account = settings.Production.account;
const url = settings.Production.url;
const role = settings.Production.role;
const tokenID = settings.Production.tokenID;
const tokensecret = settings.Production.tokenSecret;
try{
    if(email!=="" && account!=="" && role!=="" && url!=="")
        {
            createScript(email, account, url, role, importFolder, netsuiteFolder,  tokenID, tokensecret);
        }else{
            fs.writeFile('script-importfiles.sh', ` `, function (err) {
                if (err) throw err;
                console.log('Emptied script-importfiles.sh');
              });
        
              fs.writeFile('script-listfiles.sh', ` `, function (err) {
                if (err) throw err;
                console.log('Emptied script-listfiles.sh');
              });
        }
}catch(e){
    console.log(e.message);
}


function createScript(email, account, url, role, importfolder) {

fs.readFile('listfiles.txt', 'utf8', function(err, contents) {
    //Remove the authenticated description
    let splitRemove = "";
    splitRemove = contents.replace('Using token based authentication. The following options will no longer be supported after NetSuite 20.2 release: -email, -role, -account, and -url. Start using the authenticate command to grant authorization to your roles and accounts. For more information, see https://system.netsuite.com/app/help/helpcenter.nl?fid=section_157052592790.html. ','');

    //add double quotes in each path for sdf path syntax
    splitRemove = splitRemove.split(".js ").join(".js\" \"");
    //splitRemove = splitRemove.split(".txt ").join(".txt\" \"");
    
    //Remove the last word word "Done."
    let str = splitRemove;
    let lastIndex = str.lastIndexOf(`"Done.`);
    str = str.substring(0, lastIndex);
    str = "\""+str;

    console.log(str);
//script file to be generated and run in terminal 
let importFiles =`#!/usr/bin/expect -f

set timeout 20

spawn ./sdfcli importfiles -paths ${str} -account ${account} -email ${email} -p /opt/atlassian/pipelines/agent/build/${importfolder} -role ${role} -url ${url}

expect "Existing files will be overwritten, do you want to continue? Type YES to continue.\\r"

send -- "Yes\\r"

expect eof`;

    fs.writeFile('script-importfiles.sh', `${importFiles}`, function (err) {
        if (err) throw err;
        console.log('script-importfiles.sh script generated');
      });

});

}

