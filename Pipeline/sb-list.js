const settings = require("../account-config.json");
const fs = require('fs');

const netsuiteFolder = "\"/SuiteScripts\"";
const email = settings.Sandbox.email;
const account = settings.Sandbox.account;
const url = settings.Sandbox.url;
const role = settings.Sandbox.role;
const tokenID = settings.Sandbox.tokenID;
const tokensecret = settings.Sandbox.tokenSecret;

try{
    if(email!=="" && account!=="" && role!=="" && url!=="")
        {
            createScript(email, account, url, role, netsuiteFolder, tokenID, tokensecret);
        }else{
            fs.writeFile('script-importfiles.sh', ` `, function (err) {
                if (err) throw err;
                console.log('Saved!');
              });
        
              fs.writeFile('script-listfiles.sh', ` `, function (err) {
                if (err) throw err;
                console.log('Saved!');
              });
        }
}catch(e){
    console.log(e.message);
}


function createScript(email, account, url, role, netsuitefolder, tokenid, tokensecret) {
//file_list=\"./sdfcli listfiles -folder ${netsuitefolder} -account ${account} -email ${email} -role ${role} -url ${url}\"
//-tokenid 0508bcf9823d5a92ab1b36425fbeab0d626e45939b2fecf8602e2fb06502eb1c
//-tokensecret bd312776d9c73b1676c0a25f392f9ece8de5a7906a9c2264a8be8e80e3d97458

let listfiles = `#!/bin/bash
./sdfcli savetoken -account ${account} -email ${email} -role ${role} -tokenid ${tokenid} -tokensecret ${tokensecret} -url ${url}

file_list=$(./sdfcli listfiles -folder ${netsuitefolder} -account ${account} -email ${email} -role ${role} -url ${url})
    
$echo $file_list
    
echo $file_list > listfiles.txt
`;
    fs.writeFile('script-listfiles.sh', `${listfiles}`, function (err) {
        if (err) throw err;
        console.log('Saved!');
      });

     
}

