# Deploy 2.0
#
# Runs from within Pipelines. Do not run manually.

# go to root directory
cd ..

# list file
ls

# current path
pwd

if [ -z "$(git push --porcelain)" ]; then 
 # Working directory clean
 #git checkout master
 #git pull origin master
 exit
else 
 git add .
 git commit -m "[skip ci] Build Number ${BITBUCKET_BUILD_NUMBER}"
 git push
 git log -1
 #set -e; exit;
 exit
fi