/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime','N/search', 'N/record', 'N/ui/serverWidget', 'N/url'],
    function(runtime,search, record, serverWidget, url) {
        function beforeLoad(scriptContext) {

            var currentRecord = scriptContext.newRecord;
            log.debug('currentRecord:', currentRecord);
            if (scriptContext.type == 'view') {
                var form = scriptContext.form;
                var recordType = currentRecord.type;
                var recordid = currentRecord.id;
                log.debug('recordType:', recordType);
                var transactionType = currentRecord.getValue('cseg_type_of_trans');
				var userObj = runtime.getCurrentUser();
				var ADMINISTRATOR = 3;
				log.debug("Internal ID of current user role: " + userObj.role);		

               var approvalStatus = currentRecord.getValue('custbody_ub_payout_status');


                var outputUrl = url.resolveScript({
                    scriptId: 'customscript_st_writecheck_plain_print',
                    deploymentId: 'customdeploy_rsk_st_writecheck_plainprnt',
                    returnExternalUrl: false
                });
                // Adding parameters to pass in the suitelet.
                outputUrl += '&action=GET';
                outputUrl += '&recordid=' + recordid;

                // Creating function to redirect to the suitelet.
                var stringScript = "window.open('" + outputUrl + "','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";

                if (transactionType == 1 && approvalStatus == 6) {
                    var printLabel = form.addButton({
                        id: 'custpage_printId',
                        label: 'Plain Print',
                        functionName: stringScript
                    });
                }
				
				  else if (transactionType == 1 && userObj.role == ADMINISTRATOR && approvalStatus == 3 || approvalStatus == 1) {
                    var printLabel = form.addButton({
                        id: 'custpage_printId',
                        label: 'Plain Print',
                        functionName: stringScript
                    });
                } 


            }


        }
        return {
            beforeLoad: beforeLoad
        };
    });