/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
  ** @Author     :  Meena
 ** @Dated       :  21 Feb 2020
 ** @Version     :  2.0
 ** @Description :  Suitelet to print the data from Write Check record after the userevent redirects here.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/record', 'N/xml', 'N/render', 'N/http', 'N/format', 'N/search'],
    function(record, xml, render, http, format, search) {
        function onRequest(context) {
            var ITEM = "Withholding Tax Payable - Expanded (WE_PH)";
            //log.debug('item',ITEM);

            if (context.request.method == 'GET') {
                // Getting the parameters from Userevent.
                var action = context.request.parameters.action;
                var recordid = context.request.parameters.recordid;
                var loadrec = record.load({
                    type: 'check',
                    id: recordid
                    //isDynamic: true
                });

                var subsidiary = loadrec.getText({
                    fieldId: 'subsidiary'
                });
                var supplierName = loadrec.getText({
                    fieldId: 'entity'
                }) || loadrec.getText({
                    fieldId: 'custbody_alternate_payee'
                });
                var pcvNo = loadrec.getValue({
                    fieldId: 'transactionnumber'
                });
                var address = loadrec.getValue({
                    fieldId: 'address'
                });
                var date = loadrec.getText({
                    fieldId: 'trandate'
                });
                var amountInWords = loadrec.getValue({
                    fieldId: 'custbody_cashsale_totalword'
                });
                log.debug('amountInWords', amountInWords);
                var totalAmount = loadrec.getValue({
                    fieldId: 'total'
                });



                var apvNumber = loadrec.getValue({
                    fieldId: 'custbody_apv_number'
                });
                var poNumber = loadrec.getValue({
                    fieldId: 'custbody_print_ref_po'
                });
                var rskNumber = loadrec.getValue({
                    fieldId: 'custbody_rskponum'
                });
                var bankAccount = loadrec.getText({
                    fieldId: 'account'
                });
                var checkNo = loadrec.getValue({
                    fieldId: 'tranid'
                });
                var datePrint = loadrec.getValue({
                    fieldId: 'custbody_nameon_checkprint'
                });

                var preparedBy = loadrec.getText({
                    fieldId: 'custbody_prepared_by'
                });
                var dateAndTimeCreated = loadrec.getText({
                    fieldId: 'custbody_date_time_create'
                });
                var terms = loadrec.getValue({
                    fieldId: 'terms'
                });
				
				var supplierId = loadrec.getValue({
					fieldId: 'entity'
				})
				
				log.audit('legalName',legalName);
				
				 var loadVendorRec = record.load({
                    type: 'vendor',
                    id: supplierId
                 });
				 
				 log.audit('loadVendorRec',loadVendorRec);
				 
				 var legalName = loadVendorRec.getText({
					 fieldId: 'legalname'
				 });
				 
				 log.audit('legalName',legalName);

                /* var currency = '₱'; */
                /* log.debug('currency',currency); */

                // Creating the htmlvar file to displayed in the suitelet.
                htmlvar = '';
                htmlvar += '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
                htmlvar += '<pdf>\n'
                htmlvar += '<head>';
                htmlvar += '<macrolist>';
                htmlvar += '<macro id="nlheader">';
                htmlvar += '<table align="center">';
                htmlvar += '<tr>';
                htmlvar += '<td colspan="4" align="center" padding-bottom="15" style="font-family: Times New Roman, Times, serif; font-size:20px"><b>' + xml.escape(subsidiary) + '- CHECK VOUCHER</b></td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="170px" padding-top="5" align="left" style="font-family: Times New Roman, Times, serif; font-size:10px"><p align="left"><b>PAID TO:&nbsp;</b></p></td>';
                htmlvar += '<td width="340px" padding-top="5" padding-left="5px" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>' + xml.escape(supplierName == '' ? legalName : supplierName) + '</b></td>';
                htmlvar += '<td width="170px" padding-top="5" align="left" style="font-family: Times New Roman, Times, serif; font-size:10px"><p align="left"><b>PCV NO:&nbsp;</b></p></td>';
                htmlvar += '<td width="200px" padding-top="5" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>' + xml.escape(pcvNo) + '</b></td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="120px" padding-top="5" style="font-family: Times New Roman, Times, serif; font-size:10px;"><b>ADDRESS:&nbsp;</b></td>';
                htmlvar += '<td width="380px" padding-top="5" padding-left="5px" padding-right="30px" style="font-family: Times New Roman, Times, serif; font-size:10px;"><b>' + xml.escape(address) + '</b></td>';
                htmlvar += '<td padding-top="5" padding-left="22px" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>DATE:</b></td>';
                htmlvar += '<td padding-top="5" padding-right="20px" style="font-family: Times New Roman, Times, serif; font-size:10px;">' + xml.escape(date) + '</td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td></td>';
                htmlvar += '<td padding-top="8" align="left" style="text-align:left!important; font-family: Times New Roman, Times, serif; font-size:10px"><b>Received from ' + subsidiary + '&nbsp;the amount of&nbsp;' + amountInWords + '&nbsp;(' + xml.escape(totalAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + ')&nbsp;in full payment of amount described below:</b></td></tr>';
                htmlvar += '</table>';
                htmlvar += '</macro>';

                htmlvar += '<macro id="nlfooter">';
                htmlvar += '<table align="center">';
                htmlvar += '<tr>';
                htmlvar += '<td colspan = "4" align="center" font-size="font-family: Times New Roman, Times, serif; 10px">--- Nothing Follows ---</td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="100px" align="center" padding-top="5" padding-left="6" style="font-family: Times New Roman, Times, serif; font-size:10px">Printed By:</td>';
                htmlvar += '<td width="100px" align="center" padding-top="5" padding-left="17" style="font-family: Times New Roman, Times, serif; font-size:10px">Checked By:</td>';
                htmlvar += '<td width="100px" align="center" padding-top="5" padding-left="38" style="font-family: Times New Roman, Times, serif; font-size:10px">Approved By:</td>';
                htmlvar += '<td width="100px" align="center" padding-top="5" padding-left="47" style="font-family: Times New Roman, Times, serif; font-size:10px">Received By:</td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="200px" padding-top="5" padding-left="5">_______________</td>';
                htmlvar += '<td width="200px" padding-top="5" padding-left="19">_______________</td>';
                htmlvar += '<td width="200px" padding-top="5" padding-left="41">_______________</td>';
                htmlvar += '<td width="200px" padding-top="5" padding-left="47">_______________</td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="200px" align="center" padding-top="1" padding-left="8" style="font-family: Times New Roman, Times, serif; font-size:10px"><p align="center">' + xml.escape(preparedBy.toUpperCase()) + '</p></td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="200px" align="center" padding-left="3" style="font-family: Times New Roman, Times, serif; font-size:10px">' + xml.escape(dateAndTimeCreated) + '</td>';
                htmlvar += '</tr>';
                htmlvar += '</table>';




                htmlvar += '</macro>';
                htmlvar += '</macrolist>';

                htmlvar += '</head>';


                htmlvar += '<body header="nlheader" header-height="30%" footer="nlfooter" footer-height="20%" width="17.50cm" height="14.00cm" padding="0.3in 0.5in 0.5in 0.5in">';

                htmlvar += '<table width="100%" border="1" align="center">';
                htmlvar += '<tr>';
                htmlvar += '<th width="55%" border-right="1" border-bottom="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>APV No. / DESCRIPTION</b></th>';
                htmlvar += '<th width="15%" border-right="1" border-bottom="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>PO NO.</b></th>';
                htmlvar += '<th width="15%" border-right="1" border-bottom="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>TERMS</b></th>';
                htmlvar += '<th width="15%" border-bottom="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>AMOUNT</b></th>';
                htmlvar += '</tr>';



                var numLines = loadrec.getLineCount({
                    sublistId: 'expense'
                });

                log.debug('numLines:', numLines);



                for (var i = 0; i < numLines; i++) { //1

                    var getAccount = loadrec.getSublistText({
                        sublistId: 'expense',
                        fieldId: 'account',
                        line: i
                    });
                    log.debug('getAccount', getAccount);

                    var getMemo = loadrec.getSublistText({
                        sublistId: 'expense',
                        fieldId: 'memo',
                        line: i
                    });

                    log.debug('getMemo', getMemo);

                    var getAmount = loadrec.getSublistValue({
                        sublistId: 'expense',
                        fieldId: 'amount',
                        line: i
                    });

                    log.debug('getAmount', getAmount);

                    var referencePo = loadrec.getText({
                        fieldId: 'custbody_ref_doc'
                    });

                    log.emergency('referencePo', referencePo);

                    var referenceRFP = loadrec.getText({
                        fieldId: 'custbody_ref_rfp_num'
                    });

                    log.emergency('referenceRFP', referenceRFP);




                    htmlvar += '<tr>';
                    htmlvar += '<td border-right="1" style="font-family: Times New Roman, Times, serif; font-size:10px">' + (referencePo == undefined ? '' : referencePo) + ',&nbsp;&nbsp;' + (referenceRFP == undefined ? '' : referenceRFP) + ',&nbsp;&nbsp;&nbsp;' +
                        getAccount + ',&nbsp;&nbsp;&nbsp;' + getMemo + '&nbsp; </td>';
                    htmlvar += '<td border-right="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"></td>';
                    htmlvar += '<td border-right="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"></td>'; //terms
                    htmlvar += '<td align="right" style="font-family: Times New Roman, Times, serif; font-size:10px">' + xml.escape(getAmount == undefined ? '0' : getAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + '</td>';
                    htmlvar += '</tr>';



                }
                htmlvar += '</table>';
                htmlvar += '<table width="100%" align="center" border-bottom="1" border-left="1" border-right="1">';
                htmlvar += '<tr>';
                htmlvar += '<td border-bottom="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>PAYMENT INFORMATION</b></td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td padding-left="3px" style="font-family: Times New Roman, Times, serif; font-size:10px"><b>Check No.:&nbsp; ' + xml.escape(bankAccount) + ',&nbsp;&nbsp;&nbsp;' + xml.escape(checkNo) + ',&nbsp;&nbsp;' + xml.escape(datePrint == undefined ? ' ' : datePrint) + '&nbsp;&nbsp;PHP' + xml.escape(totalAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + '</b></td>';
                htmlvar += '</tr>';

                htmlvar += '</table>';


                htmlvar += '</body>';
                htmlvar += '</pdf>';




                var file = render.xmlToPdf({
                    xmlString: htmlvar
                });

                context.response.writeFile(file, true);
                return;
            }
        }


        return {
            onRequest: onRequest
        };
    });