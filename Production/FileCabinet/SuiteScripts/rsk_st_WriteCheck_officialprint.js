/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
  ** @Author     :  Meena
 ** @Dated       :  21 Feb 2020
 ** @Version     :  2.0
 ** @Description :  Suitelet to print the data from Write Check record after the userevent redirects here.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/record', 'N/xml', 'N/render', 'N/http', 'N/format', 'N/search'],
    function(record, xml, render, http, format, search) {
        function onRequest(context) {
            var ITEM = "Withholding Tax Payable - Expanded (WE_PH)";
            //log.debug('item',ITEM);

            if (context.request.method == 'GET') {
                // Getting the parameters from Userevent.
                var action = context.request.parameters.action;
                var recordid = context.request.parameters.recordid;
                var loadrec = record.load({
                    type: 'check',
                    id: recordid
                    //isDynamic: true
                });


                var transactionNumber = loadrec.getText({
                    fieldId: 'transactionnumber'
                });

                var payee = loadrec.getText({
                    fieldId: 'entity'
                });

                var transDate = loadrec.getText({
                    fieldId: 'custbody_trans_date'
                });

                log.debug('transDate', transDate);

                var address = loadrec.getText({
                    fieldId: 'address'
                });

                var preparedBy = loadrec.getText({
                    fieldId: 'custbody_prepared_by'
                });

                var dateAndTimeCreated = loadrec.getText({
                    fieldId: 'custbody_date_time_create'
                });

                var userTotal = loadrec.getValue({
                    fieldId: 'usertotal'
                });

                //userTotal = parseInt(userTotal);
                log.debug('userTotal', typeof(userTotal));

                var amountInWords = loadrec.getText({
                    fieldId: 'custbody_cashsale_totalword'
                });

                var accountName = loadrec.getText({
                    fieldId: 'account'
                });
              
				//add check number & check date format
                var checkNum = loadrec.getText({
                    fieldId: 'tranid'
                });

                var dateName = loadrec.getText({
                    fieldId: 'custbody_nameon_checkprint'
                });


                //log.debug('amountInWords',amountInWords);




                // Creating the htmlvar file to displayed in the suitelet.
                htmlvar = '';
                htmlvar += '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
                htmlvar += '<pdf>\n'
                htmlvar += '<head>';
                htmlvar += '<macrolist>';
                htmlvar += '<macro id="nlheader">';



                htmlvar += '</macro>';

                htmlvar += '<macro id="nlfooter">';

                htmlvar += '</macro>';
                htmlvar += '</macrolist>';

                htmlvar += '</head>';


                //htmlvar += '<body header="nlheader" header-height="30%" footer="nlfooter" footer-height="40%" width="21.50cm" height="16.50cm" padding="0.3in 0.5in 0.5in 0.5in" size="Letter">';
                htmlvar += '<body header="nlheader" header-height="2%" footer="nlfooter" footer-height="1%" padding="0.1in 0.1in 0.1in 0.1in" width="16.50cm" height="20cm">';

                htmlvar += '<table width="100%" padding-right="1in" padding-left="0.9in" padding-top="-6px">';
                htmlvar += '<tr>';
                htmlvar += '<td border="0" align="right" font-size="10px" padding-top="50px">No.&nbsp;' + xml.escape(transactionNumber) + '</td>';
                htmlvar += '</tr>';
                htmlvar += '</table>'

                htmlvar += '<table border="0" width="100%" padding-right="1in" padding-left="0.9in">';
                htmlvar += '<tr>';
                htmlvar += '<td border="0" align="left" font-size="11px" width="200px" padding-top="20px">&nbsp;' + xml.escape(payee) + '</td>';
                htmlvar += '<td border="0" align="right" font-size="11px" width="20px" padding-top="20px">&nbsp;' + transDate + '</td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td border="0" colspan="2" font-size="10px" align="left" padding-top="2px" style="text-align: right;">&nbsp;<p align="left">' + xml.escape(address) + '</p></td>';
                htmlvar += '</tr>';
                htmlvar += '</table>';

				  htmlvar += '<table width="100%" padding-top="40px">'
				   htmlvar += '<tr>';
                    htmlvar += '<td width="550px" font-size="12px" align="left" padding-left="40px">Payment for A.P. Voucher #:</td>';
                    htmlvar += '<td width="80px"></td></tr>';
					
                var numLines = loadrec.getLineCount({
                    sublistId: 'expense'
                });

                log.debug('numLines:', numLines);

					

                for (var i = 0; i < numLines; i++) { //1

                    var getAccount = loadrec.getSublistText({
                        sublistId: 'expense',
                        fieldId: 'account',
                        line: i
                    });
                    log.debug('getAccount', getAccount);

                    var getAmount = loadrec.getSublistText({
                        sublistId: 'expense',
                        fieldId: 'amount',
                        line: i
                    });


                    log.debug('getAmount', typeof(getAmount));
				

                 
                   
                    htmlvar += '<tr>';

                    htmlvar += '<td align="left" font-size="12px" padding-top="8px" padding-right="100px" padding-left="40px">' + xml.escape(getAccount) + '</td>';
                    htmlvar += '<td align="right" font-size="12px" padding-right="80px">' + getAmount + '</td>';

                    htmlvar += '</tr>';
                }

                htmlvar += '<tr>';
                htmlvar += '<td align="left" font-size="12px" padding-top="8px" padding-right="100px" padding-left="40px"><p align="left">' + xml.escape(accountName) + '&nbsp;' + checkNum + '&nbsp;' + dateName +'</p></td></tr>';

                htmlvar += '</table>'; 



                htmlvar += '<table width="100%" padding-top="220px">';
                htmlvar += '<tr>';
                htmlvar += '<td border="0" width="420px" align="left" style="font-size:10px" padding-left="10px"><b>Printed By:&nbsp;' + preparedBy + '&nbsp;' + dateAndTimeCreated + '</b></td>';
                htmlvar += '<td border="0" width="80px" font-size="10px" align="left"><b>' + userTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</b></td>';
                htmlvar += '</tr>';

                htmlvar += '</table>';
                //total words 
                htmlvar += '<table width="100%" padding-top="9px" padding-left="2.3in" padding-right="0.6in">';
                htmlvar += '<tr>';
                htmlvar += '<td align="center" border="0" width="160px" style="font-size:10px"><b>' + amountInWords + '</b></td>';
                htmlvar += '</tr>';
                htmlvar += '</table>';

                htmlvar += '<table width="100%" padding-top="-3px" padding-left="3.8in" padding-right="1.3in">';
                htmlvar += '<tr>';
                htmlvar += '<td align="center" border="0" font-size="10px" width="80px"><b>' + userTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</b></td>';
                htmlvar += '</tr>';
                htmlvar += '</table>'; 



                htmlvar += '</body>';
                htmlvar += '</pdf>';




                var file = render.xmlToPdf({
                    xmlString: htmlvar
                });

                context.response.writeFile(file, true);
                return;

            }
        }

        return {
            onRequest: onRequest
        };
    });