/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
 /***************************************************************************************  
 ** Copyright (c) 1998-2020 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      : Aayushi Chaplot
 **@Dated       : 6-02-2020
 **@Version     : 2.x
 **@Description : Describing about item should not be repeat in Sublist of purchase order
 ***************************************************************************************/
define(['N/record'],function(record) {
    /**
     * Validation function to be executed when sublist line is inserted.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    /* function validateInsert(scriptContext) {
		//alert('inside function');
		log.debug('inside function');
		var scriptObj = scriptContext.currentRecord;
		if(scriptContext.sublistId == 'item'){
			var getItemValue = scriptObj.getCurrentSublistText({
				sublistId:'item',
				fieldId:'item'
			});
			alert( 'get item ='+getItemValue);
			
			
			
		}
		
    } */
    function validateLine(scriptContext) {
		var currentRec = scriptContext.currentRecord;

		var lineCount = currentRec.getLineCount({sublistId: "item"});
		
		if(scriptContext.sublistId == 'item'){
			
			var currentItemValue = currentRec.getCurrentSublistValue({
				sublistId:'item',
				fieldId:'item'
			});
			//log.debug('current Item value=',currentItemValue);
			
			for(var i = 0; i < lineCount; i++){				
				
              var currentIndex = currentRec.getCurrentSublistIndex({sublistId: 'item'});
              
              if(currentIndex == i)
                continue;
              
				var lineItemValue = currentRec.getSublistValue({
					sublistId:'item',
					fieldId:'item',
					line:i
				});
              
              	console.log(i+" - "+lineItemValue);
				
				if(currentItemValue == lineItemValue){
					alert('You can not add same item twice');
                  	return false;
				}
			}
			
		}
		return true;
    }

   

    return {		
		//validateInsert: validateInsert,
		validateLine: validateLine
	};
    
});
