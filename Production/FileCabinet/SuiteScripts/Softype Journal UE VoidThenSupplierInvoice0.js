/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
*/
define(['N/runtime','N/transaction', 'N/search', 'N/record', 'N/url', 'N/redirect'],
    function(runtime,transaction, search, record, url, redirect) {
        		
        function afterSubmit(scriptContext) {
			
			var scriptObj = runtime.getCurrentScript();
			var JOURNAL_TYPE_2550M = scriptObj.getParameter({name: 'custscript_2550m_journal_type'});
			
            var currentRecord = scriptContext.newRecord;
            log.emergency('afterSubmit',currentRecord);
			
			var journalType = currentRecord.getValue({
				fieldId: 'custbody_journaltypes'
			});
			
            log.emergency('journalType',journalType);
            log.emergency('JOURNAL_TYPE_2550M',JOURNAL_TYPE_2550M);
            log.emergency('journalType == JOURNAL_TYPE_2550M',journalType == JOURNAL_TYPE_2550M);
			if (Number(journalType) == Number(JOURNAL_TYPE_2550M)) {
				var output = url.resolveScript({
					scriptId: 'customscript_rsk_st_checkforvoidjv',
					deploymentId: 'customdeploy_rsk_st_checkforvoidjv'
				});
				
				redirect.redirect({
					url: output,
					parameters: {"internalid": currentRecord.id}
				});
			}
		}
		
		
        return {
            afterSubmit: afterSubmit
		};
	});	