/**

 * @NApiVersion 2.x

 * @NScriptType Suitelet

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 22/01/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : Suitelet To Provide Reason Upon Rejecting A Check.  

 **************************************************************************************************************************************/
 define(['N/ui/serverWidget', 'N/search', 'N/record', 'N/url', 'N/https', 'N/runtime', 'N/format', 'N/error','N/redirect'],
    function(serverWidget, search, record, url, https, runtime, format, error,redirect) {
        function onRequest(context) {

            //var action = context.request.parameters.action;
            log.audit('Context', JSON.stringify(context));

            if (context.request.method === 'GET') {

                var recordType=context.request.parameters.recType;
                var recordId=context.request.parameters.recId;          

                var form = serverWidget.createForm({
                    title: 'Reason Of Rejection'
                });
                /*var fileFilters = new Array();
                var fileColumns = new Array();

                fileFilters.push(search.createFilter({
                    name: 'name',
                    operator: 'is',
                    values: 'Softype_CS_Consolidate_Mul_Invoice.js'
                }));

                var clientSearch = search.create({
                    type: 'file',
                    filters: fileFilters
                }).run().getRange(0, 1000);
                //log.debug('FileSearch', clientSearch);

                form.clientScriptFileId = clientSearch[0].id;*/

                var recordTypeField = form.addField({
                    id: 'custpage_record_type',
                    type: serverWidget.FieldType.TEXT,
                    label: 'Record',
                });
                if (recordType) {
                    recordTypeField.defaultValue = recordType;
                }
                recordTypeField.updateDisplayType({
                    displayType: serverWidget.FieldDisplayType.DISABLED
                });

                var recordIdField = form.addField({
                    id: 'custpage_record_id',
                    type: serverWidget.FieldType.TEXT,
                    label: 'Record Id',
                });
                if (recordId) {
                    recordIdField.defaultValue = recordId;
                }
                recordIdField.updateDisplayType({
                    displayType: serverWidget.FieldDisplayType.DISABLED
                });

                var commentField = form.addField({
                    id: 'custpage_comment',
                    type: serverWidget.FieldType.TEXTAREA,
                    label: 'Reason',
                });
                commentField.isMandatory = true;

                
                var submitButton = form.addSubmitButton({
                    label: 'Submit'
                });

                var scriptObj = runtime.getCurrentScript();
                log.audit("Remaining Usage", scriptObj.getRemainingUsage());

                context.response.writePage(form);
            }else{
                var scriptObj = runtime.getCurrentScript();
                log.audit("Remaining Usage Start", scriptObj.getRemainingUsage());
                log.emergency('parameters',context.request.parameters)
                var comment = context.request.parameters.custpage_comment;
                var recordType = context.request.parameters.custpage_record_type;
                var recordId = context.request.parameters.custpage_record_id;

                log.debug('recordType after submit',recordType)
                log.debug('recordId after submit',recordId)

                var checkRecObj = record.load({
                    type: recordType,
                    id: recordId,
                    isDynamic: true
                });

                checkRecObj.setValue({
                    fieldId: 'usertotal',
                    value: 0
                });
        
                checkRecObj.setValue({
                    fieldId: 'total',
                    value: 0
                });
            
                checkRecObj.setValue({
                    fieldId: 'voided',
                    value: true
                });
                checkRecObj.setValue({
                    fieldId: 'memo',
                    value: 'VOID'
                });

                checkRecObj.setValue({
                    fieldId: 'custbody_reject_reason',
                    value: comment
                });

                checkRecObj.setValue({
                    fieldId: 'custbody_ub_payout_status',
                    value: 2
                });
        
                var lineCountArray=[];
                lineCountArray.push(checkRecObj.getLineCount('item'))
                lineCountArray.push(checkRecObj.getLineCount('expense')) 
                log.emergency('lineCountArray',lineCountArray)
                for(var lineArr=0;lineArr<lineCountArray.length;lineArr++){
                    var sublist='';
                    if(lineArr==0){
                        sublist='item'
                    }else{
                        sublist='expense'
                    }
                    for(var i = 0; i < lineCountArray[lineArr]; i++){
                        checkRecObj.selectLine({sublistId: sublist,line: i});
                        checkRecObj.setCurrentSublistValue({sublistId:sublist,fieldId:'amount',line:i,value:0});
                        checkRecObj.setCurrentSublistValue({sublistId:sublist,fieldId:'grossamt',line:i,value:0});
                        checkRecObj.commitLine({sublistId:sublist});
                    }
                }

                checkRecObj.save()

                /*record.submitFields({
                    type: recordType,
                    id: recordId,
                    values: {
                        'custbody_reject_reason':comment
                    }
                });*/
        
                redirect.toRecord({
                 type : recordType,
                 id : recordId,
                 //parameters: {'e':'T'}
                });
            }
        }

        return {
            onRequest: onRequest
        };

});

