/**
	* @NApiVersion 2.x
	* @NScriptType ClientScript
	* @NModuleScope SameAccount
*/


/***************************************************************************************  
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**@Author      :  Amol Jagkar
	**@Dated       :  24/02/2020
	**@Version     :  2.0
	**@Description :  Discount Calculation for PO
	**@Version     :  1.0
***************************************************************************************/
define(['N/record','N/search','N/runtime'],
	function(record,search,runtime) {
		
		var STANDALONE_FORM_ID = runtime.getCurrentScript().getParameter('custscript_po_standalone_formid');  // 136 - Standalone Purchase Order
		var IMPORT_FORM_ID = runtime.getCurrentScript().getParameter('custscript_po_import_formid');  // 146 - Import Purchase Order
		
		var discount = {};
			
		function fieldChanged(scriptContext) {
			var currentRecordObj = scriptContext.currentRecord;
			var currentForm = currentRecordObj.getValue('customform');
			
			// console.log("scriptContext - "+scriptContext);
			// console.log("currentForm - "+currentForm);
			// console.log("FORM_ID - "+FORM_ID);
			// console.log("currentForm == FORM_ID - "+(Number(currentForm) == Number(FORM_ID)));
			
			if(Number(currentForm) == Number(STANDALONE_FORM_ID) || Number(currentForm) == Number(IMPORT_FORM_ID)){
				
				if(scriptContext.sublistId == 'item'){
					
					if(scriptContext.fieldId == 'item' || scriptContext.fieldId == 'custcol_custom_rate' || scriptContext.fieldId == 'quantity'){						
						refreshValues(currentRecordObj);
					}
					
					if(scriptContext.fieldId == "custcol_custom_rate"){
						var customRate = currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_custom_rate'
						});
						var quantity = currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'quantity'
						});
						
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_custom_rate',
							value: parseFloat(customRate),//.toFixed(3),
							ignoreFieldChange: true
						});
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'rate',
							value: parseFloat(customRate)//.toFixed(3)
						});
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'amount',
							value: (parseFloat(customRate) * quantity).toFixed(2)
						});
					}
					
					if(	scriptContext.fieldId == 'custcol_discper' /*First Discount*/ || 
						scriptContext.fieldId == 'custcol_2nddiscper' /*Second Discount*/ || 
						scriptContext.fieldId == 'custcol_disc3rd' /*Third Discount*/ ){
						
						console.log("Discount Applied");
						
					
						// First Discount
						if(scriptContext.fieldId == "custcol_discper"){
							CalculateFirstDiscount(currentRecordObj, true);
						}
						
						//Second Discount
						if(scriptContext.fieldId == "custcol_2nddiscper"){
							CalculateSecondDiscount(currentRecordObj, true);							
						}
						
						//Third Discount
						if(scriptContext.fieldId == "custcol_disc3rd"){
							CalculateThirdDiscount(currentRecordObj, true);
						}
					}
					
					if(scriptContext.fieldId == 'custcol_vatinclusivefields'){
					
						var vatInclusive = currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_vatinclusivefields'
						});
						
						var quantity = GetQuantity(currentRecordObj);
						
						/* var rate = parseFloat(currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'rate'
						})); */
						
						var customRate = currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_custom_rate'
						});
						
						
						//////////////////////
						var firstDiscount = currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_discper'
						});

						var secondDiscount = currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_2nddiscper'
						});

						var thirdDiscount = currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_disc3rd'
						});
						
						var discountType = "";
						
						var flag = false;
						
						if(firstDiscount != 0){
							discount["First"] = firstDiscount;
							discountType = "First";
							flag = true;
						}
						if(secondDiscount != 0){
							discount["Second"] = secondDiscount;
							discountType = "Second";
							flag = true;
						}
						if(thirdDiscount != 0){
							discount["Third"] = thirdDiscount;
							discountType = "Third";
							flag = true;
						}

						var rateAfterDiscount = calculateDiscount(quantity, discount, discountType, customRate);
						
						
						//////////////////////
						
						
						
						console.log("vatInclusive => " + vatInclusive);
						console.log("Rate => " + rate);
						
						if(flag){
						console.log("rateAfterDiscount.standardRate => "+rateAfterDiscount.standardRate);
						var rate = rateAfterDiscount.standardRate;
						
							if(vatInclusive){// && rate == customRate){
							
								console.log("rate / 1.12 => " + (rate / 1.12));
								
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'rate',
									value: (rate / 1.12)//.toFixed(3)
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'amount',
									value: ((rate / 1.12) * quantity).toFixed(2)
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_discamt',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_discamt'})) / 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_2nddiscamt',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_2nddiscamt'})) / 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_3rddiscamt',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_3rddiscamt'})) / 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_subtotal1',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_subtotal1'})) / 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_linedistotal',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_linedistotal'})) / 1.12).toFixed(2),
									ignoreFieldChange: true
								});
							}else{
								var rate = currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_custom_rate'
								});
								refreshValues(currentRecordObj);
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'rate',
									value: (rate)//.toFixed(3)
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'amount',
									value: (rate * quantity).toFixed(2)
								});
								/* currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_discamt',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_discamt'})) * 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_2nddiscamt',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_2nddiscamt'})) * 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_3rddiscamt',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_3rddiscamt'})) * 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_subtotal1',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_subtotal1'})) * 1.12).toFixed(2),
									ignoreFieldChange: true
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_linedistotal',
									value: (parseFloat(currentRecordObj.getCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'custcol_linedistotal'})) * 1.12).toFixed(2),
									ignoreFieldChange: true
								}); */
								
							}
						}else{
						
							var rate = currentRecordObj.getCurrentSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_custom_rate'
							});
							if(vatInclusive){
								
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'rate',
									value: (rate / 1.12)//.toFixed(3)
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'amount',
									value: ((rate / 1.12) * quantity).toFixed(2)
								});
							}else{
							
								refreshValues(currentRecordObj);
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'rate',
									value: rate//.toFixed(3)
								});
								currentRecordObj.setCurrentSublistValue({
									sublistId: 'item',
									fieldId: 'amount',
									value: (rate * quantity).toFixed(2)
								});
							}
						}
						/* // First Discount
						CalculateFirstDiscount(currentRecordObj, false);
						
						// Second Discount
						CalculateSecondDiscount(currentRecordObj, false);							
						
						// Third Discount
						CalculateThirdDiscount(currentRecordObj, false); */
						
					}
				}
			}
		}
		function validateLine(scriptContext){
			
			var currentRecordObj = scriptContext.currentRecord;
			var currentForm = currentRecordObj.getValue('customform');
			
			if(Number(currentForm) == Number(STANDALONE_FORM_ID) || Number(currentForm) == Number(IMPORT_FORM_ID)){
				
				var quantity = GetQuantity(currentRecordObj);
				var rate = GetRate(currentRecordObj);
				
				var firstDiscount = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_discper'
				});

				var secondDiscount = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_2nddiscper'
				});

				var thirdDiscount = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_disc3rd'
				});
						
				
				var vatInclusive = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_vatinclusivefields'
				});
				
				var vat = 1;
				
				if(vatInclusive){
					vat = 1.12;
				}			
				
				
				
				/* currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_linedistotal',
					value: (currentRecordObj.getCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_subtotal1'
					}) * vat).toFixed(2)
				}); */
				var discountType = "";
						
				var flag = false;
				
				if(firstDiscount != 0){
					discount["First"] = firstDiscount;
					discountType = "First";
					flag = true;
				}
				if(secondDiscount != 0){
					discount["Second"] = secondDiscount;
					discountType = "Second";
					flag = true;
				}
				if(thirdDiscount != 0){
					discount["Third"] = thirdDiscount;
					discountType = "Third";
					flag = true;
				}

				var rateAfterDiscount = calculateDiscount(quantity, discount, discountType, rate);
				if(!!(rateAfterDiscount)){
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_linedistotal',
						value: (rateAfterDiscount.amount).toFixed(2)
					});
				}
				
				
				console.log("Line flag " + flag);
				if(flag){
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_customamttotal',
						value: (quantity * rate).toFixed(2)
					});	
				}else{
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_customamttotal',
						value: (currentRecordObj.getCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'grossamt'
						}))
					});					
				}
				
			}
			
			return true;
		}
		
		function GetQuantity(currentRecordObj){
			var quantity = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'quantity'
			});
			return quantity;
						
		}
		
		function GetRate(currentRecordObj){
			var rate = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_custom_rate'
			}) || currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'rate'
			});
			return rate;
		}
		
		function CalculateFirstDiscount(currentRecordObj, flag){
	
			var quantity = GetQuantity(currentRecordObj);
			
			var rate = GetRate(currentRecordObj);
			if(!flag){
				rate = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'rate'
				});
			}
			
			var firstDiscount = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_discper'
			});
			
			if(Number.isNaN(parseFloat(firstDiscount))){
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_discper',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_discamt',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_2nddiscper',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_2nddiscamt',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_disc3rd',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_3rddiscamt',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_subtotal1',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_linedistotal',
					value: 0,
					ignoreFieldChange: true
				});
				
			}else{
				
				discount["First"] = firstDiscount;
				
				var amountAfterFirstDiscount = calculateDiscount(quantity, discount, "First", rate)
				
				if(!!(amountAfterFirstDiscount)){
				
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'rate',
						value: (amountAfterFirstDiscount.standardRate)//.toFixed(3)
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'amount',
						value: (quantity * (amountAfterFirstDiscount.standardRate)).toFixed(2)
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_discamt',
						value: (amountAfterFirstDiscount.discount).toFixed(2),
						ignoreFieldChange: true
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_subtotal1',
						value: (amountAfterFirstDiscount.amount),//.toFixed(3),
						ignoreFieldChange: true
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_linedistotal',
						value: (amountAfterFirstDiscount.amount),//.toFixed(3),
						ignoreFieldChange: true
					});
					
					if(flag){
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_2nddiscper',
							value: 0,
							ignoreFieldChange: true
						});
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_2nddiscamt',
							value: 0,
							ignoreFieldChange: true
						});
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_disc3rd',
							value: 0,
							ignoreFieldChange: true
						});
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_3rddiscamt',
							value: 0,
							ignoreFieldChange: true
						});
					}
				}			
			}
		}
		
		function CalculateSecondDiscount(currentRecordObj, flag){
			
			var quantity = GetQuantity(currentRecordObj);
			
			var rate = GetRate(currentRecordObj);
			if(!flag){
				rate = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'rate'
				});
			}
		
			var firstDiscount = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_discper'
			});
		
			var secondDiscount = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_2nddiscper'
			});
			
			if(Number.isNaN(parseFloat(firstDiscount)) && Number.isNaN(parseFloat(secondDiscount))){
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_2nddiscper',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_2nddiscamt',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_disc3rd',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_3rddiscamt',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_subtotal1',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_linedistotal',
					value: 0,
					ignoreFieldChange: true
				});
			}else{
				
				discount["First"] = firstDiscount;
				discount["Second"] = secondDiscount;
				
				var amountAfterSecondDiscount = calculateDiscount(quantity, discount, "Second", rate)
				
				if(!!(amountAfterSecondDiscount)){
				
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'rate',
						value: (amountAfterSecondDiscount.standardRate)//.toFixed(3)
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'amount',
						value: (quantity * amountAfterSecondDiscount.standardRate).toFixed(2)
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_2nddiscamt',
						value: (amountAfterSecondDiscount.discount).toFixed(2),
						ignoreFieldChange: true
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_subtotal1',
						value: (amountAfterSecondDiscount.amount).toFixed(2),
						ignoreFieldChange: true
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_linedistotal',
						value: (amountAfterSecondDiscount.amount).toFixed(2),
						ignoreFieldChange: true
					});
					if(flag){
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_disc3rd',
							value: 0,
							ignoreFieldChange: true
						});
						currentRecordObj.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_3rddiscamt',
							value: 0,
							ignoreFieldChange: true
						});
					}
				}
			}
		}
		
		function CalculateThirdDiscount(currentRecordObj, flag){
			
			var quantity = GetQuantity(currentRecordObj);
			
			var rate = GetRate(currentRecordObj);
			if(!flag){
				rate = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'rate'
				});
			}
		
			var firstDiscount = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_discper'
			});
		
			var secondDiscount = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_2nddiscper'
			});
		
			var thirdDiscount = currentRecordObj.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_disc3rd'
			});
			
			if(Number.isNaN(parseFloat(firstDiscount)) && Number.isNaN(parseFloat(secondDiscount)) && Number.isNaN(parseFloat(thirdDiscount))){
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_disc3rd',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_subtotal1',
					value: 0,
					ignoreFieldChange: true
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_linedistotal',
					value: 0,
					ignoreFieldChange: true
				});
			}else{
				
				discount["First"] = firstDiscount;
				discount["Second"] = secondDiscount;
				discount["Third"] = thirdDiscount;
				
				var amountAfterThirdDiscount = calculateDiscount(quantity, discount, "Third", rate)
				
				if(!!(amountAfterThirdDiscount)){
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'rate',
						value: (amountAfterThirdDiscount.standardRate)//.toFixed(3)
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'amount',
						value: (quantity * amountAfterThirdDiscount.standardRate).toFixed(2)
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_3rddiscamt',
						value: (amountAfterThirdDiscount.discount),
						ignoreFieldChange: true
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_subtotal1',
						value: (amountAfterThirdDiscount.amount).toFixed(2),
						ignoreFieldChange: true
					});
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_linedistotal',
						value: (amountAfterThirdDiscount.amount).toFixed(2),
						ignoreFieldChange: true
					});
				}
			}
		}
		
		function refreshValues(currentRecordObj){
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_discper',
				value: 0,
				ignoreFieldChange: true
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_discamt',
				value: 0,
				ignoreFieldChange: true
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_2nddiscper',
				value: 0,
				ignoreFieldChange: true
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_2nddiscamt',
				value: 0,
				ignoreFieldChange: true
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_disc3rd',
				value: 0,
				ignoreFieldChange: true
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_3rddiscamt',
				value: 0,
				ignoreFieldChange: true
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId:'item',
				fieldId:'custcol_subtotal1',
				value:0,
				ignoreFieldChange: true
			});	
			currentRecordObj.setCurrentSublistValue({
				sublistId:'item',
				fieldId:'custcol_totaldiscount_calculation',
				value:0,
				ignoreFieldChange: true
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_linedistotal',
				value: 0
			});
			currentRecordObj.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_vatinclusivefields',
				value:false,
				ignoreFieldChange: true
			});
		}
		
        //This function is used for the calculation	of all the discounts.
        function calculateDiscount(quantity, discount, discountType, rate) {
		
			console.log(discountType);
			if(discountType == "First"){
			
				console.log("First Discount = " + discount["First"]);
				console.log("Rate = " + rate);
				var firstDiscountCalculation = ((100 - discount["First"]) / 100) * rate;
				console.log("First Discount calculation = " + firstDiscountCalculation);
				
				var amountAfterFirstDiscount = quantity * (rate - firstDiscountCalculation);
				console.log("Amount after first Discount = " + amountAfterFirstDiscount);
				return {amount: amountAfterFirstDiscount, standardRate: firstDiscountCalculation, discount: amountAfterFirstDiscount};
				
			}else if(discountType == "Second"){
				console.log("First Discount = " + discount["First"]);
				console.log("Rate = " + rate);
				var firstDiscountCalculation = ((100 - discount["First"]) / 100) * rate;
				console.log("First Discount calculation = " + firstDiscountCalculation);
				
				var amountAfterFirstDiscount = quantity * (rate - firstDiscountCalculation);
				console.log("Amount after first Discount = " + amountAfterFirstDiscount);
				
				//Second Discount
				console.log("Second Discount = " + discount["Second"]);
				var secondDiscountCalculation = firstDiscountCalculation * (100 - discount["Second"]) / 100;
				console.log("Second Discount Calculation = " + secondDiscountCalculation);
				var secondDiscountAmount = (firstDiscountCalculation - secondDiscountCalculation) * quantity;
				console.log("Second Discount Calculation with Quantity = " + secondDiscountAmount);
				
				var amountAfterSecondDiscount = amountAfterFirstDiscount + secondDiscountAmount;
				return {amount: amountAfterSecondDiscount, standardRate: secondDiscountCalculation, discount: secondDiscountAmount};
				
			}else if(discountType == "Third"){
				console.log("First Discount = " + discount["First"]);
				console.log("Rate = " + rate);
				var firstDiscountCalculation = ((100 - discount["First"]) / 100) * rate;
				console.log("First Discount calculation = " + firstDiscountCalculation);
				
				var amountAfterFirstDiscount = quantity * (rate - firstDiscountCalculation);
				console.log("Amount after first Discount = " + amountAfterFirstDiscount);
				
				//Second Discount
				console.log("Second Discount = " + discount["Second"]);
				var secondDiscountCalculation = firstDiscountCalculation * (100 - discount["Second"]) / 100;
				console.log("Second Discount Calculation = " + secondDiscountCalculation);
				var secondDiscountAmount = (firstDiscountCalculation - secondDiscountCalculation) * quantity;
				console.log("Second Discount Calculation with Quantity = " + secondDiscountAmount);
				
				var amountAfterSecondDiscount = amountAfterFirstDiscount + secondDiscountAmount;
				
				//Third Discount
				console.log("Third Discount = " + discount["Third"]);
				var thirdDiscountCalculation = secondDiscountCalculation * (100 - discount["Third"]) / 100;
				console.log("Third Discount Calculation = " + thirdDiscountCalculation);
				var thirdDiscountAmount = (secondDiscountCalculation - thirdDiscountCalculation) * quantity;
				console.log("Third Discount Calculation with Quantity = " + thirdDiscountAmount);
				
				var amountAfterThirdDiscount = amountAfterSecondDiscount + thirdDiscountAmount;
				
				return {amount: amountAfterThirdDiscount, standardRate: thirdDiscountCalculation, discount: thirdDiscountAmount};
			}
		}
		return {			
			fieldChanged: fieldChanged,
			validateLine: validateLine
		};
		
	});							