/**
 * @NApiVersion 2.x
 * @NScriptType restlet
*/
/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Creates Item Fulfillment & Debit Note creation
***************************************************************************************/
define(['N/record', 'N/search', 'N/runtime', 'N/log', 'N/format'], function(record, search, runtime, log, format) {
	
	var RSK_ITEM_FULFILLMENT_FORM = 40;
	var GRNI_ACCOUNT = 115;
	var STATUS_APPROVED = 2;
	
	return {
		post : function(data){
		

			
			/* request= {
				"vraId": "29818",
				"typeofTransaction": "Plain",
				"itemData": [
					{
						"item": "2299",
						"quantity": "20"
					}
				]
			}
			*/
			
			if(typeof data === 'string'){
				data = JSON.parse(data);
			}
			
			log.debug("jsonData",JSON.stringify(data));		
			
			// https://tc39.github.io/ecma262/#sec-array.prototype.find
			if (!Array.prototype.find) {
			  Object.defineProperty(Array.prototype, 'find', {
				value: function(predicate) {
				  // 1. Let O be ? ToObject(this value).
				  if (this == null) {
					throw new TypeError('"this" is null or not defined');
				  }

				  var o = Object(this);

				  // 2. Let len be ? ToLength(? Get(O, "length")).
				  var len = o.length >>> 0;

				  // 3. If IsCallable(predicate) is false, throw a TypeError exception.
				  if (typeof predicate !== 'function') {
					throw new TypeError('predicate must be a function');
				  }

				  // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
				  var thisArg = arguments[1];

				  // 5. Let k be 0.
				  var k = 0;

				  // 6. Repeat, while k < len
				  while (k < len) {
					// a. Let Pk be ! ToString(k).
					// b. Let kValue be ? Get(O, Pk).
					// c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
					// d. If testResult is true, return kValue.
					var kValue = o[k];
					if (predicate.call(thisArg, kValue, k, o)) {
					  return kValue;
					}
					// e. Increase k by 1.
					k++;
				  }

				  // 7. Return undefined.
				  return undefined;
				},
				configurable: true,
				writable: true
			  });
			}
			
			
			try{
			
				var scriptObj = runtime.getCurrentScript();
				GRNI_ACCOUNT = scriptObj.getParameter({name: 'custscript_grni_account'});
				RSK_ITEM_FULFILLMENT_FORM = scriptObj.getParameter({name: 'custscript_rsk_item_fulfillment_form'});
				
				/* var vraSearch = search.create({
						type: search.Type.VENDOR_RETURN_AUTHORIZATION,
						columns: [{
							name: 'internalid'
						}],
						filters: [ 
						{
							name: 'custbody_rskvenretauth',
							operator: 'is',
							values: [data.vraId]
						}]
					}).run().getRange(0,1000);		

				log.debug("vraSearch", vraSearch); */
				
				var vraID = data.vraId;//vraSearch[0].id;
				log.debug("vraid", vraID);

				var mapping = {};

				var vendorreturnauthorizationSearchObj = search.create({
				   type: "vendorreturnauthorization",
				   filters:
				   [
					  ["type","anyof","VendAuth"], 
					  "AND", 
					  ["internalidnumber","equalto",vraID], 
					  "AND", 
					  ["mainline","is","F"], 
					  "AND", 
					  ["taxline","is","F"]
				   ],
				   columns:
				   [
					  search.createColumn({
						 name: "custitem_inventoryassetaccount",
						 join: "item",
						 label: "Inventory Account"
					  }),
					  search.createColumn({
						 name: "internalid",
						 join: "item",
						 label: "Internal ID"
					  }),
					  search.createColumn({name: "location", label: "Location"}),
					  search.createColumn({name: "cseg_type_of_trans", label: "Type of Transaction"}),
					  search.createColumn({name: "item", label: "Item"}),
					  search.createColumn({name: "department", label: "Department"}),
					  search.createColumn({name: "class", label: "Division"}),
					  search.createColumn({name: "subsidiary", label: "Subsidiary"}),
					  search.createColumn({name: "grossamount", label: "Amount (Gross)"}),
					  search.createColumn({
						 name: "cseg_itemclassifica",
						 join: "item",
						 label: "Item Classification"
					  })
				   ]
				}).run().getRange(0,999);
				// var searchResultCount = vendorreturnauthorizationSearchObj.runPaged().count;
				var subsidiary;
				var grossamount = 0;
				var checkedItems = {};
				
				// log.debug("vendorreturnauthorizationSearchObj result count",searchResultCount);
				// vendorreturnauthorizationSearchObj.run().each(function(result){
				
				for(var zz = 0; zz < vendorreturnauthorizationSearchObj.length; zz++){
				
					//try{
						var data = JSON.parse(JSON.stringify(vendorreturnauthorizationSearchObj[zz]));
						
						log.debug("data", JSON.stringify(data));

						var classification, account;

						var item = (data["values"]["item.internalid"][0].value);
						var department = (data["values"]["department"][0].value);
						var division = (data["values"]["class"][0].value);												

						if((data["values"]["item.cseg_itemclassifica"]).length != 0){
							classification = (data["values"]["item.cseg_itemclassifica"][0].value);
						}
						subsidiary = (data["values"]["subsidiary"][0].value);
						location = (data["values"]["location"][0].value);
						typeOfTransaction = (data["values"]["cseg_type_of_trans"][0].value);
						grossamount = (data["values"]["grossamount"]);
						log.debug("grossamount - "+item, grossamount);
						
						if(grossamount == 0)
							continue;

						if((data["values"]["item.custitem_inventoryassetaccount"]).length != 0){
							account = (data["values"]["item.custitem_inventoryassetaccount"][0].value);
						}else{
							if(classification != null || classification != undefined){
								var rskInventorySegment = search.create({
									type: "customrecord_subwiseitemmap",
									columns: [{
										name: 'custrecord_inventoryac_custseg'
									}],
									filters: [{
										name: 'custrecord_subsidiaryinventoryitem',
										operator: 'is',
										values: [subsidiary]
									},{
										name: 'custrecord_itemclassificationsegment',
										operator: 'is',
										values: [classification]
									}]
								}).run().getRange(0,1000);

								log.debug("rskInventorySegment", JSON.stringify(rskInventorySegment));
								account = rskInventorySegment[0].getValue("custrecord_inventoryac_custseg");
							}
						}

						mapping[item] = {account:account, grossamount: grossamount, department: department, division: division};

					// }catch(err){
						// log.debug("Error",err);
					// }
				   // return true;
				}
				
				var jvRecord = CreateJV(mapping, subsidiary, location, typeOfTransaction);
												
				var newRecord = record.transform({
					fromType: record.Type.VENDOR_RETURN_AUTHORIZATION,
					fromId: vraID,
					toType: record.Type.ITEM_FULFILLMENT,
					isDynamic: false,
				});
				
				var itemData = data.itemData;
				
				newRecord.setValue({
					fieldId: "customform",
					value: RSK_ITEM_FULFILLMENT_FORM
				});
				
				newRecord.setValue({
					fieldId: "custbody_createdfrom",
					value: jvRecord
				});
										
				/* for(var i = 0; i < itemData.length; i++){
					var lineNum = i;
					
					log.debug("itemData[i]", itemData[i]);
					if(itemData[i].quantity > 0){
						var lineItem  = search.create({
							type: search.Type.ITEM,
							columns: [{
								name: 'internalid'
							}],
							filters: [ 
							{
								name: 'custitem_rskitemid',
								operator: 'is',
								values: [itemData[i].item]
							}]
						}).run().getRange(0,1000);
						
						log.debug("lineItem[i]", lineItem[0].id);
						
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'item',
							line: lineNum,
							value: parseInt(lineItem[0].id)
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'quantity',
							line: lineNum,
							value: itemData[i].quantity
						});
					}
				} */
								
				var recordFulfillment = newRecord.save();
							
				log.debug("recordid",recordFulfillment);
				
				/* var debitNoteRecord = record.transform({
					fromType: record.Type.VENDOR_RETURN_AUTHORIZATION,
					fromId: vraID,
					toType: record.Type.VENDOR_CREDIT,
					isDynamic: false,
				});
				
				var typeOfTransaction = search.create({
					type: "customrecord_cseg_type_of_trans",
					columns: [{
						name: 'internalid'
					}],
					filters: [ 
					{
						name: 'name',
						operator: 'is',
						values: [data.typeofTransaction]
					}]
				}).run().getRange(0,1000);
				
				debitNoteRecord.setValue({
					fieldId: "cseg_type_of_trans",
					value: typeOfTransaction[0].getValue("internalid")
				});
				
				var recordDebitNote = debitNoteRecord.save(); */
				
				var JVRec = record.load({type:"journalentry", id: jvRecord});
				JVRec.setValue({
					fieldId: "custbody_createdfrom",
					value: recordFulfillment
				});
				JVRec.save();
				
				log.debug("Result", JSON.stringify({itemFulfillment : recordFulfillment/* , debitNote : recordDebitNote */}));
				return {itemFulfillment : recordFulfillment/* , debitNote : recordDebitNote */};
			}catch(err){
				log.debug("Error", err);
				return {message:"Failure", error: err};		
			}
			
			//return "Success";
		}
	}
	
	function CreateJV(mapping, subsidiary, location, typeOfTransaction){
			
		
		var NO_DEPARTMENT = 102, NO_DIVISION = 7;
		
		log.debug("mapping", JSON.stringify(mapping));
		
		var newJVRecord = record.create({
			type: "journalentry",
			isDynamic: false
		});	
		
		newJVRecord.setValue({
			fieldId: "customform",
			value: 120
		});
				
		newJVRecord.setValue({
			fieldId: "subsidiary",
			value: subsidiary
		});
				
		newJVRecord.setValue({
			fieldId: "location",
			value: location
		});
		
		newJVRecord.setValue({
			fieldId: "cseg_type_of_trans",
			value: typeOfTransaction
		});
		
		newJVRecord.setValue({
			fieldId: "approvalstatus",
			value: STATUS_APPROVED
		});	
		
		/* newJVRecord.setValue({
			fieldId: "department",
			value: NO_DEPARTMENT
		});	
		
		newJVRecord.setValue({
			fieldId: "class",
			value: NO_DIVISION
		});	 */
		
		var counter = 0, totalDebit = 0;
		for (var key in mapping) {
			if (mapping.hasOwnProperty(key)) {
				
				var data = mapping[key];
				
				log.debug("data", JSON.stringify(data));
		
				newJVRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'account',
					line: counter,
					value: data.account
				});
				newJVRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'debit',
					line: counter,
					value: 0
				});
				
				newJVRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'credit',
					line: counter,
					value: data.grossamount
				});
								
				newJVRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'department',
					line: counter,
					value: Number(data.department)
				});
				newJVRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'class',
					line: counter,
					value: Number(data.division)
				});
				
				totalDebit += Number(data.grossamount);
				counter++;
			}
		}
				
		newJVRecord.setSublistValue({
			sublistId: 'line',
			fieldId: 'account',
			line: counter,
			value: GRNI_ACCOUNT
		});
		
		newJVRecord.setSublistValue({
			sublistId: 'line',
			fieldId: 'debit',
			line: counter,
			value: totalDebit
		});
		newJVRecord.setSublistValue({
			sublistId: 'line',
			fieldId: 'credit',
			line: counter,
			value: 0
		});
		newJVRecord.setSublistValue({
			sublistId: 'line',
			fieldId: 'department',
			line: counter,
			value: Number(NO_DEPARTMENT)
		});
		newJVRecord.setSublistValue({
			sublistId: 'line',
			fieldId: 'class',
			line: counter,
			value: Number(NO_DIVISION)
		});
		var recordid = newJVRecord.save({
					enableSourcing: true,
					ignoreMandatoryFields: true
				});
		log.debug("recordid JV",recordid);
		
		return recordid;
	}
	
	function parseAndFormatDateString(myDate) {

		var initialFormattedDateString = myDate;
		var parsedDateStringAsRawDateObject = format.parse({
			value: initialFormattedDateString,
			type: format.Type.DATE,

		});

		return parsedDateStringAsRawDateObject;
		
	}

});