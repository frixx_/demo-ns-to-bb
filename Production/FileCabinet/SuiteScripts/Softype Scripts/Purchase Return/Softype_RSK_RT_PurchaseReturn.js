/**
 * @NApiVersion 2.x
 * @NScriptType restlet
*/
/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Creates Vendor Return Authorization record
***************************************************************************************/
define(['N/record', 'N/search', 'N/log', 'N/format'], function(record, search, log, format) {
	return {
		post : function(data){
			
			/* request= {
				"requestID": "19",
				"date": "01/17/2020",
				"supplier": "634",
				"potype": "Local Purchase Order Trade",
				"referenceNumber": "StandandAlone4",
				"purchaseorder": null,
				"itemData": [
					{
						"item": "2006",
						"quantity": "3",
						"taxcode": "10",
						"rate": "12"
					}
				]
			} */
			
			// log.debug("data", data);		
			if(typeof data === 'string'){
				data = JSON.parse(data);
			}
			
			log.debug("jsonData",JSON.stringify(data));		
			
			
			// https://tc39.github.io/ecma262/#sec-array.prototype.find
			if (!Array.prototype.find) {
			  Object.defineProperty(Array.prototype, 'find', {
				value: function(predicate) {
				  // 1. Let O be ? ToObject(this value).
				  if (this == null) {
					throw new TypeError('"this" is null or not defined');
				  }

				  var o = Object(this);

				  // 2. Let len be ? ToLength(? Get(O, "length")).
				  var len = o.length >>> 0;

				  // 3. If IsCallable(predicate) is false, throw a TypeError exception.
				  if (typeof predicate !== 'function') {
					throw new TypeError('predicate must be a function');
				  }

				  // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
				  var thisArg = arguments[1];

				  // 5. Let k be 0.
				  var k = 0;

				  // 6. Repeat, while k < len
				  while (k < len) {
					// a. Let Pk be ! ToString(k).
					// b. Let kValue be ? Get(O, Pk).
					// c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
					// d. If testResult is true, return kValue.
					var kValue = o[k];
					if (predicate.call(thisArg, kValue, k, o)) {
					  return kValue;
					}
					// e. Increase k by 1.
					k++;
				  }

				  // 7. Return undefined.
				  return undefined;
				},
				configurable: true,
				writable: true
			  });
			}
			
			// try{
				
				/* var poid = search.create({
						type: search.Type.PURCHASE_ORDER,
						columns: [{
							name: 'internalid'
						}],
						filters: [ 
						{
							name: 'custbody_rskponum',
							operator: 'is',
							values: [data.purchaseorder]
						}]
					}).run().getRange(0,1000); */	
				
				var newRecord = null;
				
				var NO_DEPARTMENT = 102, NO_DIVISION = 7;
				
				if(data.purchaseorder != null){
					newRecord = record.transform({
						fromType: record.Type.PURCHASE_ORDER,
						fromId: data.purchaseorder,//poid[0].getValue("internalid"),
						toType: record.Type.VENDOR_RETURN_AUTHORIZATION,
						isDynamic: false,
					});
					
					var poRecord = record.load({
						type: "purchaseorder", 
						id: data.purchaseorder,
						isDynamic: false,
					});
					
					var poTypeID = poRecord.getValue({fieldId: 'custbody_purchaseordertypelist'});
					
					newRecord.setValue({
						fieldId: "custbody_purchaseordertypelist",
						value: poTypeID
					});
				} else{
					newRecord = record.create({
						type: record.Type.VENDOR_RETURN_AUTHORIZATION,
						isDynamic: false,
					});
					newRecord.setValue({
						fieldId: "tranid",
						value: data.referenceNumber
					});
					newRecord.setValue({
						fieldId: "entity",
						value: data.supplier
					});
					
					var potype = search.create({
						type: "customlist_potype",
						columns: [{
							name: 'internalid'
						}],
						filters: [ 
						{
							name: 'name',
							operator: 'is',
							values: [data.potype]
						}]
					}).run().getRange(0,1000);
					
					newRecord.setValue({
						fieldId: "custbody_purchaseordertypelist",
						value: potype[0].getValue("internalid")
					});
					
				}
				
				var numLines = newRecord.getLineCount({
					sublistId: 'item'
				});
				
				var lineItemDataIndex = {};
				
				for(var zz = 0; zz < numLines; zz++){
					var item = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'item',
						line: zz
					});
					var quantity = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'quantity',
						line: zz
					});
					var department = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'department',
						line: zz
					});
					var division = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'class',
						line: zz
					});
					
					newRecord.setSublistValue({
						sublistId: 'item',
						fieldId: 'quantity',
						value: 0,
						line: zz
					});
					
					if(lineItemDataIndex[item] == undefined){
						lineItemDataIndex[item] = [{"index": zz, "quantity": quantity, "department": department, "division": division, "flag": true}];
					}
					else{
						var oldData = lineItemDataIndex[item];
						oldData.push({"index": zz, "quantity": quantity, "department": department, "division": division, "flag": true});
						lineItemDataIndex[item] = oldData;
					}
				}
								
				// log.debug("newRecord",newRecord);
				
				/* newRecord.setValue({
					fieldId: "entity",
					value: data.supplier
				}); */
				
				newRecord.setValue({
					fieldId: "trandate",
					value: parseAndFormatDateString(data.date)
				});
				
				newRecord.setValue({
					fieldId: "custbody_rskvenretreq",
					value: data.requestID||""
				});
				
				var typeOfTransaction = search.create({
					type: "customrecord_cseg_type_of_trans",
					columns: [{
						name: 'internalid'
					}],
					filters: [ 
					{
						name: 'name',
						operator: 'is',
						values: [data.typeofTransaction]
					}]
				}).run().getRange(0,1000);
				
				newRecord.setValue({
					fieldId: "cseg_type_of_trans",
					value: typeOfTransaction[0].getValue("internalid")
				});
				
				/* newRecord.setValue({
					fieldId: "custbody_purchaseordertypelist",
					value: data.potype
				});	 */
				
				var itemData = data.itemData;
				for(var i = 0; i < itemData.length; i++){
					
					log.debug("itemData[i]", itemData[i]);
					
					var length = 0;
					
					if(lineItemDataIndex[itemData[i].item] != undefined){
						length = lineItemDataIndex[itemData[i].item].length;
					}
					
					/* newRecord.selectLine({
						sublistId: 'item',
						line: lineNum
					}); */
					
					var quantityRSK = itemData[i].quantity;
					
					var lineNum = 0, quantityremaining = 0;
					
					if(length > 0){
					
						for(var zz = 0; zz < lineItemDataIndex[itemData[i].item].length; zz++){
						
							lineNum = lineItemDataIndex[itemData[i].item][zz].index;
							quantityremaining = lineItemDataIndex[itemData[i].item][zz].quantity;
							var department = lineItemDataIndex[itemData[i].item][zz].department;
							var division = lineItemDataIndex[itemData[i].item][zz].division;
							
							var quantity = 0;
							if(quantityremaining <= quantityRSK){
								quantity = quantityremaining;
							}else if(quantityremaining > quantityRSK){
								quantity = quantityRSK;
							}
							quantityRSK = quantityRSK - quantity;
							
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'quantity',
								line: lineNum,
								value: quantity
							});
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'rate',
								line: lineNum,
								value: itemData[i].rate
							});
							
							if(department != null){
								newRecord.setSublistValue({
									sublistId: 'item',
									fieldId: 'department',
									line: lineNum,
									value: department
								});		
							}
							if(division != null){
								newRecord.setSublistValue({
									sublistId: 'item',
									fieldId: 'class',
									line: lineNum,
									value: division
								});	
							}
							
							/* newRecord.commitLine({
								sublistId: 'item'
							}); */
							/*  */
						}
				
					}else{
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'item',
							line: lineNum,
							value: itemData[i].item//lineItem[0].getValue("internalid")
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'quantity',
							line: lineNum,
							value: quantity
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'rate',
							line: lineNum,
							value: itemData[i].rate
						});
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'department',
							line: lineNum,
							value: NO_DEPARTMENT
						});		
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'class',
							line: lineNum,
							value: NO_DIVISION
						});	
						newRecord.setSublistValue({
							sublistId: 'item',
							fieldId: 'taxcode',
							line: lineNum,
							value: itemData[i].taxcode
						});
					}
				}
				log.debug("newRecord",newRecord);
				
				var recordID = newRecord.save({
					enableSourcing: true,
					ignoreMandatoryFields: true
				});
				log.debug("Result",JSON.stringify({NsId : recordID, POType : data.potype, typeofTransaction : data.typeofTransaction, RequestId : data.requestID, message: "Success"}));
				
				return {NsId : recordID, POType : data.potype, typeofTransaction : data.typeofTransaction, RequestId : data.requestID, message: "Success"};
			// }catch(err){
				// return {message:"Failure", error: err};		
			// }
			
			//return "Success";
		}
	}
	
	
	function parseAndFormatDateString(myDate) {

		var initialFormattedDateString = myDate;
		var parsedDateStringAsRawDateObject = format.parse({
			value: initialFormattedDateString,
			type: format.Type.DATE,

		});

		return parsedDateStringAsRawDateObject;
		
	}
		
});