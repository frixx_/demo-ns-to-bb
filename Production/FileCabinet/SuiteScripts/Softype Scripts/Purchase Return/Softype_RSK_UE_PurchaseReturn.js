/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Call RSK API for Location Master
***************************************************************************************/

define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, record, runtime, log) {
		
		function afterSubmit(context) {
		
			var newRecordDetails = context.newRecord;
			try{
				//log.debug('context',context);
				
				//log.debug('newRecordDetails', newRecordDetails);
				
				var scriptObj = runtime.getCurrentScript();
				var createAPI = scriptObj.getParameter({name: 'custscript_create_api_purchasereturn'});

				/**********************************************************
				 *************************API CALL************************
				**********************************************************/

				var headers = {
					"Content-Type": "application/json"
				};	//Headers if Any
				
				var response;
				var status = newRecordDetails.getValue({fieldId: 'status'});
				
				
				var createdfrom = newRecordDetails.getValue({fieldId: 'createdfrom'});
				
				var requestID = newRecordDetails.getValue({fieldId: 'custbody_rskvenretreq'});
				
				
				var potype = "";
								
				var poTypeRecord = record.load({
					type: "customlist_potype", 
					id: newRecordDetails.getValue({fieldId: 'custbody_purchaseordertypelist'}),
					isDynamic: false,
				});
				
				potype = poTypeRecord.getValue({fieldId: 'name'});
			
				
				log.debug('after submit status', status);
				
				if(status !== "B"){
					return;
				}
								
				var url = createAPI;	//RSK API URL
				var jsonBody = {
					"NsId": newRecordDetails.id,
					"POType": potype,
					"requestId":requestID
				};	
				
				log.debug("jsonBody - Create", jsonBody);			
				
				// If the API is https compliant
				response = http.request({
					method: https.Method.POST,
					url: url,
					body: JSON.stringify(jsonBody),
					headers: headers
				});
				log.debug("response", response);	
				
				var dataCode = response.code; 
				
				if(dataCode == "200"){
					log.debug("dataCode", dataCode);	
				}
			}
			catch(err){
				log.debug("errr", err);	
				SetFlag(newRecordDetails, 400, "Server Error");
				return;
			}
		}
				
		function SetFlag(newRecordDetails, dataCode, message){
			
			var rec = record.load({type : newRecordDetails.type, id : newRecordDetails.id});
			
			rec.setValue({
				fieldId: "custbody_vendor_return_flag",	//Flag Checkbox Field
				value: true,				
				ignoreFieldChange: true
			});
			rec.setValue({
				fieldId: "custbody_vendor_return_remark",	//Flag Remark Field
				value: dataCode + " : " + message,				
				ignoreFieldChange: true
			});
			rec.save();
		}
		
		return {
			afterSubmit: afterSubmit
		};
	});	