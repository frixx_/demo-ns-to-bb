/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
/***************************************************************************************
	** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  06th March, 2020
 ** @Version     :  2.x
 ** @Description :  Suitelet for Deposit Print.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/ui/serverWidget', 'N/record', 'N/render', 'N/xml', 'N/search'],

    function(serverWidget, record, render, xml, search) {


        function onRequest(context) {


            var recId = context.request.parameters.recordId;

            var objRecord = record.load({
                type: record.Type.DEPOSIT,
                id: recId,
                isDynamic: true

            });

            var docNo = objRecord.getText({
                fieldId: 'tranid'

            });


            var subsidiary = objRecord.getText({
                fieldId: 'subsidiary'

            });
            if (subsidiary == null)
                subsidiary == '';

            var subsidiary = objRecord.getText({
                fieldId: 'subsidiary'

            });
            if (subsidiary == null)
                subsidiary == '';

            var date = objRecord.getText({
                fieldId: 'trandate'

            });
            if (date == null)
                date == '';

            var memo = objRecord.getText({
                fieldId: 'memo'

            });
            if (memo == null)
                memo == '';


            var checkDate = objRecord.getText({
                fieldId: 'custbody_trans_date'

            });
            if (checkDate == null)
                checkDate == '';

            var checkLink = objRecord.getText({
                fieldId: 'custbody_writechecklink'

            });
            if (checkLink == null)
                checkLink == '';

            var checkLinkValue = objRecord.getValue({
                fieldId: 'custbody_writechecklink'

            });
            if (checkLinkValue == null)
                checkLinkValue == '';

            var RFP = objRecord.getText({
                fieldId: 'custbody_ref_rfp_num'

            });
            if (RFP == null)
                RFP == '';

            var checkAmount = objRecord.getText({
                fieldId: 'total'

            });

            var customerName = objRecord.getSublistText({
                sublistId: 'other',
                fieldId: 'entity',
                line: 0
            });

            var checkNumber = objRecord.getSublistValue({
                sublistId: 'other',
                fieldId: 'refnum',
                line: 0
            });

            if (checkNumber == null)
                checkNumber == '';

            var paymentMethod = objRecord.getSublistText({
                sublistId: 'other',
                fieldId: 'paymentmethod',
                line: 0
            });

            if (paymentMethod == null)
                paymentMethod == '';


            var bankAccount = objRecord.getSublistText({
                sublistId: 'other',
                fieldId: 'account',
                line: 0
            });

            if (bankAccount == null)
                bankAccount == '';




            if (checkLinkValue) {

                var fieldLookUp = search.lookupFields({
                    type: search.Type.CHECK,
                    id: checkLinkValue,
                    columns: ['custbody_trans_date', 'trandate', 'custbody_run_bal']
                });

                var writeCheckDate = fieldLookUp.trandate;
                var checkDate = fieldLookUp.custbody_trans_date;
                var balanceFromWriteCheck = fieldLookUp.custbody_run_bal;
            }




            var form = serverWidget.createForm({
                title: 'Print'
            });

            var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
            htmlvar += '<pdf>\n';




            htmlvar += '<body  font-size="9" width="9.5in" height="6.5in">';


            htmlvar += '<table border="1px" width="100%"  margin-bottom="20px">';
            htmlvar += '<tr border-bottom="1px">';


            htmlvar += '<td align="center"><b>' + xml.escape({
                xmlText: subsidiary
            }) + '</b></td> ';
            htmlvar += '</tr>';
            htmlvar += '<tr>';

            htmlvar += '<td align="center"><b>Collection</b></td> ';


            htmlvar += '</tr>';
            htmlvar += '</table>';




            htmlvar += '<table border="1px" width="100%"  margin-bottom="20px">';
            htmlvar += '<tr >';


            htmlvar += '<td width="15%"><p align="left"><b>Customer Name</b></p></td> ';
            htmlvar += '<td width="25%"><b>' + xml.escape({
                xmlText: customerName
            }) + '</b></td> ';
            htmlvar += '<td></td> '

            htmlvar += '<td width="15%" align="right"><b>Date</b></td> ';
            htmlvar += '<td width="25%" align="right"><b>' + date + '</b></td> ';

            htmlvar += '</tr>';

            htmlvar += '<tr>';


            htmlvar += '<td width="15%"><b>Memo</b></td> ';
            htmlvar += '<td width="25%"><b>' + xml.escape({
                xmlText: memo
            }) + '</b></td> ';
            htmlvar += '<td></td> '

            htmlvar += '<td width="15%" align="right"><p align="left"><b>Doc No.</b></p></td> ';
            htmlvar += '<td width="25%" align="right"><b>' + docNo + '</b></td> ';

            htmlvar += '</tr>';

            htmlvar += '</table>';




            htmlvar += '<table border="1px" width="100%"  margin-bottom="5px">';
            htmlvar += '<tr >';


            htmlvar += '<td><p align="left"><b>Write Check Date</b></p></td> ';
            htmlvar += '<td ><b>Write Check Link</b></td> ';


            htmlvar += '<td><b>RFP</b></td> ';
            htmlvar += '<td ><b>Balance From Write Check </b></td> ';
            htmlvar += '<td ></td> ';

            htmlvar += '</tr>';



            htmlvar += '<tr>';
            htmlvar += '<td>' + writeCheckDate + '</td> ';

            htmlvar += '<td>' + xml.escape({
                xmlText: checkLink
            }) + '</td> '

            htmlvar += '<td>' + xml.escape({
                xmlText: RFP
            }) + '</td> ';
            htmlvar += '<td>' + numberWithCommas(balanceFromWriteCheck) + '</td> ';
            htmlvar += '<td ></td> ';

            htmlvar += '</tr>';




            htmlvar += '<tr >';


            htmlvar += '<td><p align="left"><b>Check Date</b></p></td> ';
            htmlvar += '<td ><b>Check Number</b></td> ';
            htmlvar += '<td><b>Bank Account(Check Deposited)</b></td> '

            htmlvar += '<td><b>Check Amount</b></td> ';
            htmlvar += '<td ><b>Payment Method</b></td> ';


            htmlvar += '</tr>';



            htmlvar += '<tr>';
            htmlvar += '<td>' + checkDate + '</td> ';

            htmlvar += '<td>' + checkNumber + '</td> '
            htmlvar += '<td>' + xml.escape({
                xmlText: bankAccount
            }) + '</td> '
            htmlvar += '<td>' + checkAmount + '</td> ';
            htmlvar += '<td>' + paymentMethod + '</td> ';

            htmlvar += '</tr>';




            htmlvar += '</table>';




            htmlvar += '</body>\n</pdf>';


            var pdfFile = render.xmlToPdf({
                xmlString: htmlvar
            });

            return context.response.writeFile(pdfFile, true);




            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }




        }



        return {
            onRequest: onRequest
        };
    });