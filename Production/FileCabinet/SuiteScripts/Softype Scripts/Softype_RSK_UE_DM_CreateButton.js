/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */ 
 
define(['N/ui/serverWidget','N/log', 'N/error', 'N/runtime', 'N/record','N/url'],
  function(serverWidget,log, error, runtime, record, url) {

	function beforeLoad_createButton(context)
	{
		if (context.type != context.UserEventType.CREATE){
			log.debug('Start');
			var recordId = context.newRecord.id;
				log.debug('recordId',recordId);
			var loadRec = record.load({
				type: record.Type.VENDOR_CREDIT, 
				id: recordId,
				isDynamic: true,
			});

			var transactionType = loadRec.getValue({
				fieldId: 'cseg_type_of_trans'
			});
			log.debug('transactionType',transactionType);
			
			if (context.type === context.UserEventType.DELETE)
						return;
			
			try
			{
				if (context.type === context.UserEventType.VIEW){
					
					if(transactionType == 1){ // For Plain Debit Memo Print
						var outputUrl = url.resolveScript({
							scriptId:'customscript_softype_rsk_dmprint', // To be edited
							deploymentId:'customdeploy_softype_rsk_dmprint', // To be edited
							returnExternalUrl:false
						});
					} else if(transactionType == 2){ // For Official Debit Memo Print
						var outputUrl = url.resolveScript({
							scriptId:'customscript_softype_rsk_st_official_dm', // To be edited
							deploymentId:'customdeploy_softype_rsk_st_official_dm', // To be edited
							returnExternalUrl:false
						});
					} else {
						return;
					}
					log.debug('outputUrl',outputUrl);
					outputUrl += '&recordId='+recordId;
					var stringScript="window.open('"+outputUrl+"','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";

					 var printButton = context.form.addButton({
						id: 'custpage_print',
						label: 'Print',
						functionName: stringScript
					});
					
				}
			}
			catch(e)
			{
				log.error('Error Details', e.toString());
				throw e;
			}
		}
	}

	
	
		return {
		  beforeLoad: beforeLoad_createButton
		};
	  }
	);
