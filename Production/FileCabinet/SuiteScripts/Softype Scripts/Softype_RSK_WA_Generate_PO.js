/**
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  17th December, 2019
 ** @Version     :  2.x
 ** @Description :  This script is deployed on custom record(parent record) 'Liquidation of Broker/Employee'
					and attached to the Workflow 'Set Broker approval' to generate Supplier Invoice.
					This script creates standalone Vendor Bills.
					 
 
 ***************************************************************************************/
define(['N/record', 'N/runtime', 'N/redirect', 'N/error', 'N/format'],
    function(record, runtime, redirect, error, format) {


        function onAction(scriptContext) {


            var customForm = Number(runtime.getCurrentScript().getParameter("custscript_softype_wa_vendbill_form"));
            var withoutRFPCustomForm = Number(runtime.getCurrentScript().getParameter("custscript_softype_wa_without_rfp_form"));
			
            var approvedApprovalStatus = Number(runtime.getCurrentScript().getParameter("custscript_softype_wa_approval_status"));
            var whTax_PayableInternalId = Number(runtime.getCurrentScript().getParameter("custscript_softype_wa_wht_payable"));
            var whTax_PayableInternalIdExpense = Number(runtime.getCurrentScript().getParameter("custscript_softype_wa_wht_expense"));

            var id = scriptContext.newRecord.id;
            var type = scriptContext.newRecord.type;
            var objRecord = scriptContext.newRecord;



            var INVOICE_JSON = [];
            var supplierInvoiceArray = [];
            var COMMON_DATA = [];

            var itemFlag = false;
            var expenseFlag = false;

            var departmentForCurrentLine;
            var divisionForCurrentLine;



		// log.debug('id',id);
		// log.debug('type',type);
            // var objRecord = record.load({
                // type: type,
                // id: id,
                // isDynamic: true
            // });



            var approvalStatus = objRecord.getValue({
                fieldId: 'custrecord_approval_sts'
            });


            var invoiceStatus = objRecord.getValue({
                fieldId: 'custrecord_invoice_status'
            });

            var brokerLocation = objRecord.getValue({
                fieldId: 'custrecord_loc_broker'
            });

            var subsidiary = objRecord.getValue({
                fieldId: 'custrecord_subsidiary'
            });

            var department = objRecord.getValue({
                fieldId: 'custrecord_department'
            });

            var billOfLading = objRecord.getValue({
                fieldId: 'custrecord_bill_of_lading'
            });

            var checkLink = objRecord.getValue({
                fieldId: 'custrecord_write_chk_link'
            });

            var LCNumber = objRecord.getValue({
                fieldId: 'custrecord_lc_number'
            });

            var liquidationOfBillNo = objRecord.getValue({
                fieldId: 'recordid'
            });

            var RFPNumber = objRecord.getValue({
                fieldId: 'custrecord_rfp_number'
            });
			log.debug('RFPNumber',RFPNumber);

            var division = objRecord.getValue({
                fieldId: 'custrecord_liq_div'
            });
			log.debug('liquidationOfBillNo',liquidationOfBillNo);

            COMMON_DATA.push({

                brokerLocation: brokerLocation,
                subsidiary: subsidiary,
                department: department,
                billOfLading: billOfLading,
                approvalStatus: approvalStatus,
                checkLink: checkLink,
                LCNumber: LCNumber,
                liquidationOfBillNo: liquidationOfBillNo,
                RFPNumber: RFPNumber,
                division: division


            });


            if (invoiceStatus == true) {
                return;


            }



            var lineCount = objRecord.getLineCount({
                sublistId: 'recmachcustrecord_lin_broker_inv'
            });

            for (var i = 0; i < lineCount; i++) {


                var item = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_item_broker_inv',
                    line: i
                });

                var expense = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_expense_account',
                    line: i
                });

                var quantity = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_qty',
                    line: i
                });

                var rate = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_rate',
                    line: i
                });

                var tax = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_internalid_vat_code',
                    line: i
                });

                var amount = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_gross_amount',
                    line: i
                });

                var whTaxApply = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wht_check',
                    line: i
                });
                var whTaxCodeId = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wht_internalid',
                    line: i
                });

                var reference = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_ref_broker',
                    line: i
                });
                var supplier = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_supplier_bro_inv',
                    line: i
                });
                var currency = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_currency_bro_inv',
                    line: i
                });
                var exchangeRate = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_exc_rate',
                    line: i
                });
                var invoiceDate = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_inv_date',
                    line: i
                });

                if (!!(invoiceDate)) {

                    var formattedInvoiceDate = parseAndFormatDateString(invoiceDate);
                }

                var counterDate = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_counter_date',
                    line: i
                });

                if (!!(counterDate)) {

                    var formattedCounterDate = parseAndFormatDateString(counterDate);
                }

                var dueDate = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_due_date',
                    line: i
                });
                if (!!(dueDate)) {

                    var formattedDueDate = parseAndFormatDateString(dueDate);
                }

                var memo = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_memo_bro_inv',
                    line: i
                });

                var or = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_orref',
                    line: i
                });

                var orDate = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_or_date_bro_inv',
                    line: i
                });
                if (!!(orDate)) {

                    var formattedORDate = parseAndFormatDateString(orDate);
                }
                var terms = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_terms',
                    line: i
                });

                var typeOfTransaction = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'cseg_type_of_trans',
                    line: i
                });

                var itemClassification = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'cseg_itemclassifica',
                    line: i
                });

                // var VATType = objRecord.getSublistValue({
                    // sublistId: 'recmachcustrecord_lin_broker_inv',
                    // fieldId: 'custrecord_vat_type_brok_bill',
                    // line: i
                // });

                var whTaxRate = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wth_tax_rate',
                    line: i
                });

                var whTaxBaseAmount = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_amount',
                    line: i
                });

                var whTaxAmount = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wht_amount',
                    line: i
                });

                var whTaxAmountPositive = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wht_amount',
                    line: i
                });

                var tradeNonTrade = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_liqu_trade_nontrade',
                    line: i
                });

                var department = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_liquidation_department',
                    line: i
                });

                var division = objRecord.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_liquidation_division',
                    line: i
                });


                /** Converting WHTax Amount to negative , 
                	when supplier invoice is made it converts 
                	it to positive.
                	
                **/
                whTaxAmount = -Math.abs(whTaxAmount);


                INVOICE_JSON.push({
                    "supplier": supplier,
                    "reference": reference,
                    "item": item,
                    "quantity": quantity,
                    "rate": rate,
                    "tax": tax,
                    "amount": amount,
                    "whTaxApply": whTaxApply,
                    "currency": currency,
                    "exchangeRate": exchangeRate,
                    "formattedInvoiceDate": formattedInvoiceDate,
                    "formattedCounterDate": formattedCounterDate,
                    "formattedDueDate": formattedDueDate,
                    "memo": memo,
                    "or": or,
                    "formattedORDate": formattedORDate,
                    "terms": terms,
                    "typeOfTransaction": typeOfTransaction,
                    "itemClassification": itemClassification,
                  //  "VATType": VATType,
                    "whTaxCodeId": whTaxCodeId,
                    "whTaxRate": whTaxRate,
                    "whTaxBaseAmount": whTaxBaseAmount,
                    "whTaxAmount": whTaxAmount,
                    "expense": expense,
                    "tradeNonTrade": tradeNonTrade,
                    "department": department,
                    "division": division


                });



            }




            /** Grouping it reference wise **/
            var uniqueReferenceJson = groupBy(INVOICE_JSON, 'reference');

            log.debug('uniqueReferenceJson', JSON.stringify(uniqueReferenceJson));
           

            var FINAL_INVOICE_JSON = [];
            for (var key in uniqueReferenceJson) {


                var references = uniqueReferenceJson[key];

                // group it supplier wise
                var supplierObj = groupBy(references, 'supplier');

                for (var subKey in supplierObj) {


                    FINAL_INVOICE_JSON.push(supplierObj);


                }


            }

            log.debug('FINAL_INVOICE_JSON', JSON.stringify(FINAL_INVOICE_JSON));
            log.debug('FINAL_INVOICE_JSON.length', FINAL_INVOICE_JSON.length);




            for (var a = 0; a < FINAL_INVOICE_JSON.length; a++) {

                /** vendBillObj contains the key and its data **/
                var vendBillObj = FINAL_INVOICE_JSON[a];

                /** pKey is the grouped key.
					for is used because there is 
					no need to know length.									
                **/



                for (var pKey in vendBillObj) {

                    var whTaxAmountTotal = 0;
                    var whTaxAmountTotalExpense = 0;
                    var createVendorBill = vendBillObj[pKey]; // will give array of data


                    if (createVendorBill.length > 0) {


                        var vendoBillObj = record.create({
                            type: record.Type.VENDOR_BILL,
                            isDynamic: true,

                        });

                        

                        if (COMMON_DATA[0].RFPNumber) {
							
						
							
								
                            vendoBillObj.setValue({
                                fieldId: 'customform',
                                value: customForm,

                            });
						log.debug('RFPNumber',RFPNumber);
						log.debug('COMMON_DATA[0].RFPNumber',COMMON_DATA[0].RFPNumber);
							 vendoBillObj.setValue({
                            fieldId: 'custbody_ref_rfp_num',
                            value: COMMON_DATA[0].RFPNumber

                        });


                        } else{
							
						
							
                            vendoBillObj.setValue({
                                fieldId: 'customform',
                                value: withoutRFPCustomForm,

                            });



                        }
						
						
                        vendoBillObj.setValue({
                            fieldId: 'entity',
                            value: Number(vendBillObj[pKey][0].supplier),

                        });

						
                        vendoBillObj.setValue({
                            fieldId: 'subsidiary',
                            value: Number(COMMON_DATA[0].subsidiary),

                        });


                        vendoBillObj.setValue({
                            fieldId: 'location',
                            value: Number(COMMON_DATA[0].brokerLocation),

                        });

                        vendoBillObj.setValue({
                            fieldId: 'department',
                            value: Number(COMMON_DATA[0].department),

                        });

                        vendoBillObj.setValue({
                            fieldId: 'class',
                            value: COMMON_DATA[0].division,

                        });


                        vendoBillObj.setValue({
                            fieldId: 'tranid',
                            value: vendBillObj[pKey][0].reference,

                        });

                        vendoBillObj.setValue({
                            fieldId: 'currency',
                            value: Number(vendBillObj[pKey][0].currency),

                        });

                        vendoBillObj.setValue({
                            fieldId: 'approvalstatus',
                            value: approvedApprovalStatus

                        });


                        vendoBillObj.setValue({
                            fieldId: 'exchangerate',
                            value: vendBillObj[pKey][0].exchangeRate,

                        });

                       


                        if (vendBillObj[pKey][0].formattedInvoiceDate != "" &&  !!(vendBillObj[pKey][0].formattedInvoiceDate)) {

                            vendoBillObj.setValue({
                                fieldId: 'trandate',
                                value: parseAndFormatDateString(vendBillObj[pKey][0].formattedInvoiceDate)

                            });
                        }


                        if (vendBillObj[pKey][0].formattedDueDate != "" &&  !!(vendBillObj[pKey][0].formattedDueDate)) {
                            vendoBillObj.setValue({
                                fieldId: 'duedate',
                                value: parseAndFormatDateString(vendBillObj[pKey][0].formattedDueDate)
                            });
                        }

                        vendoBillObj.setValue({
                            fieldId: 'memo',
                            value: vendBillObj[pKey][0].memo,

                        });

                        vendoBillObj.setValue({
                            fieldId: 'custbody_ub_payout_or_num',
                            value: vendBillObj[pKey][0].or,

                        });


                        if (vendBillObj[pKey][0].formattedORDate != "" &&  !!(vendBillObj[pKey][0].formattedORDate)) {
                            vendoBillObj.setValue({
                                fieldId: 'custbody_ub_payout_or_date',
                                value: parseAndFormatDateString(vendBillObj[pKey][0].formattedORDate)

                            });
                        }

                        vendoBillObj.setValue({
                            fieldId: 'terms',
                            value: vendBillObj[pKey][0].terms,

                        });
						
						
                        vendoBillObj.setValue({
                            fieldId: 'cseg_type_of_trans',
                            value: vendBillObj[pKey][0].typeOfTransaction,

                        });
						
                        vendoBillObj.setValue({
                            fieldId: 'cseg_itemclassifica',
                            value: vendBillObj[pKey][0].itemClassification,

                        });


                        if (vendBillObj[pKey][0].formattedCounterDate != "" &&  !!(vendBillObj[pKey][0].formattedCounterDate)) {
                            vendoBillObj.setValue({
                                fieldId: 'custbody_counter_date',
                                value: parseAndFormatDateString(vendBillObj[pKey][0].formattedCounterDate)
                            });
                        }



					log.debug('COMMON_DATA[0].checkLink',COMMON_DATA[0].checkLink);
                        vendoBillObj.setValue({
                            fieldId: 'custbody_writechecklink',
                            value: COMMON_DATA[0].checkLink,

                        });

                        // vendoBillObj.setValue({
                            // fieldId: 'custbody_vattype',
                            // value: vendBillObj[pKey][0].VATType,

                        // });



                        vendoBillObj.setValue({
                            fieldId: 'custbody_letterofcreditnum',
                            value: COMMON_DATA[0].LCNumber,

                        });


                        vendoBillObj.setValue({
                            fieldId: 'custbody_billoflasdingnum',
                            value: COMMON_DATA[0].billOfLading,

                        });


                        vendoBillObj.setValue({
                            fieldId: 'custbody_liquidationnumber_bill',
                            value: COMMON_DATA[0].liquidationOfBillNo,

                        });

                        vendoBillObj.setValue({
                            fieldId: 'custbody_tradenontrade',
                            value: vendBillObj[pKey][0].tradeNonTrade,

                        });




                        for (var c = 0; c < createVendorBill.length; c++) {

                            if (createVendorBill[c].item) {

                                itemFlag = true;

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'item',
                                    value: Number(createVendorBill[c].item)
                                });



                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'quantity',
                                    value: createVendorBill[c].quantity


                                });


                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'taxcode',
                                    value: createVendorBill[c].tax


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'rate',
                                    value: createVendorBill[c].rate


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'custcol_4601_witaxapplies',
                                    value: createVendorBill[c].whTaxApply


                                });
		
                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'custcol_4601_witaxcode',
                                    value: createVendorBill[c].whTaxCodeId


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'custcol_4601_witaxrate',
                                    value: createVendorBill[c].whTaxRate


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'custcol_4601_witaxbaseamount',
                                    value: createVendorBill[c].whTaxBaseAmount


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'custcol_4601_witaxamount',
                                    value: createVendorBill[c].whTaxAmount


                                });
                                
                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'department',
                                    value: createVendorBill[c].department


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'class',
                                    value: createVendorBill[c].division


                                });



                                /** To calulate the total WHTax amount of the line items
                                	to set as rate when the Witholding Tax Item is added,
                                
                                **/
                                if (whTaxApply == true) {

                                    whTaxAmountTotal = whTaxAmountTotal + createVendorBill[c].whTaxAmount;


                                }

                                vendoBillObj.commitLine({

                                    sublistId: 'item'
                                });

                            } else {


                                expenseFlag = true;

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'account',
                                    value: Number(createVendorBill[c].expense)
                                });



                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'amount',
                                    value: createVendorBill[c].amount


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'department',
                                    value: createVendorBill[c].department


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'class',
                                    value: createVendorBill[c].division


                                });

                                departmentForCurrentLine = createVendorBill[c].department;
                                divisionForCurrentLine = createVendorBill[c].division;

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'taxcode',
                                    value: createVendorBill[c].tax

                                });

								
                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'custcol_4601_witaxapplies',
                                    value: createVendorBill[c].whTaxApply


                                });
								

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'custcol_4601_witaxcode_exp',
                                    value: createVendorBill[c].whTaxCodeId


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'custcol_4601_witaxrate_exp',
                                    value: createVendorBill[c].whTaxRate


                                });
								log.debug('createVendorBill[c].whTaxBaseAmount',createVendorBill[c].whTaxBaseAmount);
                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'custcol_4601_witaxbamt_exp',
                                    value: createVendorBill[c].whTaxBaseAmount


                                });

                                vendoBillObj.setCurrentSublistValue({
                                    sublistId: 'expense',
                                    fieldId: 'custcol_4601_witaxamt_exp',
                                    value: createVendorBill[c].whTaxAmount


                                });




                                /** To calulate the total WHTax amount of the line expense
                                	to set as rate when the Witholding Tax Item is added,
                                
                                **/
                                if (whTaxApply == true) {

                                    whTaxAmountTotalExpense = whTaxAmountTotalExpense + createVendorBill[c].whTaxAmount;


                                }

                                vendoBillObj.commitLine({
                                    sublistId: 'expense'
                                });


                            }



                        }

                        /** For adding the Witholding Tax Item - Start **/
                        if (itemFlag == true) {

                            vendoBillObj.selectNewLine({
                                sublistId: 'item'
                            });

                            log.debug('inside item flag');

                           

                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item',
                                value: whTax_PayableInternalId
                            });
							
							

                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'rate',
                                value: whTaxAmountTotal
                            });
							
							

                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'department',
                                value: COMMON_DATA[0].department

                            });
							

                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'class',
                                value: COMMON_DATA[0].division


                            });
							
							 




                            vendoBillObj.commitLine({

                                sublistId: 'item'
                            });


                        }
                        /** For adding the Witholding Tax Item - End **/


                        /** For adding the Witholding Tax for Expense - Start **/
                        if (expenseFlag == true) {

                            log.debug('inside expense flag');


                            vendoBillObj.selectNewLine({
                                sublistId: 'expense'
                            });



                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'expense',
                                fieldId: 'account',
                                value: whTax_PayableInternalIdExpense
                            });


                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'expense',
                                fieldId: 'taxcode',
                                value: 5 //by default undefined tax code
                            });


                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'expense',
                                fieldId: 'amount',
                                value: whTaxAmountTotalExpense
                            });

                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'expense',
                                fieldId: 'department',
                                value: COMMON_DATA[0].department

                            });

                            vendoBillObj.setCurrentSublistValue({
                                sublistId: 'expense',
                                fieldId: 'class',
                                value: COMMON_DATA[0].division


                            });



                            vendoBillObj.commitLine({

                                sublistId: 'expense'
                            });


                        }
                        /** For adding the Witholding Tax for Expense- End **/




                        var recordId = vendoBillObj.save({
                            ignoreMandatoryFields: true

                        });

                        supplierInvoiceArray.push(recordId);

                       
                        if (recordId) {

                            record.submitFields({
                                type: type,
                                id: id,
                                values: {
                                    'custrecord_invoice_status': true,

                                }

                            });


                        }

                        log.debug('supplierInvoiceArray', supplierInvoiceArray);
                        record.submitFields({

                            type: type,
                            id: id,
                            values: {

                                'custrecord_created_supplier_invoice': supplierInvoiceArray
                            }

                        });


                    }


                }


            }



        }


        /** Grouping on the basis of key passed **/
        function groupBy(xs, key) {

            var result = xs.reduce(function(rv, x) {

                (rv[x[key]] = rv[x[key]] || []).push(x);

                return rv;
            }, {});

            return result;

        }


        /** Returns Formatted Date **/
        function parseAndFormatDateString(myDate) {

            var initialFormattedDateString = myDate;
            var parsedDateStringAsRawDateObject = format.parse({
                value: initialFormattedDateString,
                type: format.Type.DATE,
                // type: format.Type.DATETIME,
                // timezone: format.Timezone.timezone

            });

            return parsedDateStringAsRawDateObject;

        }


        return {

            onAction: onAction
        }
    });