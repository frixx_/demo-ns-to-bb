/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Call RSK API for Location Master
***************************************************************************************/

define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/ui/serverWidget', 'N/log'],
	function(http, https, record, runtime, serverWidget, log) {
	
		var SUPPLIER_INVOICE_IMPORT = 124;
	
		function beforeLoad(context){
			var newRecordDetails = context.newRecord;
			
			var formId = newRecordDetails.getValue({
				fieldId: "customform"
			});
			
			if(formId == SUPPLIER_INVOICE_IMPORT){
				var supplierContractNo = newRecordDetails.getValue({
					fieldId: "custbody_pocon_supcontract"
				});
				
				var importEntryNo = newRecordDetails.getValue({
					fieldId: "custbody_bill_importentrynum"
				});
				
				var form = context.form;
				
				if(!!(supplierContractNo)){
					var supplierContractNoField = form.getField({
						id: 'custbody_pocon_supcontract'
					});
					
					supplierContractNoField.updateDisplayType({
						displayType : serverWidget.FieldDisplayType.INLINE
					});
				}
				if(!!(importEntryNo)){
					var importEntryNoField = form.getField({
						id: 'custbody_bill_importentrynum'
					});
					
					importEntryNoField.updateDisplayType({
						displayType : serverWidget.FieldDisplayType.INLINE
					});
				}
			}			
		}
		
		function afterSubmit(context) {
		
			var newRecordDetails = context.newRecord;
			try{				
				log.debug('newRecordDetails', newRecordDetails);
											
				var requestForPayment = newRecordDetails.getValue({
					fieldId: "custbody_ref_rfp_num"
				});
				
				var invoicesArray = new Array();
				
				if(requestForPayment != null || requestForPayment != ""){
					var rec = record.load({type : "customrecord_rfp", id : requestForPayment});
					
					var getSupplierInvoicesFromRFP = rec.getValue({
						fieldId:'custrecord_rfp_supplierbill'
					});
					
					log.debug("new Invoice", newRecordDetails.id);
					
					log.debug("getSupplierInvoicesFromRFP", getSupplierInvoicesFromRFP);
										
					for(var i = 0; i < getSupplierInvoicesFromRFP.length; i++){
						invoicesArray.push(getSupplierInvoicesFromRFP[i]);
					}
					
					invoicesArray.push(newRecordDetails.id);
					
					log.debug("invoicesArray Type", typeof(invoicesArray));
					
					/* rec.setValue({
						fieldId: "custrecord_rfp_supplierbill",
						value: newRecordDetails.id,				
						ignoreFieldChange: true
					}); */
					
					rec.setValue({
						fieldId: "custrecord_rfp_supplierbill",
						value: invoicesArray,				
						ignoreFieldChange: true
					});
					
					rec.save();
				}				
			}
			catch(err){
				log.debug("Error",err);
				return;
			}
		}
				
		return {
			beforeLoad: beforeLoad,
			afterSubmit: afterSubmit
		};
	});	