/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : On selection of RFP in Check record set value of check in RFP record & Supplier Invoices attached to the same
***************************************************************************************/

define(['N/http', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, search, record, runtime, log) {
		
		function beforeSubmit(context) {
		
			var newRecordDetails = context.newRecord;
			try{				
				log.debug('newRecordDetails', newRecordDetails);
				
				var requestForPayment = newRecordDetails.getValue({
					fieldId: "custbody_ref_rfp_num"//"custbody_advancerequest"
				});
				
				log.debug('requestForPayment', requestForPayment);
								
				if(!!(requestForPayment)){
					var RFPRecord = record.load({type : "customrecord_rfp", id : requestForPayment});
					
					var checkLinkValidation = RFPRecord.getValue({
						fieldId: "custrecord_writecheck"
					});
				    log.debug('requestForPayment', checkLinkValidation);
					
					if(!!(checkLinkValidation)){
						// throw "Please choose another RFP record.";
						// return;
						
						checkLinkValidation = new Array();
					}
					
					checkLinkValidation.push(newRecordDetails.id);
				    log.debug('requestForPayment', checkLinkValidation);
					
					RFPRecord.setValue({
						fieldId: "custrecord_writecheck",
						value: checkLinkValidation,				
						ignoreFieldChange: true
					});
					
					
					var supplierInvoices = RFPRecord.getValue({
						fieldId: "custrecord_rfp_supplierbill"
					});
					
					for(var i = 0; i < supplierInvoices.length; i++){
						
						var invoiceRecord = record.load({type : "vendorbill", id : supplierInvoices[i]});
						invoiceRecord.setValue({
							fieldId: "custbody_writechecklink",
							value: newRecordDetails.id,				
							ignoreFieldChange: true
						});
						invoiceRecord.save();
					}	
					
					
					var liquidationRecordId = RFPRecord.getValue({
						fieldId: "custrecord_liquidation_trans"
					});
					
					if(liquidationRecordId != ""){
						var liquidationRecord = record.load({type : "customrecord_supplier_inv_broker", id : liquidationRecordId});
						
						liquidationRecord.setValue({
							fieldId: "custrecord_write_chk_link",
							value: newRecordDetails.id,				
							ignoreFieldChange: true
						});
						
						liquidationRecord.save();
					}
					
					RFPRecord.save();
				}				
			}
			catch(err){
				log.debug('Error', err);
				return;
			}
		}

		function afterSubmit(context) {
			var newRecordDetails = context.newRecord;
			try{
				
				var debitMemo = newRecordDetails.getValue({
					fieldId: "custbody_relatedvendorcr_ck"
				});
				var transactionnumber = (search.lookupFields({
					type: "check",
					id: newRecordDetails.id,
					columns: ['transactionnumber']
				}))['transactionnumber'];
				
				log.debug("debitMemo", debitMemo);
				log.debug("transactionnumber", transactionnumber);
				// log.debug("tranid", tranid);
				// log.debug("newRecordDetails", newRecordDetails);
				
				if(!!(debitMemo)){
					var id = record.submitFields({
						type: "vendorcredit",
						id: debitMemo[0],
						values: {
							"custbody_writechecklink" : newRecordDetails.id,
							"tranid" : transactionnumber
						},
						options: {
							enableSourcing: false,
							ignoreMandatoryFields : true
						}
					});
				}
				
				
				var pnDeposit = newRecordDetails.getValue({
					fieldId: "custbody_pn_deposit"
				});
				
				if(!!(pnDeposit)){
					var id = record.submitFields({
						type: "deposit",
						id: pnDeposit,
						values: {
							"custbody_writechecklink" : newRecordDetails.id
						},
						options: {
							enableSourcing: false,
							ignoreMandatoryFields : true
						}
					});
				}
				
			}catch(err){
				log.debug('Error', err);
				return;				
			}
		}
				
		return {
			beforeSubmit: beforeSubmit,
			afterSubmit: afterSubmit
		};
	});	