/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Running balance of Liquidation
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {
		
		function beforeLoad(context) {
		
			log.debug("context", context);
			
			if(context.type != "view"){
				return;
			}
			
			var newRecordDetails = context.newRecord;
			var scriptObj = runtime.getCurrentScript();
			// var createAPI = scriptObj.getParameter({name: 'custscript_create_api_item'});
			
			var amount = newRecordDetails.getValue({
				fieldId: "usertotal"
			});
			var liquidatedAmount = newRecordDetails.getValue({
				fieldId: "custbody_liq_amt_tv"
			});
			
			var values = {};
			
			values["custbody_run_bal"] = parseFloat(amount||0) - parseFloat(liquidatedAmount||0);
			
			log.debug("amount", amount);
			log.debug("liquidatedAmount", liquidatedAmount);
			
			var id = record.submitFields({
				type: newRecordDetails.type,//record.Type.PURCHASE_ORDER,
				id: newRecordDetails.id,
				values: values,
				options: {
					enableSourcing: false,
					ignoreMandatoryFields : true
				}
			});
		}
		return {
			beforeLoad: beforeLoad
		};
	});		