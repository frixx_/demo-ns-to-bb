/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Running balance of RFP
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {
		
		var STANDARD_BILL_PAYMENT_REQUEST = 2;
		
		function beforeLoad(context) {
		
			log.debug("context", context);
			
			if(context.type != "view"){
				return;
			}
			
			var newRecordDetails = context.newRecord;
			var scriptObj = runtime.getCurrentScript();
			// var createAPI = scriptObj.getParameter({name: 'custscript_create_api_item'});
			
			var rfpType = newRecordDetails.getValue({
				fieldId: "custrecord_rfptype"
			});
			
			var liquidatedAmount = newRecordDetails.getValue({
				fieldId: "custrecord_liq_amt_tv"
			});
			var payableAmount = newRecordDetails.getValue({
				fieldId: "custrecord_pay_amt_tv"
			});
			
			var paidAmount = 0;
			var values = {};
			
			if(rfpType == STANDARD_BILL_PAYMENT_REQUEST){
				paidAmount = newRecordDetails.getValue({
					fieldId: "custrecord_std_paid_amt_tv"
				});
				values["custrecord_std_paid_amt"] = parseFloat(paidAmount||0);
				values["custrecord_pay_amt"] = parseFloat(payableAmount||0);
				values["custrecord_std_run_bal"] = parseFloat(paidAmount||0) - parseFloat(payableAmount||0);
			}else{
				paidAmount = newRecordDetails.getValue({
					fieldId: "custrecord_adv_paid_amt_tv"
				});
				values["custrecord_adv_paid_amt"] = parseFloat(paidAmount||0);
				values["custrecord_liq_amt"] = parseFloat(liquidatedAmount||0);
				values["custrecord_adv_run_bal"] = parseFloat(paidAmount||0) - parseFloat(liquidatedAmount||0);
			}
			
			log.debug("liquidatedAmount", liquidatedAmount);
			log.debug("payableAmount", payableAmount);
			log.debug("paidAmount", paidAmount);
			log.debug("newRecordDetails.type", newRecordDetails.type);
			log.debug("newRecordDetails.id", newRecordDetails.id);
			log.debug("values", JSON.stringify(values));
			
			var id = record.submitFields({
				type: newRecordDetails.type,//record.Type.PURCHASE_ORDER,
				id: newRecordDetails.id,
				values: values,
				options: {
					enableSourcing: false,
					ignoreMandatoryFields : true
				}
			});
		}
		return {
			beforeLoad: beforeLoad
		};
	});		