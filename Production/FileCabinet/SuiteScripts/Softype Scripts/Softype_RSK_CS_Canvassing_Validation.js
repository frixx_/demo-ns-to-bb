/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  18 November, 2019
 ** @Version     :  2.x
 ** @Description :  Client Script for putting validations on Canvassing Record - 'customrecord_canvassing_parent_record' and Child Record - 'customrecord_canvassing_child_record'.

 ***************************************************************************************/
define(['N/ui/dialog', 'N/currentRecord', 'N/record'],


    function(dialog, currentRecord, record) {


        /**
         * Function to be executed after page is initialized.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
         *
         * @since 2015.2
         */
        function pageInit(scriptContext) {


        }


        /**
         * Function to be executed when field is changed.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @param {string} scriptContext.sublistId - Sublist name
         * @param {string} scriptContext.fieldId - Field name
         * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
         * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
         *
         * @since 2015.2
         */

        function fieldChanged(scriptContext) {

            var fieldId = scriptContext.fieldId;
            var currentRecordObj = currentRecord.get();
            var customParentRecord = 'customrecord_canvassing_parent_record';
			var customChildRecord = 'customrecord_canvassing_child_record';
           
            var currentRec = scriptContext.currentRecord.type;
           


            if (currentRec == customParentRecord) {
                /** To check if the Purchase Requisition Qty is not greater than PO Qty. **/
                if (fieldId == 'custrecord_canvassing_qty') {


                    var currentRecordObj = currentRecord.get();

                    var prQty = currentRecordObj.getCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_qty_canvas'

                    });


                    var poQty = currentRecordObj.getCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_canvassing_qty'

                    });


                    if (poQty > prQty) {

                        netsuiteAlert('Purchase Order Quantity is greater than Purchase Requisition Quantity. ');

                        currentRecordObj.setCurrentSublistValue({

                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_canvassing_qty',
                            value: ''

                        });

                        return;


                    }


                }
				
            }
			
            if (currentRec == customChildRecord) {
                /** To check if the Purchase Requisition Qty is not greater than PO Qty. **/
                if (fieldId == 'custrecord_canvassing_qty') {


                    var currentRecordObj = currentRecord.get();

                    var prQty = currentRecordObj.getValue({

                        fieldId: 'custrecord_qty_canvas'

                    });


                    var poQty = currentRecordObj.getValue({
                     
                        fieldId: 'custrecord_canvassing_qty'

                    });


                    if (poQty > prQty) {

                        netsuiteAlert('Purchase Order Quantity is greater than Purchase Requisition Quantity. ');

                        currentRecordObj.setValue({

                            fieldId: 'custrecord_canvassing_qty',
                            value: ''

                        });

                        return;


                    }


                }
				
            }
			
			
			
			
			
			
			
			
			
			
			
			


        }




        /** For message Pop-Up **/

        function netsuiteAlert(message) {
            var options = {
                title: "Alert",
                message: message
            };

            function success(result) {
                console.log("Success with value " + result);
            }

            function failure(reason) {
                console.log("Failure: " + reason);
            }

            dialog.alert(options).then(success).catch(failure);
        }




        return {


            fieldChanged: fieldChanged


        }
    });