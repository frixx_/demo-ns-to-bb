/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  8th January, 2020
 ** @Version     :  2.x
 ** @Description :  This script calculates First, Second, Third level Discounts 
					and Line Discount Total, calculates Standard Rate if it is VAT inclusive.

 ***************************************************************************************/
define(['N/record', 'N/runtime'],

    function(record, runtime) {



        /**
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */

        function beforeSubmit(scriptContext) {


            var formId = runtime.getCurrentScript().getParameter('custscript_form_id');
            var currentRecordObj = scriptContext.newRecord;

            var customForm = currentRecordObj.getValue({
                fieldId: 'customform'
            });



            if (formId != customForm) {

                return;
            }


            log.debug('runtime.executionContext ', runtime.executionContext);

            if (runtime.executionContext == 'CSVIMPORT') {

                var lineCount = currentRecordObj.getLineCount({
                    sublistId: 'item'
                });


                for (var p = 0; p < lineCount; p++) {

                    var custom_rate = currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_custom_rate',
                        line: p
                    });

                    /** Setting custom rate in the standard rate **/
                    currentRecordObj.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'rate',
                        value: custom_rate,
                        line: p
                    });

                    var First_discount = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_discper',
                        line: p
                    }));

                    var second_Discount = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_2nddiscper',
                        line: p
                    }));

                    var third_Discount = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_disc3rd',
                        line: p
                    }));




                    /** Calculation of First Level Discount - Start **/
                    if (First_discount) {

                        log.debug('First_discount ', 'First_discount');

                        var firstDiscountPercentage = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'custcol_discper',
                            line: p
                        }));

                        /** If the first discount is 0, set the first discount amount to blank **/
                        if (firstDiscountPercentage == 0 || firstDiscountPercentage == '0') {

                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_discamt',
                                value: '',
                                line: p
                            });


                        }

                        var rate = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'rate',
                            line: p
                        }));


                        var iQuantity = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            line: p
                        }));


                        /** If the quantity is null or blank, set it to 1 **/
                        if (iQuantity == '' || iQuantity == null || iQuantity == 'null')
                            iQuantity = 1;


                        var stdAmt = rate * iQuantity;


                        /** Calculating first discount amount **/
                        var firstCalDiscount = Number(stdAmt * firstDiscountPercentage) / 100;

                        log.audit('firstCalDiscount ', firstCalDiscount);


                        var firstDiscountedAmt = Number(stdAmt) - Number(firstCalDiscount);



                        if (!isNaN(firstCalDiscount) && firstCalDiscount != 0 && firstCalDiscount != '0') {
                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_discamt',
                                value: firstCalDiscount.toFixed(4),
                                line: p
                            });

                        }

                        /** Setting 'After Discount Amount' **/
                        if (!isNaN(firstDiscountedAmt) && (firstDiscountedAmt != undefined)) {

                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_subtotal1',
                                value: Number(firstDiscountedAmt),
                                line: p
                            });

                            /** Setting Amount after subtracting discount amount**/
                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'amount',
                                value: Number(firstDiscountedAmt),
                                line: p
                            });


                            /** Calculating standard rate and setting **/
                            var rate_af_firstDisc = Number(firstDiscountedAmt) / Number(iQuantity);

                            log.audit('rate_af_firstDisc ', rate_af_firstDisc);

                            if (firstDiscountedAmt == '0' && firstDiscountPercentage == '100') {
                                currentRecordObj.setSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'rate',
                                    value: 0,
                                    line: p
                                });


                            } else {
                                currentRecordObj.setSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'rate',
                                    value: rate_af_firstDisc,
                                    line: p
                                });



                            }

                        }



                    }
                    /** Calculation of First Level Discount - End **/




                    /** Calculation of second level discount - Start **/
                    if (second_Discount) {

                        log.debug('second_Discount ', 'second_Discount');

                        var secondDiscountPrec = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'custcol_2nddiscper',
                            line: p
                        }));

                        /** If the second discount is 0, set the second discount amount to blank **/
                        if (secondDiscountPrec == 0 || secondDiscountPrec == '0') {

                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_2nddiscamt',
                                value: '',
                                line: p
                            });

                        }

                        var iQuantity = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            line: p
                        }));

                        var rateAfterFirstDis = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'rate',
                            line: p
                        }));


                        var stdAmt1 = rateAfterFirstDis * iQuantity;


                        /** Calculating second discount amount **/
                        var secondCalDiscount = Number(stdAmt1 * secondDiscountPrec) / 100;

                        log.audit('secondCalDiscount ', secondCalDiscount);


                        var secondDiscountedAmt = Number(stdAmt1) - Number(secondCalDiscount);


                        if (!isNaN(secondCalDiscount) && secondCalDiscount != 0 && secondCalDiscount != '0') {

                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_2nddiscamt',
                                value: secondCalDiscount.toFixed(4),
                                line: p
                            });

                        }


                        /** Setting 'After Discount Amount' **/
                        if (!isNaN(secondDiscountedAmt)) {
                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_subtotal1',
                                value: Number(secondDiscountedAmt), //.toFixed(4)
                                line: p
                            });


                            /** Setting Amount after subtracting discount amount**/
                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'amount',
                                value: secondDiscountedAmt,
                                line: p
                            });



                            /** Calculating standard rate and setting **/
                            var secondCalDiscount1 = Number(rateAfterFirstDis * secondDiscountPrec) / 100;
                            var rate_af_secondDisc = Number(rateAfterFirstDis) - Number(secondCalDiscount1);

                            log.audit('rate_af_secondDisc ', rate_af_secondDisc);

                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'rate',
                                value: rate_af_secondDisc,
                                line: p
                            });



                        }



                    }
                    /** Calculation of Second Level Discount - End **/




                    /** Calculation of Third Level Discount - Start **/
                    if (third_Discount) {

                        log.debug('third_Discount ', 'third_Discount');

                        var thirdDiscountPrec = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'custcol_disc3rd',
                            line: p
                        }));

                        /** If the second discount is 0, set the second discount amount to blank **/
                        if (thirdDiscountPrec == 0 || thirdDiscountPrec == '0') {

                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_2nddiscamt',
                                value: '',
                                line: p
                            });

                        }



                        var iQuantity = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            line: p
                        }));

                        var rateAfterSecondDis = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'rate',
                            line: p
                        }));


                        var stdAmt2 = rateAfterSecondDis * iQuantity;

                        /** Calculating third discount amount **/
                        var thirdCalDiscount = Number(stdAmt2 * thirdDiscountPrec) / 100;

                        log.audit('thirdCalDiscount ', thirdCalDiscount);



                        if (!isNaN(thirdCalDiscount) && thirdCalDiscount != 0 && thirdCalDiscount != '0') {
                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_3rddiscamt',
                                value: thirdCalDiscount.toFixed(4),
                                line: p
                            });
                        }

                        var thirdDiscountedAmt = Number(stdAmt2) - Number(thirdCalDiscount);

                        /** Setting 'After Discount Amount' **/
                        if (!isNaN(thirdDiscountedAmt)) {
                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_subtotal1',
                                value: Number(thirdDiscountedAmt), //.toFixed(4)
                                line: p
                            });

                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'amount',
                                value: thirdDiscountedAmt,
                                line: p
                            });


                            var thirdCalDiscount1 = Number(rateAfterSecondDis * thirdDiscountPrec) / 100;
                            var rate_af_thirdDisc = Number(rateAfterSecondDis) - Number(thirdCalDiscount1);

                            log.audit('rate_af_thirdDisc ', rate_af_thirdDisc);


                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'rate',
                                value: rate_af_thirdDisc,
                                line: p
                            });
                        }




                    }
                    /** Calculation of third level discount - End **/



                    /** Caculation of LINE DISCOUNT TOTAL - Start **/

                    var firstDiscountPercentage = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_discper',
                        line: p
                    }));


                    var secondDiscountPrec = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_2nddiscper',
                        line: p
                    }));


                    var thirdDiscountPrec = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_disc3rd',
                        line: p
                    }));

                    if (firstDiscountPercentage || secondDiscountPrec || thirdDiscountPrec) {

                        var firstdiscamt = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'custcol_discamt',
                            line: p
                        }));


                        var seconddiscamt = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'custcol_3rddiscamt',
                            line: p
                        }));


                        var thirddiscamt = Number(currentRecordObj.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'custcol_2nddiscamt',
                            line: p
                        }));


                        var totaldisctamt = firstdiscamt + seconddiscamt + thirddiscamt;


                        currentRecordObj.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'custcol_linedistotal',
                            value: totaldisctamt,
                            line: p
                        });



                    }
                    /** Caculation of LINE DISCOUNT TOTAL - End **/


                    /** Caculation if VAT inclusive - Start **/
                    var vat_checkbox = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_vatinclusivefields',
                        line: p
                    }));

                    log.audit('vat_checkbox', vat_checkbox);

                    /** If the standard rate is VAT inclusive - vat_checkbox == 1 **/
                    if (vat_checkbox) {


                        if (vat_checkbox == 1) {

                            /** Getting 'After Discount Amount' and 'Tax Rate' **/
                            var subtotal = Number(currentRecordObj.getSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_subtotal1',
                                line: p
                            }));


                            var taxrate = Number(currentRecordObj.getSublistValue({
                                sublistId: 'item',
                                fieldId: 'taxrate1',
                                line: p
                            }));


                            var subtotalrate = (subtotal / iQuantity);

                            var newratevatdis = subtotalrate / (1 + taxrate / 100);


                            currentRecordObj.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'rate',
                                value: newratevatdis,
                                line: p
                            });


                        }


                    }
                    /** Caculation if VAT inclusive - End **/




                    /** Calculating 'CUSTOM AMOUNT TOTAL' i.e quantity with rate entered by user.(Print) **/

                    var iQuantity = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        line: p
                    }));

                    var custom_rate = currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_custom_rate',
                        line: p
                    });


                    var amtTotal = iQuantity * custom_rate;

                    currentRecordObj.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_customamttotal',
                        value: amtTotal,
                        line: p
                    });


                }


            }


        }




        return {

            beforeSubmit: beforeSubmit
        }

    });