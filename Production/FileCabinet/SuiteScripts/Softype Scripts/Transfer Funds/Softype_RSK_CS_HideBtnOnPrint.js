/**
	* @NApiVersion 2.x
	* @NScriptType ClientScript
	* @NModuleScope SameAccount
*/
/***************************************************************************************  
	** Copyright (c) 1998-2018 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
	**                      
	**@Author      :  Amol Jagkar
	**@Dated       :  01st October 2018	
	**@Version     :  2.x
	**@Description :  Client Script For Hiding the buttons on Print
***************************************************************************************/
define(['N/currentRecord', 'N/url', 'N/search', 'N/https', 'N/log'],
	
    function(currentRecord, url, search, https, log) {
		
		function pageInit(scriptContext) {
			
			//log.debug("Inside script");
		
			var style = document.createElement('style');
			
			/* var css = "@media print{";
			css += "		#edit {";
			css += "			display: none;";
			css += "		}";
			css += "		#_back {";
			css += "			display: none;";
			css += "		}";
			css += "		#secondaryedit {";
			css += "			display: none;";
			css += "		}";
			css += "		#secondary_back {";
			css += "			display: none;";
			css += "		}";
			css += "		#spn_ACTIONMENU_d1 {";
			css += "			display: none;";
			css += "		}";
			css += "		#spn_secondaryACTIONMENU_d1 {";
			css += "			display: none;";
			css += "		}";
			css += "	}"; */
			
			
			
		 	var css = "@media print{";
			css += "		.uir-header-buttons {";
			css += "			display: none;";
			css += "		}";	
			css += "		.uir-secondary-buttons {";
			css += "			display: none;";
			css += "		}";	
			css += "	}";
			
			style.innerHTML = css;
			document.head.appendChild(style); 
			
						
        }
		
		return {
            pageInit: pageInit
		};
		
	});	