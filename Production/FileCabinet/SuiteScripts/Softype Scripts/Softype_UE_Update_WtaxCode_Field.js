/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  
 **                       
 **@Author      : Farhan Shaikh
 **@Dated       : 04/10/2019   DD/MM/YYYY
 **@Version     : 
 **@Description : UserEvent Script on Credit Memo to set the Credit Memo Number created on the Policy Billing Information Records.  

 **************************************************************************************************************************************/
define(['N/record', 'N/runtime', 'N/search', 'N/format'],
    function(record, runtime, search, format) {
        var orNumber=0,orDate=0;
        var linecount=0;
        var linecountExpense=0;
        var formToTriggerOn = runtime.getCurrentScript().getParameter('custscript_form');
        var accountToTrigger = runtime.getCurrentScript().getParameter('custscript_account');
        var jvForm = runtime.getCurrentScript().getParameter('custscript_jv_form_for_deferred');
        var jvAccount = runtime.getCurrentScript().getParameter('custscript_account_for_jv');
        var glAccountSavedSearch = runtime.getCurrentScript().getParameter('custscript_search_for_gl_account');
        /*function beforeLoad(scriptContext) {
            var recordObjBeforeLoad=scriptContext.newRecord;
            orNumber=recordObjBeforeLoad.getValue('custbody_ub_payout_or_num');
            orDate=recordObjBeforeLoad.getValue('custbody_ub_payout_or_date');
        }*/


        /**
         * Function definition to be triggered after record is submitted.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */

        function beforeSubmit(scriptContext) {
            /*var currentRecordObj = scriptContext.newRecord;
            var orNumberSubmit=currentRecordObj.getValue('custbody_ub_payout_or_num');
            var orDateSubmit=currentRecordObj.getValue('custbody_ub_payout_or_date');
            log.emergency('OR Fields','OR Number : '+orNumber+' orDate : '+orDate+' orNumberSubmit : '+orNumberSubmit+' orDateSubmit : '+orDateSubmit)
            if (orNumberSubmit!=orNumber && orDateSubmit!=orDate) {
                log.debug('OR Number & Date NOT Equal')
                saveRecord(scriptContext)
            }*/
            log.emergency('context',scriptContext)
            saveRecord(scriptContext)

            dueDate(scriptContext)
        }

        function saveRecord(context){
            var currentRecordObj = context.newRecord; //.get();
            var currentRecordID = context.newRecord.id;
            log.audit('Current Record Id',currentRecordID)
            var totalSelected = [];
            var allBillsSelected=[];
            linecount = currentRecordObj.getLineCount({sublistId:'item'});
            linecountExpense = currentRecordObj.getLineCount({sublistId:'expense'});
            //alert('linecount : '+linecount)
            if (true) { //100 //currentRecordObj.getValue('customform')==formToTriggerOn
                if(linecount > 0){
                    var taxCodeColumn = currentRecordObj.getSublistValue({sublistId:'item',fieldId:'custcol_4601_witaxcode',line:0});
                    var custTaxField=currentRecordObj.getValue('custbody_wtax_code_name')
                    if (taxCodeColumn != custTaxField) {
                        if (taxCodeColumn!='') {
                            var wTaxDetails = search.lookupFields({
                                type: 'customrecord_4601_witaxcode',
                                id: taxCodeColumn,
                                columns: ['custrecord_4601_wtc_name']
                            });
                            log.audit('wTaxDetails',wTaxDetails)
                            var getId = wTaxDetails.custrecord_4601_wtc_name;
                            currentRecordObj.setValue('custbody_wtax_code_name',getId)
                            //currentRecordObj.save();
                        }else{
                            currentRecordObj.setValue('custbody_wtax_code_name','')
                            //urrentRecordObj.save();
                        }
                    }
                }else if(linecountExpense>0){
                    var taxCodeColumn = currentRecordObj.getSublistValue({sublistId:'expense',fieldId:'custcol_4601_witaxcode_exp',line:0});
                    var custTaxField=currentRecordObj.getValue('custbody_wtax_code_name')
                    if (taxCodeColumn != custTaxField) {
                        if (taxCodeColumn!='') {
                            var wTaxDetails = search.lookupFields({
                                type: 'customrecord_4601_witaxcode',
                                id: taxCodeColumn,
                                columns: ['custrecord_4601_wtc_name']
                            });
                            log.audit('wTaxDetails',wTaxDetails)
                            var getId = wTaxDetails.custrecord_4601_wtc_name;
                            currentRecordObj.setValue('custbody_wtax_code_name',getId)
                            //afterSubmit(context)
                            //currentRecordObj.save();
                        }else{
                            currentRecordObj.setValue('custbody_wtax_code_name','')
                            //afterSubmit(context)
                            //urrentRecordObj.save();
                        }
                    }
                }
            }
        }

        function dueDate(context){
            var currentRecordObj = context.newRecord; //.get();
            var currentRecordID = context.newRecord.id;
            log.audit('Current Record Id Due Date',currentRecordID);

            var counterDate=currentRecordObj.getValue('custbody_counter_date')
            var term=currentRecordObj.getValue('terms')

            if ((term !='' && term!=null && term!=undefined) && (counterDate !='' && counterDate!=null && counterDate!=undefined)) {
                log.audit('term -> Due Date',term);
                log.audit('counterDate -> Due Date',counterDate);
                var termDaysLookup=search.lookupFields({
                    type:'term',
                    id:term,
                    columns:['daysuntilnetdue']
                });
                var termDays=termDaysLookup.daysuntilnetdue;

                var newDueDate=addDays(counterDate,termDays)
                currentRecordObj.setValue('duedate',newDueDate)
            }
        }

        //This function adds days to the date specified /
        function addDays(theDate, days) {
            return new Date(theDate.getTime() + days*24*60*60*1000);
        }

        function afterSubmit(context){
            var flag=false;
            log.debug('AFTER SUBMIT','AFTER SUBMIT')
            var currentRecordObjForLoad = context.newRecord;
            log.audit('currentRecordObjForLoad',currentRecordObjForLoad)
            var searchForGLAccount=search.load(glAccountSavedSearch);
            searchForGLAccount.filters.push(search.createFilter({
                name: 'internalid',
                operator: 'is',
                values: currentRecordObjForLoad.id
            }));
            var searchObj=searchForGLAccount.run().getRange({start:0,end:1000});
            for(var j=0;j<searchObj.length;j++){
                var account=searchObj[j].getValue('account');
                if (account==accountToTrigger) {
                    flag=true;
                    break;
                }
                //log.audit('search Account',account)
            }
            if (flag) {
                var currentRecordObj=record.load({
                    type:currentRecordObjForLoad.type,
                    id:currentRecordObjForLoad.id
                })
                log.emergency('currentRecordObj',currentRecordObj)
                var jvCreated=currentRecordObj.getValue('custbody_jv_created');
                var invForm=currentRecordObj.getValue('customform');
                var subsidiary=currentRecordObj.getValue('subsidiary');
                var division=currentRecordObj.getValue('class');
                var department=currentRecordObj.getValue('department');
                var location=currentRecordObj.getValue('location');
                var currency=currentRecordObj.getValue('currency');
                var memo=currentRecordObj.getValue('memo');
                log.audit('jvCreated',jvCreated)
                log.audit('invForm',invForm)
                if (!jvCreated && invForm==formToTriggerOn) {
                    var orNumber=currentRecordObj.getValue('custbody_ub_payout_or_num');
                    var orDate=currentRecordObj.getValue('custbody_ub_payout_or_date');
                    var typeOfTrans=currentRecordObj.getValue('cseg_type_of_trans');
                    log.audit('orNumber',orNumber)
                    log.audit('orDate',orDate)
                    log.audit('linecount',linecount)
                    log.audit('linecountExpense',linecountExpense)
                    if (orNumber!=undefined && orNumber!=null && orNumber!='' && orDate!=undefined && orDate!=null && orDate!='') {
                        var jvRecordObj=record.create({
                            type:'journalentry',
                            isDynamic:true
                        });
    
                        jvRecordObj.setValue('customform',jvForm);
                        jvRecordObj.setValue('custbody_ub_payout_or_date',orDate);
                        jvRecordObj.setValue('trandate',orDate);
                        jvRecordObj.setValue('custbody_ub_cpy_check_created_frm',currentRecordObj.id);
                        jvRecordObj.setValue('custbody_ub_payout_or_num',orNumber);
                        jvRecordObj.setValue('subsidiary',subsidiary);
                        jvRecordObj.setValue('currency',currency);
                        jvRecordObj.setValue('approvalstatus',2);
                        jvRecordObj.setValue('cseg_type_of_trans',typeOfTrans);
                        jvRecordObj.setValue('class',division);
                        jvRecordObj.setValue('location',location);
                        jvRecordObj.setValue('department',department);
                        jvRecordObj.setValue('memo',memo);
    
                        var linecount = currentRecordObj.getLineCount({sublistId:'item'});
                        var linecountExpense = currentRecordObj.getLineCount({sublistId:'expense'});
                        var jsonArray=[];
                        if (linecount>0) {
                            for(var i=0;i<linecount;i++){
                                var taxAmount = currentRecordObj.getSublistValue({
                                 sublistId: 'item',
                                 fieldId: 'tax1amt',
                                 line: i
                                });
    
                                if (taxAmount!=0) {
                                    var amount = currentRecordObj.getSublistValue({
                                     sublistId: 'item',
                                     fieldId: 'amount',
                                     line: i
                                    });
    
                                    var taxCode = currentRecordObj.getSublistValue({
                                     sublistId: 'item',
                                     fieldId: 'taxcode',
                                     line: i
                                    });

                                    var actualTaxCode=getActualTaxCode(taxCode)
    
                                    var lineItemJson={
                                        'amount':amount,
                                        'taxcode':taxCode,
                                        'actualTaxCode':actualTaxCode
                                    }
        
                                    jsonArray.push(lineItemJson)
                                }
                            }
                        }else if (linecountExpense>0) {
                            for(var i=0;i<linecountExpense;i++){
                                var taxAmount = currentRecordObj.getSublistValue({
                                 sublistId: 'expense',
                                 fieldId: 'tax1amt',
                                 line: i
                                });
                                if (taxAmount!=0) {
                                    var amount = currentRecordObj.getSublistValue({
                                     sublistId: 'expense',
                                     fieldId: 'amount',
                                     line: i
                                    });
                                    var taxCode = currentRecordObj.getSublistValue({
                                     sublistId: 'expense',
                                     fieldId: 'taxcode',
                                     line: i
                                    });

                                    var actualTaxCode=getActualTaxCode(taxCode)
        
                                    var lineItemJson={
                                        'amount':amount,
                                        'taxcode':taxCode,
                                        'actualTaxCode':actualTaxCode
                                    }
        
                                    jsonArray.push(lineItemJson)
                                }
                            }       
                        }
                        log.audit('jsonArray',jsonArray);
                        for(var i=0;i<jsonArray.length;i++){
                            var debitAmount=jsonArray[i].amount;
                            var taxCode=jsonArray[i].taxcode;
                            var actualTaxCode=jsonArray[i].actualTaxCode
                            jvRecordObj.selectNewLine({ 
                                sublistId: 'line'      
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'account',
                             value: jvAccount //214
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'debit',
                             value: debitAmount
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'taxcode',
                             value: actualTaxCode //taxCode
                            });
                            jvRecordObj.commitLine({ 
                                sublistId: 'line'
                            });
                            jvRecordObj.selectNewLine({ 
                                sublistId: 'line'      
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'account',
                             value: jvAccount //214
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'credit',
                             value: debitAmount
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'taxcode',
                             value: taxCode //actualTaxCode
                            });
                            jvRecordObj.commitLine({ 
                                sublistId: 'line'
                            });
                        }
    
                        var createdJournalId=jvRecordObj.save()
                        log.debug('CREATED JournalId',createdJournalId)
                        if(createdJournalId){
                            currentRecordObj.setValue('custbody_jv_created',true);
                            currentRecordObj.save()
                        }
                    }
                }
            }
            
        }

        function getActualTaxCode(taxcode){
            var filterForTax=new Array();
            var columnForTax=new Array();
            //log.emergency('Inside Function', 'getJobs');

            filterForTax.push(search.createFilter({
                name: 'custrecord_deferred_tax_codes',
                operator: search.Operator.IS,
                values: taxcode
            }));

            columnForTax.push(search.createColumn('custrecord_actual_tax_codes')); 

            var searchedMatchedTaxCode = search.create({
                'type': 'customrecord_match_tax_code',
                'filters': filterForTax,
                'columns': columnForTax
            }).run().getRange(0, 1000);

            if (searchedMatchedTaxCode.length>0) {
                var code=searchedMatchedTaxCode[0].getValue('custrecord_actual_tax_codes')
            }

            return code;
        }

        return {
            beforeSubmit: beforeSubmit,
            afterSubmit:afterSubmit
        };
    }
);