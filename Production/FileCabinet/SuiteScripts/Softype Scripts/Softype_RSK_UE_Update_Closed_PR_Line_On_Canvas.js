/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Siddhi Kadam
 **@Dated       :  27th February, 2020
 **@Version     :  2.x
 **@Description :  This script is deployed on Purchase Requisition to check if any of the line item is closed, 
				   if it is closed and also has the canvassing link attached in the body field go that canvasssing  
				   record and search for that purchase requisition and line item to close the check box.
 ***************************************************************************************/
define(['N/ui/serverWidget', 'N/log', 'N/error', 'N/record', 'N/url', 'N/runtime', 'N/search'],

    function(serverWidget, log, error, record, url, runtime, search) {


        function afterSubmit(scriptContext) {

            var id = scriptContext.newRecord.id;
            var type = scriptContext.newRecord.type;

            var currentRecordObj = scriptContext.newRecord;

            var closedPRItems = [];
            var canvasJSON = [];



            var canvassingLink = currentRecordObj.getValue({
                fieldId: 'custbody_canvassing_link'

            });

            /** If the canvassing link is not there, then return. **/
            if (!canvassingLink) {
                return;
            }



            var lineCount = currentRecordObj.getLineCount({
                sublistId: 'item'
            });

            for (var i = 0; i < lineCount; i++) {


                var isClosed = currentRecordObj.getSublistValue({

                    sublistId: 'item',
                    fieldId: 'isclosed',
                    line: i

                });


                /** If any of the line item is closed then push the item in json **/
                if (isClosed == true) {


                    var PRItem = currentRecordObj.getSublistValue({

                        sublistId: 'item',
                        fieldId: 'item',
                        line: i

                    });


                    closedPRItems.push({
                        PRItem: PRItem

                    });


                }


            }

            /** If the json is empty then return. **/
            if (!closedPRItems.length > 0) {

                return;
            }


            /** Loading the canvassing record to update the line item. **/
            var canvasRecordObj = record.load({
                type: 'customrecord_canvassing_parent_record',
                id: canvassingLink
                //isDynamic: true
            });

            var canvasLineCount = canvasRecordObj.getLineCount({
                sublistId: 'recmachcustrecord_link_canvassing',


            });




            for (var c = 0; c < canvasLineCount; c++) {


                var canvasPRNO = canvasRecordObj.getSublistValue({
                    sublistId: 'recmachcustrecord_link_canvassing',
                    fieldId: 'custrecord_pr_no',
                    line: c

                });


                var canvasItem = canvasRecordObj.getSublistValue({
                    sublistId: 'recmachcustrecord_link_canvassing',
                    fieldId: 'custrecord_item_canvas',
                    line: c


                });

                canvasJSON.push({

                    canvasPRNO: canvasPRNO,
                    canvasItem: canvasItem,
                    lineNo: c


                });


            }
			log.debug('canvasJSON ', JSON.stringify(canvasJSON));
			log.debug('closedPRItems ', JSON.stringify(closedPRItems));

            /** Looping through the canvasJSON to check if the PR No. on canvassing matches**/
            for (var p = 0; p < canvasJSON.length; p++) {


                if (canvasJSON[p].canvasPRNO == id) {

                    /** Looping through the closedPRItems to check if the item on canvassing matches to the PR item. **/
                    for (var s = 0; s < closedPRItems.length; s++) {


						log.audit('canvasJSON[p].canvasItem ', canvasJSON[p].canvasItem);
						
						log.audit('closedPRItems[s].PRItem ', closedPRItems[s].PRItem);
						
                        if (canvasJSON[p].canvasItem == closedPRItems[s].PRItem) {

                            var lineNo = canvasJSON[p].lineNo;
							
						    log.audit('lineNo ', closedlineNo);

                            canvasRecordObj.setSublistValue({
                                sublistId: 'recmachcustrecord_link_canvassing',
                                fieldId: 'custrecord_canvas_item_closed',
                                line: lineNo,
                                value: true


                            });


                        }

                    }

                }

            }


            var recordId = canvasRecordObj.save({

                ignoreMandatoryFields: true
            });

            log.debug('Canvassing Record Id ', recordId)




        }




        return {
            afterSubmit: afterSubmit
        };
    }
);