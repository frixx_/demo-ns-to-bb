/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  11th November, 2019
 ** @Version     :  2.0
 ** @Description :  Suitelet script for Purchase Requisition Print.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/ui/serverWidget', 'N/search', 'N/record', 'N/render', 'N/xml',  'N/format'],

    function(serverWidget, search, record, render, xml,   format) {


        function onRequest(context) {

            var recId = context.request.parameters.recordId;

            var preparedByJobTitle;
            var verifiedByJobTitle;
            var approvedByJobTitle;
            var requestedByJobTitle;


            var objRecord = record.load({
                type: record.Type.PURCHASE_REQUISITION,
                id: recId,
                isDynamic: true,
            });


            var department = objRecord.getText({
                fieldId: 'department'
            });

            var tranDate = objRecord.getText({
                fieldId: 'trandate'
            });

            var dueDate = objRecord.getText({
                fieldId: 'duedate'
            });

            var category = objRecord.getText({
                fieldId: 'custbody_reqcategory'
            });

            var rsNo = objRecord.getValue({
                fieldId: 'tranid'
            });

            var memo = objRecord.getValue({
                fieldId: 'memo'
            });


            var preparedBy = objRecord.getText({
                fieldId: 'custbody_prepared_by'
            });

            var preparedByValue = objRecord.getValue({
                fieldId: 'custbody_prepared_by'
            });

            var verifiedBy = objRecord.getText({
                fieldId: 'custbody_req_verify_by'
            });

            var verifiedByValue = objRecord.getValue({
                fieldId: 'custbody_req_verify_by'
            });

            var approvedBy = objRecord.getText({
                fieldId: 'custbody_req_appr_by'
            });

            var approvedByValue = objRecord.getValue({
                fieldId: 'custbody_req_appr_by'
            });

            var canvasser = objRecord.getText({
                fieldId: 'custbody_canvasser_pr'
            });

            var location = objRecord.getText({
                fieldId: 'location'
            });

            var requestor = objRecord.getText({
                fieldId: 'entity'
            });

            var requestorValue = objRecord.getValue({
                fieldId: 'entity'
            });
			
			var subsidiary = objRecord.getText({
                fieldId: 'subsidiary'
            });




            if (preparedByValue) {

                var preparedByLookUp = search.lookupFields({
                    type: search.Type.EMPLOYEE,
                    id: preparedByValue,
                    columns: ['title']
                });


                if (preparedByLookUp)
                    preparedByJobTitle = preparedByLookUp.title;

            }

            if (verifiedByValue) {

                var verifiedByLookUp = search.lookupFields({
                    type: search.Type.EMPLOYEE,
                    id: verifiedByValue,
                    columns: ['title']
                });


                if (verifiedByLookUp)
                    verifiedByJobTitle = verifiedByLookUp.title;


            }


            if (approvedByValue) {

                var approvedByLookUp = search.lookupFields({
                    type: search.Type.EMPLOYEE,
                    id: approvedByValue,
                    columns: ['title']
                });

                if (approvedByLookUp)
                    approvedByJobTitle = approvedByLookUp.title

            }

            if (requestorValue) {

                var requestedByLookUp = search.lookupFields({
                    type: search.Type.EMPLOYEE,
                    id: requestorValue,
                    columns: ['title']
                });

                if (requestedByLookUp)
                    requestedByJobTitle = requestedByLookUp.title

            }



            if (requestedByJobTitle == null)
                requestedByJobTitle = '';


            if (preparedByJobTitle == null)
                preparedByJobTitle = '';

            if (verifiedByJobTitle == null)
                verifiedByJobTitle = '';

            if (approvedByJobTitle == null)
                approvedByJobTitle = '';


            if (requestor == null)
                requestor = '';

            if (preparedBy == null)
                preparedBy = '';

            if (verifiedBy == null)
                verifiedBy = '';

            if (approvedBy == null)
                approvedBy = '';

            if (department == null)
                department = '';

            if (tranDate == null)
                tranDate = '';

            if (dueDate == null)
                dueDate = '';

            if (category == null)
                category = '';

            if (rsNo == null)
                rsNo = '';

            if (memo == null)
                memo = '';

            if (location == null)
                location = '';
			
			if (canvasser == null)
                canvasser = '';
			
			if (subsidiary == null)
                subsidiary = '';
			
			
			

            var form = serverWidget.createForm({
                title: 'Acknowledgement Receipt'
            });

            var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
            htmlvar += '<pdf>\n';
            htmlvar += '<head>';
            htmlvar += '<style>';



            htmlvar += '</style>';

            htmlvar += '<macrolist>';
            htmlvar += '<macro id="nlheader">';

            htmlvar += '</macro>';



            htmlvar += '<macro id="nlfooter">';



            htmlvar += '<table width="100%"  margin-bottom="20px" border="1px">';

            htmlvar += '<tr border-bottom="20px;">';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" border-right="1px"><b>Prepared By:</b></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" border-right="1px"><b>Requested By:</b></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" border-right="1px"><b>Verified By:</b></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" ><b>Approved By:</b></td> ';
            htmlvar += '</tr>';
			
			
			htmlvar += '<tr>';
            htmlvar += '<td style=" height: 50px;width:25%;align:center; vertical-align:bottom" border-right="1px" >_____________________</td> ';
            htmlvar += '<td style=" height: 50px;width:25%;align:center; vertical-align:bottom" border-right="1px">_____________________</td> ';
            htmlvar += '<td style=" height: 50px;width:25%;align:center; vertical-align:bottom" border-right="1px">_____________________</td> ';
            htmlvar += '<td style=" height: 50px;width:25%;align:center; vertical-align:bottom" >_____________________</td> ';
            htmlvar += '</tr>';


            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 16px;width:25%;align:center; vertical-align:bottom" border-right="1px"><b>' + xml.escape({
                xmlText: preparedBy
            }) + '</b></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center; vertical-align:bottom" border-right="1px"><b>' + xml.escape({
                xmlText: requestor
            }) + '</b></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center; vertical-align:bottom" border-right="1px"><b>' + xml.escape({
                xmlText: verifiedBy
            }) + '</b></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center; vertical-align:bottom" ><b>' + xml.escape({
                xmlText: approvedBy
            }) + '</b></td> ';
            htmlvar += '</tr>';

           
            htmlvar += '<tr>';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" border-right="1px"><p align="left"><b>' + xml.escape({
                xmlText: preparedByJobTitle
            }) + '</b></p></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" border-right="1px"><p align="left"><b>' + xml.escape({
                xmlText: requestedByJobTitle
            }) + '</b></p></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" border-right="1px"><p align="left"><b>' + xml.escape({
                xmlText: verifiedByJobTitle
            }) + '</b></p></td> ';
            htmlvar += '<td style=" height: 16px;width:25%;align:center;" ><b>' + xml.escape({
                xmlText: approvedByJobTitle
            }) + '</b></td> ';
			
            htmlvar += '</tr>';

            htmlvar += '</table>';




            htmlvar += " </macro>";

            htmlvar += " </macrolist>";
            htmlvar += '</head>';




            htmlvar += '<body  font-size="10" footer="nlfooter" footer-height="12%" >';

            htmlvar += '<table width="100%" border="1px solid black">';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 30px;align:center;width:80%;" border-right="1px " font-size="15px"><b>'+ xml.escape({
                xmlText: subsidiary
            })  + ' - ' + location + '</b></td> ';
            htmlvar += '<td style=" height: 30px;align:center;width:20%;" border-bottom="1px" >Category</td> ';
            htmlvar += '</tr>';

            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 16px;align:center;width:80%;" border-right="1px" font-size="15px"><b>Requisition Slip</b></td> ';
            htmlvar += '<td style=" height: 16px;align:center;width:20%;" >' + category + '</td> ';
            htmlvar += '</tr>';

            htmlvar += '</table>';



            htmlvar += '<table width="100%" border-left="1px ">';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px " border-right="1px">RS No.:</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px" border-right="1px">' + rsNo + '</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px" border-right="1px">RS Date:</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:40%;" border-bottom="1px" border-right="1px">' + tranDate + '</td> ';
            htmlvar += '</tr>';

            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px" border-right="1px">Cost Center:</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px" border-right="1px">' + xml.escape({
                xmlText: department
            }) + '</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px" border-right="1px">Target Date :</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:40%;" border-bottom="1px" border-right="1px">' + dueDate + '</td> ';
            htmlvar += '</tr>';

            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px" border-right="1px ">Canvasser</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px" border-right="1px">' + xml.escape({
                xmlText: canvasser
            }) + '</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:20%;" border-bottom="1px " border-right="1px">Reference:</td> ';
            htmlvar += '<td style=" height: 25px;align:center;width:40%;" border-bottom="1px " border-right="1px">'  + xml.escape({
                xmlText: memo
            })  + '</td> ';
            htmlvar += '</tr>';

            htmlvar += '</table>';




            htmlvar += '<table width="100%" border-bottom="1px">';

            htmlvar += '<tr style=" border-bottom: 1px ">';

            htmlvar += '<td style=" height: 20px;width:5%;align:center;" border-right="1px" border-left="1px"><p align="left"><b>No.</b></p></td> ';
            htmlvar += '<td style=" height: 20px; width:15%;align:center;" border-right="1px"><b>Quantity</b></td>'
            htmlvar += '<td style=" height: 20px; width:5%;align:center;" border-right="1px"><b>Unit</b></td> ';
            htmlvar += '<td style=" height: 20px; width:15%;align:center;" border-right="1px"><b>Classification</b></td>'
            htmlvar += '<td style=" height: 20px;width:20%;align:center;" border-right="1px"><b>Item Description</b></td> ';
			 htmlvar += '<td style=" height: 20px;width:10%;align:center;" border-right="1px"><p align="left"><b>Machine</b></p></td>'
            htmlvar += '<td style=" height: 20px;width:15%;align:center;" border-right="1px"><b>Remarks</b></td> ';
           
          //  htmlvar += '<td style="height: 20px;width:15%;align:center;" border-right="1px"><p align="left"><b>Purpose</b></p></td> ';

            htmlvar += '</tr>';



            var lineCount = objRecord.getLineCount({
                sublistId: 'item'
            });

            
            for (var l = 0; l < lineCount; l++) {


                var quantity = objRecord.getSublistText({
                    sublistId: 'item',
                    fieldId: 'quantity',
                    line: l
                });

                var classification = objRecord.getSublistText({
                    sublistId: 'item',
                    fieldId: 'cseg_itemclassifica',
                    line: l
                });
                var itemDescription = objRecord.getSublistValue({
                    sublistId: 'item',
                    fieldId: 'description',
                    line: l
                });

                var remarks = objRecord.getSublistValue({
                    sublistId: 'item',
                    fieldId: 'custcol_remark_line',
                    line: l
                });

                var intendedTo = objRecord.getSublistText({
                    sublistId: 'item',
                    fieldId: 'custcol_intended_to',
                    line: l
                });
				
				var units = objRecord.getSublistText({
                    sublistId: 'item',
                    fieldId: 'units_display',
                    line: l
                });


                if (quantity == null) {
                    quantity = '';

                }
                if (classification == null) {
                    classification = '';

                }
                if (remarks == null) {
                    remarks = '';

                }

                if (intendedTo == null) {
                    intendedTo = '';

                }
				
				if (units == null) {
                    units = '';

                }


                var lineNum = l + 1;

                htmlvar += '<tr   >';

                htmlvar += '<td style=" height: 20px;width:5%;align:center;" border-right="1px" border-left="1px"><p align="left">' + lineNum + '</p></td> ';
                htmlvar += '<td style=" height: 20px; width:15.5%;align:center;" border-right="1px">' + quantity + '</td>'

                htmlvar += '<td style=" height: 20px; width:5%;align:center;" border-right="1px">' + xml.escape({
                    xmlText: units
                }) + '</td> ';
                htmlvar += '<td style=" height: 20px; width:12.5%; font-size:9pt" border-right="1px"><p align="left">' + xml.escape({
                    xmlText: classification
                }) + '</p></td>'
                htmlvar += '<td style=" height: 20px;width:14%;" border-right="1px"><p align="left">' + xml.escape({
                    xmlText: itemDescription
                }) + '</p></td> ';
				
				 htmlvar += '<td style=" height: 20px;width:10%;align:center;" border-right="1px"><p align="left">' + xml.escape({
                    xmlText: intendedTo
                }) + '</p></td>'
                htmlvar += '<td style=" height: 20px;width:15%;align:center;" border-right="1px"><p align="left">' + xml.escape({
                    xmlText: remarks
                }) + '</p></td> ';
               
              //  htmlvar += '<td style="height: 20px;width:15%;align:center;" border-right="1px" ></td> ';



                htmlvar += '</tr>';


            }


            htmlvar += '</table>';

            htmlvar += '</body>\n</pdf>';

            var pdfFile = render.xmlToPdf({
                xmlString: htmlvar
            });
			
            return context.response.writeFile(pdfFile, true);




        }


        return {
            onRequest: onRequest
        };
    });