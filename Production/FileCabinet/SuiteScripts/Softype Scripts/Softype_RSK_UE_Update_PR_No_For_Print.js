/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  07th February, 2020
 ** @Version     :  2.x
 ** @Description :  Setting PR No. on the body field for Purchase Order Print.
 ***************************************************************************************/
define(['N/record', 'N/runtime'],

    function(record, runtime) {


        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */
        function afterSubmit(scriptContext) {



            var id = scriptContext.newRecord.id;
            var type = scriptContext.newRecord.type;
            var currentRecord = scriptContext.newRecord;

            var customFormParameter = runtime.getCurrentScript().getParameter("custscript_po_form_id");

            var customForm = currentRecord.getValue({
                fieldId: 'customform'

            });

            if (customFormParameter != customForm) {

                return;
            }



            var linkedOrder = currentRecord.getSublistValue({
                sublistId: 'item',
                fieldId: 'linkedorder',
                line: 0
            });
            log.debug('Linked Order', linkedOrder)

            var recordId = record.submitFields({
                type: type,
                id: id,
                values: {
                    'custbody_pr_number_po': linkedOrder
                }


            });
            log.debug('Updated Record', recordId);



        }



        return {


            afterSubmit: afterSubmit
        };

    });