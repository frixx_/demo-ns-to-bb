/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  16th September, 2019
 ** @Version     :  2.0
 ** @Description :  This script is deployed on custom record(parent record) 'Canvassing Record'
				    for adding the No. of Quotation based on the values entered by the user in the 
					No. of Quotation column.

 ***************************************************************************************/
define(['N/record', 'N/runtime', 'N/redirect'],

    function(record, runtime, redirect) {

        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {string} scriptContext.type - Trigger type
         * @param {Form} scriptContext.form - Current form
         * @Since 2015.2
         */
        function beforeLoad(scriptContext) {




        }


        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */

        function beforeSubmit(scriptContext) {




        }

        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */
        function afterSubmit(scriptContext) {


            var id = scriptContext.newRecord.id;
            var type = scriptContext.newRecord.type;


            log.debug('after submit - type', scriptContext.type);

            if (scriptContext.type == 'edit') {
                var objRecord = record.load({
                    type: type,
                    id: id,
                    isDynamic: true,
                });

                var lineCount = objRecord.getLineCount({
                    sublistId: 'recmachcustrecord_link_canvassing'
                });


                for (var l = 0; l < lineCount; l++) {


                    var itemValue = objRecord.getSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_item_canvas',
                        line: l
                    });


                    var prQuantity = objRecord.getSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_qty_canvas',
                        line: l
                    });

                    var noOfQuotation = objRecord.getSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_no_quotation',
                        line: l
                    });
                    log.audit('noOfQuotation', noOfQuotation);




                    if (noOfQuotation) {

                        for (var q = 0; q < noOfQuotation - 1; q++) {



                            objRecord.selectNewLine({
                                sublistId: 'recmachcustrecord_link_canvassing'
                            });


                            objRecord.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_link_canvassing',
                                fieldId: 'custrecord_item_canvas',
                                value: itemValue
                            });

                            objRecord.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_link_canvassing',
                                fieldId: 'custrecord_qty_canvas',
                                value: prQuantity
                            });


                            objRecord.commitLine({
                                sublistId: 'recmachcustrecord_link_canvassing'
                            });




                        }
                    }




                }


                var recId = objRecord.save();




                /** Reloading Again to set 'custrecord_no_quotation' to empty
		      using standard obj so that it does not create the item line
			  based on quotation.
			  

		  **/

                var obj_Record = record.load({
                    type: type,
                    id: id,
                    // isDynamic: true,
                });

                var line_Count = obj_Record.getLineCount({
                    sublistId: 'recmachcustrecord_link_canvassing'
                });


                for (var b = 0; b < line_Count; b++) {


                    var no_Of_Quotation = obj_Record.getSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_no_quotation',
                        line: b
                    });


                    obj_Record.setSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_no_quotation',
                        line: b,
                        value: ''
                    });


                }

                obj_Record.save();




            }

        }




        return {

            //beforeLoad: beforeLoad,
            //beforeSubmit: beforeSubmit,
              afterSubmit: afterSubmit
        };

    });