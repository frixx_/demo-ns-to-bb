/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : After creation of Item record in Netsuite script calls RSK API to create Item Master in RSK System
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {
		
		function afterSubmit(context) {
			
			/* log.debug('runtime',runtime);
			log.debug('runtime.executionContext',runtime.executionContext);
			log.debug('context',context); */
			
			var newRecordDetails = context.newRecord;
			// log.debug('newRecordDetails', newRecordDetails);
			
			try{
				
				var scriptObj = runtime.getCurrentScript();
				var createAPI = scriptObj.getParameter({name: 'custscript_create_api_item'});	// RSK Create API
				var updateAPI = scriptObj.getParameter({name: 'custscript_update_api_item'});	// RSK Update API
				
				/**********************************************************
					*************************API CALL************************
				**********************************************************/
				
				var headers = {
					"Content-Type": "application/json"
				};	//Headers if Any
				
				var response;
				
				var supplierIDRSK = "", categoryIDRSK = "";
				
				/* Getting selected NetSuite Supplier ID */
				var supplierID = newRecordDetails.getSublistValue({
					sublistId: 'itemvendor',
					fieldId: 'vendor',
					line: 0
				});
				
				log.debug("supplierID",supplierID);						
				
				/* Searching for RSK ID for the selected Supplier */
				if(!!supplierID){
					supplierIDRSK = (search.lookupFields({
						type: search.Type.VENDOR,
						id: supplierID,
						columns: ['custentity_rsksuppleirid']
					}))["custentity_rsksuppleirid"];
					
					log.debug("supplierIDRSK",supplierIDRSK);
				}
				
				/* Getting selected NetSuite RSK Item Category ID */
				var categoryId = newRecordDetails.getValue({
					fieldId: 'custitem_rskitemcategory'
				});			
				
				/* Searching for RSK ID for the selected Item Category */
				if(!!categoryId){
					categoryIDRSK = (search.lookupFields({
						type: "customrecord_rskcategory",
						id: categoryId,
						columns: ['custrecord_categoryid']
					}))["custrecord_categoryid"];
				}
				
				var assetClass = newRecordDetails.getValue({
					fieldId: 'custitem_asset_class'
				});
				
				var IsTrade = false;
				if(categoryId == '4'){
					IsTrade = true;	
				}
				
				var IsNonTrade = false;
				if(categoryId == '1'){
					IsNonTrade = true;					
				}
				
				var cost = newRecordDetails.getValue({fieldId: 'cost'});
				
				var rskID = newRecordDetails.getValue({fieldId: 'custitem_rskitemid'});
				
				var isInActive = newRecordDetails.getValue({fieldId: 'isinactive'});
				// log.debug("isInActive",isInActive);
				
				if(context.type == 'create' || rskID == ""){
					
					var jsonBody = {
						"NsId": newRecordDetails.id,
						"SupplierId": Number(supplierIDRSK) == 0 ? 1 : Number(supplierIDRSK),
						"BrandId": 1,
						"CategoryId": Number(categoryIDRSK),//categoryIDRSK,
						"UnitId": Number(newRecordDetails.getValue({
							fieldId: 'unitstype'
						})),
						"ProductName": newRecordDetails.getValue({
							fieldId: 'displayname'
						}),
						"Description": newRecordDetails.getValue({
							fieldId: 'purchasedescription'
						}),
						"SKU": newRecordDetails.getValue({
							fieldId: 'itemid'
						}),
						"Barcode": newRecordDetails.getValue({
							fieldId: 'custitem_rskbarcode'
						}),
						"ItemCode": newRecordDetails.getValue({
							fieldId: 'itemid'
						}),
						"IsActive": !(isInActive),
						"IsTrade": IsTrade,
						"IsNonTrade": IsNonTrade,
						//"AssetClass":Number(assetClass),
						"TaxCodeId": 1,/*Number(newRecordDetails.getValue({
							fieldId: 'custitem_4601_defaultwitaxcode'
						})),*/
						"Price": cost,
						"PriceVat": cost * (1+12/100),
						"Vat": 12
					};	
					
					log.debug("Create - JSON", jsonBody);			
					
					response = http.request({
						method: https.Method.POST,
						url: createAPI,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					
					SetRSKID(newRecordDetails, response, true);
					
				}else if(context.type == 'edit'){
					
					var jsonBody = {
						"NsId": newRecordDetails.id,
						"Id" : Number(newRecordDetails.getValue({
							fieldId: 'custitem_rskitemid'
						})),
						"SupplierId": Number(supplierIDRSK) == 0 ? 1 : Number(supplierIDRSK),
						"BrandId": 1,
						"CategoryId": Number(categoryIDRSK),//categoryIDRSK,
						"UnitId": Number(newRecordDetails.getValue({
							fieldId: 'unitstype'
						})),
						"ProductName": newRecordDetails.getValue({
							fieldId: 'displayname'
						}),
						"Description": newRecordDetails.getValue({
							fieldId: 'purchasedescription'
						}),
						"SKU": newRecordDetails.getValue({
							fieldId: 'itemid'
						}),
						"Barcode": newRecordDetails.getValue({
							fieldId: 'custitem_rskbarcode'
						}),
						"ItemCode": newRecordDetails.getValue({
							fieldId: 'itemid'
						}),
						"IsActive": !(isInActive),
						"IsTrade": IsTrade,
						"IsNonTrade": IsNonTrade,
						//"AssetClass":Number(assetClass),
						"TaxCodeId": 1,/*Number(newRecordDetails.getValue({
							fieldId: 'custitem_4601_defaultwitaxcode'
						})),*/
						"Price": cost,
						"PriceVat": cost * (1+12/100),
						"Vat": 12
					};	
					
					log.debug("Edit - JSON", jsonBody);	
					
					response = http.request({
						method: https.Method.PUT,
						url: updateAPI,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					SetRSKID(newRecordDetails, response, true);
				}
			}catch(err){
				SetFlag(newRecordDetails, 400, "Server Error");
				return;
			}
		}
		
		function SetRSKID(newRecordDetails, response, flag){
			/* Checking for the Response */
			var data = JSON.parse(response.body); 
			var dataCode = response.code; 
			log.debug("response",response);
			log.debug("dataCode",dataCode);
			
			if(dataCode == "200"){
				
				var rec = record.load({type : newRecordDetails.type, id : newRecordDetails.id});
				
				rec.setValue({
					fieldId: "custitem_itemmaster_flag",	//Flag Checkbox Field
					value: false,				
					ignoreFieldChange: true
				});
				
				rec.setValue({
					fieldId: "custitem_itemaster_remark",	//Flag Remark Field
					value: "",				
					ignoreFieldChange: true
				});	
				
				rec.save();
				
				return;
				
			}else{
				SetFlag(newRecordDetails, dataCode, data.ModelState);
			}
		}
		
		function SetFlag(newRecordDetails, dataCode, message){
			/* Flagging the records if the API failed */
			
			var rec = record.load({type : newRecordDetails.type, id : newRecordDetails.id});
			
			rec.setValue({
				fieldId: "custitem_itemmaster_flag",	//Flag Checkbox Field
				value: true,				
				ignoreFieldChange: true
			});
			rec.setValue({
				fieldId: "custitem_itemaster_remark",	//Flag Remark Field
				value: dataCode + " : " + JSON.stringify(message),				
				ignoreFieldChange: true
			});
			rec.save();
			
		}
		
		return {
			afterSubmit: afterSubmit
		};
	});		