/**
	* @NApiVersion 2.x
	* @NScriptType Suitelet
	* @NModuleScope SameAccount
*/

/***************************************************************************************  
	** Copyright (c) 1998-2018 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
	**                      
	**@Author      :  Siddhi Kadam
	**@Dated       :  5th February, 2020
	**@Version     :  2.x
	**@Description :  Suitelet for check Print.
***************************************************************************************/
define(['N/ui/serverWidget', 'N/record', 'N/render', 'N/file', 'N/runtime', 'N/xml','N/format','N/search'],
	
    function(serverWidget, record, render, file, runtime, xml,format,search) {
		
        function onRequest(context) {
			
			
            var recordId = context.request.parameters.recordId;
            var recordType = context.request.parameters.recordType;
            
			var objRecord = record.load({
                type: recordType,
                id: recordId,
                isDynamic: true,
			});
			
			//Amol - Changed check date
			var checkDate = objRecord.getText({
                fieldId: 'custbody_nameon_checkprint'//'trandate'
			});
			
			/* var nameonCheckPrint = objRecord.getValue({
                fieldId: 'custbody_name_checkprint'
			}); */
			// Amol - Changed Payee
			var nameonCheckPrint = objRecord.getText({
                fieldId: 'custbody_alternate_payee'
			});
			
			
			var enity = objRecord.getValue({
                fieldId: 'entity'
			});
			
			var legalNameLookUp = search.lookupFields({
			 type: search.Type.VENDOR,
			 id: enity,
			 columns: ['legalname']
			});
			
			var legalName = legalNameLookUp.legalname;
			
			
			
			
			
			
			/* var userTotal = objRecord.getValue({
                fieldId: 'usertotal'
			}); */
			
			// Amol - Getting Text
			var userTotal = objRecord.getText({
                fieldId: 'usertotal'
			}) || objRecord.getText({
                fieldId: 'total'
			});
			
			var amountInWords = objRecord.getText({
                fieldId: 'custbody_cashsale_totalword'
			});
			
			if (checkDate == null)
			checkDate = '';
			
			if (userTotal == null){
				userTotal = '';
			}else{
				while(userTotal.indexOf(",") > 0)
					userTotal = userTotal.replace(",","")
				log.debug("userTotal", userTotal);
				userTotal = Number(userTotal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				if(userTotal.length - userTotal.indexOf(".") == 2){
					userTotal = userTotal.concat("0");
				}else{
					userTotal = userTotal;
				}
				
				if(userTotal.indexOf(".") == -1){
					userTotal = userTotal.concat(".00");
				}
			}
			
			if (amountInWords == null)
			amountInWords = '';
			
			if (legalName == null)
			legalName = '';
		
			var payeeName = "";
			if(nameonCheckPrint){
				payeeName = nameonCheckPrint;
			}else{
				payeeName = legalName;
			}
			var form = serverWidget.createForm({
                title: 'Account'
			});
			
		    var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
            htmlvar += '<pdf>\n';
            // htmlvar += '<head>';
            // htmlvar += '<style>';
			
			// htmlvar += '</style>';
			// htmlvar += '</head>';
			
			// Amol
			// htmlvar += '<body padding="1.4cm 1.4cm 1.4cm 1.4cm" size="A4">';
			htmlvar += '<body padding="1cm 1.4cm 1.4cm 0.8cm" >';
			htmlvar += '<div style="position: relative;font-family:Courier;top= -14pt;height: 250pt;width: 612pt;page-break-inside: avoid;font-size: 11pt;padding-top: -20px;">'
			htmlvar += '<table style="position: absolute;overflow: hidden;top: -11pt;height: 7pt;width: 120pt;font-size: 11pt;font-weight: bold;"><tr>'
			htmlvar += '<td style="text-align: justify;">'
			// Amol - Dont need for payees account only
			//htmlvar += '<div style="text-align: left;padding-top:22px;"><span style="font-size:10px;left:-400px;overflow: hidden;padding-top: 5px;">FOR PAYEES ACCOUNT ONLY</span></div>'
			htmlvar += '<div style="text-align: left;padding-top:22px;"><span style="font-size:11pt;left:-400px;overflow: hidden;padding-top: 5px;"></span></div>'
			htmlvar += '</td></tr></table>'
			// htmlvar += '<table style="position: absolute;overflow: hidden;left: 405pt;top: -9pt;height: 18pt;width: 108pt;font-size:12pt;font-weight: bold;padding-top: 21px;"><tr>'
			htmlvar += '<table style="position: absolute;overflow: hidden;left: 433pt;top: -9pt;height: 18pt;width: 140pt;font-size:11pt;font-weight: bold;padding-top: 21px;"><tr>'
			htmlvar += '<td style="font-family: courier;">&nbsp;&nbsp;'+checkDate+'</td>'
			htmlvar += '</tr></table>'
			log.debug("payee length", payeeName.length);
			
			if(payeeName.length > 46){
				width = 360;
				top = 0;
				left = 38;
			}else{
				width = 393;
				top = 13;
				left = 38;
			}
			htmlvar += '<table style="position: absolute;overflow: hidden;left: '+left+'pt;top: '+top+'pt;height: 16pt;width: '+width+'pt;font-size:11pt;font-weight: bold;padding-top: 15px;"><tr>'
			
			// if(nameonCheckPrint){
			htmlvar += '<td style="font-family: courier;">**'+ xml.escape({xmlText: payeeName})+'**</td>'
			// }else{
				
			// htmlvar += '<td style="font-family: courier;">**'+ xml.escape({xmlText: legalName})+'**</td>'
				
			// }
			
			
			htmlvar += '</tr></table>'
			htmlvar += '<table style="position: absolute;overflow: hidden;left: 428pt;top: 15pt;height: 16pt;width: 113pt;font-size:11pt;font-weight: bold;padding-top: 16px;"><tr>'
			htmlvar += '<td style="font-family: courier;">&nbsp;&nbsp;'+(userTotal)+'</td>'
			htmlvar += '</tr></table>'
			
			if(amountInWords.length > 70){
				top = 41;
				fontSize = 10;
			}else{
				top = 41;
				fontSize = 11;
			}
			htmlvar += '<table style="position: absolute;overflow: hidden;left: 15pt;top: '+top+'pt;height: 18pt;width: 85%;font-size:'+fontSize+'pt;font-weight: 600;padding-top: 10px;"><tr>'
			//Amol - Added double asterisk before & after
			htmlvar += '<td style="font-family: courier;">**'+ xml.escape({xmlText:amountInWords})+'**</td>'
			htmlvar += '</tr></table>'
			htmlvar += '</div>'
						
			htmlvar += '</body>';
		    htmlvar +='\n</pdf>';
			
            var pdfFile = render.xmlToPdf({
                xmlString: htmlvar
			});
			
		return context.response.writeFile(pdfFile, true);
		
		
		
        }
		
		
		
        return {
		onRequest: onRequest
        };
		});		