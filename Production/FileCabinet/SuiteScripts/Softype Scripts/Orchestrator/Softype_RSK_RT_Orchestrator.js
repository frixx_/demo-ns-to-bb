/**
 * @NApiVersion 2.x
 * @NScriptType restlet
*/
/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : Orchestrator Functionality to set up RSK ID's to NetSuite records
***************************************************************************************/

define(['N/record', 'N/search', 'N/log', 'N/format'], function(record, search, log, format) {
	return {
		post : function(data){
			
			/* request = {
							"Type" : "purchaseorder",
							"Id" : "2345",
							"RSKId" : "2134"				
						} */
			
			if(typeof data === 'string'){
				data = JSON.parse(data);
			}
			
			log.debug("jsonData",JSON.stringify(data));
			
			try{
				
				var fieldName = "", flagFieldName = "", flagRemarkField = "";
				
				if(data.Type === "purchaseorder"){
					fieldName = "custbody_rskponum";
					flagFieldName = "custbody_flagcheck";
					flagRemarkField = "custbody_remarkfield";
				}else if(data.Type === "vendor"){
					fieldName = "custentity_rsksuppleirid";
					flagFieldName = "custentity_itemmasterflag";
					flagRemarkField = "custentity_itemmaster_remakrs";
				}else if(data.Type === "item"){
					data.Type = "noninventoryitem";
					fieldName = "custitem_rskitemid";
					flagFieldName = "custitem_itemmaster_flag";
					flagRemarkField = "custitem_itemaster_remark";
				}else if(data.Type === "location"){
					fieldName = "custrecord_location_rskidfield";
					flagFieldName = "custrecord_location_flagfield";
					flagRemarkField = "custrecord_location_remark";
				}else if(data.Type === "itemcategory"){
					data.Type = "customrecord_rskcategory";
					fieldName = "custrecord_categoryid";
					flagFieldName = "custrecord_categoryflag";
					flagRemarkField = "custrecord_categoryremarks";
				}else if(data.Type === "vendorreturnauthorization"){
					data.Type = "vendorreturnauthorization";
					fieldName = "custbody_rskvenretauth";
				}
				
				/* var id = record.submitFields({
					type: data.Type,
					id: data.Id,
					values: {
						fieldName: data.RSKId
					},
					options: {
						enableSourcing: false,
						ignoreMandatoryFields : true
					}
				}); */
				
				
				// if(data.Type === "purchaseorder"){
					var values = {};
					
					values[fieldName] = data.RSKId;
					if(flagFieldName != ""){
						values[flagFieldName] = false;
					}
					if(flagRemarkField != ""){
						values[flagRemarkField] = "";
					}
					if(data.InternalContractNo != null){
						values["custbody_pocon_intcontractnum"] = data.InternalContractNo;
					}
					
					log.debug("Values", values);
					
					var id = record.submitFields({
						type: data.Type,//record.Type.PURCHASE_ORDER,
						id: data.Id,
						values: values,
						options: {
							enableSourcing: false,
							ignoreMandatoryFields : true
						}
					});
				// }
				/* else{
				
					log.debug("data.Type",data.Type);
					log.debug("flagFieldName",flagFieldName);
					log.debug("flagRemarkField",flagRemarkField);
					var orchestratorRecord = record.load({
						type: data.Type,
						id: data.Id,
						isDynamic : true
					});
					
					orchestratorRecord.setValue({
						fieldId: fieldName,
						value: data.RSKId,				
						ignoreFieldChange: true
					});
					orchestratorRecord.setValue({
						fieldId: flagFieldName,  	//Flag Field
						value: false,				
						ignoreFieldChange: true
					});
					orchestratorRecord.setValue({
						fieldId: flagRemarkField,	//Flag Remark Field
						value: "",				
						ignoreFieldChange: true
					});
				 
					
					orchestratorRecord.save();
				
				
				} */
				
				return {message: "Success"};
			}catch(err){
				return {message:"Failure", error: err};		
			}
			
			//return "Success";
		}
	}
	
	
	function parseAndFormatDateString(myDate) {

		var initialFormattedDateString = myDate;
		var parsedDateStringAsRawDateObject = format.parse({
			value: initialFormattedDateString,
			type: format.Type.DATE,

		});

		return parsedDateStringAsRawDateObject;
		
	}
		
});