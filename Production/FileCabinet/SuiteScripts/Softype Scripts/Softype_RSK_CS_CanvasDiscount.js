/**
	*@NApiVersion 2.x
	*@NScriptType ClientScript
*/
/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol Jagkar
	**@Dated : 07 October 2019
	**@Version : 2.0
	**@Description : Client Script deployed on Canvasing Custom Record which will calculate discount from the Line fields.
***************************************************************************************************************/
define(['N/ui/dialog', 'N/record', 'N/search', 'N/runtime', 'N/url', 'N/https', 'N/log'],
	
    function(dialog, record, search, runtime, url, https) {
        var SearchItems = [];
        var getFirstDiscount;
        var getSecondDiscount;
        var getThirdDiscount;
		var initialAmount;
		var po_rate;
		var flag = true;
		var discount = {};
		//var split_taxrate;
		
		
        //This function is run when the vendor discounts and rates are changed
        function fieldchanged(context) {
			
			var newRecord = context.currentRecord;
            var sublistName = context.sublistId;
            var fieldId = context.fieldId;
						
            //This condition is for the change of the discount fields
			
            if(fieldId == 'custrecord_discount1' || fieldId == 'custrecord1' || fieldId == 'custrecord_discount3' || fieldId == 'custrecord_canvas_rate') {
				
				console.log('field change '+fieldId)
				
                var numLines = newRecord.getLineCount({
                    sublistId: 'recmachcustrecord_link_canvassing'
				});
				//alert('numLines'+numLines);
				
                //This loop is to get the discounts and rate of items.
                for(var a = 0; a <= numLines; a++) {
					
					getFirstDiscount = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_discount1'
					});
					
					//alert('getFirstDiscount'+getFirstDiscount);
					
					getSecondDiscount = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord1'
					});
					
					getThirdDiscount = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_discount3'
					});
					
                    initialAmount = parseFloat(newRecord.getCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_canvas_rate'
					}))//.toFixed(3);
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_po_rate',
						value: initialAmount//.toFixed(3)
					});
					
					var quantity = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_canvassing_qty'
					});
					//initialAmount = initialAmount.toFixed(3);
					
					//alert('Initial Amount'+initialAmount);
					
					//If the user leaves the 1 discount blank then set it to 0 for calculation
					if(getFirstDiscount == ' ' || getFirstDiscount == null || getFirstDiscount == undefined || getFirstDiscount == 0) {
						
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_discount1',
							value: 0,
							ignoreFieldChange: true
						});
						
					}
					//If the user leaves the 2 discount blank then set it to 0 for calculation
					if(getSecondDiscount == ' ' || getSecondDiscount == null || getSecondDiscount == undefined || getSecondDiscount == 0) {
						
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord1',
							value: 0,
							ignoreFieldChange: true
						});
						
					}
					//If the user leaves the 3 discount blank then set it to 0 for calculation
					if(getThirdDiscount == ' ' || getThirdDiscount == null || getThirdDiscount == undefined || getThirdDiscount == 0) {
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_discount3',
							value: 0,
							ignoreFieldChange: true
						});
						
					}
					
                    //If there is no discount then set 0 for discounts
                    if(getFirstDiscount && getFirstDiscount != 0 && getFirstDiscount != '') {
                        //Calculating first discount				
                        // var discount1 = calculateDiscount(getFirstDiscount, initialAmount);
						discount["First"] = getFirstDiscount;
						var discount1 = calculateDiscount(quantity, discount, "First", initialAmount);
                        newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_rate_no_vat',
                            value: parseFloat(discount1.standardRate)//.toFixed(3)
						});
						//Subtract Amount to first discount rate to get balance and multiply with quantity for first discount amount.
						// var discountAmt1 = (initialAmount - discount1) * quantity;
						var discountAmt1 = discount1.amount;
						//Setting first discount amount
						newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_disc1_amount',
                            value: parseFloat(discountAmt1).toFixed(2)
						});
						
						
						}else {
						
                        // var discount1 = calculateDiscount(0, initialAmount)
						discount["First"] = 0;
						var discount1 = calculateDiscount(quantity, discount, "First", initialAmount);
                        newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_rate_no_vat',
                            value: parseFloat(discount1.standardRate)//.toFixed(3)
						});
						//var discountAmt1 = discount1 * quantity;
						//Setting first discount amount
						newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_disc1_amount',
                            value: 0
						});
						
					}
					//alert('discount1'+discount1);
					
					if(getFirstDiscount != 0 && getSecondDiscount && getSecondDiscount != '' && getSecondDiscount != 0) {
                        //Calculating second discount
                        // var discount2 = calculateDiscount(getSecondDiscount, discount1);
						
						discount["First"] = getFirstDiscount;
						discount["Second"] = getSecondDiscount;
                        var discount2 = calculateDiscount(quantity, discount, "Second", initialAmount);
                        newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_rate_no_vat',
                            value: parseFloat(discount2.standardRate).toFixed(2)
						});
						var amtAfterFirstDiscount = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_disc1_amount'
						});
						//Subtract amount with second discount and multiply with quantity to get balance. 
						// var secondAmt = (initialAmount - discount2) * quantity;
						// var discountAmt2 = parseFloat(secondAmt) - parseFloat(amtAfterFirstDiscount);
						//Setting first discount amount
						newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_disc2_amount',
                            value: parseFloat(discount2.discount).toFixed(2)
						});
					}
					else {
						
						
                        // var discount2 = calculateDiscount(0, discount1);
						discount["First"] = getFirstDiscount;
						discount["Second"] = 0;
                        var discount2 = calculateDiscount(quantity, discount, "Second", initialAmount);
                        newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_rate_no_vat',
                            value: parseFloat(discount2.standardRate).toFixed(2)
						});
						//var discountAmt2 = discount2 * quantity;
						//Setting first discount amount
						newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_disc2_amount',
                            value: 0
						});
						
					}
					
					if(getFirstDiscount == 0 && getSecondDiscount != 0){
						
						alert('Enter First Discount');
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord1',
							value: 0,
							ignoreFieldChange: true
						});
					}
					
					if(getSecondDiscount == 0 && getThirdDiscount != 0){
						
						alert('Enter Second Discount');
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_discount3',
							value: 0,
							ignoreFieldChange: true
						});
					}
					//alert('discount2'+discount2);
					if(getFirstDiscount != 0  && getSecondDiscount != 0 && getThirdDiscount && getThirdDiscount != ' ') {
                        //Calculating third discount	
                        // var discount3 = calculateDiscount(getThirdDiscount, discount2);
						discount["First"] = getFirstDiscount;
						discount["Second"] = getSecondDiscount;
						discount["Third"] = getThirdDiscount;
                        var discount3 = calculateDiscount(quantity, discount, "Third", initialAmount);
                        newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_rate_no_vat',
                            value: (discount3.standardRate)//.toFixed(3)
						});
						var amtAfterFirstDiscount = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_disc1_amount'
						});
						var amtAfterSecondDiscount = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_disc2_amount'
						});
						//Subtract amount with third discount and multiply with quantity to get balance. 
						// var thirdAmt = (initialAmount - discount3) * quantity;
						// var totAmt = parseFloat(thirdAmt) - parseFloat(amtAfterFirstDiscount);
						// var discountAmt3 = parseFloat(totAmt) - parseFloat(amtAfterSecondDiscount);
						//Setting first discount amount
						newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_disc3_amount',
                            value: parseFloat(discount3.discount).toFixed(2)
						});
					}
					else {
						//alert('Enter Second Discount');
                        // var discount3 = calculateDiscount(0, discount2);
						discount["First"] = getFirstDiscount;
						discount["Second"] = getSecondDiscount;
						discount["Third"] = 0;
                        var discount3 = calculateDiscount(quantity, discount, "Third", initialAmount);
                        newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_rate_no_vat',
                            value: (discount3.standardRate)//.toFixed(3)
						});
						//var discountAmt3 = discount3 * quantity;
						//Setting first discount amount
						newRecord.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_link_canvassing',
                            fieldId: 'custrecord_disc3_amount',
                            value: 0
						});
						
					}
					//alert('discount3'+discount3);
				}
				newRecord.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_link_canvassing',
                    fieldId: 'custrecord_canvas_rate',
                    value: initialAmount,
					ignoreFieldChange: true
				});
				
			}
			
			//On selection of tax code get the tax rate from the tax item and calculate the rate after the tax rate.
			if(fieldId =='custrecord_tax_code'){
				
				//fieldId == 'custrecord_vat_inclusive' 
				
				var taxcode = newRecord.getCurrentSublistValue({
					sublistId: 'recmachcustrecord_link_canvassing',
					fieldId: 'custrecord_tax_code'
				});
				var vat_checkbox = newRecord.getCurrentSublistValue({
					sublistId: 'recmachcustrecord_link_canvassing',
					fieldId: 'custrecord_vat_inclusive'
				});
				var quantity = newRecord.getCurrentSublistValue({
					sublistId: 'recmachcustrecord_link_canvassing',
					fieldId: 'custrecord_canvassing_qty'
				});
				//alert('vat_checkbox'+ vat_checkbox);
				// Lookup the tax code and get the tax rate from tax setup record.
				if(taxcode !='' && vat_checkbox == true){
					var taxcode_lookup = search.lookupFields({
						type: 'salestaxitem',
						id: taxcode,
						columns: ['rate']
					});
					//alert('taxcode_lookup' + taxcode_lookup);
					
					var tax_rate = taxcode_lookup.rate;
					//alert('tax_rate' + tax_rate);
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_tax_rate',
						value: tax_rate
					});
					
					var finaldiscount = 0;
					var rateAfterDiscount = GetFinalDiscount(newRecord)
					if(!!(rateAfterDiscount)){
						finaldiscount = rateAfterDiscount.standardRate;
						
						//finaldiscount = finaldiscount.toFixed(3);
						
						// Get the tax rate and rate after discount field and calculate the po rate by removing the tax from the after discount rate.
						// P.S. The Rate is actaully tax inclusive that's why reverse Calculation
						var split_taxrate = tax_rate.replace('%','');
						
						var po_ratedis = finaldiscount - (finaldiscount / (1 + split_taxrate) * split_taxrate);
						// console.log("po_ratedis ("+po_ratedis+") = "+finaldiscount+" (finaldiscount) " - +"("+finaldiscount+"(finaldiscount) " +" + "+ " / "+ "(1 + "+split_taxrate +"(split_taxrate)) * "+split_taxrate +"(split_taxrate)"));
						
						//po_ratedis = po_ratedis.toFixed(3)
						//alert('po_ratedis' + po_ratedis);
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_po_rate',
							value: po_ratedis//.toFixed(3)
						});
						//Getting the discount rate for calculating amount
						var discountamt = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_discountamt'
						});
						
						//discountamt = discountamt.toFixed(3);
						//Calculate the Amount after discount by mulitplying qty with rate after discount.
						var amtAfterDis = discountamt * quantity;
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_total_discount_amount',
							value: rateAfterDiscount.amount//amtAfterDis.toFixed(2)
						});
					}else{
					
						initialAmount = parseFloat(newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_canvas_rate'
						}));//.toFixed(3);
						
						var split_taxrate = tax_rate.replace('%','');
						
						var po_ratedis = initialAmount - (initialAmount / (1 + split_taxrate) * split_taxrate);
					
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_po_rate',
							value: po_ratedis//.toFixed(3)
						});
					}
				}
				/* else{
					
					var rate_no_VAT = newRecord.getCurrentSublistValue({
					sublistId: 'recmachcustrecord_link_canvassing',
					fieldId: 'custrecord_rate_no_vat'
					});	
					
					newRecord.setCurrentSublistValue({
					sublistId: 'recmachcustrecord_link_canvassing',
					fieldId: 'custrecord_po_rate',
					value: rate_no_VAT.toFixed(4)
					});
				} */
				//If the taxcode is blank then set the tax rate blank and set the actual rate without tax rate on rate after discount.
				if(taxcode ==''){
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_tax_rate',
						value: ''
					});
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_po_rate',
						value: parseFloat(initialAmount)//.toFixed(3)
					});
					//Calculate the Amount after discount by mulitplying qty with rate.
					var amtAfterDis = initialAmount * quantity;
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_total_discount_amount',
						value: amtAfterDis.toFixed(2)
					});
					
				}
			}	
			
			// This is triggered when the rate after discount field is changed to set the discount amount field and it sets the Po rate. 
			
			if(fieldId == 'custrecord_rate_no_vat'){
				
				var finaldiscount = 0;
				var rateAfterDiscount = GetFinalDiscount(newRecord)
				if(!!(rateAfterDiscount)){
					finaldiscount = rateAfterDiscount.standardRate;
					
					/* 	
						var finaldiscount = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_rate_no_vat'
					});  */
					//finaldiscount = finaldiscount.toFixed(3)
					var quantity = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_canvassing_qty'
					});
					//
					// Get the amount of discount applied by subtracting rate with rate after discount.
					var discount_amount = (initialAmount - finaldiscount);
					/* var discountamt = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_discountamt'
					});	 */	
					//Calculate the Amount after discount by mulitplying qty with rate after discount.
					//discount_amount = discount_amount.toFixed(3);
					var amtAfterDis = discount_amount * quantity;
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_discountamt',
						value: discount_amount.toFixed(2)
					});
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_po_rate',
						value: finaldiscount//.toFixed(3)
					});
					//Calculate the Amount after discount by mulitplying qty with rate after discount.
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_total_discount_amount',
						value: rateAfterDiscount.amount//amtAfterDis.toFixed(2)
					});
					
					var vat_checkbox = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_vat_inclusive'
					});
					if(vat_checkbox == true){
						
						// Get the tax rate and rate after discount field and calculate the po rate by removing the tax from the after discount rate.
						// P.S. The Rate is actaully tax inclusive that's why reverse Calculation
						var taxcode = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_tax_code'
						});
						var taxcode_lookup = search.lookupFields({
							type: 'salestaxitem',
							id: taxcode,
							columns: ['rate']
						});
						//alert('taxcode_lookup' + taxcode_lookup);
						
						var tax_rate = taxcode_lookup.rate;
						//alert('tax_rate' + tax_rate);
						
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_tax_rate',
							value: tax_rate
						});
						var split_taxrate = tax_rate.replace('%','');
						
						var po_ratedis = finaldiscount - (finaldiscount / (1 + split_taxrate) * split_taxrate);
						
						//Getting the discount rate for calculating amount
						var discountamt = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_discountamt'
						});	
						discountamt = discountamt;//.toFixed(3);						
						//alert('po_ratedis' + po_ratedis);
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_po_rate',
							value: po_ratedis.toFixed(3)
						});
						//Calculate the Amount after discount by mulitplying qty with rate after discount.
						var amtAfterDis = discountamt * quantity;
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_total_discount_amount',
							value: rateAfterDiscount.amount.toFixed(2)//amtAfterDis.toFixed(2)
						});
						
					}
				}
			} 
			if(fieldId == 'custrecord_vat_inclusive'){
				
				var taxcode = newRecord.getCurrentSublistValue({
					sublistId: 'recmachcustrecord_link_canvassing',
					fieldId: 'custrecord_tax_code'
				});
				
				var vat_checkbox = newRecord.getCurrentSublistValue({
					sublistId: 'recmachcustrecord_link_canvassing',
					fieldId: 'custrecord_vat_inclusive'
				});
				
				if(vat_checkbox == false){
					
					var rate_no_VAT = newRecord.getCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_rate_no_vat'
					});	
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_po_rate',
						value: rate_no_VAT//.toFixed(3)
					});
				}
				if(vat_checkbox == true && taxcode !=''){
					
					
					var taxcode_lookup = search.lookupFields({
						type: 'salestaxitem',
						id: taxcode,
						columns: ['rate']
					});
					//alert('taxcode_lookup' + taxcode_lookup);
					
					var tax_rate = taxcode_lookup.rate;
					//alert('tax_rate' + tax_rate);
					
					newRecord.setCurrentSublistValue({
						sublistId: 'recmachcustrecord_link_canvassing',
						fieldId: 'custrecord_tax_rate',
						value: tax_rate
					});
					var split_taxrate = tax_rate.replace('%','');
					
					var finaldiscount = 0;
					var rateAfterDiscount = GetFinalDiscount(newRecord)
					if(!!(rateAfterDiscount)){
						finaldiscount = rateAfterDiscount.standardRate;
						
						
						var po_ratedis = finaldiscount - (finaldiscount / (1 + split_taxrate) * split_taxrate);
						
						//Getting the discount rate for calculating amount
						var discountamt = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_discountamt'
						});	
						discountamt = discountamt;//.toFixed(3);
						var quantity = newRecord.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_canvassing_qty'
						});						
						//alert('po_ratedis' + po_ratedis);
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_po_rate',
							value: po_ratedis//.toFixed(3)
						});
						
						//Calculate the Amount after discount by mulitplying qty with rate after discount.
						var amtAfterDis = discountamt * quantity;
						newRecord.setCurrentSublistValue({
							sublistId: 'recmachcustrecord_link_canvassing',
							fieldId: 'custrecord_total_discount_amount',
							value: rateAfterDiscount.amount//amtAfterDis.toFixed(2)
						});
						
					}
					//alert('amtAfterDis' + amtAfterDis);
				}
			}
			
		}  
		//This function is used to disable the VAT checkbox on adding the line to reduce the complexity and so the user cannot edit the line again.
		
		/* function lineInit(context) {
			var currentRecordObj = context.currentRecord;
		var sublistName = context.sublistId;
		var itemCount = currentRecordObj.getLineCount('recmachcustrecord_link_canvassing');
		var line = currentRecordObj.getCurrentSublistIndex({sublistId : sublistName});
		console.log('Line '+line+' itemCount '+itemCount);
		//var formid = currentRecordObj.getValue('customform');
		log.debug('line: ', line);
		var override = currentRecordObj.getCurrentSublistValue({
		sublistId : sublistName,
		fieldId : 'custrecord_vat_inclusive'
		});
		
		var Line = line - 1;
		if(Line >= 0){
		var vatinclField = currentRecordObj.getSublistField({
		sublistId : sublistName,
		fieldId : 'custrecord_vat_inclusive',
		line : Line
		});	
		vatinclField.isDisabled = false;
		}	
		//if(form_id == formid){ 
		if (itemCount > line) {
		
		// alert('override'+ override+'Lineindex'+line);
		var vatinclField = currentRecordObj.getSublistField({
		sublistId : sublistName,
		fieldId : 'custrecord_vat_inclusive',
		line : line
		});
		if(override == 1){
		vatinclField.isDisabled = true;
		}else{
		vatinclField.isDisabled = false;
		}
		}
		//}
		} */			
		
        /***************************************************************************************************************/
		
        //This function is used for the calculation	of all the discounts.
        /* function calculateDiscount(value1, value2) {
			console.log("Value 1 = " + value1);
			console.log("Value 2 = " + value2);
            var discountedAmt = ((100 - value1) / 100) * value2;
			console.log("Discount Amount = " + discountedAmt);
            var result = discountedAmt.toFixed(3);
			console.log("Result = " + result);
            return result;
			
		} */
		
		function GetFinalDiscount(newRecord){
			
			var discountType = "";
			
			var flag = false;
			
			initialAmount = parseFloat(newRecord.getCurrentSublistValue({
				sublistId: 'recmachcustrecord_link_canvassing',
				fieldId: 'custrecord_canvas_rate'
			}));//.toFixed(3);
			
			var quantity = newRecord.getCurrentSublistValue({
				sublistId: 'recmachcustrecord_link_canvassing',
				fieldId: 'custrecord_canvassing_qty'
			});
			
			getFirstDiscount = newRecord.getCurrentSublistValue({
				sublistId: 'recmachcustrecord_link_canvassing',
				fieldId: 'custrecord_discount1'
			});
			
			getSecondDiscount = newRecord.getCurrentSublistValue({
				sublistId: 'recmachcustrecord_link_canvassing',
				fieldId: 'custrecord1'
			});
			
			getThirdDiscount = newRecord.getCurrentSublistValue({
				sublistId: 'recmachcustrecord_link_canvassing',
				fieldId: 'custrecord_discount3'
			});
			
			if(getFirstDiscount != 0){
				discount["First"] = getFirstDiscount;
				discountType = "First";
				flag = true;
			}
			if(getSecondDiscount != 0){
				discount["Second"] = getSecondDiscount;
				discountType = "Second";
				flag = true;
			}
			if(getThirdDiscount != 0){
				discount["Third"] = getThirdDiscount;
				discountType = "Third";
				flag = true;
			}
			
			var rateAfterDiscount = calculateDiscount(quantity, discount, discountType, initialAmount);
			
			return rateAfterDiscount;
		}
		
		function calculateDiscount(quantity, discount, discountType, rate) {
			
			console.log("Discount Type : "+discountType+" Quantity :"+quantity+" Rate :"+rate);
			if(discountType == "First"){
				
				console.log("First Discount = " + discount["First"]);
				console.log("Rate = " + rate);
				var firstDiscountCalculation = ((100 - discount["First"]) / 100) * rate;
				console.log("First Discount calculation = " + firstDiscountCalculation);
				
				var amountAfterFirstDiscount = quantity * (rate - firstDiscountCalculation);
				console.log(quantity+"(quantity) * (" + rate + "(rate) - "+firstDiscountCalculation+" (firstDiscountCalculation)");
				console.log("Amount after first Discount = " + amountAfterFirstDiscount);
				return {amount: amountAfterFirstDiscount, standardRate: firstDiscountCalculation, discount: amountAfterFirstDiscount};
				
				}else if(discountType == "Second"){
				console.log("First Discount = " + discount["First"]);
				console.log("Rate = " + rate);
				var firstDiscountCalculation = ((100 - discount["First"]) / 100) * rate;
				console.log("First Discount calculation = " + firstDiscountCalculation);
				
				var amountAfterFirstDiscount = quantity * (rate - firstDiscountCalculation);
				console.log("Amount after first Discount = " + amountAfterFirstDiscount);
				
				//Second Discount
				console.log("Second Discount = " + discount["Second"]);
				var secondDiscountCalculation = firstDiscountCalculation * (100 - discount["Second"]) / 100;
				console.log("Second Discount Calculation = " + secondDiscountCalculation);
				var secondDiscountAmount = (firstDiscountCalculation - secondDiscountCalculation) * quantity;
				console.log("Second Discount Calculation with Quantity = " + secondDiscountAmount);
				
				var amountAfterSecondDiscount = amountAfterFirstDiscount + secondDiscountAmount;
				return {amount: amountAfterSecondDiscount, standardRate: secondDiscountCalculation, discount: secondDiscountAmount};
				
				}else if(discountType == "Third"){
				console.log("First Discount = " + discount["First"]);
				console.log("Rate = " + rate);
				var firstDiscountCalculation = ((100 - discount["First"]) / 100) * rate;
				console.log("First Discount calculation = " + firstDiscountCalculation);
				
				var amountAfterFirstDiscount = quantity * (rate - firstDiscountCalculation);
				console.log("Amount after first Discount = " + amountAfterFirstDiscount);
				
				//Second Discount
				console.log("Second Discount = " + discount["Second"]);
				var secondDiscountCalculation = firstDiscountCalculation * (100 - discount["Second"]) / 100;
				console.log("Second Discount Calculation = " + secondDiscountCalculation);
				var secondDiscountAmount = (firstDiscountCalculation - secondDiscountCalculation) * quantity;
				console.log("Second Discount Calculation with Quantity = " + secondDiscountAmount);
				
				var amountAfterSecondDiscount = amountAfterFirstDiscount + secondDiscountAmount;
				
				//Third Discount
				console.log("Third Discount = " + discount["Third"]);
				var thirdDiscountCalculation = secondDiscountCalculation * (100 - discount["Third"]) / 100;
				console.log("Third Discount Calculation = " + thirdDiscountCalculation);
				var thirdDiscountAmount = (secondDiscountCalculation - thirdDiscountCalculation) * quantity;
				console.log("Third Discount Calculation with Quantity = " + thirdDiscountAmount);
				
				var amountAfterThirdDiscount = amountAfterSecondDiscount + thirdDiscountAmount;
				
				return {amount: amountAfterThirdDiscount, standardRate: thirdDiscountCalculation, discount: thirdDiscountAmount};
			}
		}
        return {
            fieldChanged: fieldchanged,
			//lineInit : lineInit
			
		};
	});					