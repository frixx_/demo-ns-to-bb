/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  
 **                       
 **@Author      : Farhan Shaikh
 **@Dated       : 15/11/2019   DD/MM/YYYY
 **@Version     : 
 **@Description : UserEvent Script on Check Record To Calculate Gross Amount (Amount + Custom Tax Code) .  

 **************************************************************************************************************************************/
define(['N/record', 'N/runtime', 'N/search', 'N/format'],
    function(record, runtime, search, format) {
        var accountToSet = runtime.getCurrentScript().getParameter('custscript_acc_to_set_on_jv');
        var approvedStatus = runtime.getCurrentScript().getParameter('custscript_approved_status');
        function afterSubmit(context){
            var flag=false;
            log.debug('AFTER SUBMIT','AFTER SUBMIT')
            var currentRecordObj = context.newRecord;
            log.audit('currentRecordObj',currentRecordObj)

            var jvCreated=currentRecordObj.getValue('custrecord_jv_created');
            
            if (jvCreated) {
                return;
            }

            var orNumber=currentRecordObj.getValue('custrecord_or_num');
            var orDate=currentRecordObj.getValue('custrecord_or_date');
            var orAmount=currentRecordObj.getValue('custrecord_or_amt');
            var taxCode=currentRecordObj.getValue('custrecord__com_checks_tax_code');
            var actualTaxCode=getActualTaxCode(taxCode)
            var createdFrom=currentRecordObj.getValue('custrecord_sup_invoice_ref');
            var invoiceLookup = search.lookupFields({
                type: 'vendorbill',
                id: createdFrom,
                columns: ['subsidiary','cseg_type_of_trans','currency','location','department','class']
            });
            log.audit('invoiceLookup',invoiceLookup)
            var subsidiary = invoiceLookup.subsidiary[0].value;
            if (invoiceLookup.cseg_type_of_trans[0]!=undefined && invoiceLookup.cseg_type_of_trans[0]!=null && invoiceLookup.cseg_type_of_trans[0]!='') {
                var typeOfTrans = invoiceLookup.cseg_type_of_trans[0].value;
            }
            if (invoiceLookup.currency[0]!=undefined && invoiceLookup.currency[0]!=null && invoiceLookup.currency[0]!='') {
                var currency = invoiceLookup.currency[0].value;
            }
            if (invoiceLookup.location[0]!=undefined && invoiceLookup.location[0]!=null && invoiceLookup.location[0]!='') {
                var location = invoiceLookup.location[0].value;
            }
            if (invoiceLookup.department[0]!=undefined && invoiceLookup.department[0]!=null && invoiceLookup.department[0]!='') {
                var department = invoiceLookup.department[0].value;
            }

            if (invoiceLookup.class[0]!=undefined && invoiceLookup.class[0]!=null && invoiceLookup.class[0]!='') {
                var division = invoiceLookup.class[0].value;
            }
            
            var jvRecordObj=record.create({
                type:'journalentry',
                isDynamic:true
            });
            jvRecordObj.setValue('customform',123); //101
            jvRecordObj.setValue('custbody_ub_payout_or_date',orDate);
            jvRecordObj.setValue('trandate',orDate);
            jvRecordObj.setValue('custbody_ub_cpy_check_created_frm',createdFrom);
            jvRecordObj.setValue('custbody_ub_payout_or_num',orNumber);
            if (subsidiary!=undefined && subsidiary!=null && subsidiary!=''){
                jvRecordObj.setValue('subsidiary',subsidiary);
            }

            if (division!=undefined && division!=null && division!=''){
                jvRecordObj.setValue('class',division);
            }

            if (currency!=undefined && currency!=null && currency!=''){
                jvRecordObj.setValue('currency',currency);
            }

            if (typeOfTrans!=undefined && typeOfTrans!=null && typeOfTrans!=''){
                jvRecordObj.setValue('cseg_type_of_trans',typeOfTrans);
            }

            if (location!=undefined && location!=null && location!=''){
                jvRecordObj.setValue('location',location);
            }

            if (department!=undefined && department!=null && department!=''){
                jvRecordObj.setValue('department',department);
            }
            jvRecordObj.setValue('approvalstatus',approvedStatus);

            jvRecordObj.selectNewLine({ 
                sublistId: 'line'      
            });
            jvRecordObj.setCurrentSublistValue({
             sublistId: 'line',
             fieldId: 'account',
             value: accountToSet //214
            });
            jvRecordObj.setCurrentSublistValue({
             sublistId: 'line',
             fieldId: 'debit',
             value: orAmount
            });
            jvRecordObj.setCurrentSublistValue({
             sublistId: 'line',
             fieldId: 'taxcode',
             value: actualTaxCode //taxCode
            });
            jvRecordObj.commitLine({ 
                sublistId: 'line'
            });
            jvRecordObj.selectNewLine({ 
                sublistId: 'line'      
            });
            jvRecordObj.setCurrentSublistValue({
             sublistId: 'line',
             fieldId: 'account',
             value: accountToSet //214
            });
            jvRecordObj.setCurrentSublistValue({
             sublistId: 'line',
             fieldId: 'credit',
             value: orAmount
            });
            jvRecordObj.setCurrentSublistValue({
             sublistId: 'line',
             fieldId: 'taxcode',
             value: taxCode //actualTaxCode
            });
            jvRecordObj.commitLine({ 
                sublistId: 'line'
            });

            var createdJournalId=jvRecordObj.save()
            log.debug('CREATED JournalId',createdJournalId)


            record.submitFields({
                type: 'customrecord_com_checks_or_details',
                id: currentRecordObj.id,
                values: {
                    'custrecord_jv_created': 'T',
                    'custrecord_created_jv':createdJournalId
                }
            });
            /*currentRecordObj.setValue('custrecord_jv_created',true);
            if (createdJournalId!=undefined && createdJournalId!='' && createdJournalId!=null) {
            }*/
        }

        function getActualTaxCode(taxcode){
            var filterForTax=new Array();
            var columnForTax=new Array();
            //log.emergency('Inside Function', 'getJobs');

            filterForTax.push(search.createFilter({
                name: 'custrecord_deferred_tax_codes',
                operator: search.Operator.IS,
                values: taxcode
            }));

            columnForTax.push(search.createColumn('custrecord_actual_tax_codes')); 

            var searchedMatchedTaxCode = search.create({
                'type': 'customrecord_match_tax_code',
                'filters': filterForTax,
                'columns': columnForTax
            }).run().getRange(0, 1000);

            if (searchedMatchedTaxCode.length>0) {
                var code=searchedMatchedTaxCode[0].getValue('custrecord_actual_tax_codes')
            }

            return code;
        }

        return {
            afterSubmit:afterSubmit
        };
    }
);