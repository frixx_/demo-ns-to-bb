/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Siddhi Kadam.
 ** @Dated      :  03 March, 2020
 **@Version     :  2.x
 **@Description :  To create custom print button on Liquidation Record - 'customrecord_supplier_inv_broker	'.
 ***************************************************************************************/
define(['N/ui/serverWidget', 'N/log', 'N/error', 'N/record', 'N/url'],

    function(serverWidget, log, error, record, url) {

        function beforeLoad(context) {

            var recordId = context.newRecord.id;

            var currentRecObj = context.newRecord;




            if (context.type === context.UserEventType.DELETE)
                return;


            try {

                if (context.type === context.UserEventType.VIEW) {


                    var outputUrl = url.resolveScript({
                        scriptId: 'customscript_softype_st_liqui_print',
                        deploymentId: 'customdeploy_softype_st_liqui_print',
                        returnExternalUrl: false
                    });

                    outputUrl += '&recordId=' + recordId;
                    var stringScript = "window.open('" + outputUrl + "','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";


                    var printButton = context.form.addButton({
                        id: 'custpage_print',
                        label: 'Print',
                        functionName: stringScript
                    });

                }

            } catch (e) {

                log.error('Error Details', e.toString());
                throw e;

            }

        }



        return {
            beforeLoad: beforeLoad
        };
    }
);