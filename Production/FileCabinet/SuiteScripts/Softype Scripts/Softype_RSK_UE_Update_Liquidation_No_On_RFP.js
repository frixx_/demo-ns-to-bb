/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Siddhi Kadam
 **@Dated       :  10th February, 2020
 **@Version     :  2.x
 **@Description :  This is script is deployed on custom record 'Liquidation of Broker/Employee' to get the Liquidation Id and set it on the RFP custom record 
				   'Request for Payment'
 ***************************************************************************************/
define(['N/log', 'N/record'],

    function(log, record) {

        function afterSubmit(scriptContext) {



            var currentRecordObj = scriptContext.newRecord;
            var id = scriptContext.newRecord.id;

            var RFPNumber = [];

            log.debug('scriptContext.type', scriptContext.type);

            if (scriptContext.type == 'xedit') {

                return;
            }



            RFPNumber = currentRecordObj.getValue({
                fieldId: 'custrecord_rfp_number'


            });


            if (RFPNumber) {

                for (var i = 0; i < RFPNumber.length; i++) {

                    var recordId = record.submitFields({
                        type: 'customrecord_rfp',
                        id: RFPNumber[i],
                        values: {
                            'custrecord_liquidation_trans': id
                        }

                    });
					
                }
				

            }



        }



        return {

            afterSubmit: afterSubmit
        };

    });