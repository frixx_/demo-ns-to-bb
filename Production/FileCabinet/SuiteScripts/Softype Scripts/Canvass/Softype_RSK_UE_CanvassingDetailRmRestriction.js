/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Restriction on deletion
***************************************************************************************/

define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, record, runtime, log) {
		
		function beforeSubmit(context) {
		
				log.debug('context', context);
			var newRecordDetails = context.newRecord;
			try{				
				if(context.type === "delete"){
                  	throw "You can not delete Canvassing Details record";
					return;
				}
			}
			catch(err){
				log.debug('Error', err);
				return;
			}
		}
				
		return {
			beforeSubmit: beforeSubmit
		};
	});	