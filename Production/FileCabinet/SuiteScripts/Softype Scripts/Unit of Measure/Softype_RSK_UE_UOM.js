/**
*@NApiVersion 2.x
*@NScriptType UserEventScript
**/

/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : Call RSK API for UOM creation in RSK
***************************************************************************************/


define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, record, runtime, log) {
		
		function afterSubmit(context) {
			
			log.debug('runtime',runtime);
			log.debug('runtime.executionContext',runtime.executionContext);
			log.debug('context',context);
			
			var newRecordDetails = context.newRecord;
			log.debug('newRecordDetails', newRecordDetails);
			
		
			var scriptObj = runtime.getCurrentScript();
			var createAPI = scriptObj.getParameter({name: 'custscript_uom_create'});
			var editAPI = scriptObj.getParameter({name: 'custscript_uom_edit'});
			
			
			/**********************************************************
			 *************************API CALL************************
			**********************************************************/
													
			var token = null;	// Token if Any
			var headers = {
					"Content-Type": "application/json"
			};	//Headers if Any
			
			var response;
				
			var isInActive = newRecordDetails.getValue({fieldId: 'isinactive'});	
			if(context.type == 'create'){
				
				var url = createAPI;	//RSK API URL
				var jsonBody = {
					"NsId": newRecordDetails.id,
					"Name": newRecordDetails.getValue({
													fieldId: 'name'
												}),
					"UnitTypeId": 1,
					"IsActive": !(isInActive)
				};	
							
				log.debug("jsonBody - Create", jsonBody);			
							
				// If the API is https compliant
				response = http.request({
					method: https.Method.POST,
					url: url,
					body: JSON.stringify(jsonBody),
					headers: headers
				});
				log.debug("response", response);		
							
			}/* else if(context.type == 'edit'){
				
				var url = editAPI;	//RSK API URL
				var jsonBody = {
							"Id" : newRecordDetails.getValue({
															fieldId: 'custrecord_location_rskidfield'
														}) || "",
							"Name": newRecordDetails.getValue({
															fieldId: 'name'
														}),
							"LocationTypeId": 1,
							"IsActive": !(isInActive)
							};	
							
				log.debug("jsonBody - Edit", jsonBody);
				
				// If the API is https compliant
				response = http.request({
					method: https.Method.PUT,
					url: url,
					body: JSON.stringify(jsonBody),
					headers: headers
				});
			} */
			
		}
				
		function SetFlag(newRecordDetails, dataCode, message){
			newRecordDetails.setValue({
							fieldId: "custrecord_location_flagfield",	//Flag Checkbox Field
							value: true,				
							ignoreFieldChange: true
						});
			newRecordDetails.setValue({
							fieldId: "custrecord_location_remark",	//Flag Remark Field
							value: dataCode + " : " + message,				
							ignoreFieldChange: true
						});
		}
		
		return {
			afterSubmit: afterSubmit
		};
	});