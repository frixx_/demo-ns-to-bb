/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author  : Farhan Shaikh
 **@Dated   : 26/09/2019   DD/MM/YYYY
 **@Version :         1.0
 **@Description :     Workflow Action script to Void Check Upon Rejection 
 ***************************************************************************************/
define(['N/record','N/search',"N/log","N/url","N/redirect","N/runtime"],    function(record,search,log,url,redirect,runtime){
    
    function voidCheck(context){

        try{
            var currentRecordId = context.newRecord.id;
            log.audit('currentRecordId',currentRecordId);
            var checkRecObj = context.newRecord;
            
            checkRecObj.setValue({
                fieldId: 'usertotal',
                value: 0
            });
    
            checkRecObj.setValue({
                fieldId: 'total',
                value: 0
            });
        
            checkRecObj.setValue({
                fieldId: 'voided',
                value: true
            });
            checkRecObj.setValue({
                fieldId: 'memo',
                value: 'VOID'
            });
    
            var lineCountArray=[];
            lineCountArray.push(checkRecObj.getLineCount('item'))
            lineCountArray.push(checkRecObj.getLineCount('expense')) 
            log.emergency('lineCountArray',lineCountArray)
            for(var lineArr=0;lineArr<lineCountArray.length;lineArr++){
                var sublist='';
                if(lineArr==0){
                    sublist='item'
                }else{
                    sublist='expense'
                }
                for(var i = 0; i < lineCountArray[lineArr]; i++){
                    checkRecObj.selectLine({sublistId: sublist,line: i});
                    checkRecObj.setCurrentSublistValue({sublistId:sublist,fieldId:'amount',line:i,value:0});
                    checkRecObj.setCurrentSublistValue({sublistId:sublist,fieldId:'grossamt',line:i,value:0});
                    checkRecObj.commitLine({sublistId:sublist});
                }
            }
        }catch(e){
            log.emergency('error',JSON.stringify(e))
        }
    }

    return {
        onAction: voidCheck
    };
});