/**
*@NApiVersion 2.x
*@NScriptType UserEventScript
**/

/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : Call RSK API for Item Master
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {
					
		var STATUS_APPROVED = 2;
		
		var REGULAR_BILLING = "100";
		
		var PRICE_VARIANCE_JV = 3;
		
		var PRICE_VARIANCE_ACCOUNT = 1634;
		var GRNI_ACCOUNT = 691;
		
		function beforeSubmit(context) {
			var newRecordDetails = context.newRecord;
			log.debug('newRecordDetails', newRecordDetails);
			log.debug('newRecordDetails.id', newRecordDetails.id);
					
			var scriptObj = runtime.getCurrentScript();
			REGULAR_BILLING = scriptObj.getParameter({name: 'custscript_venbill_regular_billing_form'});
			PRICE_VARIANCE_JV = scriptObj.getParameter({name: 'custscript_venbill_price_variance_jv_typ'});
			PRICE_VARIANCE_ACCOUNT = scriptObj.getParameter({name: 'custscript_venbill_price_variance_jv_act'});
			GRNI_ACCOUNT = scriptObj.getParameter({name: 'custscript_venbill_grni_jv_act'});

			var approvalStatus = newRecordDetails.getValue({
									 fieldId: 'approvalstatus'
								 });

			var formID = newRecordDetails.getValue({
									 fieldId: 'customform'
								 });
								 
			var jvCreated = newRecordDetails.getValue({
								 fieldId: 'custbody_jv_created'
							 });
							 
			var vatType = newRecordDetails.getValue({
								 fieldId: 'custbody_vattype'
							 });
			
			log.debug('approvalStatus',approvalStatus);
			
			if(approvalStatus !== "2"){
				return;
			}
			log.debug('formID',formID !== REGULAR_BILLING);
			
			if(formID !== REGULAR_BILLING){
				return;
			}
			
			if(jvCreated){
				return;
			}
			
			
			var lineCount = newRecordDetails.getLineCount({
				sublistId: 'item'
			});
			
			log.debug('lineCount',lineCount);
			
			var totalVariance = 0;
			
			for(var i = 0; i < lineCount; i++){
				var variance = newRecordDetails.getSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_variance',
					line: i
				});
				
				log.debug('variance '+i,variance);
				
				var quantity = newRecordDetails.getSublistValue({
					sublistId: 'item',
					fieldId: 'quantity',
					line: i
				});		

				log.debug('quantity '+i,quantity);
				
				totalVariance += quantity * variance;
				
				log.debug('totalVariance '+i,totalVariance);
			}					 
			
			
			log.debug('totalVariance',totalVariance);
			
			if(totalVariance !== 0){
				var recordid = CreateJV(newRecordDetails, totalVariance, vatType);
				
				log.debug('recordid',recordid);
				
				newRecordDetails.setValue({
					fieldId: "custbody_pricvariance_jv",
					value: true,				
					ignoreFieldChange: true
				});			
				
				newRecordDetails.setValue({
					fieldId: "custbody_pricvariance_jvlink",
					value: recordid,				
					ignoreFieldChange: true
				});			
				
				
			}
			else{
				return;
			}
			
		}
		
		function CreateJV(newRecordDetails, varianceAmount, vatType){

			
			var newJVRecord = record.create({
				type: "journalentry",
				isDynamic: false
			});	
			
			log.debug('newRecordDetails.subsidiary', newRecordDetails.getValue({
														 fieldId: 'subsidiary'
													 }));
			
			log.debug('newRecordDetails.id', newRecordDetails.id);
			
			log.debug('ID', newRecordDetails.getValue({
														 fieldId: 'id'
													 }));
			
			newJVRecord.setValue({
				fieldId: "subsidiary",
				value: 	newRecordDetails.getValue({
							fieldId: 'subsidiary'
						})
			});
			
			newJVRecord.setValue({
				fieldId: "approvalstatus",
				value: STATUS_APPROVED
			});	
			
			newJVRecord.setValue({
				fieldId: "custbody_vattype",
				value: vatType
			});	
			
			newJVRecord.setValue({
				fieldId: "custbody_createdfrom",
				value: newRecordDetails.id
			});
			
			newJVRecord.setValue({
				fieldId: "custbody_journaltypes",
				value: PRICE_VARIANCE_JV
			});
			
			
			log.debug('Debit Account', varianceAmount > 0 ? PRICE_VARIANCE_ACCOUNT : GRNI_ACCOUNT);
			log.debug('Credit Account', varianceAmount > 0 ? GRNI_ACCOUNT : PRICE_VARIANCE_ACCOUNT);
			
			//Debit
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'account',
				line: 0,
				value: varianceAmount > 0 ? PRICE_VARIANCE_ACCOUNT : GRNI_ACCOUNT
			});
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'debit',
				line: 0,
				value: Math.abs(varianceAmount)
			});
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'credit',
				line: 0,
				value: 0
			});
			
			//Credit
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'account',
				line: 1,
				value: varianceAmount > 0 ? GRNI_ACCOUNT : PRICE_VARIANCE_ACCOUNT
			});					
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'debit',
				line: 1,
				value: 0
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'credit',
				line: 1,
				value: Math.abs(varianceAmount)
			});
			
			var recordid = newJVRecord.save();
			log.debug("recordid JV",recordid);
			
			return recordid;
		}
				
		return {
			beforeSubmit: beforeSubmit
		};
	});