/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  
 **                       
 **@Author      : Farhan Shaikh
 **@Dated       : 04/10/2019   DD/MM/YYYY
 **@Version     : 
 **@Description : UserEvent Script on Credit Memo to set the Credit Memo Number created on the Policy Billing Information Records.  

 **************************************************************************************************************************************/
define(['N/record', 'N/runtime', 'N/search', 'N/format','N/url','N/https'],


    function(record, runtime, search, format,url,https) {
        //For Setting Due Date Of First Invoice On TranDate Field
        var companyCheckForm = runtime.getCurrentScript().getParameter('custscript_company_check_form');
        var ubPayoutForm = runtime.getCurrentScript().getParameter('custscript_ub_payout_bill_form');
        function beforeLoad(scriptContext) {
            var currentRecordObj = scriptContext.newRecord;
            log.emergency('currentRecordObj id',currentRecordObj)
            var linecount = currentRecordObj.getLineCount({sublistId:'apply'});
            if (currentRecordObj.getValue('customform')) {
                //log.debug('Before Submit','FORM IS 115')
                if(linecount > 0){
                    for(var i = 0; i < linecount; i++){
                        //var wTaxCodeSearchObj=search.load('customsearch62')
                        var checked = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'apply',line:i});
                        log.emergency('checked',checked)
                        if(checked){
                            if (i==0) {
                                var dueDate = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'duedate',line:i});
                                log.debug('dueDate',dueDate)
                                currentRecordObj.setValue('trandate',dueDate)
                                break;
                            }
                        }
                    }
                }
            }
        }

        function beforeSubmit(scriptContext){
            var currentRecordObj = scriptContext.newRecord;
            approvalStatus=currentRecordObj.getValue('approvalstatus')

            var linecount = currentRecordObj.getLineCount({sublistId:'apply'});
            if (currentRecordObj.getValue('customform')==companyCheckForm) {
                log.debug('Before Submit','FORM IS 115')
                if(linecount > 0){
                    for(var i = 0; i < linecount; i++){
                        //var wTaxCodeSearchObj=search.load('customsearch62')
                        var checked = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'apply',line:i});
                        if(checked){
                            if (i==0) {
                                var dueDate = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'duedate',line:i});
                                log.debug('dueDate',dueDate)
                                //currentRecordObj.setValue('trandate',dueDate)
                                break;
                            }
                        }
                    }
                }
            }
        }


        /**

         * Function definition to be triggered after record is submitted.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */

        function afterSubmit(scriptContext) {
            saveRecord(scriptContext)
        }

        function saveRecord(context){
            log.emergency('executionContext',runtime.executionContext)  
            var currentRecordObj = context.newRecord; //.get();
            log.emergency('currentRecordObj',currentRecordObj)
            var approvalStatus=currentRecordObj.getValue('approvalstatus')
            if (approvalStatus=='' || approvalStatus==undefined || approvalStatus==null) {
                approvalStatus=2
            }
            var totalSelected = [];
            var allBillsSelected=[];
            var linecount = currentRecordObj.getLineCount({sublistId:'apply'});
            log.emergency('linecount',linecount)
            if (currentRecordObj.getValue('customform')==ubPayoutForm) {
                if(linecount > 0){
                    for(var i = 0; i < linecount; i++){
                        //var wTaxCodeSearchObj=search.load('customsearch62')
                        var checked = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'apply',line:i});
                        //var getId = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'CUSTBODY_WTAX_CODE_NAME',line:i});
                        var bills=currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'internalid',line:i});
                        //alert(bills)
                        if(checked){
                            allBillsSelected.push(bills);
                        }
                    }
                    log.emergency('allBillsSelected',allBillsSelected)
                    var orField=currentRecordObj.getValue('custbody_ub_payout_or_num')
                    var orDateField=currentRecordObj.getValue('custbody_ub_payout_or_date')
                    log.emergency('orField',orField)
                    log.emergency('orDateField',orDateField)
    
                    //var refField=currentRecordObj.getValue('custbody_ub_payout_ref')
                    if (allBillsSelected.length >=  1 && orField!='') {
                         
                        for(var i=0;i<allBillsSelected.length;i++){
                            record.submitFields({
                                type: 'vendorbill',
                                id: allBillsSelected[i],
                                values: {
                                    'custbody_ub_payout_or_num': orField,
                                    'custbody_ub_payout_or_date': orDateField
                                }
                            });
                            
                            /*log.emergency('allBillsSelected[i]',allBillsSelected[i])
                            var vendorbillRecord=record.load({
                                type:'vendorbill',
                                id:allBillsSelected[i]
                            })
    
                            log.emergency('orDateField',orDateField)
    
                            vendorbillRecord.setValue('custbody_ub_payout_or_num',orField);
                            vendorbillRecord.setValue('custbody_ub_payout_or_date',orDateField);
                            vendorbillRecord.save()*/
    
                            var outputUrl = url.resolveScript({
                                scriptId: 'customscript_st_update_wtax_field_inv',
                                deploymentId: 'customdeploy_st_update_wtax_field_inv',
                                returnExternalUrl: true
    
                            });
    
                            outputUrl += '&action=getBillCreditId';
                            outputUrl += '&customRecId=' + allBillsSelected[i];
    
                            log.emergency('outputUrl',outputUrl)
                            var response = https.request({
                                method:https.Method.GET,
                                url:outputUrl
                            });
    
                            log.emergency('response',response)
                        }
                        
                    }
                }
            }else if (currentRecordObj.getValue('customform')==companyCheckForm) {
                log.debug('FORM IS 115')
                if(linecount > 0){
                    for(var i = 0; i < linecount; i++){
                        //var wTaxCodeSearchObj=search.load('customsearch62')
                        var checked = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'apply',line:i});
                        //var getId = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'CUSTBODY_WTAX_CODE_NAME',line:i});
                        var bills=currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'internalid',line:i});
                        //var bills=currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'duedate',line:i});
                        //alert(bills)
                        if(checked){
                            if (i==0) {
                                var dueDate = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'duedate',line:i});
                                log.debug('dueDate',dueDate)
                                currentRecordObj.setValue('trandate',dueDate)
                            }
                            var total=currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'total',line:i});
                            var amountDue=currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'due',line:i});
                            var paymentAmount=currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'amount',line:i});
                            var json={
                                'bills':bills,
                                'amount':total,
                                'amountDue':amountDue,
                                'paymentAmount':paymentAmount
                            }
                            allBillsSelected.push(json);
                        }
                    }
                    log.emergency('allBillsSelected',allBillsSelected)
                    var orField=currentRecordObj.getValue('custbody_ub_payout_or_num')
                    var orDateField=currentRecordObj.getValue('custbody_ub_payout_or_date')
                    var reversalIsChecked=currentRecordObj.getValue('custbody_cpy_check_reversal')
                    log.emergency('orField',orField)
                    log.emergency('orDateField',orDateField)
    
                    //var refField=currentRecordObj.getValue('custbody_ub_payout_ref')
                    log.audit('allBillsSelected.length',allBillsSelected.length)
                    log.audit('approvalStatus',approvalStatus)
                    log.audit('reversalIsChecked',reversalIsChecked)
                    if (allBillsSelected.length >=  1 && orField!='' && orDateField!='' && approvalStatus==2 && !reversalIsChecked) {
                        log.audit('inside main if')
                        for(var i=0;i<allBillsSelected.length;i++){
                            var invoiceLookup=search.lookupFields({
                                type:'vendorbill',
                                id:allBillsSelected[i].bills,
                                columns:['custbody_ar_or']
                            })
                            log.audit('invoiceLookup',invoiceLookup)
                            /*if (allBillsSelected[i].paymentAmount<=allBillsSelected[i].amountDue && invoiceLookup.custbody_ar_or) {
                                var orRecordCreated=createOrRecord(allBillsSelected[i].bills,orField,orDateField,allBillsSelected[i].paymentAmount)
                                callSuitelet(orRecordCreated) //&& invoiceLookup.custbody_ar_or==false
                            }else*/
                            /*if(allBillsSelected[i].paymentAmount==allBillsSelected[i].amountDue){ 
                                log.audit('invoiceLookup.custbody_ar_or','FALSE')
                                var itemAmount=0;
                                var expenseAmount=0;
                                var itemTaxAmount=0
                                var expenseTaxAmount=0;
                                var taxCode=0;

                                var supplierInvRecObj=record.load({
                                    type:'vendorbill',
                                    id:allBillsSelected[i].bills,
                                    isDynamic:true
                                });
                                var itemLineCount=supplierInvRecObj.getLineCount({sublistId:'item'})
                                var expenseLineCount=supplierInvRecObj.getLineCount({sublistId:'expense'})
                                if (itemLineCount>0) {
                                    var itemJson=calculateTaxAmount(supplierInvRecObj,itemLineCount,'item')
                                    itemAmount=itemJson.totalAmount;
                                    itemTaxAmount=itemJson.totalTaxAmount;
                                    taxCode=itemJson.taxCode;
                                }

                                if (expenseLineCount>0) {
                                    var expenseJson=calculateTaxAmount(supplierInvRecObj,expenseLineCount,'expense')
                                    expenseAmount=expenseJson.totalAmount;
                                    expenseTaxAmount=expenseJson.totalTaxAmount;
                                    if (taxCode==0) {
                                        taxCode=expenseJson.taxCode;
                                    }
                                }

                                var totalAmountForORrecord=Number(itemAmount)+Number(expenseAmount)
                                var totalTaxAmountForOrRecord=Number(itemTaxAmount)+Number(expenseTaxAmount)
                                if (totalAmountForORrecord!=0 && totalTaxAmountForOrRecord!=0) {
                                    var orRecordCreated=createOrRecord(allBillsSelected[i].bills,orField,orDateField,totalAmountForORrecord,totalTaxAmountForOrRecord,taxCode)
                                    callSuitelet(orRecordCreated) 
                                }
                                
                            }else */
                            if(allBillsSelected[i].paymentAmount<=allBillsSelected[i].amountDue){
                                var paymentAmount=allBillsSelected[i].paymentAmount;
                                var taxCode=0;
                                var taxRate=0;
                                var supplierInvRecObj=record.load({
                                    type:'vendorbill',
                                    id:allBillsSelected[i].bills,
                                    isDynamic:true
                                });
                                var itemLineCount=supplierInvRecObj.getLineCount({sublistId:'item'})
                                var expenseLineCount=supplierInvRecObj.getLineCount({sublistId:'expense'})
                                if (itemLineCount>0) {
                                    var getTaxDetails=getTaxCode(supplierInvRecObj,itemLineCount,'item')
                                    taxCode=getTaxDetails.taxCode
                                    taxRate=getTaxDetails.taxRate
                                }
                                log.audit('getTaxDetails','getTaxDetails : '+getTaxDetails)
                                if (expenseLineCount>0 && taxCode==0) {
                                    var getTaxDetails=getTaxCode(supplierInvRecObj,expenseLineCount,'expense')
                                    taxCode=getTaxDetails.taxCode
                                    taxRate=getTaxDetails.taxRate
                                }
                                log.audit('getTaxDetails After','getTaxDetails : '+getTaxDetails)
                                log.audit('Partial Payment','taxCode : '+taxCode)
                                if (taxCode!=0) {
                                    var taxAmountCalculated=paymentAmount*parseFloat(taxRate)/100;
                                    var orRecordCreated=createOrRecord(allBillsSelected[i].bills,orField,orDateField,Number(paymentAmount)-Number(taxAmountCalculated),taxAmountCalculated,taxCode)
                                    callSuitelet(orRecordCreated)
                                }
                            }else{
                                log.audit('invoice Already Paid In full')
                            }

                            if (i==allBillsSelected.length-1) {
                                record.submitFields({
                                    type: 'vendorpayment',
                                    id: currentRecordObj.id,
                                    values: {
                                        'custbody_cpy_check_reversal': true
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }

        function callSuitelet(orRecordCreated){
            var outputUrl = url.resolveScript({
                scriptId: 'customscript_st_update_wtax_field_inv',
                deploymentId: 'customdeploy_st_update_wtax_field_inv',
                returnExternalUrl: true
            });
            outputUrl += '&action=getCompanyCheckOrDetail';
            outputUrl += '&customRecId=' + orRecordCreated;
            log.emergency('outputUrl',outputUrl)
            var response = https.request({
                method:https.Method.GET,
                url:outputUrl
            });
            log.emergency('response',response)
        }

        function createOrRecord(invoiceId,orNumber,orDate,orAmount,orTaxAmount,taxCode){
            var companyCheckOrRecordObj=record.create({
                type:'customrecord_com_checks_or_details',
                isDynamic:true
            })
            companyCheckOrRecordObj.setValue('custrecord_sup_invoice_ref',invoiceId)
            companyCheckOrRecordObj.setValue('custrecord_or_num',orNumber)
            companyCheckOrRecordObj.setValue('custrecord_or_date',orDate)
            companyCheckOrRecordObj.setValue('custrecord_or_amt',orAmount)
            companyCheckOrRecordObj.setValue('custrecord__com_checks_tax_code',taxCode)
            companyCheckOrRecordObj.setValue('custrecord_com_checks_tax_amount',orTaxAmount)
            var orRecordCreated=companyCheckOrRecordObj.save();
            log.debug('orRecordCreated',orRecordCreated)

            return orRecordCreated;
        }

        function calculateTaxAmount(supplierInvRecObj,lineCount,sublist){
            var totalAmount=0;
            var totalTaxAmount=0;
            var taxCode='';
            for(var i=0;i<lineCount;i++){
                var deferred=supplierInvRecObj.getSublistValue({sublistId:sublist,fieldId:'custcol_deferred_line',line:i})
                if (deferred) {
                    if (i==0) {
                        taxCode=supplierInvRecObj.getSublistValue({sublistId:sublist,fieldId:'taxcode',line:i})
                    }
                    var grossamt=supplierInvRecObj.getSublistValue({sublistId:sublist,fieldId:'grossamt',line:i})
                    var taxAmount=supplierInvRecObj.getSublistValue({sublistId:sublist,fieldId:'tax1amt',line:i})
                    totalAmount+=Number(grossamt)
                    totalTaxAmount+=Number(taxAmount)
                }
            }
            log.audit('totalAmount',totalAmount)
            log.audit('totalTaxAmount',totalTaxAmount)

            totalAmount=Number(totalAmount)-Number(totalTaxAmount)

            var jsonToReturn={
                'totalAmount':totalAmount,
                'totalTaxAmount':totalTaxAmount,
                'taxCode':taxCode
            }
            return jsonToReturn;
        }

        function getTaxCode(supplierInvRecObj,lineCount,sublist){
            var totalAmount=0;
            var totalTaxAmount=0;
            var taxCode='';
            var taxRate='';
            for(var i=0;i<lineCount;i++){
                var deferred=supplierInvRecObj.getSublistValue({sublistId:sublist,fieldId:'custcol_deferred_line',line:i})
                if (deferred) {
                    if (i==0) {
                        taxCode=supplierInvRecObj.getSublistValue({sublistId:sublist,fieldId:'taxcode',line:i})
                        taxRate=supplierInvRecObj.getSublistValue({sublistId:sublist,fieldId:'taxrate1',line:i})
                        //taxRate=taxRate.replace('%','')
                    }
                }
            }
            if (taxCode!='') {
                var jsonToReturn={
                    'taxCode':taxCode,
                    'taxRate':taxRate
                }
                return jsonToReturn;
            }else{
                return 0;
            }
        }

    return {
        beforeLoad:beforeLoad,
        afterSubmit: afterSubmit,
        beforeSubmit:beforeSubmit
    };
});