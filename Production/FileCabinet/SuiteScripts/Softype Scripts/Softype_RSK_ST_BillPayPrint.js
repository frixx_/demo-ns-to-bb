/***************************************************************************************  
** Copyright (c) 1998-2018 Softype, Inc.
** P3 Zenith Central | Luzon Avenue | Cebu Business Park, Cebu City | Philippines
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
**                      
**@Author      :  Renato Cada Jr
**@Dated       :  25 February 2020	
**@Description :  This is script is for bill payment official print
**
**@NApiVersion 2.1
**@NScriptType Suitelet
**@NModuleScope SameAccount
*****************************************************************************************/

define(['N/record', 'N/xml', 'N/render', 'N/http','N/format','N/search', 'N/runtime'],
		
function(record, xml, render, http,format, search, runtime) {
	var searchResult = {};
	
    function onRequest(context) {	
    	
    		
    		const currentUserId = runtime.getCurrentUser().id;
    		const loadEmployee = record.load({
    			type: 'employee',
    			id: currentUserId
    		});
    		const currentUser = loadEmployee.getText({fieldId: 'entityid'});
    		
    		const recordId = context.request.parameters.recordid;
    		log.error('currentUser', currentUser);
    		log.error('recordId', recordId);
			const loadRec = record.load
			({
				type: 'vendorpayment',
				id: recordId
				//isDynamic: true
			});
			
    	const transactionnumber = loadRec.getText({ fieldId: 'transactionnumber'});
    	const entity = loadRec.getText({ fieldId: 'entity'});
    	const entityId = loadRec.getValue({ fieldId: 'entity'});
    	const custbody_trans_date = loadRec.getText({ fieldId: 'custbody_trans_date'});
    	const address = loadRec.getText({ fieldId: 'address'});
    	const custbody_prepared_by = loadRec.getText({ fieldId: 'custbody_prepared_by'});
    	const custbody_date_time_create = loadRec.getText({ fieldId: 'custbody_date_time_create'});
    	const total = loadRec.getText({ fieldId: 'total'});
    	const custbody_cashsale_totalword = loadRec.getText({ fieldId: 'custbody_cashsale_totalword'});
    	const account = loadRec.getText({ fieldId: 'account'});
    	const accountid = loadRec.getValue({ fieldId: 'account'});
    	const tranid = loadRec.getText({ fieldId: 'tranid'});
    	const custbody_nameon_checkprint = loadRec.getText({ fieldId: 'custbody_nameon_checkprint'});
    	const custbody_alternate_payee = loadRec.getText({ fieldId: 'custbody_alternate_payee'});
    	
    	const loadSupplier = record.load({
    		type: 'vendor',
    		  id: entityId
    	});
    	const legalname = loadSupplier.getText({fieldId: 'legalname'});
    	
    	const loadAccount = record.load({
    		type: 'account',
    		  id: accountid
    	});
    	const accountLegalName = loadAccount.getText({fieldId: 'legalname'});
    	var htmlvar = `<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">
    	<pdf>
    	<head>
    	   		<macrolist>
    			<macro id="nlheader">`;
	       // CV # 
	       htmlvar+=`<table width="100%" padding-right="1in" padding-left="0.9in" padding-top="3px">
		        <tr>
					<td border="0" align="right" padding-top="50px">No.&nbsp;${transactionnumber}</td>
				</tr>
			</table>`;
	       
		   //Paid to, date, address 
	       let namePadding = "20px";
	       let datePadding = "20px";
	       if(entity.length>39){
	    	   //reduce padding
	    	   namePadding = "3px";
	    	   datePadding = "14px";
	       }
	       let paidTo='';
	       try{
	       //if alternate payee is blank get value from supplier master legal name
	        paidTo = custbody_alternate_payee===""? legalname: custbody_alternate_payee;
	       }catch(e){
	    	   log.error('error message', e.message);
	       }
		   htmlvar+=`<table border="0" width="100%" padding-right="1.5in" padding-left="0.6in">
	   		 	<tr>
					<td border="0" align="left" width="200px" padding-top="${namePadding}">&nbsp;${paidTo}</td>
					<td border="0" align="right" width="20px" padding-top="${datePadding}">&nbsp;${custbody_trans_date}</td>
				</tr>
				<tr>
					<td border="0" colspan="2" align="left" padding-top="2px" style="text-align: right;">&nbsp;${address}</td>
				</tr>
			</table>`;
	       htmlvar+=`&nbsp;
    			</macro>`;//end of header 
	       
    	   htmlvar+=`<macro id="nlfooter">`;
    	   
    	   //Check #
    	   htmlvar+=`<table width="100%" padding-top="-50px" padding-right="1.3in">
		   		 <tr>
		   		 	<td border="0" width="390px" align="left" style="font-size:10px" padding-left="10px"><b>Printed By:${currentUser}&nbsp;${custbody_date_time_create}</b></td>
		    		<td border="0" width="150px" align="right"><b>${total}</b></td>
		    	</tr>
		  		</table>`;
    	   
    	   //total words 
    	   htmlvar+=`<table width="100%" padding-top="-26px" padding-left="2.0in" padding-right="1in">
		    	<tr>
		    		<td align="center" border="0" width="160px" style="font-size:10px"><b>${custbody_cashsale_totalword}</b></td>
		    		</tr>
		  		</table>`;
    	   
    	   //total
    	   htmlvar+=`<table width="100%" padding-top="-18px" padding-left="3.5in" padding-right="1.8in">
		     	<tr>
		    		<td align="center" border="0" width="80px"><b>${total}</b></td>
		    		</tr>
		  		</table>`;
    	   
    	   htmlvar+=`&nbsp;
  			</macro>
    	  </macrolist>`;
    	   
    	   //style
    	   htmlvar+=`<style type="text/css">*{
    		    font-family: NotoSans, sans-serif;
    	   }
			table {
				font-size: 10pt;
				table-layout: fixed;
			}
            th {
                font-weight: bold;
                font-size: 9pt;
                vertical-align: middle;
                padding: 5px 6px 3px;
                background-color: #e3e3e3;
                color: #333333;
            }
            td {
                padding: 4px 6px;
              	font-size: 11px
            }
			td p { align:left }
            b {
                font-weight: bold;
                color: #333333;
            }
            table.header td {
                padding: 0;
                font-size: 11pt;
            }
            table.footer td {
                padding: 0;
                font-size: 9pt;
            }
            table.itemtable th {
                padding-bottom: 10px;
                padding-top: 10px;
            }
            table.body td {
                padding-top: 2px;
            }
            table.total {
                page-break-inside: avoid;
            }
            tr.totalrow {
                background-color: #e3e3e3;
                line-height: 200%;
            }
            td.totalboxtop {
                font-size: 13pt;
                background-color: #e3e3e3;
            }
            td.addressheader {
                font-size: 9pt;
                padding-top: 6px;
                padding-bottom: 2px;
            }
            td.address {
                padding-top: 0;
            }
            td.totalboxmid {
                font-size: 28pt;
                padding-top: 20px;
                background-color: #e3e3e3;
            }
            span.title {
                font-size: 28pt;
            }
            span.number {
                font-size: 17pt;
            }
            hr {
                width: 100%;
                color: #d3d3d3;
                background-color: #d3d3d3;
                height: 1px;
            }
            
            </style>`;
    	  
    	   htmlvar+=`</head>    	    	
    	   <body header="nlheader" header-height="20%" footer="nlfooter" footer-height="31%" padding="0.1in 0.1in 0.1in 0.1in" width="16.50cm" height="20cm">`;
    	
    	   htmlvar +=`<table border="0">
			    <tr>
			    <td>
			    <table width="100%" padding-top="8px">
			    <tr>
				    <td width="550px" align="left" padding-left="20px">Payment for A.P. Voucher #:</td>
				    <td width="80px"></td>
			    </tr>`;
    	   		
			    const billPayLineCount = loadRec.getLineCount({ sublistId: 'apply' });
		    	//log.error('bill payment line count', billPayLineCount);
		    	
	    		for(let i=0; i<billPayLineCount; i++){
	        		if(billPayLineCount>0){
	        			const refnum = loadRec.getSublistValue
	    				({
	    					sublistId: 'apply',
	    					fieldId: 'refnum',
	    					line: i
	    				});
	        			//log.error('refnum', refnum);
	        			
	        			let CUSTBODY_APV_NUMBER = loadRec.getSublistValue
	    				({
	    					sublistId: 'apply',
	    					fieldId: 'CUSTBODY_APV_NUMBER',
	    					line: i
	    				});
	        			//log.error('CUSTBODY_APV_NUMBER', CUSTBODY_APV_NUMBER);
	        			
	        			let total = loadRec.getSublistValue
	    				({
	    					sublistId: 'apply',
	    					fieldId: 'total',
	    					line: i
	    				});
	        			//log.error('total', total);
	        			total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	        			
	        			 htmlvar+=`
		     			    	<tr>
		     			   			 <td align="left" padding-left="25px">&nbsp;${CUSTBODY_APV_NUMBER}&nbsp;${refnum}</td>
		     			    		 <td align="right" padding-right="130px">${total}</td>
		     			    	</tr>`;
	        			 
	        			const searchDetail = `Supplier Invoice #${refnum}`;
	        			
	        			//If credits line item is not empty
	        	    	const match = searchCreatedFrom(searchDetail);	
	        	    	try{
	        	    		if(match){
	        	    			 htmlvar+=`
	        	    			 <tr>
	        	 				    <td align="left" padding-left="30px">&nbsp;${searchResult.transactionnumber}&nbsp;${searchResult.tranid}</td>
	        	 				    <td align="right" padding-right="130px">${searchResult.amount}</td>
	        	    			</tr>`;	
	        	    		}
	        	    	}catch(e){
	        	    		
	        	    	}
	        	    	
	        		}	
	        		
	        	}
			   //=========================================================================================
			  htmlvar+=`</table>
			  </td>
			  </tr>
			     <tr>
			     <td>
			     <table width="100%" padding-top="0px">
			    <tr>
			    	<td width="400px" padding-left="30px"><p>${accountLegalName}&nbsp;${tranid}&nbsp;${custbody_nameon_checkprint}</p></td>
			    	<td width="40px"></td>
			    	</tr>
			  </table> 
			  </td></tr>
			  </table>`;
    		
    	htmlvar+=`</body>
        </pdf>`; //end of the template
    	
    	var file = render.xmlToPdf
		({
			xmlString: htmlvar
		});

		context.response.writeFile(file, true);
		return;
    }
  //==============================================================================
  // create a search for credits line item since it is not accessible in current record 
  function searchCreatedFrom(searchDetail){
	  var vendorcreditSearchObj = search.create({
		   type: "vendorcredit",
		   filters:
		   [
		      ["type","anyof","VendCred"], 
		      "AND", 
		      ["mainline","is","T"], 
		      "AND", 
		      ["createdfrom","noneof","@NONE@"], 
		      "AND", 
		      ["createdfrom.type","anyof","VendBill"]
		   ],
		   columns:
		   [
		      search.createColumn({name: "transactionnumber", label: "Transaction Number"}),
		      search.createColumn({name: "tranid", label: "Document Number"}),
		      search.createColumn({name: "amount", label: "Amount"}),
		      search.createColumn({name: "createdfrom", label: "Created From"})
		   ]
		});
		var searchResultCount = vendorcreditSearchObj.runPaged().count;
		//log.error("vendorcreditSearchObj result count",searchResultCount);
		
		let match = false;
		vendorcreditSearchObj.run().each(function(result){
			const tranid = result.getValue({
				name: 'tranid'
			});
			//log.error('tranid',tranid);
			
			const createdfrom = result.getText({
				name: 'createdfrom'
			});
			//log.error('createdfrom',createdfrom);
			
			const transactionnumber = result.getValue({
				name: 'transactionnumber'
			});
			//log.error('transactionNumber',transactionNumber);
			
			let amount = result.getValue({
				name: 'amount'
			});
			//log.error('amount',amount);
			amount = Math.abs(amount);
			amount = amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			if(createdfrom==searchDetail){
				match = true;
				//add result to the globally initialized object searchResult
				searchResult.tranid = tranid;
				searchResult.createdfrom = createdfrom;
				searchResult.transactionnumber = transactionnumber;
				searchResult.amount = amount;
				log.error('tranid',searchResult.tranid);
				log.error('createdfrom',searchResult.createdfrom);
				log.error('transactionnumber',searchResult.transactionnumber);
				log.error('amount',searchResult.amount);		
			}
		   // .run().each has a limit of 4,000 results
		   return true;
		});
		return match;
  }
  //================================================================================
    return {
        onRequest: onRequest
    };
    
});
