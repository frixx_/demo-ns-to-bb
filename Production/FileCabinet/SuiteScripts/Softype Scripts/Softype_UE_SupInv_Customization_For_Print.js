/**
* @NApiVersion 2.x
* @NScriptType UserEventScript
* @NModuleScope SameAccount
*/
/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  
 **                       
 **@Author      : Farhan Shaikh
 **@Dated       : 30/12/2019   DD/MM/YYYY
 **@Version     : 
 **@Description : UserEvent Script on Supplier Invoice Record To Set 'Reference PO','REFERENCE RR' Fields For Print.  

 **************************************************************************************************************************************/
 define(['N/search','N/record'],function(search,record){
    var taxCodeGlobal=''
    var wTaxCodeGlobal=''
    function beforeSubmit(context){
        var currentRecord=context.newRecord;
        var currentRecordId=currentRecord.id;

        log.debug('currentRecordId',currentRecordId);

        if (context.type!='create' && context.type!='edit' && context.type!='copy') {
            return;
        }

        var form=currentRecord.getValue('customform')

        /*if (form!=100 && form!=120) {
            return;
        }*/

        var refPo=currentRecord.getValue('custbody_print_ref_po')
        log.debug('refPo',refPo);
        var purchaseOrderLineCount=currentRecord.getLineCount('purchaseorders');
        log.debug('purchaseOrderLineCount',purchaseOrderLineCount);
        if (true) { //purchaseOrderLineCount!=0 && purchaseOrderLineCount!=-1 //&& refPo.length==0
            var poIdsToSetInRefPoField=[];
            var irIds=[];
            var latestDate='';

            if (purchaseOrderLineCount!=0 && purchaseOrderLineCount!=-1) {
                //To Get All The Purchase Order 
                for(var i=0;i<purchaseOrderLineCount;i++){
                    var poId=currentRecord.getSublistValue('purchaseorders','id',i)
                    poIdsToSetInRefPoField.push(poId)
    
                    if (i==purchaseOrderLineCount-1) {
                        currentRecord.setValue('custbody_print_ref_po',poIdsToSetInRefPoField)
                    }
                }
            }else{
                var poDocNum=currentRecord.getValue('podocnum')
                log.debug('poDocNum',poDocNum);
                if (poDocNum!=null && poDocNum!=undefined && poDocNum!='') {
                    poIdsToSetInRefPoField.push(poDocNum)
                    currentRecord.setValue('custbody_print_ref_po',poIdsToSetInRefPoField)
                }
            }

            if (poIdsToSetInRefPoField.length!=0) {
                //To Get All The Item Receipt of Each Purchase Order 
                for(var i=0;i<poIdsToSetInRefPoField.length;i++){
                    var poRecord=record.load({
                        type:'purchaseorder',
                        id:poIdsToSetInRefPoField[i]
                    });
    
                    var poLinks=poRecord.getLineCount('links');
    
                    if (poLinks>0) {
                        for(var j=0;j<poLinks;j++){
                            var typeOfRecord=poRecord.getSublistValue('links','type',j)
                            if (typeOfRecord=='Item Receipt') {
                                var irId=poRecord.getSublistValue('links','id',j);
                                irIds.push(irId);
                                var tranDate=poRecord.getSublistText('links','trandate',j);
                                if (latestDate=='') {
                                    latestDate=tranDate;
                                }else{
                                    var comparedDate=compareDate(latestDate,tranDate)
                                    latestDate=comparedDate;
                                    log.debug('comparedDate',comparedDate)
                                    log.debug('latestDate',latestDate)
                                }
                            }
                        }
                    }
                }
    
                if (irIds.length>0) {
                    var rrNos=searchItemReceiptForRRNo(irIds)
                    currentRecord.setValue('custbody_si_rr_no',rrNos)
                    currentRecord.setValue('custbody_print_ref_rr',irIds)
                }
    
                if (latestDate!='') {
                    currentRecord.setText('custbody_print_rr_date',latestDate)
                }
            }



            var lineCountArray=[];
            lineCountArray.push(currentRecord.getLineCount('item'))
            lineCountArray.push(currentRecord.getLineCount('expense')) 
            log.emergency('lineCountArray',lineCountArray)

            //for WTax Code
            var wtaxAndPercent='';
            var jsonWithWtaxDetails=[];
            var wtaxCodes=[]
            for(var lineArr=0;lineArr<lineCountArray.length;lineArr++){
                var sublist='';
                if(lineArr==0){
                    sublist='item'
                }else{
                    sublist='expense'
                }
                for(var i = 0; i < lineCountArray[lineArr]; i++){
                    var wtaxCode=currentRecord.getSublistValue({sublistId:sublist,fieldId:'custcol_4601_witaxcode',line:i});
                    var wtaxRate=currentRecord.getSublistValue({sublistId:sublist,fieldId:'custcol_4601_witaxrate',line:i});
                    log.emergency('wtaxCode',wtaxCode)
                    log.emergency('wtaxRate',wtaxRate)
                    if (wtaxRate=='') {
                        continue;
                    }

                    if (wtaxCode=='' || wtaxCode==null || wtaxCode==undefined) {
                        continue
                    }

                    var wtaxDetails={
                        'wtaxCode':Number(wtaxCode),
                        'line':i,
                        'wtaxRate':wtaxRate
                    }
                    jsonWithWtaxDetails.push(wtaxDetails)
                    if(wtaxDetails.wtaxCode != null && wtaxDetails.wtaxCode !='' && wtaxDetails.wtaxCode != undefined){
                        wtaxCodes.push(wtaxDetails.wtaxCode)
                    }
                    //wtaxRate=wtaxRate+'%'
                    /*if (i==lineCountArray.length-1 && i==lineCountArray[lineArr]-1) {
                        wtaxAndPercent+=wtaxCode+' '+wtaxRate+'.0%'
                    }else{
                        wtaxAndPercent+=wtaxCode+' '+wtaxRate+'.0%,'
                    }*/
                }


            }

            var wtaxFilter=new Array();
            var wtaxColumns=new Array();
            log.debug("wtaxCodes.length", JSON.stringify(wtaxCodes.length));
            // if (wtaxCodes.length!=0){
            if (wtaxCodes != null && wtaxCodes.length!=0) {
            
            log.debug("wtaxCodes", JSON.stringify(wtaxCodes));
            
                wtaxFilter.push(search.createFilter({
                    name: 'internalid',
                    operator: search.Operator.ANYOF,
                    values: wtaxCodes
                }));
                wtaxColumns.push(search.createColumn('custrecord_4601_wtc_name'));
            
                var searchedWtax = search.create({
                    'type': 'customrecord_4601_witaxcode',
                    'filters': wtaxFilter,
                    'columns': wtaxColumns
                }).run().getRange(0, 1000);
                log.emergency('searchedWtax',searchedWtax)
                for(var i=0;i<jsonWithWtaxDetails.length;i++){
                    var jsonCode=jsonWithWtaxDetails[i].wtaxCode
                    for(var j=0;j<searchedWtax.length;j++){
                        var searchedCode=searchedWtax[j].id
                        log.emergency('searchedCode',searchedCode)
                        log.emergency('jsonCode',jsonCode)
                        if (searchedCode==jsonCode) {
                            var wtaxName=searchedWtax[j].getValue('custrecord_4601_wtc_name')
                            if (i==lineCountArray.length-1 && i==lineCountArray[lineArr]-1) {
                                wtaxAndPercent+=wtaxName+' '+jsonWithWtaxDetails[i].wtaxRate+'.0%'
                                break;
                            }else{
                                wtaxAndPercent+=wtaxName+' '+jsonWithWtaxDetails[i].wtaxRate+'.0%,'
                                break;
                            }
                        }
                    }
                }
    
                if (wtaxAndPercent.charAt(wtaxAndPercent.length-1)==',') {
                    wtaxAndPercent=wtaxAndPercent.slice(0,wtaxAndPercent.length-1)
                }
                log.emergency('wtaxAndPercent',wtaxAndPercent)
    
                if (wtaxAndPercent!='') {
                    var wtaxAndPercentWithoutDuplicates=removeDuplicates(wtaxAndPercent)
                    currentRecord.setText('custbody_print_wtax_code',wtaxAndPercentWithoutDuplicates)
                    wTaxCodeGlobal=wtaxAndPercentWithoutDuplicates
                }
            }else{
                currentRecord.setText('custbody_print_wtax_code','')
            }


            //for TaxCode
            wtaxAndPercent=''
            for(var lineArr=0;lineArr<lineCountArray.length;lineArr++){
                var sublist='';
                if(lineArr==0){
                    sublist='item'
                }else{
                    sublist='expense'
                }
                for(var i = 0; i < lineCountArray[lineArr]; i++){
                    var wtaxCode=currentRecord.getSublistValue({sublistId:sublist,fieldId:'taxcode_display',line:i});
                    var wtaxRate=currentRecord.getSublistValue({sublistId:sublist,fieldId:'taxrate1',line:i});
                    log.emergency('taxCode',wtaxCode)
                    log.emergency('taxRate',wtaxRate)
                    if (wtaxCode=='' || wtaxCode==undefined || wtaxCode=='') {
                        continue;
                    }
                    var inputLabel=JSON.stringify(wtaxCode).charAt(1);
                    log.emergency('inputLabel',inputLabel)
                    //var deferredLabel=wtaxCode.startsWith('D');
                    if (inputLabel=='I') {
                        log.emergency('I')
                       wtaxCode=wtaxCode.slice(6,wtaxCode.length)
                    }else if (inputLabel=='D') {
                        log.emergency('D')
                        wtaxCode=wtaxCode.slice(9,wtaxCode.length)
                    }
                    if (wtaxCode=='' && wtaxRate=='') {
                        continue;
                    }
                    log.emergency('taxCode',wtaxCode)
                    //wtaxRate=wtaxRate+'%'
                    if (i==lineCountArray.length-1 && i==lineCountArray[lineArr]-1) {
                        wtaxAndPercent+=wtaxCode+' '+wtaxRate+'.0%'
                    }else{
                        wtaxAndPercent+=wtaxCode+' '+wtaxRate+'.0%,'
                    }
                }
            }

            if (wtaxAndPercent.charAt(wtaxAndPercent.length-1)==',') {
                wtaxAndPercent=wtaxAndPercent.slice(0,wtaxAndPercent.length-1)
            }
            log.emergency('Tax Code',wtaxAndPercent)

            if (wtaxAndPercent!='') {
                var taxAndPercentWithoutDuplicates=removeDuplicates(wtaxAndPercent)
                currentRecord.setText('custbody_print_tax_code',taxAndPercentWithoutDuplicates)
                taxCodeGlobal=taxAndPercentWithoutDuplicates
            }
        }

    }

    function afterSubmit(context){ //when invoices are generated using acr=tion script taxcode display wont be there thats y using this
        var currRecord=context.newRecord;
        if (context.type!='create' && context.type!='copy') { //&& context.type!='edit'
            return;
        }
        log.emergency('Context Type',context.type)
        beforeSubmit(context)
        record.submitFields({
            type: 'vendorbill',
            id: currRecord.id,
            values: {
                'custbody_print_tax_code': taxCodeGlobal,
                'custbody_print_wtax_code':wTaxCodeGlobal
            }
        });
    }

    function removeDuplicates(stringToSet){
        var a=stringToSet
        var b=a.split(',')
        for(var i=0;i<b.length;i++){
           if(b.lastIndexOf(b[i])!=b.indexOf(b[i])){
                //console.log('inside if')
                while(b.lastIndexOf(b[i])!=b.indexOf(b[i])){
                    //console.log('inside while')
                    b.pop(b.indexOf(b[i]))
                }
            }
        }

        return String(b)
    }


    function searchItemReceiptForRRNo(irIds){
        var rrNumbers=[];
        var searchFilter=new Array();
        var searchColumns=new Array();
        searchFilter.push(search.createFilter({
            name: 'internalid',
            operator: search.Operator.ANYOF,
            values: irIds
        }));
        searchFilter.push(search.createFilter({
            name: 'mainline',
            operator: search.Operator.IS,
            values: true
        }));
        searchColumns.push(search.createColumn('custbody_si_rr_no'));
    
        var searchedItemReceipt = search.create({
            'type': 'itemreceipt',
            'filters': searchFilter,
            'columns': searchColumns
        }).run().getRange(0, 1000);
        log.emergency('searchedItemReceipt',searchedItemReceipt)

        for(var i=0;i<searchedItemReceipt.length;i++){
            var rrNo=searchedItemReceipt[i].getValue('custbody_si_rr_no')
            if (rrNo!='' && rrNo!=undefined && rrNo!=null) {
                rrNumbers.push(rrNo)
            }
        }

        return String(rrNumbers)
    }

    function compareDate(previousDate,currentDate){
        var startDate=previousDate;
        var endDate=currentDate;
        var arrStartDate = startDate.split("/");
        //var date1 = new Date(arrStartDate[2], arrStartDate[1], arrStartDate[0]);
        var arrEndDate = endDate.split("/");
        //var date2 = new Date(arrEndDate[2], arrEndDate[1], arrEndDate[0]);
        var date1=JSON.stringify(arrStartDate)
        var date2=JSON.stringify(arrEndDate)

        log.debug('date1',date1)
        log.debug('date2',date2)

        if (date1>date2) {
            return previousDate;
        }else{
            return currentDate;
        }
    }
    return{
        beforeSubmit:beforeSubmit,
        afterSubmit:afterSubmit
    }
 })