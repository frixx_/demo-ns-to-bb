/**
	* @NApiVersion 2.x
	* @NScriptType ClientScript
	* @NModuleScope SameAccount
*/


/***************************************************************************************  
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**@Author      :  Amol Jagkar
	**@Dated       :  24/02/2020
	**@Version     :  2.0
	**@Description :  Vat Calculation for SI
	**@Version     :  1.0
***************************************************************************************/
define(['N/record','N/search','N/runtime'],
	function(record,search,runtime) {
		
		/* function pageInit(scriptContext){
			var currentRecordObj = scriptContext.currentRecord;
			
			var lines = currentRecordObj.getLineCount({
				sublistId: 'item'
			});
			for(var i = 0; i < lines; i++){
				var lineNum = currentRecordObj.selectLine({
					sublistId: 'item',
					line: i
				});
			
				var rate = currentRecordObj.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'rate',
					line: lineNum
				});
				currentRecordObj.setCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'rate',
					value: parseFloat(rate).toFixed(3),
					line: lineNum
				});
			}
		} */
		
		function fieldChanged(scriptContext) {
			var currentRecordObj = scriptContext.currentRecord;
			
			if(scriptContext.sublistId == 'item'){
				
				if(scriptContext.fieldId == "custcol_vat_incls" && currentRecordObj.type == "vendorbill"){
					// var vatInclusiveCheck = currentRecordObj.getCurrentSublistValue({
						// sublistId: 'item',
						// fieldId: 'custcol_vatinclusivefields'
					// });
					var vatInclusiveDisplay = currentRecordObj.getCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_vat_incls'
					});
					var rate = currentRecordObj.getCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'rate'
					});
					
					// if(vatInclusiveCheck){
						// alert("Selected Item is already Vat inclusive");
						// currentRecordObj.setCurrentSublistValue({
							// sublistId: 'item',
							// fieldId: 'custcol_vat_incls',
							// value: vatInclusiveCheck,
							// ignoreFieldChange: true
						// });
					// }else{
						
						if(vatInclusiveDisplay){
							currentRecordObj.setCurrentSublistValue({
								sublistId: 'item',
								fieldId: 'rate',
								value: parseFloat(rate / 1.12)//.toFixed(3)
							});
						}else{
							currentRecordObj.setCurrentSublistValue({
								sublistId: 'item',
								fieldId: 'rate',
								value: parseFloat(rate * 1.12)//.toFixed(3)
							});							
						}
					// }
				}
				
				if(scriptContext.fieldId == "grossamt"){
					
					var quantity = currentRecordObj.getCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'quantity'
					});
					
					var amount = currentRecordObj.getCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'amount'
					});
					
					currentRecordObj.setCurrentSublistValue({
						sublistId: 'item',
						fieldId: 'rate',
						value: parseFloat(amount / quantity)//.toFixed(3)
					});
				}
			}
		}
		
		return {			
			// pageInit: pageInit,
			fieldChanged: fieldChanged
		};
		
	});										