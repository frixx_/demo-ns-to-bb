/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */
 
/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : Disables DPD's of Item Classification & Inventory Segment
***************************************************************************************/

define(['N/search','N/record','N/ui/serverWidget','N/url'],
	function(search,record,serverWidget,url){
		function beforeLoad(scriptContext){
			
			var currentRecord = scriptContext.newRecord; 
				log.debug('currentRecord:',currentRecord);
				// if(scriptContext.type == 'create'){
					var form = scriptContext.form;
					var recordType = currentRecord.type;
					var recordid = currentRecord.id;
					log.debug('recordType:',recordType);
						
					var division;
					if(recordType == "journalentry"){
						division = form.getSublist({id: 'line'}).getField({id: 'class'});
					}else if(recordType == "vendorbill"){
						division = form.getSublist({id: 'item'}).getField({id: 'class'});
						var divisionExpense = form.getSublist({id: 'expense'}).getField({id: 'class'});			
						divisionExpense.updateDisplayType({
							displayType : serverWidget.FieldDisplayType.DISABLED
						});
						var vatInclusive = form.getSublist({id: 'item'}).getField({id: 'custcol_vatinclusivefields'});			
						vatInclusive.updateDisplayType({
							displayType : serverWidget.FieldDisplayType.HIDDEN
						});
					}else{
						division = form.getSublist({id: 'item'}).getField({id: 'class'});
					}
									
					division.updateDisplayType({
						displayType : serverWidget.FieldDisplayType.DISABLED
					});
			
		}
		return{
			beforeLoad: beforeLoad
		};
});