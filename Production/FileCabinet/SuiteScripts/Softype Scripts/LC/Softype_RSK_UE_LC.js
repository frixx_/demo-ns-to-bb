/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : On selection of PO in LC record set value of LC in PO
***************************************************************************************/

define(['N/http', 'N/search', 'N/record', 'N/runtime', 'N/log', 'N/ui/serverWidget', 'N/error'],
	function(http, search, record, runtime, log, serverWidget, error) {
		
		function beforeLoad(context) {
			if(context.type == "edit"){
				var form = context.form;
				
				var newRecordDetails = context.newRecord;
				var poLink = newRecordDetails.getValue({
					fieldId: "custrecord_lc_polink"//"custbody_advancerequest"
				});
				
				log.debug('poLink', poLink);
				log.debug('!!(poLink)', !!(poLink));
				if(!!(poLink)){
					var poLinkField = form.getField({
						id: 'custrecord_lc_polink'
					});
					
					poLinkField.updateDisplayType({
						displayType : serverWidget.FieldDisplayType.INLINE
					});
				}
			}
		}
		
		function beforeSubmit(context) {
			
			var newRecordDetails = context.newRecord;
			log.debug('newRecordDetails', newRecordDetails);
			
			var poLink = newRecordDetails.getValue({
				fieldId: "custrecord_lc_polink"//"custbody_advancerequest"
			});
			
			log.debug('poLink', poLink);
			
			if(!!(poLink)){
				var PORecord = record.load({type : "purchaseorder", id : poLink});
				
				var LCValidation = PORecord.getValue({
					fieldId: "custbody_letterofcreditnum"
				});
				log.debug('LCValidation', LCValidation);
				
				if(!!(LCValidation)){
					var flag = true;
					if(context.type == "edit"){
						if(newRecordDetails.id == LCValidation){
							flag = false;
						}
					}
					if(flag){
						throw "Please choose another RFP record.";
						return;
					}
				}else{
				
					PORecord.setValue({
						fieldId: "custbody_letterofcreditnum",
						value: newRecordDetails.id
					});
				
				}
				
				PORecord.save();
			}				
		}
		
		
		return {
			beforeLoad: beforeLoad,
			beforeSubmit: beforeSubmit
		};
	});			