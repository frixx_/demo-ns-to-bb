/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 31 December 2019
	**@Version : 2.0
	**@Description : Call RSK API for Purchase Order Close
***************************************************************************************/


define(['N/http', 'N/https', 'N/format', 'N/record', 'N/search', 'N/runtime', 'N/log'],
	function(http, https, format, record, search, runtime, log) {
	
		var closeFlag = false;
		
		function beforeLoad(context) {
			
			// log.debug('before Load context',context);
			
			// log.debug('before Load oldRecord',context.oldRecord);
			// log.debug('before Load newRecord',context.newRecord);
			
			if(context.type == 'create')
				return;
			
			var newRecord = context.newRecord;
			
			var closed = newRecord.getValue({
				fieldId: "custbody_poclosecheckbox"
			});
			
			if(!closed)
				return;
			
			try{
				var scriptObj = runtime.getCurrentScript();
				var closeAPI = scriptObj.getParameter({name: 'custscript_po_close_rsk'});
				var restletAPI = scriptObj.getParameter({name: 'custscript_po_close_restlet'});
				
				
				var status = newRecord.getValue("status");
				
				var poTypeRecord = record.load({
						type: "customlist_potype", 
						id: newRecord.getValue({fieldId: 'custbody_purchaseordertypelist'}),
						isDynamic: false,
					});
					
				var potype = poTypeRecord.getValue({fieldId: 'name'});
				
				if(status === "Closed"){
															
					// var url='http://123.136.77.34:7280/InventoryReganIntegration/api/PurchaseOrder/Close';	//RSK API URL
				
					var url = closeAPI;
				
					var headers = {
						"Content-Type": "application/json"
					};
					var jsonBody = {
						"NsId": newRecord.id,
						"Id":Number(newRecord.getValue({
							fieldId: 'custbody_rskponum'
						})),
						"POType":potype
					};
					
					log.debug("NsId",newRecord.id);
					log.debug("Id",Number(newRecord.getValue({
							fieldId: 'custbody_rskponum'
						})));
					log.debug("JSON.stringify(jsonBody)",JSON.stringify(jsonBody));
									
					// If the API is https compliant
					var response = http.request({
						method: https.Method.PUT,
						url: url,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					var dataCode = response.code; 
					log.debug("response",response);
					log.debug("dataCode",dataCode);
					
					if(dataCode == "200"){
						try{
							
							var id = record.submitFields({
								type: record.Type.PURCHASE_ORDER,
								id: newRecord.id,
								values: {
									"custbody_poclosecheckbox": true
								},
								options: {
									enableSourcing: false,
									ignoreMandatoryFields : true
								}
							});
							
						}catch(err){
							log.debug("Exception", err);
						}
					}
									
				}else 
					return;
				
			}catch(err){
				log.debug("err",err);
				return {message:"Failure", error: err};		
			}
		}
		
		function beforeSubmit(context){
			try{
				var newRecord = context.newRecord;
				log.debug("newRecord", newRecord);
				if(closeFlag){
					var newRecord = context.newRecord;
								
					var closed = newRecord.getValue({
						fieldId: "custbody_poclosecheckbox"
					});
					
					if(!closed)
						return;
					
					newRecord.setValue({
						fieldId: "custbody_poclosecheckbox",
						value: true
					});
					
					log.debug("closeFlag", closeFlag);
				}
				
			}catch(err){
				log.debug("Exception", err);
			}
		}
		
		return {
			beforeLoad: beforeLoad,
			beforeSubmit: beforeSubmit
		};
	});	