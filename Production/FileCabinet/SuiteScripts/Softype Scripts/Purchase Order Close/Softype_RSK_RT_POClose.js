/**
 * @NApiVersion 2.x
 * @NScriptType restlet
*/
define(['N/http', 'N/https', 'N/record', 'N/search', 'N/log', 'N/format'], function(http, https, record, search, log, format) {
	return {
		post : function(data){
			
			if(typeof data === 'string'){
				data = JSON.parse(data);
			}
			
			log.debug("jsonData",JSON.stringify(data));		
					
			try{
								
				var newRecord = record.load({
					type: data.Type, 
					id: data.Id,
					isDynamic: true
				});
				
				newRecord.setValue({
					fieldId: "custbody_poclosecheckbox",
					value: true
				});
				
				log.debug("newRecord", newRecord);
				
				var recordID = newRecord.save();
				log.debug("recordid",recordID);
				
				return {recordid : recordID, message: "Success"};
			}catch(err){
				log.debug("err",err);
				return {message:"Failure", error: err};		
			}
			
			//return "Success";
		}
	}
});