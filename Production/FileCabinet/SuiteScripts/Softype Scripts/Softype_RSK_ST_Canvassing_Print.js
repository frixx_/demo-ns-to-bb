/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  24th October, 2019
 ** @Version     :  2.x
 ** @Description :  Suitelet script for Canvassing Print.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/ui/serverWidget', 'N/record', 'N/render', 'N/xml', 'N/format', 'N/runtime'],

    function(serverWidget,record, render, xml, format,runtime) {


        function onRequest(context) {


            var SUBLIST_JSON = [];
            var finalArray = [];
            var temp = {};
            var PR_JSON = [];
			var slash =  '/';

           
			var reganURL = runtime.getCurrentScript().getParameter("custscript_canvas_regan_logo_url");
			var kirinURL = runtime.getCurrentScript().getParameter("custscript_canvas_kirin_logo_url");
			var supremeURL = runtime.getCurrentScript().getParameter("custscript_canvas_supreme_logo_url");


            var kirinURLEsc = xml.escape({
                xmlText: kirinURL
            });

            var reganURLEsc = xml.escape({
                xmlText: reganURL
            });
            var supremeURLEsc = xml.escape({
                xmlText: supremeURL
            });


            var recId = context.request.parameters.recordId;
           
            log.debug('recId', recId);
           

            var objRecord = record.load({
                type: 'customrecord_canvassing_parent_record',
                id: recId,
                isDynamic: true
            });


            var subsidiary = objRecord.getValue({
                fieldId: 'custrecord_canvassersubsidiary'
            });

            var canvasser = objRecord.getText({
                fieldId: 'custrecord_canvasser'
            });
			
			 var canvassingNo = objRecord.getText({
                fieldId: 'name'
            });
			
			var telephone = objRecord.getText({
                fieldId: 'custrecord_canvass_office_phone'
            });
			
			var faxNumber = objRecord.getValue({
                fieldId: 'custrecord_canvass_fax_num'
            });
			
			var email = objRecord.getText({
                fieldId: 'custrecord_canvass_email'
            });
			
			var transactionDate = objRecord.getText({
                fieldId: 'custrecord_tran_date'
            });

			if(transactionDate == null || transactionDate == '')
				transactionDate = '';

			if(telephone == null || telephone == '')
				telephone = '';

			if(faxNumber == null || faxNumber == '')
				faxNumber = '';

			
			if(email == null || email == '')
				email = '';

			if(faxNumber == null || faxNumber == ''){
				
				slash = ''
				
			}
			


            var form = serverWidget.createForm({
                title: 'Acknowledgement Receipt'
            });

            var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
            htmlvar += '<pdf>\n';
            htmlvar += '<head>';


            htmlvar += '<macrolist>';
			
            htmlvar += '<macro id="nlheader">';

            htmlvar += '</macro>';



            htmlvar += '<macro id="nlfooter">';

            htmlvar += " </macro>";



            htmlvar += " </macrolist>";
            htmlvar += '</head>';

            htmlvar += '<body  font-size="10">';
			
		    htmlvar += '<table width="100%"  margin-bottom="20px">';
            htmlvar += '<tr>';
            htmlvar += '<td width="33%"  align="center" vertical-align="middle"> <img   style= "width:50%; height:50%" src= "' + reganURLEsc + '" /></td>';
			htmlvar += '<td width="33%"  align="center" vertical-align="middle"><img  style= "width:20%; height:20%" src= "' + supremeURLEsc + '" /></td> ';
            htmlvar += '<td width="33%"  align="center" vertical-align="middle"><img style= "width:20%; height:20%" src= "' + kirinURLEsc + '" /></td> ';
           
            htmlvar += '</tr>';
            htmlvar += '</table>';




            htmlvar += '<table width="100%" >';
            htmlvar += '<tr margin-top="20px">';
            htmlvar += '<td style=" height: 20px;align:left;font-size:11;">Date: ' + transactionDate + ' </td> ';
            htmlvar += '</tr>';
            
			htmlvar += '<tr>';
            htmlvar += '<td style=" height: 20px;align:left;font-size:11;">Canvassing No. :&nbsp; ' + canvassingNo + '</td> ';
            htmlvar += '</tr>';
			htmlvar += '<tr>';
            htmlvar += '<td style=" height: 20px;align:left;font-size:11;">Canvasser :&nbsp; ' + canvasser + '</td> ';
            htmlvar += '</tr>';
			
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;font-size:11;">ATTENTION TO:</td>'
            htmlvar += '</tr>';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;font-size:11;">Please quote the following items below. Kindly give your best offer.</td>'
            htmlvar += '</tr>';
            htmlvar += ' </table>';



            htmlvar += '<table width="100%" margin-bottom="10px">';

            htmlvar += '<tr style=" border-top: 1px solid black;margin-top:10px;border-bottom: 1px solid black">';

            htmlvar += '<td style=" height: 20px;width:5%;" border-right="1px" border-left="1px"><p align="left"><b>ITEM NO.</b></p></td> ';
            htmlvar += '<td style=" height: 20px; width:20%;" border-right="1px"><b>ITEM</b></td>'
            htmlvar += '<td style=" height: 20px; width:10%;" border-right="1px"><b>QTY.</b></td> ';
            htmlvar += '<td style=" height: 20px; width:10%;" border-right="1px"><b>OFFER/BRAND</b></td>'
            htmlvar += '<td style=" height: 20px;width:15%;align:right;" border-right="1px"><b>AVAILIBILITY</b></td> ';

            htmlvar += '<td style=" height: 20px;width:10%;align:right;" border-right="1px"><b>DISCOUNT</b></td> ';
            htmlvar += '<td style=" height: 20px;width:10%;align:right;" border-right="1px"><p align="left"><b>PRICE PER UNIT(Vat Inclusive)</b></p></td>'
            htmlvar += '<td style="height: 20px;width:15%;align:right;" border-right="1px"><p align="left"><b>TOTAL PRICE</b></p></td> ';

            htmlvar += '</tr>';



            var lineCount = objRecord.getLineCount({
                sublistId: 'recmachcustrecord_link_canvassing'
            });

           

            for (var l = 0; l < lineCount; l++) {

                var originalPrQty = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_link_canvassing',
                    fieldId: 'custrecord_original_pr_qty',
                    line: l
                });
                

                if (originalPrQty == 'T') {

                    var itemValue = objRecord.getSublistText({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_item_canvas',
                        line: l
                    });

                    var prNo = objRecord.getSublistText({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_pr_no',
                        line: l
                    });

                    var prQuantity = objRecord.getSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_qty_canvas',
                        line: l
                    });

                    SUBLIST_JSON.push({

                        itemValue: itemValue,
                        prNo: prNo,
                        prQuantity: prQuantity

                    });



                }

            }

			log.audit('SUBLIST_JSON', SUBLIST_JSON);
			
            SUBLIST_JSON.forEach(function(obj) {

                if (!temp[obj.itemValue]) {

                    temp[obj.itemValue] = obj.prQuantity;

                } else {

                    temp[obj.itemValue] = Number(temp[obj.itemValue]) + obj.prQuantity;

                }

            });
           



            for (var key in temp) {

                finalArray.push({

                    itemValue: key,

                    prQuantity: temp[key]
                })
            }

            log.audit('finalArray', finalArray);



            for (var i = 0; i < finalArray.length; i++) {

                var lineNum = i + 1;

                htmlvar += '<tr  border-bottom="1px" style="page-break-inside : avoid; page-break-before:auto">';

                htmlvar += '<td style=" height: 16px;width:5%; align:center;" border-right="1px" border-left="1px"><p align="left">' + lineNum + '</p></td> ';
                htmlvar += '<td style=" height: 16px; width:22%;" border-right="1px">' + xml.escape({
                    xmlText: finalArray[i].itemValue
                }) + '</td>'
                htmlvar += '<td style=" height: 16px; width:10%;align:center;" border-right="1px">' + finalArray[i].prQuantity + '</td> ';
                htmlvar += '<td style=" height: 16px; width:10%;" border-right="1px"></td>'
                htmlvar += '<td style=" height: 16px;width:15%;align:right;" border-right="1px"></td> ';

                htmlvar += '<td style=" height: 16px;width:10%;align:right;" border-right="1px"></td> ';
                htmlvar += '<td style=" height: 16px;width:10%;align:right;" border-right="1px"></td>'
                htmlvar += '<td style="height: 16px;width:13%;align:right;" border-right="1px" ></td> ';


                htmlvar += '</tr>';


            }


            htmlvar += '</table>';


            htmlvar += '<table width="100%" >';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 16px;width:80%;align:right;" margin-top="20px;" >TOTAL</td> ';
            htmlvar += '<td style=" height: 16px;width:20%;align:right;" border="1px" margin-bottom="20px;"></td> ';
            htmlvar += '</tr>';

            htmlvar += '</table>';
			
			
			htmlvar += '<table width="100%"  margin-bottom="20px" style="page-break-inside : avoid; page-break-before:auto">';
            htmlvar += '<tr border-bottom="20px;">';
            htmlvar += '<td style=" height: 20px;width:30%;align:left;">PAYMENT TERMS :</td> ';
            htmlvar += '<td style=" height: 20px;width:70%;align:left;" ></td> ';
            htmlvar += '</tr>';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;width:30%;align:left;"  >DELIVERY DATE :</td> ';
            htmlvar += '<td style=" height: 20px;width:70%;align:left;" ></td> ';
            htmlvar += '</tr>';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;width:30%;align:left;"  >WARRANTY :</td> ';
            htmlvar += '<td style=" height: 20px;width:70%;align:left;"  ></td> ';
            htmlvar += '</tr>';

            htmlvar += '</table>';
			
			
			
			
            htmlvar += '<table width="100%" style="page-break-inside : avoid; page-break-before:auto">';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;" margin-top="20px;" >Kindly re-fax or email us.</td> ';

            htmlvar += '</tr>';
			
			htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;" margin-top="20px;" >Tels. / Fax : &nbsp; '+ telephone + ' ' +slash + ' ' + faxNumber + ' </td> ';

            htmlvar += '</tr>';
			
			htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;" margin-top="20px;" >Email : &nbsp;'  + xml.escape({
                xmlText: email
            }) + '</td> ';

            htmlvar += '</tr>';
			
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;" margin-top="20px;" ><b>' + xml.escape({
                xmlText: 'Thank you & God Bless!'
            }) + '</b></td> ';

            htmlvar += '</tr>';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;" margin-top="20px;" ><b>' + xml.escape({
                xmlText: canvasser
            }) + '</b></td> ';

            htmlvar += '</tr>';
            htmlvar += '<tr >';
            htmlvar += '<td style=" height: 20px;align:left;" margin-top="20px;" ><b>Purchasing Department</b></td> ';

            htmlvar += '</tr>';

            htmlvar += '</table>';
			
			
			
			
			




            htmlvar += '</body>\n</pdf>';


            var pdfFile = render.xmlToPdf({
                xmlString: htmlvar
            });
			
            return context.response.writeFile(pdfFile, true);




        }



        return {
            onRequest: onRequest
        };
    });