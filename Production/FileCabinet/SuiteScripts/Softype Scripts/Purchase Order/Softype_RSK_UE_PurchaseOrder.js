/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Call RSK API for Purchase Order
***************************************************************************************/


define(['N/http', 'N/https', 'N/format', 'N/record', 'N/search', 'N/runtime', 'N/log'],
	function(http, https, format, record, search, runtime, log) {
		
		function afterSubmit(context) {
			/* 
			log.debug('runtime',runtime);
			log.debug('runtime.executionContext',runtime.executionContext);
			log.debug('context',context); */
			
			var scriptObj = runtime.getCurrentScript();
			var createAPI = scriptObj.getParameter({name: 'custscript_create_api_po'});
			
			// if(context.type != 'edit')
			// return;
			
			var newRecordDetails = context.newRecord;
			log.debug('newRecordDetails', newRecordDetails);
			
			try{
				
				var approvalStatus = newRecordDetails.getValue({
					fieldId: 'approvalstatus'
				});
				
				log.debug('approvalStatus',approvalStatus);
				
				var rskID = newRecordDetails.getValue({fieldId: 'custrecord_categoryid'});	
				
				if(approvalStatus !== "2"){
					return;
				}
				
				var poTypeRecord = record.load({
					type: "customlist_potype", 
					id: newRecordDetails.getValue({fieldId: 'custbody_purchaseordertypelist'}),
					isDynamic: false,
				});
				
				var potype = poTypeRecord.getValue({fieldId: 'name'});
				
				log.debug("potype", potype);
				
				if(potype == "Local  Services" || potype == "Import  Services"){
					return;
				}
				/* var potype = newRecordDetails.getValue({fieldId: 'custbody_purchaseordertypelist'});
					
					if(potype == "5"){
					return;
				} */
				/**********************************************************
					*************************API CALL************************
				**********************************************************/
				
				var poItems = new Array();
				
				var lineCount = newRecordDetails.getLineCount({
					sublistId: 'item'
				});
				
				var itemsChecked = {};
				var counter = 0;				
				for(var i = 0; i < lineCount; i++){
					var item = newRecordDetails.getSublistValue({
						sublistId: 'item',
						fieldId: 'item',
						line: i
					});
					
					var itemRSK = search.lookupFields({
						type: search.Type.ITEM,
						id: item,
						columns: ['custitem_rskitemid']
					})["custitem_rskitemid"];
					
					var quantity = newRecordDetails.getSublistValue({
						sublistId: 'item',
						fieldId: 'quantity',
						line: i
					});
					
					var cost = newRecordDetails.getSublistValue({
						sublistId: 'item',
						fieldId: 'rate',
						line: i
					});
					
					var costVat = newRecordDetails.getSublistValue({
						sublistId: 'item',
						fieldId: 'taxrate1',
						line: i
					});
					
					var vat = newRecordDetails.getSublistValue({
						sublistId: 'item',
						fieldId: 'tax1amt',
						line: i
					});
					
					var grossAmt = newRecordDetails.getSublistValue({
						sublistId: 'item',
						fieldId: 'grossamt',
						line: i
					});
					
					var weight = newRecordDetails.getSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_weight',
						line: i
					});
					
					log.debug("weight", weight);
					
					var jsonObject = {};
						
					if(itemsChecked[itemRSK] == undefined){
						itemsChecked[itemRSK] = counter;
						
						jsonObject = {
							"ProductId": Number(itemRSK),
							"Quantity": quantity,
							"Cost": cost,
							"CostVat": cost * (1 + vat / 100),
							"Vat": vat,
							"Weight": parseFloat(weight)
						}
						
						poItems.push(jsonObject);
						counter++;
						log.debug("itemsChecked[itemRSK] - " + itemsChecked[itemRSK], JSON.stringify(jsonObject));
					}
					else {
						
						
						poItems[itemsChecked[itemRSK]] = {
							"ProductId": poItems[itemsChecked[itemRSK]].ProductId,
							"Quantity": poItems[itemsChecked[itemRSK]].Quantity + quantity,
							"Cost": cost,
							"CostVat": cost * (1 + vat / 100),
							"Vat": vat,
							"Weight": parseFloat(poItems[itemsChecked[itemRSK]].Weight) + parseFloat(weight)
						};
						log.debug("poItems", JSON.stringify(poItems));
						log.debug("itemsChecked[itemRSK]", itemsChecked[itemRSK]);
					}
					
				}
				
				var purchaserEmail = "", supplierIDRSK = "", locationIDRSK = "", intendedLocationIDRSK = "";
				
				var purchaserID = newRecordDetails.getValue({
					fieldId: 'recordcreatedby'
				});
				
				var supplierID = newRecordDetails.getValue({
					fieldId: 'entity'
				});				
				var locationID = newRecordDetails.getValue({
					fieldId: 'location'
				});				
				var intendedLocationID = newRecordDetails.getValue({
					fieldId: 'custbody_po_intended_location'
				});										
				if(!!purchaserID){
					purchaserEmail = search.lookupFields({
						type: search.Type.EMPLOYEE,
						id: purchaserID,
						columns: ['email']
					})["email"];
				}
				if(!!supplierID){
					supplierIDRSK = search.lookupFields({
						type: search.Type.VENDOR,
						id: supplierID,
						columns: ['custentity_rsksuppleirid']
					})["custentity_rsksuppleirid"];
				}
				if(!!locationID){							
					locationIDRSK = search.lookupFields({
						type: search.Type.LOCATION,
						id: locationID,
						columns: ['custrecord_location_rskidfield']
					})["custrecord_location_rskidfield"];
				}
				if(!!intendedLocationID){							
					intendedLocationIDRSK = search.lookupFields({
						type: search.Type.LOCATION,
						id: intendedLocationID,
						columns: ['custrecord_location_rskidfield']
					})["custrecord_location_rskidfield"];
				}		
				
				var trandate = 	newRecordDetails.getValue({
					fieldId: 'trandate'
				});
				var trandate = 	newRecordDetails.getValue({
					fieldId: 'trandate'
				});
				var amount =	newRecordDetails.getValue({
					fieldId: 'total'
				})
				
				// log.debug("trandate", formatDate(trandate));	
				// log.debug("amount", amount);									
				
				var jsonBody = {};
				
				if(potype == "Import Trade" || potype == "Import Non Trade"){
				
					var companyid = (search.lookupFields({
						type: "subsidiary",
						id: newRecordDetails.getValue({
								fieldId: 'custbody_pocon_companyid'
							}),
						columns: ['custrecord_import_subsidiariy']
					}))['custrecord_import_subsidiariy'];
						
					
					//custrecord_import_subsidiariy
				
					jsonBody = {
						"NsId": newRecordDetails.id,
						"ReferenceNo":newRecordDetails.getValue({
							fieldId: 'tranid'
						}),
						"PurchaserId": 1, //purchaserEmail,
						"SupplierId": Number(supplierIDRSK),
						"LocationId": Number(locationIDRSK),
						"IntendedLocationId": intendedLocationIDRSK != "" ? Number(intendedLocationIDRSK) : "",
						"OrderDate": formatDate(trandate),
						"POType": potype,		
						"StatusId": 1, /*Number(newRecordDetails.getValue({
							fieldId: 'status'
						}))*/
						
						///////////////////////////////////////
						
						"internal_contract_no": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_intcontractnum'
						}),
						"CompanyId": companyid,
						"supplier_contract_no": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_supcontract'
						}),
						"LC_terms_id": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_lcterms'
						}),
						"Indentor_id": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_indentor'
						}),
						"Indentor_name": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_indentor_name'
						}),
						"Indentor_email": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_indentor_email'
						}),
						"currency_id": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_currencyid'
						}),
						"contract_date": formatDate(newRecordDetails.getValue({
							fieldId: 'custbody_pocon_date'
						})),
						"due_date": formatDate(newRecordDetails.getValue({
							fieldId: 'custbody_pocon_duedate'
						})),
						"lc_shipment_terms_id": newRecordDetails.getValue({
							fieldId: 'custbody_poconshipterms'
						}),
						"city_id": newRecordDetails.getValue({
							fieldId: 'custbody_pocon'
						}),
						"delivery_tolerance": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_deliverytol'
						}),
						"supplier_tolerance": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_supptol'
						}),
						"Origin": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_countryorigin'
						}),
						"Grade": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_grade'
						}),
						"contract_amount": newRecordDetails.getValue({
							fieldId: 'custbody_pocon_amount'
						}),
											
						///////////////////////////////////////
						
						"Amount":Number(amount),
						"Description":newRecordDetails.getValue({
							fieldId: 'memo'
						}),
						"PurchaseOrderDetails":poItems
					};
				}
				else{
					jsonBody = {
						"NsId": newRecordDetails.id,
						"ReferenceNo":newRecordDetails.getValue({
							fieldId: 'tranid'
						}),
						"PurchaserId": 1, //purchaserEmail,
						"SupplierId": Number(supplierIDRSK),
						"LocationId": Number(locationIDRSK),
						"IntendedLocationId": intendedLocationIDRSK != "" ? Number(intendedLocationIDRSK) : "",
						"OrderDate": formatDate(trandate),
						"POType": potype,		
						"StatusId": 1, /*Number(newRecordDetails.getValue({
							fieldId: 'status'
						}))*/
						"Amount":Number(amount),
						"Description":newRecordDetails.getValue({
							fieldId: 'memo'
						}),
						"PurchaseOrderDetails":poItems
					};
				}
				
				
				
				if(rskID != ""){
					
					// var url='http://123.136.77.34:7280/InventoryReganIntegration/api/PurchaseOrder/Post';	//RSK API URL
					
					var url = createAPI;
					
					var token = null;	// Token if Any
					var headers = {
						"Content-Type": "application/json"
					};
					
					log.debug("jsonBody",JSON.stringify(jsonBody));
					
					// If the API is https compliant
					var response = http.request({
						method: https.Method.POST,
						url: url,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					
					log.debug("response",response);
					log.debug("response JSON",JSON.stringify(response));
					var data = JSON.parse(response.body); 
					var dataCode = response.code; 
					log.debug("data",data);
					log.debug("data.Id",data.Id);
					log.debug("dataCode",dataCode);
					
					if(dataCode == "200"){
						// exit
						
						/* newRecordDetails.setValue({
							fieldId: "custbody_integration_check",	//Flag Checkbox Field
							value: "200 - Successfully Sent to RSK System",				
							ignoreFieldChange: true
						});
						
						
						newRecordDetails.setValue({
							fieldId: "custbody_flagcheck",	//Flag Checkbox Field
							value: false,				
							ignoreFieldChange: true
						});
						
						
						newRecordDetails.setValue({
							fieldId: "custbody_remarkfield",	//Flag Remark Field
							value: "",				
							ignoreFieldChange: true
						}); */
						
						
						var id = record.submitFields({
							type: record.Type.PURCHASE_ORDER,
							id: newRecordDetails.id,
							values: {
								"custbody_integration_check": "200 - Successfully Sent to RSK System",
								"custbody_flagcheck": false,
								"custbody_remarkfield": ""
							},
							options: {
								enableSourcing: false,
								ignoreMandatoryFields : true
							}
						});
						
						/* 
							newRecordDetails.setValue({
							fieldId: "custbody_rskponum",	
							value: data.Id,				
							ignoreFieldChange: true
						});	 */
						
						return;
					}else{
						SetFlag(newRecordDetails, dataCode, data.errors);
					}
					}else{
					return;
				}
			}catch(err){
				log.debug("Error", err);
				SetFlag(newRecordDetails, 400, "Server Error");
				return;
			}
		}
		
		function formatDate(data) {
			// return format.format({value: data, type: format.Type.DATE})  
			
			var dd = data.getDate();
			var mm = data.getMonth() + 1; //January is 0!
			
			var yyyy = data.getFullYear();
			if (dd < 10) {
				dd = '0' + dd;
			} 
			if (mm < 10) {
				mm = '0' + mm;
			} 
			return yyyy + '-' + mm + '-' + dd;
		}
		
		function SetFlag(newRecordDetails, dataCode, message){
			/* var rec = record.load({type : newRecordDetails.type, id : newRecordDetails.id});
			
			newRecordDetails.setValue({
				fieldId: "custbody_flagcheck",	//Flag Checkbox Field
				value: true,
				ignoreFieldChange: true
			});
			newRecordDetails.setValue({
				fieldId: "custbody_remarkfield",	//Flag Remark Field
				value: dataCode + " : " + message,				
				ignoreFieldChange: true
			});
			rec.save(); */
			
			var values = {};
					
			values["custbody_flagcheck"] = true;
			values["custbody_remarkfield"] = dataCode + " : " + JSON.stringify(message);
			values["custbody_integration_check"] = "Error - Please contact admin";
			log.debug("Values", values);
			
			var id = record.submitFields({
				type: newRecordDetails.type,//record.Type.PURCHASE_ORDER,
				id: newRecordDetails.id,
				values: values,
				options: {
					enableSourcing: false,
					ignoreMandatoryFields : true
				}
			});
		}
		
		return {
			afterSubmit: afterSubmit
		};
	});	