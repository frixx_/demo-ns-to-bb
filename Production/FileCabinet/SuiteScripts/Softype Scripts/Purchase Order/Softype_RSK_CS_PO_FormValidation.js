/**
	* @NApiVersion 2.x
	* @NScriptType ClientScript
	* @NModuleScope SameAccount
*/
/***************************************************************************************  
	** Copyright (c) 1998-2018 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
	**                      
	**@Author      :  Amol Jagkar
	**@Dated       :  28 January 2020	
	**@Version     :  2.x
	**@Description :  Set Check based on RFP
***************************************************************************************/
define(['N/currentRecord', 'N/record', 'N/runtime', 'N/search', 'N/https', 'N/format'],
	
    function(currentRecord, record, runtime, search, https, format) {
		
		var STANDALONE_FORM_ID = runtime.getCurrentScript().getParameter('custscript_standalone_po_form');  // 136 - Standalone Purchase Order
		var IMPORT_FORM_ID = runtime.getCurrentScript().getParameter('custscript_import_po_form');  // 146 - Import Purchase Order
		var SERVICES_FORM_ID = runtime.getCurrentScript().getParameter('custscript_services_po_form');  // 138 - Services Purchase Order
				
		function fieldChanged(scriptContext) {
			
			if(scriptContext.fieldId == 'custbody_purchaseordertypelist'){
				var currentRecordObj = scriptContext.currentRecord;
				var currentForm = currentRecordObj.getValue('customform');
				var purchaseOrderType = currentRecordObj.getValue(scriptContext.fieldId);
				
				console.log("currentForm - " + currentForm);
				console.log("purchaseOrderType - " + purchaseOrderType);
				var flag = true;
				if(Number(currentForm) == Number(STANDALONE_FORM_ID)){
					if(purchaseOrderType == 1 || purchaseOrderType == 2){
						flag = true;
					}else{
						flag = false;
					}
				}else if(Number(currentForm) == Number(IMPORT_FORM_ID)){
					if(purchaseOrderType == 3 || purchaseOrderType == 4){
						flag = true;
					}else{
						flag = false;
					}
				}else if(Number(currentForm) == Number(SERVICES_FORM_ID)){
					if(purchaseOrderType == 5 || purchaseOrderType == 6 || purchaseOrderType == 7){
						flag = true;
					}else{
						flag = false;
					}
				}
				console.log("(Number(currentForm) == Number(SERVICES_FORM_ID)) - " + (Number(currentForm) == Number(SERVICES_FORM_ID)));
				console.log("(purchaseOrderType == 5 || purchaseOrderType == 6 || purchaseOrderType == 7) - " + (purchaseOrderType == 5 || purchaseOrderType == 6 || purchaseOrderType == 7));
				console.log("flag - " + flag);
				if(!flag){
					alert("Please select correct Purchase Order Type");
				}
			}
			
		}
		
		function validateField(scriptContext){
			if(scriptContext.fieldId == 'custbody_purchaseordertypelist'){
				var currentRecordObj = scriptContext.currentRecord;
				var currentForm = currentRecordObj.getValue('customform');
				var purchaseOrderType = currentRecordObj.getValue(scriptContext.fieldId);
				
				var flag = true;
				if(currentForm == STANDALONE_FORM_ID){
					if(purchaseOrderType == 1 || purchaseOrderType == 2){
						flag = false;
					}
				}else if(currentForm == IMPORT_FORM_ID){
					if(purchaseOrderType == 3 || purchaseOrderType == 4){
						flag = false;
					}
				}else if(currentForm == SERVICES_FORM_ID){
					if(purchaseOrderType == 5 || purchaseOrderType == 6 || purchaseOrderType == 7){
						flag = false;
					}
				}
				if(!flag){
					alert("Please select correct Purchase Order Type");
				}
				
				return flag;
			}	
		}
		
		return {
			fieldChanged: fieldChanged,
			validateField: validateField
		};
		
	});	