/**
*@NApiVersion 2.x
*@NScriptType UserEventScript
**/

/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : On Selection of Check on Supplier Invoice, Knock-off Vendor Bill by creating JV
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {
			
		var REQUEST_FOR_PAYMENT = 129, ADVANCES_FORM = 120, REGULAR_BILLING = 100;
		var STANDARD_BILL_PAYMENT = 45;
		var AUTOMATED_JV_FORM = 130;
		
		var ADVANCE_TO_EMPLOYEE = 3;
		var ADVANCE_TO_EMPLOYEE_PCF = 4;
		var ADVANCE_TO_BROKER = 5;
		var ADVANCE_TO_SUPPLIER_PARTIAL = 6;
		var STATUS_APPROVED = 2;
		var RFP_JV = 6;
				
		function beforeSubmit(context) {
			
			log.debug('context', context);
			if(context.type == 'delete')
				return;
			
			var newRecordDetails = context.newRecord;
			// log.debug('newRecordDetails', newRecordDetails);
						
			var scriptObj = runtime.getCurrentScript();
			
			REQUEST_FOR_PAYMENT = scriptObj.getParameter({name: 'custscript_si_check_jv_rfp_form'});
			ADVANCES_FORM = scriptObj.getParameter({name: 'custscript_si_check_jv_advances_form'});
			REGULAR_BILLING = scriptObj.getParameter({name: 'custscript_si_check_jv_regular_bill_form'});
			
			AUTOMATED_JV_FORM = scriptObj.getParameter({name: 'custscript_si_check_jv_automated_jv_form'});
			
			STANDARD_BILL_PAYMENT = scriptObj.getParameter({name: 'custscript_si_check_jv_std_bill_form'});
			
			var writeCheck = newRecordDetails.getValue({
								 fieldId: 'custbody_writechecklink'
							 });
							 
			var jvCreated = newRecordDetails.getValue({
								 fieldId: 'custbody_jv_created'
							 });
			
			var customform = newRecordDetails.getValue({
								 fieldId: 'customform'
							 });
							 
			var status = newRecordDetails.getValue({
							 fieldId: 'status'
						 });
			log.debug('status', status);
						 
			var rfpType = newRecordDetails.getValue({
							 fieldId: 'custbody_bill_rfptype'
						 });
						 
			var department = newRecordDetails.getValue({
								fieldId: 'department'
							});		
			var division = newRecordDetails.getValue({
								fieldId: 'class'
							});			 
						 
			if(status == "Paid In Full"){
				return;
			}
			log.debug("rfpType", rfpType);
			log.debug("jvCreated", jvCreated);
			log.debug("writeCheck", writeCheck);
			// log.debug("check if", writeCheck != null && writeCheck != undefined && writeCheck != "");
			log.debug("check if", !!(writeCheck));
			log.debug("2nd if", !jvCreated && (rfpType == ADVANCE_TO_EMPLOYEE || rfpType == ADVANCE_TO_EMPLOYEE_PCF || rfpType == ADVANCE_TO_BROKER || rfpType == ADVANCE_TO_SUPPLIER_PARTIAL));
			
			if(!!(writeCheck)){// != null && writeCheck != undefined && writeCheck != ""){ 
			
				if(!jvCreated && (rfpType == ADVANCE_TO_EMPLOYEE || rfpType == ADVANCE_TO_EMPLOYEE_PCF || rfpType == ADVANCE_TO_BROKER || rfpType == ADVANCE_TO_SUPPLIER_PARTIAL)){// && (customform == REQUEST_FOR_PAYMENT || customform == ADVANCES_FORM || customform == REGULAR_BILLING)){
					var recordid = CreateJV(newRecordDetails, department, division, writeCheck);
					
					log.debug("JV recordid", recordid);
								
					newRecordDetails.setValue({
						fieldId: "custbody_autojvlink",
						value: recordid,				
						ignoreFieldChange: true
					});
					
					newRecordDetails.setValue({
						fieldId: "custbody_jv_created",
						value: true,				
						ignoreFieldChange: true
					});
				}
			}
		}
		
		function CreateJV(newRecordDetails, department, division, writeCheck){
		
		
			log.debug("Department - Division", department + "-" + division);
			
			var newJVRecord = record.create({
				type: "journalentry",
				isDynamic: false
			});	
			
			// log.debug('newRecordDetails.subsidiary', newRecordDetails.getValue({
														 // fieldId: 'subsidiary'
													 // }));		 
													 		 
			var liquidationNumber = newRecordDetails.getValue({
				fieldId: 'custbody_liquidationnumber_bill'
			});
			
			newJVRecord.setValue({
				fieldId: "subsidiary",
				value: 	newRecordDetails.getValue({
							fieldId: 'subsidiary'
						})
			});
			newJVRecord.setValue({
				fieldId: "custbody_journaltypes",
				value: RFP_JV
			});
			
			newJVRecord.setValue({
				fieldId: "customform",
				value: AUTOMATED_JV_FORM
			});
			
			newJVRecord.setValue({
				fieldId: "approvalstatus",
				value: STATUS_APPROVED
			});			
			
			if(liquidationNumber!=null || liquidationNumber!=""){
				newJVRecord.setValue({
					fieldId: "custbody_liquidationnumber_bill",
					value: liquidationNumber
				});			
			}
			
			newJVRecord.setValue({
				fieldId: "custbody_billoflasdingnum",
				value: newRecordDetails.getValue({
										fieldId: 'custbody_billoflasdingnum'
									})
			});
			/* newJVRecord.setValue({
				fieldId: "class",
				value: newRecordDetails.getValue({
										fieldId: 'class'
									})
			});
			newJVRecord.setValue({
				fieldId: "department",
				value: newRecordDetails.getValue({
										fieldId: 'department'
									})
			}); */
			newJVRecord.setValue({
				fieldId: "location",
				value: newRecordDetails.getValue({
										fieldId: 'location'
									})
			});
			newJVRecord.setValue({
				fieldId: "custbody_ship_num",
				value: newRecordDetails.getValue({
										fieldId: 'custbody_ship_num'
									})
			});
			newJVRecord.setValue({
				fieldId: "custbody_letterofcreditnum",
				value: newRecordDetails.getValue({
										fieldId: 'custbody_letterofcreditnum'
									})
			});
			newJVRecord.setValue({
				fieldId: "cseg_type_of_trans",
				value: newRecordDetails.getValue({
										fieldId: 'cseg_type_of_trans'
									})
			});
			newJVRecord.setValue({
				fieldId: "custbody_bill_rfptype",
				value: newRecordDetails.getValue({
							 fieldId: 'custbody_bill_rfptype'
						 })
			});
						
			/*newJVRecord.setValue({
				fieldId: "currency",
				value: newRecordDetails.currency
			});*/
			
			newJVRecord.setValue({
				fieldId: "custbody_createdfrombill",
				value: newRecordDetails.id
			});
			
			var debitPayee = newRecordDetails.getValue({
								 fieldId: 'entity'
							 });
			var debitAccount = newRecordDetails.getValue({
								 fieldId: 'account'
							 });
			var debitAmount = newRecordDetails.getValue({
								 fieldId: 'usertotal'
							 });
							 
			var discount = newRecordDetails.getValue({
							 fieldId: 'discountamount'
						 });
						 
			debitAmount = (debitAmount - discount).toFixed(2);
			
			log.debug('debitPayee',debitPayee);
			log.debug('debitAccount',debitAccount);
			log.debug('debitAmount',debitAmount);
			
			//Debit - Supplier Invoice
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'account',
				line: 0,
				value: debitAccount
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'entity',
				line: 0,
				value: debitPayee
			});							
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'debit',
				line: 0,
				value: debitAmount
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'credit',
				line: 0,
				value: 0
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'department',
				line: 0,
				value: department
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'class',
				line: 0,
				value: division
			});
			
			var recordCheck = record.load({
				type : "check",
				id : writeCheck,
				isDynamic: false				
			})
			
			
			var creditPayee = recordCheck.getValue({
								 fieldId: 'entity'
							 });
			var creditAccount = recordCheck.getSublistValue({
									sublistId: 'expense',
									fieldId: 'account',
									line: 0
								}); 
			var creditAmount = debitAmount; 
			 /*var creditAmount = recordCheck.getValue({
								 fieldId: 'usertotal'
							 }); */
			
			
			log.debug('creditPayee',creditPayee);
			log.debug('creditAccount',creditAccount);
			log.debug('creditAmount',creditAmount);
			
			//Credit - Write Check
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'account',
				line: 1,
				value: creditAccount
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'entity',
				line: 1,
				value: creditPayee
			});							
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'debit',
				line: 1,
				value: 0
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'credit',
				line: 1,
				value: creditAmount
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'department',
				line: 1,
				value: department
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'class',
				line: 1,
				value: division
			});
			
			var recordid = newJVRecord.save();
			log.debug("recordid JV",recordid);
			
			return recordid;
		}

		function afterSubmit(context) {
					
			if(context.type == 'delete')
				return;
			
			var newRecordDetails = context.newRecord;
			// log.debug('newRecordDetails.id', newRecordDetails.id);
			
			var scriptObj = runtime.getCurrentScript();
			
			REQUEST_FOR_PAYMENT = scriptObj.getParameter({name: 'custscript_si_check_jv_rfp_form'});
			ADVANCES_FORM = scriptObj.getParameter({name: 'custscript_si_check_jv_advances_form'});
			REGULAR_BILLING = scriptObj.getParameter({name: 'custscript_si_check_jv_regular_bill_form'});
			
			AUTOMATED_JV_FORM = scriptObj.getParameter({name: 'custscript_si_check_jv_automated_jv_form'});
			
			STANDARD_BILL_PAYMENT = scriptObj.getParameter({name: 'custscript_si_check_jv_std_bill_form'});
						
			var newRecordDetails = record.load({type:"vendorbill",id:newRecordDetails.id});
			
			// log.debug('newRecordDetails', newRecordDetails);
			
			var customform = newRecordDetails.getValue({
				fieldId: 'customform'
			});		
			
			var status = newRecordDetails.getValue({
							 fieldId: 'status'
						 });
			// log.debug('status', status);
						 
			var doc = newRecordDetails.getValue({
				fieldId: 'custbody_autojvlink'
			});
			
			var location = newRecordDetails.getValue({
								fieldId: 'location'
							});		
			var department = newRecordDetails.getValue({
								fieldId: 'department'
							});		
			var division = newRecordDetails.getValue({
								fieldId: 'class'
							});		
						 
			log.debug('doc', doc);
			
			if(doc == "" || doc == null || doc == undefined){
				return;
			}
			
			try{
				var rfpType = newRecordDetails.getValue({
					fieldId: 'custbody_bill_rfptype'
				});
			
				if(status === "Open" && (rfpType == ADVANCE_TO_EMPLOYEE || rfpType == ADVANCE_TO_EMPLOYEE_PCF || rfpType == ADVANCE_TO_BROKER ||rfpType == ADVANCE_TO_SUPPLIER_PARTIAL)){// && (customform == REQUEST_FOR_PAYMENT || customform == ADVANCES_FORM || customform == REGULAR_BILLING)){
				
					// log.debug('Bill Creation');
					var billRecord = record.transform({
						fromType: record.Type.VENDOR_BILL,
						fromId: newRecordDetails.id,
						toType: record.Type.VENDOR_PAYMENT,
						isDynamic: false,
					});
					
					billRecord.setValue({
						fieldId: 'customform',
						value: STANDARD_BILL_PAYMENT
					});
					
					billRecord.setValue({
						fieldId: 'location',
						value: location
					});
					
					billRecord.setValue({
						fieldId: 'department',
						value: department
					});
					
					billRecord.setValue({
						fieldId: 'class',
						value: division
					});
					
					var numLines = billRecord.getLineCount({
						sublistId: 'apply'
					});
					
					
					for(var i = 0; i < numLines; i++){
												
						var docFromLine = billRecord.getSublistValue({
							sublistId: 'apply',
							fieldId: 'doc',
							line: i
						});
						
						if(docFromLine === doc){
							billRecord.setSublistValue({
								sublistId: 'apply',
								fieldId: 'apply',
								line: i,
								value: true
							});
							billRecord.setSublistValue({
								sublistId: 'apply',
								fieldId: 'department',
								line: i,
								value: department
							});
							billRecord.setSublistValue({
								sublistId: 'apply',
								fieldId: 'class',
								line: i,
								value: division
							});
							break;
						}
					}
					
					
					var recordid = billRecord.save({
						enableSourcing: true,
						ignoreMandatoryFields: true
					});
					
					log.debug("billRecord", recordid);
					
				}
			}catch(err){
				log.debug("Error", err);
			}		 
		}
				
		return {
			beforeSubmit: beforeSubmit,
			afterSubmit: afterSubmit,
		};
	});