/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam
 ** @Dated       :  10th September, 2019
 ** @Version     :  2.x
 ** @Description :  Suitelet for displaying all the Purchase Requisition from the saved search 'Purchase Requisition Canvassing'.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/ui/serverWidget', 'N/search', 'N/record', 'N/runtime', 'N/redirect'],

    function(serverWidget, search, record, runtime, redirect) {

        var SAVED_SEARCH_ID; // Saved Search Name - 'Purchase Requisition Canvassing'. 
        var PAGE_SIZE = 100;


        function onRequest(context) {

            if (context.request.method == 'GET') {

                SAVED_SEARCH_ID = runtime.getCurrentScript().getParameter("custscript_saved_search_id");


                var action = context.request.parameters.action;
                var canvasser = context.request.parameters.canvasser;
                var subsidiary = context.request.parameters.subsidiary;
                var department = context.request.parameters.department;
				var tradeAndNonTrade = context.request.parameters.tradeAndNonTrade;
                var pageId = parseInt(context.request.parameters.page);


                var form = serverWidget.createForm({
                    title: 'Purchase Requisition'
                });


                var CLIENT_SCRIPT_FILE_ID = callClientScript();
                form.clientScriptFileId = CLIENT_SCRIPT_FILE_ID;


                /** Adding Body Fields - Start **/

                var pageCanvasser = form.addField({
                    id: 'custpage_canvasser',
                    type: serverWidget.FieldType.SELECT,
                    label: 'Canvasser',
                    source: 'employee'
                });


                if (canvasser) {

                    pageCanvasser.defaultValue = canvasser;
                }

                // if (canvasser) {

                // pageCanvasser.defaultValue = currentUserId;
                // }
                pageCanvasser.isMandatory = true;


                var pageSubsidiary = form.addField({
                    id: 'custpage_subsidiary',
                    type: serverWidget.FieldType.SELECT,
                    label: 'Subsidiary',
                    source: 'subsidiary'
                });


                if (subsidiary) {

                    pageSubsidiary.defaultValue = subsidiary;

                }

                // var pageDepartment = form.addField({
                    // id: 'custpage_department',
                    // type: serverWidget.FieldType.SELECT,
                    // label: 'Departments',
                    // source: 'department'
                // });

                // if (department) {

                    // pageDepartment.defaultValue = department;

                // }
				
				var pageTradeAndNonTrade = form.addField({
                    id: 'custpage_tradeandnontrade',
                    type: serverWidget.FieldType.SELECT,
                    label: 'Trade/ Non-Trade',
                    source: 'customlist_tradenontrade_req'
                });

                if (tradeAndNonTrade) {

                    pageTradeAndNonTrade.defaultValue = tradeAndNonTrade;

                }
				pageTradeAndNonTrade.isMandatory = true;

                /** Adding Body Fields - End **/



                /** Adding Sublist Fields - Start **/

                var sublist = form.addSublist({
                    id: 'custpage_sublistid',
                    type: serverWidget.SublistType.LIST,
                    label: 'Canvassing Screen'

                });

                sublist.addMarkAllButtons();

                sublist.addField({
                    id: 'custpage_checkbox',
                    label: 'Mark',
                    type: serverWidget.FieldType.CHECKBOX,
                });

                sublist.addField({
                    id: 'custpage_item',
                    label: 'ITEM',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_itemvalue',
                    label: 'ITEM VALUE',
                    type: serverWidget.FieldType.TEXT
                }).updateDisplayType({

                    displayType: serverWidget.FieldDisplayType.HIDDEN

                });

                sublist.addField({
                    id: 'custpage_prquantity',
                    label: 'PR QUANTITY',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_prno',
                    label: 'PR NUMBER',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_canvasser',
                    label: 'CANVASSER',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_department',
                    label: 'DEPARTMENT',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_departmentvalue',
                    label: 'DEPARTMENT VALUE',
                    type: serverWidget.FieldType.TEXT
                }).updateDisplayType({

                    displayType: serverWidget.FieldDisplayType.HIDDEN

                });
				
				sublist.addField({
                    id: 'custpage_division',
                    label: 'DIVISION',
                    type: serverWidget.FieldType.TEXT
                });
				
				sublist.addField({
                    id: 'custpage_divisionvalue',
                    label: 'DIVISION VALUE',
                    type: serverWidget.FieldType.TEXT
                }).updateDisplayType({

                    displayType: serverWidget.FieldDisplayType.HIDDEN

                });

                

                sublist.addField({
                    id: 'custpage_prlocation',
                    label: 'LOCATION',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_prlocationvalue',
                    label: 'LOCATION VALUE',
                    type: serverWidget.FieldType.TEXT
                }).updateDisplayType({

                    displayType: serverWidget.FieldDisplayType.HIDDEN

                });

                sublist.addField({
                    id: 'custpage_subsidiary',
                    label: 'SUBSIDIARY',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_subsidiaryvalue',
                    label: 'SUBSIDIARY VALUE',
                    type: serverWidget.FieldType.TEXT
                }).updateDisplayType({

                    displayType: serverWidget.FieldDisplayType.HIDDEN

                });

                sublist.addField({
                    id: 'custpage_remark',
                    label: 'REMARKS',
                    type: serverWidget.FieldType.TEXT
                });

                sublist.addField({
                    id: 'custpage_internalid',
                    label: 'INTERNAL ID',
                    type: serverWidget.FieldType.TEXT
                }).updateDisplayType({

                    displayType: serverWidget.FieldDisplayType.HIDDEN

                });

                sublist.addField({
                    id: 'custpage_lineid',
                    label: 'LINE ID',
                    type: serverWidget.FieldType.TEXT
                }).updateDisplayType({

                    displayType: serverWidget.FieldDisplayType.HIDDEN

                });
				
				sublist.addField({
                    id: 'custpage_tradeandnontrade',
                    label: 'TRADE AND NON-TRADE',
                    type: serverWidget.FieldType.TEXT

                });
                /** Adding Sublist Fields - End **/


                form.addButton({
                    id: 'custpage_cancelButton',
                    label: 'Cancel',
                    functionName: 'cancel()'

                });
                form.addSubmitButton({
                    label: 'Submit'
                });



                /** Pagination -- Start **/

                //Run search and determine page count
                var retrieveSearch = runSearch(SAVED_SEARCH_ID, PAGE_SIZE, canvasser, subsidiary);

                if (retrieveSearch.count > 0) {

                    var pageCount = parseInt(retrieveSearch.count / PAGE_SIZE);

                    /** If the remainder is not equal to zero, increase the page count. **/
                    if (parseInt(retrieveSearch.count % PAGE_SIZE) != 0) {
                        pageCount++;

                    }

                    //Set pageId to correct value if out of index
                    if (!pageId || pageId == '' || pageId < 0) {

                        pageId = 0;

                    } else if (pageId >= pageCount) {

                        pageId = pageCount - 1;

                    }

                    //Add drop-down and options to navigate to specific page
                    var selectOptions = form.addField({
                        id: 'custpage_pageid',
                        label: 'Next Page',
                        type: serverWidget.FieldType.SELECT
                    });

                    for (i = 0; i < pageCount; i++) {

                        if (i == pageId) {

                            selectOptions.addSelectOption({
                                value: 'pageid_' + i,
                                text: ((i * PAGE_SIZE) + 1) + ' - ' + ((i + 1) * PAGE_SIZE),
                                isSelected: true
                            });

                        } else {
                            selectOptions.addSelectOption({
                                value: 'pageid_' + i,
                                text: ((i * PAGE_SIZE) + 1) + ' - ' + ((i + 1) * PAGE_SIZE)
                            });
                        }


                    }
                    /** Pagination -- End **/




                    /** Setting Sublist Value -- Start **/

                    var addResults = getAllPurchaseRequisition(retrieveSearch, pageId);

                    var j = 0;


                    addResults.forEach(function(result) {


                        var item = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_item',
                            line: j,
                            value: formatData(result.item)

                        });

                        var itemValue = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_itemvalue',
                            line: j,
                            value: formatData(result.itemValue)

                        });

                        var prQuantity = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_prquantity',
                            line: j,
                            value: formatData(result.prQuantity)

                        });

                        var prNumber = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_prno',
                            line: j,
                            value: formatData(result.prNo)

                        });

                        var canvasser = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_canvasser',
                            line: j,
                            value: formatData(result.canvasser)

                        });

                        var department = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_department',
                            line: j,
                            value: formatData(result.department)

                        });

                        var prDepartmentValue = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_departmentvalue',
                            line: j,
                            value: formatData(result.prDepartmentValue)

                        });
						
						  var prDivision = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_division',
                            line: j,
                            value: formatData(result.prDivision)

                        });
						
						
						  var prDivisionValue = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_divisionvalue',
                            line: j,
                            value: formatData(result.prDivisionValue)

                        });

                        var subsidiary = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_subsidiary',
                            line: j,
                            value: formatData(result.subsidiary)

                        });

                        var prSubsidiaryValue = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_subsidiaryvalue',
                            line: j,
                            value: formatData(result.prSubsidiaryValue)

                        });

                        var remark = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_remark',
                            line: j,
                            value: formatData(result.remark)

                        });

                        var prLocation = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_prlocation',
                            line: j,
                            value: formatData(result.prLocation)

                        });

                        var prLocationValue = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_prlocationvalue',
                            line: j,
                            value: formatData(result.prLocationValue)

                        });

                        var internalId = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_internalid',
                            line: j,
                            value: formatData(result.internalId)

                        });

                        var lineId = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_lineid',
                            line: j,
                            value: formatData(result.lineId)

                        });
					
						
                        var tradeandnontrade = sublist.setSublistValue({
                            sublistID: 'custpage_sublistid',
                            id: 'custpage_tradeandnontrade',
                            line: j,
                            value: formatData(result.tradeAndNonTrade)

                        });



                        j++;


                    });

                    /** Setting Sublist Value -- End **/



                }

                context.response.writePage(form);


            } else {


                var SUBLIST_JSON = [];
                var finalArray = [];
                var temp = {};
                var PR_JSON = [];



                var canvasser = context.request.parameters.custpage_canvasser;
                var subsidiary = context.request.parameters.custpage_subsidiary;
				var tradeAndNonTrade = context.request.parameters.custpage_tradeandnontrade;


                var lineCount = context.request.getLineCount({
                    group: 'custpage_sublistid'

                });



                /** Looping through the sublist Line Count to get the values of 
					the checked checkbox.
					
				**/
                for (var i = 0; i < lineCount; i++) {

                    var checked = context.request.getSublistValue({
                        group: 'custpage_sublistid',
                        name: 'custpage_checkbox',
                        line: i
                    });


                    if (checked == 'T') {

                        var itemValue = context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_itemvalue',
                            line: i
                        });

                        var prQuantity = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_prquantity',
                            line: i
                        }));

                        var prInternalId = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_internalid',
                            line: i
                        }));

                        var lineId = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_lineid',
                            line: i
                        }));

                        var prLocationValue = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_prlocationvalue',
                            line: i
                        }));

                        var prDepartmentValue = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_departmentvalue',
                            line: i
                        }));
						
						
                       
                        var prSubsidiaryValue = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_subsidiaryvalue',
                            line: i
                        }));
						
						var prDivision = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_division',
                            line: i
                        }));
						
						 var prDivisionValue = Number(context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_divisionvalue',
                            line: i
                        }));
						
						 var remarks = context.request.getSublistValue({
                            group: 'custpage_sublistid',
                            name: 'custpage_remark',
                            line: i
                        });


                        SUBLIST_JSON.push({

                            itemValue: itemValue,
                            prQuantity: prQuantity,
                            prInternalId: prInternalId,
                            prLocationValue: prLocationValue,
                            prDepartmentValue: prDepartmentValue,
                            prSubsidiaryValue: prSubsidiaryValue,
							prDivisionValue:prDivisionValue,
							remarks:remarks

                        });


                        PR_JSON.push(prInternalId);


                        var filteredArray = unique(PR_JSON)


                    }


                }
                log.audit('SUBLIST_JSON', SUBLIST_JSON);




                /**
					Creating Parent Record 'customrecord_canvassing_parent_record' to set the 
					canvasser unique requisition no on the body fields and 
					items, pr quantity, location, department, requisition no.
					on the line level-- Start
				
				**/
                var parentCustomRecord = record.create({
                    type: 'customrecord_canvassing_parent_record',
                    isDynamic: true

                });

                parentCustomRecord.setValue({
                    fieldId: 'custrecord_canvasser',
                    value: canvasser
                });
				
				parentCustomRecord.setValue({
                    fieldId: 'custrecord_canvas_trade_nontrade',
                    value: tradeAndNonTrade
                });

                parentCustomRecord.setValue({
                    fieldId: 'custrecord_pr_number',
                    value: filteredArray

                });


                for (var f = 0; f < SUBLIST_JSON.length; f++) {


                    parentCustomRecord.selectNewLine({
                        sublistId: 'recmachcustrecord_link_canvassing'
                    });


                    parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_item_canvas',
                        value: SUBLIST_JSON[f].itemValue
                    });

                    parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_qty_canvas',
                        value: SUBLIST_JSON[f].prQuantity
                    });

                    parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_pr_no',
                        value: SUBLIST_JSON[f].prInternalId
                    });

                    parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_original_pr_qty',
                        value: true
                    });

                    parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_pr_location',
                        value: SUBLIST_JSON[f].prLocationValue
                    });

                    parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_pr_department',
                        value: SUBLIST_JSON[f].prDepartmentValue
                    });

                    parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_suppliersub',
                        value: SUBLIST_JSON[f].prSubsidiaryValue
                    });
					
					parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_division',
                        value: SUBLIST_JSON[f].prDivisionValue
                    });
					
					parentCustomRecord.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_remarks',
                        value: SUBLIST_JSON[f].remarks
                    });

                    parentCustomRecord.commitLine({
                        sublistId: 'recmachcustrecord_link_canvassing'
                    });




                }


                var parentInternalId = parentCustomRecord.save({
                    ignoreMandatoryFields: true

                })
                log.audit('Saved Parent Custom Record ', parentInternalId);

                redirect.toRecord({
                    type: 'customrecord_canvassing_parent_record',
                    id: parentInternalId,

                });

                /** Creating Parent Record -- End**/

                var scriptObj = runtime.getCurrentScript();

                log.audit("Remaining Governance Units", scriptObj.getRemainingUsage());


            }

            /** ELSE -- End**/




            /** Loading and running the Saved Search -- 'Purchase Requisition Canvassing' **/
            function runSearch(searchId, searchPageSize, canvasser, subsidiary) {

                var filterobj = [];
                var columnobj = [];

                var mySearch = search.load({
                    id: SAVED_SEARCH_ID

                });


                filterobj = mySearch.filters;
                //columnobj = mySearch.columns;


                /** 
					Pushing the filter of canvasser, subsidiary, department(if it is selected) to 
					filter out the Purchase Requitions .
						
				**/
                if (action == 'getFilters') {

                    if (canvasser != null && canvasser != "") {

                        filterobj.push(search.createFilter({
                            name: 'custbody_canvasser_pr',
                            operator: 'is',
                            values: canvasser
                        }));
                    }
                    if (subsidiary != null && subsidiary != "") {

                        filterobj.push(search.createFilter({
                            name: 'subsidiary',
                            operator: 'is',
                            values: subsidiary
                        }));

                    }

                    // if (department != null && department != "") {

                        // filterobj.push(search.createFilter({
                            // name: 'department',
                            // operator: 'is',
                            // values: department
                        // }));

                    // }
					
					 if (tradeAndNonTrade != null && tradeAndNonTrade != "") {

                        filterobj.push(search.createFilter({
                            name: 'custbody_tradenontrade',
                            operator: 'is',
                            values: tradeAndNonTrade
                        }));

                    }
                }

                var resultSet = mySearch.run();

                var firstResult = resultSet.getRange({
                    start: 0,
                    end: 1000
                });



                var result = mySearch.runPaged({
                    pageSize: searchPageSize
                });


                return result;

            }




            /** Getting all the Purchase Requisition from the Saved Search -- Purchase Requisition Canvassing **/
            function getAllPurchaseRequisition(pagedData, pageIndex) {


                var searchPage = pagedData.fetch({
                    index: pageIndex
                });

                var transactionJson = [];

                searchPage.data.forEach(function(result) {


                    var item = result.getText({
                        name: 'item'
                    });

                    var itemValue = result.getValue({
                        name: 'item'
                    });

                    var prQuantity = result.getValue({
                        name: 'formulanumeric'
                    });

                    var prNo = result.getValue({
                        name: 'transactionname'
                    });

                    var canvasser = result.getText({
                        name: 'custbody_canvasser_pr'
                    });

                    var department = result.getText({
                        name: 'department'
                    });

                    var internalId = result.getValue({
                        name: 'internalid'
                    });

                    var lineId = result.getValue({
                        name: 'line'
                    });

                    var subsidiary = result.getText({
                        name: 'subsidiary'
                    });

                    var remark = result.getValue({
                        name: 'custcol_remark_line'
                    });

                    var prLocation = result.getText({
                        name: 'location'
                    });

                    var prLocationValue = result.getValue({
                        name: 'location'
                    });

                    var prDepartmentValue = result.getValue({
                        name: 'department'
                    });

                    var prSubsidiaryValue = result.getValue({
                        name: 'subsidiary'
                    });
					
					var tradeAndNonTrade = result.getText({
                        name: 'custbody_tradenontrade'
                    });
					
					
					var prDivision = result.getText({
                        name: 'class'
                    });
					
					
					var prDivisionValue = result.getValue({
                        name: 'class'
                    });
					
					
					log.debug('remark',remark);
					


                    transactionJson.push({

                        item: item,
                        itemValue: itemValue,
                        prQuantity: prQuantity,
                        prNo: prNo,
                        canvasser: canvasser,
                        department: department,
                        internalId: internalId,
                        lineId: lineId,
                        subsidiary: subsidiary,
                        remark: remark,
                        prLocation: prLocation,
                        prLocationValue: prLocationValue,
                        prDepartmentValue: prDepartmentValue,
                        prSubsidiaryValue: prSubsidiaryValue,
						tradeAndNonTrade:tradeAndNonTrade,
						prDivision:prDivision,
						prDivisionValue:prDivisionValue


                    });



                });
				 log.audit('transactionJson', transactionJson)

                return transactionJson;

                log.audit('transactionJson', transactionJson)



            }



            /** This function returns " " (with space) if the data in null or empty('')
				or else will give an error.
		
			**/
            function formatData(data) {

                if (data == '' || data == null) {

                    return " ";
                } else {

                    return data;
                }


            }


            /** This function returns unique element from the Array **/
            function unique(PR_ID_ARRAY) {

                var arr = []
                for (var i = 0; i < PR_ID_ARRAY.length; i++) {

                    if (arr.indexOf(PR_ID_ARRAY[i]) === -1 && PR_ID_ARRAY[i] !== '') {

                        arr.push(PR_ID_ARRAY[i]);
                    }
                }
                return arr;
            }




            function callClientScript() {

                var fileFilters = new Array();
                var fileColumns = new Array();

                fileFilters.push(search.createFilter({
                    name: 'name',
                    operator: 'is',
                    values: 'Softype_RSK_CS_Purchase_Requisition.js'
                }));

                var clientSearch = search.create({
                    type: 'file',
                    filters: fileFilters
                }).run().getRange(0, 1000);


                var clientScriptId = clientSearch[0].id;

                return clientScriptId;

            }


        }


        return {

            onRequest: onRequest

        };

    });