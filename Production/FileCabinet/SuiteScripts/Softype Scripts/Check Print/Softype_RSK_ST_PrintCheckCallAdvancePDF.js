/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11th July 2018     Raksha Singh
 *
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information"). 
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.                  
 **                       
 **@Author      :  Raksha Singh
 **@Dated       :  07/16/2018
 **@Version     :  1.0
 **@Description :   Suitelet to call the advance pdf
 ***************************************************************************************/
function printCheck(request, response){

	nlapiLogExecution('debug','Start');
	var recordId = request.getParameter('recordId');
	nlapiLogExecution('debug','recid',recordId);
    var checkObj = nlapiLoadRecord('check',recordId);
  
 // var location = nlapiLoadRecord('location',opportunityObj.getFieldValue('location'));
  
   // var companyInformation = nlapiLoadRecord('company',1);
  
  var renderer = nlapiCreateTemplateRenderer();
  
  
  // Loading txt file 
  //the advance pdf is stored in a text file of which internal id is loaded
  var loadFile = nlapiLoadFile(29073);
  renderer.setTemplate(loadFile.getValue());
  nlapiLogExecution('debug','loadFile');
  renderer.addRecord('record',checkObj);
   //renderer.addRecord('location',location);
   // renderer.addRecord('companyInformation',companyInformation);
  
 nlapiLogExecution('debug',loadFile.getValue());
  var xml = renderer.renderToString();
  var file = nlapiXMLToPDF(xml); 
  nlapiLogExecution('debug','XML String' );
  //it calls the advance pdf which contains the html code
  response.setContentType('PDF','Softype_GraphicStar_PrintCheck.pdf ','inline');
  response.write(file);
 }	
		

