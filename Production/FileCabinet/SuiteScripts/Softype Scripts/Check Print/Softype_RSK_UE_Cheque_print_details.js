/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
 
 /***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information"). 
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.                  
 **                       
 **@Author      :  Rajendran 
 **@Dated       :  5/16/2018
 **@Version     :  2.0
 **@Description :  This script is for the print of converting number to words and cheque date into specified check format
 ***************************************************************************************/
 
 
define(['N/search','N/record',"N/log","N/format"],

function(search,record,log,format) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

		var newRecord = scriptContext.newRecord;
		var checkDate = newRecord.getValue('custbody_cheque_print_date');
		log.debug('Check date',checkDate);
		if(checkDate == '' || checkDate == null){
			var newDate = new Date();
			log.debug('New Date',newDate);
			var currentYear = newDate.getFullYear();
			log.debug('Current year',currentYear);
			var currentMonth = newDate.getMonth();
			log.debug('Curr Month',currentMonth);
			var currentDate = newDate.getUTCDate();
			log.debug('Curr Date',currentDate);
			var monthArr = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
			var dateString = "";
			dateString += monthArr[currentMonth]+' '+currentDate+','+currentYear;
			log.debug('Date String',dateString);
			newRecord.setValue({
				fieldId:'custbody_cheque_print_date',
				value:dateString
			});
		}
	}		
    function afterSubmit(scriptContext) {
		if(scriptContext.type == scriptContext.UserEventType.DELETE  || scriptContext.type == scriptContext.UserEventType.XEDIT){
				return;
		}

		//if(scriptContext.type == scriptContext.UserEventType.CREATE)
		try{
    		
		//For Amt in Words 
		
		var th = ['','Thousand','Million', 'Billion','Trillion'];
		var dg = ['Zero','One','Two','Three','Four', 'Five','Six','Seven','Eight','Nine'];
		var tn = ['Ten','Eleven','Twelve','Thirteen', 'Fourteen','Fifteen','Sixteen', 'Seventeen','Eighteen','Nineteen'];
		var tw = ['Twenty','Thirty','Forty','Fifty', 'Sixty','Seventy','Eighty','Ninety'];//Eighty Eigthy
		
    	var monthList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    	var currentRecordObj = scriptContext.newRecord;
    	var currentRecordId = currentRecordObj.id;
		var currentRecordType = scriptContext.newRecord.type;
    	
    	
    	var trandate = currentRecordObj.getValue({fieldId:'trandate'});
		//var trandate1=gettingexactdate(trandate);
		//log.debug('trandate1======>',trandate1);
    	var total_amt = currentRecordObj.getValue({fieldId:'total'});
    	
		total_amt = total_amt.toString(); // to string to remove commas 
		
		total_amt = total_amt.replace(/\,/g,'');
		
		
		var inWords = number2text(total_amt);
    	 
       // var fullDate = nlapiStringToDate(trandate); 
	   
		 var fullDate = format.parse({
						value: trandate,
						type: format.Type.DATE
					});
	   // log.debug('details','fullDate(parse): '+fullDate);
      
        var date = 	fullDate.getDate();
		
		var month = monthList[fullDate.getMonth()];

		var year = fullDate.getFullYear();
		
		// log.debug('full date',month+' '+date+', '+year);
		
		//month = month.substr(0,3);
		
		var chequeDate = month+' '+date+', '+year;
		//log.debug('chequeDate',chequeDate);
		
		var nameprinted = currentRecordObj.getValue({
	    fieldId: 'custbody_nameon_checkprint'
	    });
 
        log.debug('ifnameprinted',inWords+', '+chequeDate);

		if(inWords && nameprinted){
		   var cheque_submit5 = record.submitFields({
													type:currentRecordType,
													id:currentRecordId,
													values:{
														'custbody_cashsale_totalword':inWords,
														'custbody_nameon_checkprint':chequeDate
													}
												});
		}
         if(!nameprinted){

			var getpayeeid = currentRecordObj.getValue({
			fieldId: 'entity'
			});
			log.debug('details','getpayeeid: '+getpayeeid);
			
			if(getpayeeid!=null){
				var companyname = '';
				var usertype ='';
				var checkpayeeprintname ='';
				usertype = search.lookupFields({
								type: search.Type.ENTITY,
								id: getpayeeid,
								columns: ['type']
							});
	log.debug('usertype.type[0].value',usertype.type[0]);
				if(usertype.type[0].value == 'Employee'){

							var userfield = search.lookupFields({
								type: 'employee',
								id: getpayeeid,
								columns: ['firstname']
							});
				checkpayeeprintname = userfield.firstname;	
				}
				else if(usertype.type[0].value == 'Vendor'){

							var userfield = search.lookupFields({
								type: 'vendor',
								id: getpayeeid,
								columns: ['printoncheckas','companyname']
							});
				checkpayeeprintname = userfield.printoncheckas;	
				companyname = userfield.companyname;
				//if print on check as is blank then take company name as print name 
					if(checkpayeeprintname == '' || checkpayeeprintname == null)
						checkpayeeprintname = companyname;
					
				}
				else if(usertype.type[0].text == 'Customer'){

							var userfield = search.lookupFields({
								type: 'customer',
								id: getpayeeid,
								columns: ['companyname']
							});
				checkpayeeprintname = userfield.printoncheckas;	
				companyname = userfield.companyname;
				//if print on check as is blank then take company name as print name
				if(checkpayeeprintname == '' || checkpayeeprintname == null)
						checkpayeeprintname = companyname;
				}

		log.debug('nonameprinted',inWords+', '+checkpayeeprintname);
		log.debug('checkpayeeprintname',checkpayeeprintname);
		if(inWords && checkpayeeprintname){
			var cheque_submit = record.submitFields({
													type:currentRecordType,
													id:currentRecordId,
													values:{
														'custbody_cashsale_totalword':inWords,
														'custbody_nameon_checkprint':chequeDate,
													}
												});
												
			}							
												
		  } 
	   }
	}
	catch(e){
		log.error('Error Occured',e);
	}
  }

/* This function formats the Today's Date in the format m_d_yyyy  //DIVYA
        function gettingexactdate(todaydate) {

            var actual_date = format.format({
                value: todaydate,
                type: format.Type.DATETIME,
                timezone: format.Timezone.timezone
            });

            var splitarray = actual_date.split(' ');
            //log.emergency('splitarray', splitarray);

            var actual_split_date = splitarray[0];
            //log.emergency('actual_split_date', actual_split_date);

            var date = actual_split_date.split('/');
            //log.emergency('date', date);

            var currentMonth = date[0];
            
            if(currentMonth < Number(10)){
            	
            	currentMonth = '0'+ Number(currentMonth);
            	
            }else{
            	
            	currentMonth = currentMonth;
            }
			

            var ndDate = date[1];
                   if(ndDate < Number(10)){
            	
            	ndDate = '0'+ Number(ndDate);
            	
            }else{
            	
            	ndDate = ndDate;
            }
            var currentYear = date[2];

            var formatteddate = currentMonth +"/"+ ndDate +"/"+ currentYear;

            return formatteddate;

        }*/
function number2text(value) {
    var fraction = Math.round(frac(value)*100);
    var f_text  = "";
log.debug("fraction=====>",fraction);
log.debug("value=====>",value);

    if(fraction > 0) {
		// f_text = convert_number(fraction)+" Cents"; //Changes as Moses req 
		// f_text = convert_number(fraction)+" Centvos"; //Changes as Moses req 
		f_text = convert_number(fraction)+" Centavos"; //Divya
		log.debug("f_text=====>",f_text);
		return numToWords(value)+' Pesos and '+f_text+" Only";
    }
    return numToWords(value)+' Pesos'+f_text+" Only";
}

function frac(f) {
    return f % 1;
}

function convert_number(number) {
    if (number < 0) {
        console.log("Number Must be greater than zero = " + number);
        return "";
    }
    if (number > 100000000000000000000) {
        console.log("Number is out of range = " + number);
        return "";
    }
    if (!is_numeric(number)) {
        console.log("Not a number = " + number);
        return "";
    }

    var quintillion = Math.floor(number / 1000000000000000000); /* quintillion */
    number -= quintillion * 1000000000000000000;
    var quar = Math.floor(number / 1000000000000000); /* quadrillion */
    number -= quar * 1000000000000000;
    var trin = Math.floor(number / 1000000000000); /* trillion */
    number -= trin * 1000000000000;
    var Gn = Math.floor(number / 1000000000); /* billion */
    number -= Gn * 1000000000;
    var million = Math.floor(number / 1000000); /* million */
    number -= million * 1000000;
    var Hn = Math.floor(number / 1000); /* thousand */
    number -= Hn * 1000;
    var Dn = Math.floor(number / 100); /* Tens (deca) */
    number = number % 100; /* Ones */
    var tn = Math.floor(number / 10);
    var one = Math.floor(number % 10);
    var res = "";

    if (quintillion > 0) {
        res += (convert_number(quintillion) + " quintillion");
    }
    if (quar > 0) {
        res += (convert_number(quar) + " quadrillion");
    }
    if (trin > 0) {
        res += (convert_number(trin) + " trillion");
    }
    if (Gn > 0) {
        res += (convert_number(Gn) + " billion");
    }
    if (million > 0) {
        res += (((res == "") ? "" : " ") + convert_number(million) + " million");
    }
    if (Hn > 0) {
        res += (((res == "") ? "" : " ") + convert_number(Hn) + " Thousand");
    }

    if (Dn) {
        res += (((res == "") ? "" : " ") + convert_number(Dn) + " hundred");
    }


    var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen");
    var tens = Array("", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety");

    if (tn > 0 || one > 0) {
        if (!(res == "")) {
            res += " and ";
        }
        if (tn < 2) {
            res += ones[tn * 10 + one];
        } else {

            res += tens[tn];
            if (one > 0) {
                res += ("-" + ones[one]);
            }
        }
    }

    if (res == "") {
        console.log("Empty = " + number);
        res = "";
    }
    return res;
}
function is_numeric(mixed_var) {
    return (typeof mixed_var === 'number' || typeof mixed_var === 'string') && mixed_var !== '' && !isNaN(mixed_var);
}


function numToWords(number) {

    //Validates the number input and makes it a string
    if (typeof number === 'string') {
        number = parseInt(number, 10);
    }
    if (typeof number === 'number' && isFinite(number)) {
        number = number.toString(10);
    } else {
        return 'This is not a valid number';
    }

    //Creates an array with the number's digits and
    //adds the necessary amount of 0 to make it fully
    //divisible by 3
    var digits = number.split('');
    while (digits.length % 3 !== 0) {
        digits.unshift('0');
    }


    //Groups the digits in groups of three
    var digitsGroup = [];
    var numberOfGroups = digits.length / 3;
    for (var i = 0; i < numberOfGroups; i++) {
        digitsGroup[i] = digits.splice(0, 3);
    }
    // console.log(digitsGroup); //debug

    //Change the group's numerical values to text
    var digitsGroupLen = digitsGroup.length;
    var numTxt = [
        [null, 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'], //hundreds
        [null, 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'], //tens
        [null, 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'] //ones
        ];
    var tenthsDifferent = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];

    // j maps the groups in the digitsGroup
    // k maps the element's position in the group to the numTxt equivalent
    // k values: 0 = hundreds, 1 = tens, 2 = ones
    for (var j = 0; j < digitsGroupLen; j++) {
        for (var k = 0; k < 3; k++) {
            var currentValue = digitsGroup[j][k];
            digitsGroup[j][k] = numTxt[k][currentValue];
            if (k === 0 && currentValue !== '0') { // !==0 avoids creating a string "null hundred"
                digitsGroup[j][k] += ' Hundred ';
            } else if (k === 1 && currentValue === '1') { //Changes the value in the tens place and erases the value in the ones place
                digitsGroup[j][k] = tenthsDifferent[digitsGroup[j][2]];
                digitsGroup[j][2] = 0; //Sets to null. Because it sets the next k to be evaluated, setting this to null doesn't work.
            }
        }
    }

    // console.log(digitsGroup); //debug

    //Adds '-' for gramar, cleans all null values, joins the group's elements into a string
    for (var l = 0; l < digitsGroupLen; l++) {
        if (digitsGroup[l][1] && digitsGroup[l][2]) {
            digitsGroup[l][1] += ' ';
        }
        digitsGroup[l].filter(function (e) {return e !== null});
        digitsGroup[l] = digitsGroup[l].join('');
    }

    // console.log(digitsGroup); //debug

    //Adds thousand, millions, billion and etc to the respective string.
    var posfix = [null, 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'Quintillion', 'Sextillion'];
    if (digitsGroupLen > 1) {
        var posfixRange = posfix.splice(0, digitsGroupLen).reverse();
        for (var m = 0; m < digitsGroupLen - 1; m++) { //'-1' prevents adding a null posfix to the last group
            if (digitsGroup[m]) {
                digitsGroup[m] += ' ' + posfixRange[m];
            }
        }
    }

    // console.log(digitsGroup); //debug

    //Joins all the string into one and returns it
    return digitsGroup.join(' ');
}

    return {
    //    beforeLoad: beforeLoad,
		beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});