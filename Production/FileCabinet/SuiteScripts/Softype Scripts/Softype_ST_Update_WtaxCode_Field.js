/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 */
 define(['N/search','N/record'],function(search,record){
 	function onRequest(context){
 		log.debug('Sutielet Called Method',context.request.method)
 		if (context.request.method ==='GET') {
 			log.debug('Sutielet Called',context.request.parameters.customRecId)
 			var action=context.request.parameters.action
 			if (action=='getBillCreditId') {
 				//saveRecord(context)
 				var currentRecordID = context.request.parameters.customRecId;
 				var currentRecordObj=record.load({
            	    type:'vendorbill',
            	    id:currentRecordID
            	})

            	currentRecordObj.save();
 			}

 			if (action=='getCompanyCheckOrDetail') {
 				var currentRecordID = context.request.parameters.customRecId;
 				var currentRecordObj=record.load({
            	    type:'customrecord_com_checks_or_details',
            	    id:currentRecordID
            	})

            	currentRecordObj.save();
 			}
 		}
 	}
 	return {onRequest:onRequest};
 });