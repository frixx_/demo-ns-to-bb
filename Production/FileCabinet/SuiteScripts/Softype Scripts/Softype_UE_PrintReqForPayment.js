/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Saroja Iyer
 ** @Dated       :  12 Oct 2019
 ** @Version     :  2.0
 ** @Description :  UserEvent to create a button to print the Item Receipt.

 ***************************************************************************************/

define(['N/record', 'N/url', 'N/search'],

function(record, url, search)
{

	// Function to create button.
function createButton(scriptContext)
	{
		if (scriptContext.type == scriptContext.UserEventType.VIEW) {
		// Getting the URL to open the suitelet.
		var outputUrl = url.resolveScript
		({
			scriptId: 'customscript_st_print_request_for_paymen',
			deploymentId: 'customdeploy_st_print_request_for_paymen',
			returnExternalUrl: false
		});
		var currentrecord = scriptContext.newRecord;
		var approvalStatus=currentrecord.getValue('custrecord_rfp_appr_sts');
		if (approvalStatus==3) {
			return;
		}

			// Getting the record id.
		var recordid = currentrecord.id;
		// Adding parameters to pass in the suitelet.
		outputUrl += '&action=GET';
		outputUrl += '&recordid=' + recordid;

		// Creating function to redirect to the suitelet.
		var stringScript = "window.open('"+outputUrl+"','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";

		// Creating a button on form.
		var printButton = scriptContext.form.addButton
		({
			id: 'custpage_print_request',
			label: 'Print',
			functionName: stringScript
		});
	}
	}

	return {
		beforeLoad: createButton 
	};
});