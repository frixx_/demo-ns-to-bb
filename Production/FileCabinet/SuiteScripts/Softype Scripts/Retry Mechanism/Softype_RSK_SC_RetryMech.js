/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
 
/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : Calls RSK API's for Flagged records
***************************************************************************************/

define(['N/record', 'N/search', 'N/runtime', 'N/log', 'N/task', 'N/format'],

function(record, search, runtime, task, format) {
   	
    function execute(scriptContext) {
		try{
			
			var myScript = runtime.getCurrentScript();

			var recordType = myScript.getParameter({
				name: 'custscript_recordtype'
			});
			
			var flagField = myScript.getParameter({
				name: 'custscript_flagfield'
			});
			
			log.debug("Record", recordType);
			
			var filters = new Array();
			
			filters.push(search.createFilter({
				name: flagField,
				operator: search.Operator.IS,
				values: true
			}));
			
			filters.push(search.createFilter({
				name: "isinactive",
				operator: search.Operator.IS,
				values: false
			}));
						
			if(recordType == "purchaseorder"){
				filters.push(search.createFilter({
					name: "mainline",
					operator: search.Operator.IS,
					values: true
				}));
			}
			
			var data = search.create({
								type: recordType,
								filters: filters,
								columns:[
										  'internalid'
										]
							  }).run();
							  
			var results = getAllSearchResults(data);
			
			for(var i = 0; i < results.length; i++){
				var rec = record.load({
									type: recordType, 
									id: results[i].id,
									isDynamic: true,
							   });
				rec.save();
			}
		}
		catch(e){			
			log.error('Error Occured',e);
		}
    }
	
	function getAllSearchResults(resultSet) {
		var batch, batchResults, results = [], searchStart = 0;
		do {
			batch = resultSet.getRange({ start: searchStart, end: searchStart + 1000} );
			batchResults = (batch || []).map(function (row) {
												searchStart++;
												return row;
											}, this);
			results = results.concat(batchResults);
		} while ((batchResults || []).length === 1000);

		return results;
	}
	
    return {
        execute: execute
    };
    
});