/***************************************************************************************  
	** Copyright (c) 1998-2018 Softype, Inc.
	** P3 Zenith Central | Luzon Avenue | Cebu Business Park, Cebu City | Philippines
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
	**                      
	**@Author      :  Care P
	**@Dated       :  31st October 2019
	**@Version     :  1.x
	**@Description :  Suitelet for Debit Memo 
***************************************************************************************/


/**
	* @param {nlobjRequest} request Request object
	* @param {nlobjResponse} response Response object
	* @returns {Void} Any output is written via response object
*/

function printto(request, response) {
	
	var RecId = request.getParameter('recordId');
	nlapiLogExecution('Debug', 'RecId', RecId);
	
	var vcRec = nlapiLoadRecord('vendorcredit', RecId); // Load Vendor Credit
	
	var transNo = vcRec.getFieldValue('transactionnumber');
	var supplier = vcRec.getFieldValue('entity');
	var supplierName = vcRec.getFieldText('entity');
	var supplierAddr = vcRec.getFieldValue('billaddress');
	var date = vcRec.getFieldValue('trandate');
	var memo = vcRec.getFieldValue('memo');
	var supplierInvNum = vcRec.getFieldValue('createdfrom');
	var supplierInvNumDisplay = vcRec.getFieldText('createdfrom');
	var preparedBy = vcRec.getFieldValue('custbody_prepared_by');
	
	var empRec, empName;
	try{
		if(preparedBy){
			empRec = nlapiLoadRecord('employee', preparedBy);
			empName = empRec.getFieldValue('entityid');
		}
	}catch(err){
		if(preparedBy){
			empRec = nlapiLoadRecord('partner', preparedBy);
			empName = empRec.getFieldValue('entityid');
		}	
	}
	var dateAndTimeCreated = vcRec.getFieldValue('custbody_date_time_create');
	var poNum = vcRec.getFieldValue('custbody_ref_doc');
	if(poNum){
		var poNumRec = nlapiLoadRecord('purchaseorder', poNum);
		var poNumDisplay = poNumRec.getFieldValue('tranid');
		} else {
		alert("Reference PO field is empty");
	}
	var subsidiary = vcRec.getFieldValue('subsidiary');
	var subRec = nlapiLoadRecord('subsidiary', subsidiary);
	var subAddress = subRec.getFieldValue('mainaddress_text');
	var subTelephone = subRec.getFieldValue('custrecord_subsidiary_tel');
	var explanationField = "";
	var newSuppInvNum = "";
	/*
		//Edit Supplier Invoice Format
		if(supplierInvNumDisplay != null || supplierInvNumDisplay != ""){
		var suppInvArr = supplierInvNumDisplay.split(" ");
		var suppInvNumArr = suppInvArr[2].split("");
		for(var a = 1; a < suppInvNumArr.length; a++){
		newSuppInvNum += suppInvNumArr[a];
		}
		}
	*/
	
	//Concatenate Values needed to fill in the Explanation field
	explanationField += (memo == null? "" : nlapiEscapeXML(memo));
	explanationField += " " + (poNumDisplay == null? "" : nlapiEscapeXML(poNumDisplay));
	explanationField += " " + (newSuppInvNum == null? "" : nlapiEscapeXML(newSuppInvNum));
	
	var image = 'https://5066453-sb1.app.netsuite.com/core/media/media.nl?id=658&c=5066453_SB1&h=9df4cfd5374291de4232&fcts=20191104201241&whence=';
	var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
	htmlvar += '<pdf>\n';
	htmlvar += '<head>';
	htmlvar += '<macrolist>';
	
	htmlvar += '<macro id="nlheader">';
	
	
	htmlvar += '<table style="height:auto;width:100%; border-collapse:collapse;table-layout:fixed;">';
	htmlvar += '<tr>';
	htmlvar += '<td align="right" style="width: 370px; font-size: 14pt;"><img src="' + nlapiEscapeXML(image) + '" style="height: 84.5px; width: 149.5px;" /></td>';
	htmlvar += '<td align="right" style="width: 70px;font-size: 12pt;"><b>No.</b></td>';
	htmlvar += '<td style="width: 100px;font-size: 10pt; padding-top: 5px;"><u>' + (transNo == null? "" : nlapiEscapeXML(transNo)) + '</u></td>';
	htmlvar += "</tr>";
	htmlvar += '</table>';
	htmlvar += '<table style="height:auto; width:100%; margin-top: 5px;">';
	htmlvar += '<tr>';
	htmlvar += '<td align="center" style="font-size: 10pt;">' + (subAddress == null? "" : nlapiEscapeXML(subAddress)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '<tr>';
	htmlvar += '<td align="center" style="font-size: 10pt; padding-top: 2px">' + (subTelephone == null? "" : nlapiEscapeXML(subTelephone)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '<tr>';
	htmlvar += '<td align="center" style="padding-top: 5px;"><b>DEBIT / CREDIT MEMORANDUM</b></td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	htmlvar += '</macro>';
	
	
	
	htmlvar += '<macro id="nlfooter">';
	htmlvar += " </macro>";
	htmlvar += " </macrolist>";
	htmlvar += '</head>';
	
	
	htmlvar += '<body header="nlheader" header-height="24.5%" padding="0.2in 0.2in 0.2in 0.2in" height="180mm" font-size="12" width="210mm">';
	htmlvar += '<table style="width:100%; padding: 0px; 0px; 0px; 0px; font-size:10pt;">';
	htmlvar += '<tr>';
	htmlvar += '<td colspan="8" style="border-bottom: 1;">' + (supplierName == null? "" : nlapiEscapeXML(supplierName)) + '</td>';
	htmlvar += '<td colspan="1" align="right" style="font-size: 11pt;"><i>Date</i></td>';
	htmlvar += '<td colspan="3" style="border-bottom: 1;">&nbsp;&nbsp;&nbsp;&nbsp;' + (date == null? "" : nlapiEscapeXML(date)) + '</td>';
	htmlvar += '</tr>';
htmlvar += '<tr>';
htmlvar += '<td colspan="8" style="border-bottom: 1; padding-top: 10px;">' + (supplierAddr == null? "" : nlapiEscapeXML(supplierAddr)) + '</td>';
htmlvar += '</tr>';
htmlvar += '</table>';
htmlvar += '<table style="width: 100%; margin-top: 15px; margin-bottom: 0px;">';
htmlvar += '<tr>';
htmlvar += '<td>We Debit / Credit your account as follows:</td>';
htmlvar += '</tr>';

//Concatenate Values needed to fill in the Explanation field
var lineCountItem = vcRec.getLineItemCount('item');
for (var b = 1; b <= lineCountItem; b++) {
var itemId = vcRec.getLineItemValue('item', 'item', b);
if(itemId != 8){
var item = vcRec.getLineItemText('item', 'item', b);	
var quantity = vcRec.getLineItemValue('item', 'quantity', b);
var rate = vcRec.getLineItemValue('item', 'rate', b);
explanationField += " " + (item == null? "" : nlapiEscapeXML(item));
explanationField += " " + (quantity == null? "" : nlapiEscapeXML(quantity));
explanationField += " " + (rate == null? "" : nlapiEscapeXML(rate));
}
}
nlapiLogExecution('DEBUG', 'explanationField', explanationField);

htmlvar += '</table>';
htmlvar += '<table style="width: 100%; margin-top: 0px;">';
htmlvar += '<tr>';
htmlvar += '<td colspan="9" align="center" style="border-top: 2; border-right: 2; border-bottom: 2;">E X P L A N A T I O N</td>';
htmlvar += '<td colspan="2" align="center" style="border-top: 2; border-right: 2; border-bottom: 2;">DEBIT</td>';
htmlvar += '<td colspan="2" align="center" style="border-top: 2; border-bottom: 2;">CREDIT</td>';
htmlvar += '</tr>';
htmlvar += '<tr>';
htmlvar += '<td colspan="9" align="left" style="border-right: 2; padding-left: 10px;">' + (explanationField == null? "" : nlapiEscapeXML(explanationField)) + '</td>';
htmlvar += '<td colspan="2" align="center" style="border-right: 2;"></td>';
htmlvar += '<td colspan="2" align="center"></td>';
htmlvar += '</tr>';
htmlvar += '<tr>';
htmlvar += '<td colspan="9" align="center" style="border-right: 2;">&nbsp;</td>';
htmlvar += '<td colspan="2" align="center" style="border-right: 2;">&nbsp;</td>';
htmlvar += '<td colspan="2" align="center">&nbsp;</td>';
htmlvar += '</tr>';

//GL Impact Search Starts here
var debitMemoGlImpactResult = nlapiSearchRecord('vendorcredit', null, 
[
(new nlobjSearchFilter('internalid', null, 'is', RecId))
], [ 
(new nlobjSearchColumn('account')),
(new nlobjSearchColumn('creditamount')),
(new nlobjSearchColumn('debitamount'))
]);

//Debit and Credit Arrays and Total Amount Variables
var debitArray = [];
var creditArray = [];
var totalDebit = 0;
var totalCredit = 0;

if (debitMemoGlImpactResult) {	
for(var c = 0; c < debitMemoGlImpactResult.length; c++){
var accountId = debitMemoGlImpactResult[c].getValue('account');
var accountRec = nlapiLoadRecord('account', accountId);
var accountName = accountRec.getFieldValue('acctname');
var creditAmount = debitMemoGlImpactResult[c].getValue('creditamount');
var debitAmount = debitMemoGlImpactResult[c].getValue('debitamount');

//Push Debit and Credit Array
if(debitAmount != ""){
debitArray.push({
'account': accountName,
'debitAmount': debitAmount
});
} else if(creditAmount != ""){
creditArray.push({
'account': accountName,
'creditAmount': creditAmount
});
}
}

//For Debit Accounts
for(var d = 0; d < debitArray.length; d++){
var debitAccount = debitArray[d].account;
var amountDebit = debitArray[d].debitAmount;
totalDebit += parseFloat(amountDebit);

//Display Debit Accounts
htmlvar += '<tr>';
htmlvar += '<td colspan="9" align="left" style="border-right: 2; padding-left: 10px;">' + (debitAccount == null? "" : nlapiEscapeXML(debitAccount)) + '</td>';
htmlvar += '<td colspan="2" align="center" style="border-right: 2;">' + (amountDebit == null? "" : nlapiEscapeXML(amountDebit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) + '</td>';
htmlvar += '<td colspan="2" align="center"></td>';
htmlvar += '</tr>';
}		

//For Credit Accounts
for(var e = 0; e < creditArray.length; e++){
var creditAccount = creditArray[e].account;
var amountCredit = creditArray[e].creditAmount;
totalCredit += parseFloat(amountCredit);

//Display Credit Accounts
htmlvar += '<tr>';
htmlvar += '<td colspan="9" align="left" style="border-right: 2; padding-left: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + (creditAccount == null? "" : nlapiEscapeXML(creditAccount)) + '</td>';
htmlvar += '<td colspan="2" align="center" style="border-right: 2;"></td>';
htmlvar += '<td colspan="2" align="center">' + (amountCredit == null? "" : nlapiEscapeXML(amountCredit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) + '</td>';
htmlvar += '</tr>';
}
}// end if(debitMemoGlImpactResult) condition

//Spaces After GL impact Data
htmlvar += '<tr>';
htmlvar += '<td colspan="9" align="center" style="border-right: 2; border-bottom: 2;">&nbsp;</td>';
htmlvar += '<td colspan="2" align="center" style="border-right: 2; border-bottom: 2; padding-top: 7px;"><b>' + (totalDebit == null? "" : nlapiEscapeXML(totalDebit.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) + '</b></td>';
htmlvar += '<td colspan="2" align="center" style="border-bottom: 2; padding-top: 7px;"><b>' + (totalCredit == null? "" : nlapiEscapeXML(totalCredit.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) + '</b></td>';
htmlvar += '</tr>';
htmlvar += '</table>';

htmlvar += '<table style="width: 100%; margin-top: 0px;">';
htmlvar += '<tr>';
htmlvar += '<td style="width: 330px;">Confirmed by:</td>';
htmlvar += '<td style="width: 120px;">&nbsp;</td>';
htmlvar += '<td align="center" style="width: 150px;"><img src="' + nlapiEscapeXML(image) + '" style="height: 56.33px; width: 99.67px;" /></td>';
htmlvar += '</tr>';
htmlvar += '<tr>';
htmlvar += '<td style="width: 330px; border-bottom: 1; padding-top: 5px; text-align: center;">' + (empName == null? "" : nlapiEscapeXML(empName.toUpperCase())) + '<br/>' + (dateAndTimeCreated == null? "" : nlapiEscapeXML(dateAndTimeCreated)) + '</td>';
htmlvar += '<td style="width: 120px;">&nbsp;</td>';
htmlvar += '<td align="right" style="width: 150px; padding-top: 5px;">By:_______________________________</td>';
htmlvar += '</tr>';
htmlvar += '</table>';
htmlvar += '<table style="width: 100%; margin-top: 0px; font-size: 9pt;">';
htmlvar += '<tr>';
htmlvar += '<td style="width: 450px;">&nbsp;</td>';
htmlvar += '<td align="center" style="width: 100px;">AUTHORIZED SIGNATURE</td>';
htmlvar += '</tr>';
htmlvar += '</table>';


htmlvar += '</body>';
htmlvar += '</pdf>';
var file = nlapiXMLToPDF(htmlvar);
response.setContentType('PDF', 'Print.pdf', 'inline');
response.write(file.getValue());

}