/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 28 February 2020
	**@Version : 2.0
	**@Description : Set Department & Division
***************************************************************************************/


define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, record, runtime, log) {

		function afterSubmit(context) {
		
			var VAT2550MFORM = 158;
			
			var newRecordDetails = context.newRecord;
			
			var customform = newRecordDetails.getValue({
				fieldId: 'customform'
			});
			
			if(customform != VAT2550MFORM){
				return;
			}

			var supplierInvoice = newRecordDetails.getValue({
				fieldId: 'custbody_createdfrombill'
			});
			
			var numLines = newRecordDetails.getLineCount({
				sublistId: 'line'
			});
			log.debug("NumLines - AS", numLines);
			var debitAmount = 0;
			
			for(var i = 0; i < numLines; i++){
				var debit = newRecordDetails.getSublistValue({
					sublistId : "line",
					fieldId : "debit",
					line : i
				});
				debitAmount	= debitAmount + Number(debit);
			}
			
			if(!!(supplierInvoice)){
				var id = record.submitFields({
					type: "vendorbill",//record.Type.PURCHASE_ORDER,
					id: supplierInvoice,
					values: {
						"custbody_amt_tlc_2550m": debitAmount
					},
					options: {
						enableSourcing: false,
						ignoreMandatoryFields : true
					}
				});
			}
		}
			
		return {
			afterSubmit : afterSubmit
		};
	});	