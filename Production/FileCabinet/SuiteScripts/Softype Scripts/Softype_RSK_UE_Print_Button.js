/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Siddhi Kadam
 **@Dated       :  20th February, 2020
 **@Version     :  2.x
 **@Description :  To create custom print button on Check and Deposit.
 ***************************************************************************************/
define(['N/ui/serverWidget', 'N/log', 'N/error', 'N/record', 'N/url', 'N/runtime', 'N/search'],

function(serverWidget, log, error, record, url, runtime, search) {

    function beforeLoad(context) {

        var recordId = context.newRecord.id;
        var recordType = context.newRecord.type;
        var currentRecordObj = context.newRecord;
        var userObj = runtime.getCurrentUser();
        var ADMINISTRATOR = 3;

        log.debug("Internal ID of current user role: " + userObj.role);
        log.debug("recordType " ,recordType);

        if (context.type === context.UserEventType.DELETE)
            return;


        try {

            if (recordType == 'check') {


                if (recordId) {

                    if (userObj.role != ADMINISTRATOR) {
                        var approvalStatus = currentRecordObj.getValue({
                            fieldId: 'custbody_ub_payout_status'
                        });

                        log.debug('approvalStatus', approvalStatus);

                        var printCheck = currentRecordObj.getValue({
                            fieldId: 'custbody_print_count'
                        });
                        log.debug('printCheck', printCheck);

                        if (approvalStatus != 6 || printCheck == true) {
                            return
                        }
                    }
                }

                if (context.type === context.UserEventType.VIEW) {

                    var outputUrl = url.resolveScript({
                        scriptId: 'customscript_softype_st_check_print',
                        deploymentId: 'customdeploy_softype_st_check_print',
                        returnExternalUrl: false
                    });

                    outputUrl += '&recordId=' + recordId;
                    outputUrl += '&recordType=' + "check";

                    var stringScript = "window.open('" + outputUrl + "','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";

                    var printButton = context.form.addButton({
                        id: 'custpage_print',
                        label: 'Check Print',
                        functionName: stringScript
                    });

                }
            }

            if (recordType == 'deposit') {

                if (context.type === context.UserEventType.VIEW) {

                    var outputUrl = url.resolveScript({
                        scriptId: 'customscript_softype_st_deposit_print',
                        deploymentId: 'customdeploy_softype_st_deposit_print',
                        returnExternalUrl: false
                    });

                    outputUrl += '&recordId=' + recordId;
                  

                    var stringScript = "window.open('" + outputUrl + "','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";

                    var printButton = context.form.addButton({
                        id: 'custpage_print',
                        label: 'Print',
                        functionName: stringScript
                    });

                }




            }




        } catch (e) {

            log.error('Error Details', e.toString());
            throw e;

        }


    }





return {
    beforeLoad: beforeLoad
};
}
);