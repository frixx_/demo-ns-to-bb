/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  18th February, 2020
 ** @Version     :  2.x
 ** @Description :  This script updates Division on the basis of Department entered on the line level and 
					enters current user in the Prepared By field on save record function.

 ***************************************************************************************/
define(['N/currentRecord', 'N/url', 'N/search', 'N/record', 'N/runtime'],

    function(currentRecord, url, search, record, runtime) {


        function fieldChanged(scriptContext) {


            var fieldId = scriptContext.fieldId;
            var sublistName = scriptContext.sublistId;
            var currentRecordObj = currentRecord.get();


            if ((scriptContext.sublistId == 'item' || scriptContext.sublistId == 'expense' || scriptContext.sublistId == 'line') && scriptContext.fieldId == 'department') {


                var department = currentRecordObj.getCurrentSublistValue({
                    sublistId: sublistName,
                    fieldId: 'department'

                });

                if (department != undefined) {
                 
                    if (department) {

                        var departmentLookUp = search.lookupFields({
                            type: search.Type.DEPARTMENT,
                            id: department,
                            columns: ['custrecord_dept_division']
                        });

                        var division = departmentLookUp.custrecord_dept_division[0].value;


                        currentRecordObj.setCurrentSublistValue({
                            sublistId: sublistName,
                            fieldId: 'class',
                            value: division

                        });


                    }



                }

            }


        }


        function saveRecord(scriptContext) {

            var currentRec = currentRecord.get();
            var currentUser = runtime.getCurrentUser();


            /** For setting the current user on Prepared By field on saving. **/
            var preparedBy = currentRec.getValue({
                fieldId: 'custbody_prepared_by'
            });
			
			
			log.debug('currentUser',currentUser);

            if (preparedBy != undefined) {
			
                currentRec.setValue({
                    fieldId: 'custbody_prepared_by',
                    value: currentUser.id
                });



            }
            return true;

        }




        return {

            fieldChanged: fieldChanged,
            saveRecord: saveRecord



        }
    });