/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  19th February, 2020
 ** @Version     :  2.x
 ** @Description :  This script is deployed on Check - when the user selects RFP Number on the check record 
					script validates if the RFP record 'customrecord_rfp' already has a check link.

 ***************************************************************************************/
define(['N/currentRecord', 'N/url', 'N/search', 'N/record', 'N/runtime', 'N/ui/dialog'],


function(currentRecord, url, search, record, runtime, dialog) {

    function fieldChanged(scriptContext) {

        var fieldId = scriptContext.fieldId;
        var currentRec = currentRecord.get();


        var RFPCheckForm = Number(runtime.getCurrentScript().getParameter("custscript_softype_rfpcheck_form"));

        var customForm = Number(currentRec.getValue({
            fieldId: 'customform'

        }));


        if (customForm != RFPCheckForm) {

            return;

        }

        if (fieldId == 'custbody_ref_rfp_num') {


            var RFPNumber = currentRec.getValue({
                fieldId: 'custbody_ref_rfp_num'

            });
			

        if (RFPNumber) {

            var RFPLookUp = search.lookupFields({
                type: 'customrecord_rfp',
                id: RFPNumber,
                columns: ['custrecord_writecheck']
            });
           
			
			
			if( RFPLookUp.custrecord_writecheck.length != 0 ){
				
				
				netsuiteAlert('This RFP Number already has a Check Link ');
				currentRec.setValue({
					fieldId: 'custbody_ref_rfp_num',
					value: ''
					
				});
				
				return;
			}
			
          
			

        }

    }


}


 /** For message Pop-Up **/

        function netsuiteAlert(message) {
            var options = {
                title: "Alert",
                message: message
            };

            function success(result) {
                console.log("Success with value " + result);
            }

            function failure(reason) {
                console.log("Failure: " + reason);
            }

            dialog.alert(options).then(success).catch(failure);
        }




return {


    fieldChanged: fieldChanged


}
});