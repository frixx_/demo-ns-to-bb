/**
* @NApiVersion 2.x
* @NScriptType Suitelet
*/

define(['N/record', 'N/search', 'N/log'],
    function callbackFunction(record, search, log) {
        function getFunction(context) {

			var internalid = context.request.parameters.internalid;
			
			var rec = record.load({
				type: "journalentry",
				id: internalid
			});

			var voidCheck = rec.getValue({
				fieldId: 'voided'
			});
			
			log.debug("voidCheck", voidCheck);
			if(voidCheck == "T" || voidCheck == true){
				var supplierInvoice = rec.getValue({
					fieldId: 'custbody_createdfrombill'
				});
				
				var id = record.submitFields({
					type: "vendorbill",
					id: supplierInvoice,
					values: {"custbody_amt_tlc_2550m": 0},
					options: {
						enableSourcing: false,
						ignoreMandatoryFields : true
					}
				});
				
			}
			// context.response.write("Hello World");			
        }
		
        return {
            onRequest: getFunction
        };
});