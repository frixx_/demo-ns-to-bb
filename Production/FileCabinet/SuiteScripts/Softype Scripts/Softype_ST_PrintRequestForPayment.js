/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
  ** @Author      : Meena Nag
 ** @Dated       :  16 Jan 2020
 ** @Version     :  1.0
 ** @Description :  Suitelet to print the data from Request For Payment record after the userevent redirects here.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

define(['N/record', 'N/xml', 'N/render', 'N/http','N/format' ],
    function(record, xml, render, http,format)
	{
		function onRequest(context)
		{

			if (context.request.method == 'GET')
			{
				// Getting the parameters from Userevent.
				var action = context.request.parameters.action;
				var recordid = context.request.parameters.recordid;
				var loadrec = record.load
				({
					type: 'customrecord_rfp',
					id: recordid
					//isDynamic: true
				});
				


				var custrecord_date_rfp = loadrec.getText({ fieldId: 'custrecord_date_rfp'});
				var custrecord_date_rfp_label = loadrec.getField({ fieldId: 'custrecord_date_rfp'}).label;	

				var custrecord_rfp_supplier = loadrec.getText({ fieldId: 'custrecord_rfp_supplier'});
				var custrecord_rfp_supplier_label = loadrec.getField({ fieldId: 'custrecord_rfp_supplier'}).label;

				var custrecord_subsdiary = loadrec.getText({ fieldId: 'custrecord_subsdiary'});
				var custrecord_subsdiary_label = loadrec.getField({ fieldId: 'custrecord_subsdiary'}).label;

				var owner = loadrec.getText({ fieldId: 'owner'});
				

				var name = loadrec.getText({ fieldId: 'name'});
				var name_label = loadrec.getField({ fieldId: 'name'}).label;


				var custrecord_rfp_amount = loadrec.getValue({ fieldId: 'custrecord_rfp_amount'});
				var custrecord_rfp_amount_label = loadrec.getField({ fieldId: 'custrecord_rfp_amount'}).label;

				var custrecord_rfp_due_date = loadrec.getText({ fieldId: 'custrecord_rfp_due_date'});
				var custrecord_rfp_due_date_label = loadrec.getField({ fieldId: 'custrecord_rfp_due_date'}).label;

				var custrecord_rfp_payment_method = loadrec.getText({ fieldId: 'custrecord_rfp_payment_method'});
				var custrecord_rfp_payment_method_label = loadrec.getField({ fieldId: 'custrecord_rfp_payment_method'}).label;


				var rfpType = loadrec.getText({ fieldId: 'custrecord_rfptype'});
				

				var custrecord_ref_number = loadrec.getValue({ fieldId: 'custrecord_ref_number'});
                var custrecord_ref_number_label = loadrec.getField({ fieldId: 'custrecord_ref_number'}).label;
                
                //add type of transaction
                var custrecord_type_trans = loadrec.getText({ fieldId: 'cseg_type_of_trans'});
                var custrecord_type_trans_label = loadrec.getField({ fieldId: 'cseg_type_of_trans'}).label;

				// Creating the htmlvar file to displayed in the suitelet.
				htmlvar = '';
				htmlvar+= '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
				htmlvar+= '<pdf>';
				htmlvar+= '<head>';

				htmlvar+='<style type="text/css">table { font-size: 9pt; table-layout: fixed; width: 100%; }';
				htmlvar+='th { font-weight: bold; font-size: 8pt; vertical-align: middle; padding: 5px 6px 3px; background-color: #e3e3e3; color: #333333; padding-bottom: 10px; padding-top: 10px; }';
				htmlvar+='td { padding: 4px 6px; font-size:12px}';
				htmlvar+='b { font-weight: bold; color: #333333; }';
				htmlvar+='</style>';
				htmlvar+='</head>';

				htmlvar+='<body padding="0.5in 0.5in 0.5in 0.5in" width="21.50cm" height="16.50cm" size="Letter">';
				
    			htmlvar+='<table style="width: 100%; font-size: 10pt;"><tr>';
				htmlvar+='<td colspan="4" align="center"><span style="font-size: 20pt;text-transform: uppercase;">'+rfpType+'</span></td>';
				htmlvar+='</tr>';
				htmlvar+='</table>&nbsp;';

				htmlvar+='<table width="100%"><tr>';
					htmlvar+='<td align="left" width="350px" padding-left="12"><b>'+name_label+'</b></td>';
					htmlvar+='<td align="left" width="500px">'+name+'</td>';
					htmlvar+='<td style="width: 16%;">&nbsp;</td>';
					htmlvar+='<td align="left" width="350px"><b>'+custrecord_rfp_due_date_label+'</b></td>';
					htmlvar+='<td align="left" width="350px">'+custrecord_rfp_due_date+'</td>';  
					htmlvar+='</tr>';
					htmlvar+='<tr>';
					htmlvar+='<td align="left" width="350px" padding-left="12"><b>'+custrecord_date_rfp_label+'</b></td>';
					htmlvar+='<td align="left" width="500px">'+custrecord_date_rfp+'</td>';
					htmlvar+='<td style="width: 16%;">&nbsp;</td>';
					htmlvar+='<td align="left" width="350px"><b>'+custrecord_rfp_payment_method_label+'</b></td>'; 
					htmlvar+='<td >'+custrecord_rfp_payment_method+'</td>'; 
					htmlvar+='</tr>';
					htmlvar+='<tr>';
					htmlvar+='<td align="left" width="350px" padding-left="12"><b>'+custrecord_subsdiary_label+'</b></td>';
					htmlvar+='<td align="left" width="350px" padding-right="120px">'+custrecord_subsdiary+'</td>';
                    htmlvar+='<td style="width: 12%;">&nbsp;</td>';
                    htmlvar+='<td align="left" width="350px"><b>'+custrecord_type_trans_label+'</b></td>'; 
					htmlvar+='<td >'+custrecord_type_trans+'</td>';
					htmlvar+='</tr>';
					
					htmlvar+='<tr>';
					htmlvar+='<td align="left" width="350px" padding-left="12"><b>'+custrecord_rfp_supplier_label+'</b></td>';
					htmlvar+='<td align="left" width="400px">'+custrecord_rfp_supplier+'</td>';
					htmlvar+='<td style="width: 12%;">&nbsp;</td>';
					htmlvar+='</tr>';
					
					htmlvar+='<tr>';
					htmlvar+='<td align="left" width="350px" padding-left="12"><b>'+custrecord_rfp_amount_label+'</b></td>';
					htmlvar+='<td align="left" width="400px" padding-right="40">'+custrecord_rfp_amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
					htmlvar+='<td style="width: 12%;">&nbsp;</td>';
					
					htmlvar+='</tr>';
					
					htmlvar+='<tr>';
					htmlvar+='<td align="left" width="350px" padding-left="12" padding-right="5"><b>Employee/Custodiar</b></td>';
					htmlvar+='<td align="left" width="400px">'+owner+'</td>';
					htmlvar+='</tr>';
					
					htmlvar+='<tr>';
					htmlvar+='<td align="left" width="350px" padding-left="12"><b>'+custrecord_ref_number_label+'</b></td>';
					htmlvar+='<td align="left" width="400px">'+custrecord_ref_number+'</td>';
					htmlvar+='</tr>';
					
					htmlvar+='</table>';
				
					htmlvar += '</body>';
					htmlvar += '</pdf>';


				var file = render.xmlToPdf
				({
					xmlString: htmlvar
				});

				context.response.writeFile(file, true);
				return;
			}
			
		}
	return {
		onRequest: onRequest
	};
});
