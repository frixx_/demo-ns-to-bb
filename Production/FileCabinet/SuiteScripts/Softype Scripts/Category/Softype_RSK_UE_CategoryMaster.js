/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : After creation of category record in Netsuite script calls RSK API to create Category Master in RSK System
***************************************************************************************/


define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, record, runtime, log) {
		
		function beforeSubmit(context) {
			
			/* log.debug('runtime',runtime);
			log.debug('runtime.executionContext',runtime.executionContext);*/
			log.debug('context',context);
			
		}
		function afterSubmit(context) {
			
			/* log.debug('runtime',runtime);
			log.debug('runtime.executionContext',runtime.executionContext);*/
			log.debug('context',context);
			
			if(context.type == "delete"){
				return;
			}
			
			var newRecordDetails = context.newRecord;
			// log.debug('newRecordDetails', newRecordDetails);
			try{
				var scriptObj = runtime.getCurrentScript();
				var createAPI = scriptObj.getParameter({name: 'custscript_create_api_category'});	// RSK Create API
				var updateAPI = scriptObj.getParameter({name: 'custscript_update_api_category'});	// RSK Update API
				
				/**********************************************************
					*************************API CALL************************
				**********************************************************/
				
				var headers = {
					"Content-Type": "application/json"
				};	//Headers if Any
				
				var response;
				var rskID = newRecordDetails.getValue({fieldId: 'custrecord_categoryid'});	
				if(context.type == 'create' || rskID == ""){
					
					var url = createAPI;	
					
					/* Creating JSON Body to be in a request */
					var jsonBody = {
						"NsId" : newRecordDetails.id,
						"Name" : newRecordDetails.getValue({
							fieldId: 'name'
						}),
                      	"AssetClassId" : Number(newRecordDetails.getValue({
							fieldId: 'custrecord_asset_class'
						}))
					};	
					
					log.debug("jsonBody - Create", jsonBody);
					
					response = http.request({
						method: https.Method.POST,
						url: url,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					
					log.debug("Response", response);	
					
					SetRSKID(newRecordDetails, response, true);
					
				}else if(context.type == 'edit'){
					
					var url = updateAPI;	//RSK API URL
					var jsonBody = {
						"NsId" : newRecordDetails.id,
						"Id" : 	Number(newRecordDetails.getValue({
							fieldId: 'custrecord_categoryid'
						})),
						"Name": newRecordDetails.getValue({
							fieldId: 'name'
						}),
                      	"AssetClassId" : Number(newRecordDetails.getValue({
							fieldId: 'custrecord_asset_class'
						}))
					};	
					
					log.debug("jsonBody - Edit", jsonBody);			
					
					response = http.request({
						method: https.Method.PUT,
						url: url,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					
					log.debug("Response", response);
					
					SetRSKID(newRecordDetails, response, true);
				} else {
					return;
				}
				
			}catch(err){
				log.debug("Error", err);
				SetFlag(newRecordDetails, 400, "Server Error");
				return;
			}
		}
		
		function SetRSKID(newRecordDetails, response, flag){
			/* Checking for the Response */
			var data = JSON.parse(response.body); 
			var dataCode = response.code; 
			// log.debug("data",data);
			log.debug("dataCode",dataCode);
			
			if(dataCode == "200"){
				return;
			}else{
				SetFlag(newRecordDetails, dataCode, data.message);
			}
		}
		
		function SetFlag(newRecordDetails, dataCode, message){
			
			/* Flagging the records if the API failed */
			var rec = record.load({type : newRecordDetails.type, id : newRecordDetails.id});
			
			rec.setValue({
				fieldId: "custrecord_categoryflag",	//Flag Checkbox Field
				value: true,				
				ignoreFieldChange: true
			});
			rec.setValue({
				fieldId: "custrecord_categoryremarks",	//Flag Remark Field
				value: dataCode + " : " + message,				
				ignoreFieldChange: true
			});
			
			rec.save();
		}
		
		return {
			beforeSubmit : beforeSubmit,
			afterSubmit : afterSubmit
		};
	});	