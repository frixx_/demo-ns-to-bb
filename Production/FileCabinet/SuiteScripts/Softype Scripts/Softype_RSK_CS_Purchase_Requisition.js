/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  10th September, 2019
 ** @Version     :  2.x
 ** @Description :  Client Script for Purchase Requisition Canvassing.

 ***************************************************************************************/
 
define(['N/currentRecord', 'N/url', 'N/search', 'N/record', 'N/runtime'],


    function(currentRecord, url, search, record, runtime) {


        /**
         * Function to be executed after page is initialized.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
         *
         * @since 2015.2
         */
        function pageInit(scriptContext) {



        }


        /**
         * Function to be executed when field is changed.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @param {string} scriptContext.sublistId - Sublist name
         * @param {string} scriptContext.fieldId - Field name
         * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
         * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
         *
         * @since 2015.2
         */

        function fieldChanged(scriptContext) {


            var currentRecordObj = currentRecord.get();

            /** Returns pageId for Pagination -- Start **/
            if (scriptContext.fieldId == 'custpage_pageid') {

                var pageId = currentRecordObj.getValue('custpage_pageid');

                pageId = parseInt(pageId.split('_')[1]);

                window.onbeforeunload = null;
                //location.reload();

                var suitelet_URL = url.resolveScript({

                    scriptId: getParameterFromURL('script'),
                    deploymentId: getParameterFromURL('deploy'),
                    params: {
                        'page': pageId
                    }
                });


                suitelet_URL += '&action=actionpageId';


                window.open(suitelet_URL, '_self');

            }
            /** Returns pageId for Pagination -- End **/


            /** Returns the action 'getFilters' and the 'canvasser', 'department', 'subsidiary' to filter Purchase Requisitions -- Start **/
            if (scriptContext.fieldId == 'custpage_canvasser' || scriptContext.fieldId == 'custpage_subsidiary' || scriptContext.fieldId == 'custpage_department' || scriptContext.fieldId == 'custpage_tradeandnontrade') {

                var canvasser = currentRecordObj.getValue('custpage_canvasser');
				var department = currentRecordObj.getValue('custpage_department');
                var subsidiary = currentRecordObj.getValue('custpage_subsidiary');
			    var tradeAndNonTrade = currentRecordObj.getValue('custpage_tradeandnontrade');

                window.onbeforeunload = null;

                var suitelet_URL = url.resolveScript({
                    scriptId: 'customscript_softype_st_purchase_req',
                    deploymentId: 'customdeploy_softype_st_purchase_req',
                    returnExternalUrl: false
                });

                suitelet_URL += '&action=getFilters';
                suitelet_URL += '&canvasser=' + canvasser;
                suitelet_URL += '&subsidiary=' + subsidiary;
				suitelet_URL += '&department=' + department;
				suitelet_URL += '&tradeAndNonTrade=' + tradeAndNonTrade;
				
				
				
                window.open(suitelet_URL, '_self');


            }
            /** Returns the action 'getFilters' and the 'canvasser', 'department', 'subsidiary' to filter Purchase Requisitions -- End **/

			
			

        }



        /**
         * Validation function to be executed when record is saved.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @returns {boolean} Return true if record is valid
         *
         * @since 2015.2
         */

        function saveRecord(scriptContext) {

            var scriptObj = runtime.getCurrentScript();
            var currentRecordObj = currentRecord.get();

            var ALL_PR_ID_ITEM_JSON = [];
            var PR_ID_ITEM_JSON = [];
            var nonCheckedPRItemArray = [];
            var checkedItemArray = [];


            var atLeastOneSelected = false;


            var lineCount = currentRecordObj.getLineCount('custpage_sublistid');

            for (var i = 0; i < lineCount; i++) {

                var checked = currentRecordObj.getSublistValue({
                    sublistId: 'custpage_sublistid',
                    fieldId: 'custpage_checkbox',
                    line: i
                });

                var prId = currentRecordObj.getSublistValue({
                    sublistId: 'custpage_sublistid',
                    fieldId: 'custpage_internalid',
                    line: i
                });

                var prItem = currentRecordObj.getSublistValue({
                    sublistId: 'custpage_sublistid',
                    fieldId: 'custpage_itemvalue',
                    line: i
                });

                /** JSON of all the Line's irrespective of whether it is checked or not 
                	- prId, prItem, checked
                
                **/

                ALL_PR_ID_ITEM_JSON.push({

                    prId: prId,
                    prItem: prItem,
                    checked: checked

                });



                /** JSON of only checked Line's - prId.
                	And array of checkedItemArray.
                
                **/

                if (checked == true) {

                    PR_ID_ITEM_JSON.push({

                        prId: prId

                    });

                    checkedItemArray.push(prItem)

                    atLeastOneSelected = true;

                }


            }


            /** Groups the common PR ID and assigns all the item and if it is checked or not against it **/
            var unique_PRId_Item_Json = uniquePRIdItemJson(PR_ID_ITEM_JSON);
            var unique_All_PRId_Item_Json = uniquePRIdItemJson(ALL_PR_ID_ITEM_JSON);



            for (var u = 0; u < unique_PRId_Item_Json.length; u++) {

                for (var a = 0; a < unique_All_PRId_Item_Json.length; a++) {

                    /** Checking if the prId from checked json is present in all line's json **/
                    if (unique_PRId_Item_Json[u].prId == unique_All_PRId_Item_Json[a].prId) {

                        /** If it matches get the value of checked from all line's json **/
                        for (var b = 0; b < unique_All_PRId_Item_Json[a].data.length; b++) {

                            var checked = unique_All_PRId_Item_Json[a].data[b].checked;

                            /** 
                            	If check is false push the items of that pr into an array - nonCheckedPRItemArray.
                            	(only false items are pushed because checked items are already pushed - checkedItemArray)
                            
                            **/
                            if (checked == false) {

                                var item = unique_All_PRId_Item_Json[a].data[b].prItem;

                                nonCheckedPRItemArray.push(item);

                            }

                        }

                    }

                }

            }


            /** Get the unique of checkedItemArray and nonCheckedPRItemArray **/
            var uniqueCheckedItem = unique(checkedItemArray);
            var uniqueNonCheckedItem = unique(nonCheckedPRItemArray);


            if (uniqueCheckedItem.length > uniqueNonCheckedItem.length) {

                var largeArray = uniqueCheckedItem;
                var smallArray = uniqueNonCheckedItem;

            } else {
                var smallArray = uniqueCheckedItem;
                var largeArray = uniqueNonCheckedItem;
            }




            if (uniqueNonCheckedItem.length > 0) {

                var match = findMatch(smallArray, largeArray);


                if (match.length > 0) {

                    alert('Please select all Purchase Requisition');
                    return false;

                }

            }


            if (!atLeastOneSelected) {

                alert('You must select at least one Purchase Requisition.');
                return false;
            }


            return true;

        }








        /** Pagination **/

        function getSuiteletPage(suiteletScriptId, suiteletDeploymentId, pageId) {

            window.onbeforeunload = null;
            location.reload();
            document.location = url.resolveScript({
                scriptId: suiteletScriptId,
                deploymentId: suiteletDeploymentId,
                params: {
                    'page': pageId
                }
            });
        }



        function getParameterFromURL(param) {

            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == param) {
                    return decodeURIComponent(pair[1]);
                }
            }
            return (false);
        }




        /** Returns unique elements from an Array **/
        function unique(array) {

            var arr = []
            for (var i = 0; i < array.length; i++) {

                if (arr.indexOf(array[i]) === -1 && array[i] !== '') {

                    arr.push(array[i]);
                }
            }
            return arr;
        }



        /** Returns matching elements from two array's**/
        function findMatch(array_1_small, array2_large) {

            var ary = new Array();
            for (i = 0; i < array2_large.length; i++) {
                for (z = 0; z < array_1_small.length; z++) {
                    if (array2_large[i] == array_1_small[z]) {
                        ary.push(i);

                    }
                }

            }

            return ary;


        }




        /** Groups the common PR ID and assigns all the item and if it is checked or not against it **/
        function uniquePRIdItemJson(PR_ID_ITEM_JSON) {

            var groups = {};
            for (var i = 0; i < PR_ID_ITEM_JSON.length; i++) {

                var groupName = PR_ID_ITEM_JSON[i].prId;


                if (!groups[groupName]) {
                    groups[groupName] = [];
                }
                groups[groupName].push({
                    prItem: PR_ID_ITEM_JSON[i].prItem,
                    checked: PR_ID_ITEM_JSON[i].checked
                });

            }
            PR_ID_ITEM_JSON = [];
            for (var groupName in groups) {
                PR_ID_ITEM_JSON.push({
                    prId: groupName,
                    data: groups[groupName]
                });
            }
            return PR_ID_ITEM_JSON;

        }

        function cancel() {

            window.onbeforeunload = null;
            var outputUrl = url.resolveScript({
                scriptId: 'customscript_softype_st_purchase_req',
                deploymentId: 'customdeploy_softype_st_purchase_req',
                returnExternalUrl: false
            });

            window.open(outputUrl, '_self');


        }




        return {

            //pageInit: pageInit,
            fieldChanged: fieldChanged,
            getSuiteletPage: getSuiteletPage,
            saveRecord: saveRecord,
            cancel: cancel

        }
    });