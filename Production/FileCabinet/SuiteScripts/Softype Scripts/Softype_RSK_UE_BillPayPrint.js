/***************************************************************************************  
** Copyright (c) 1998-2018 Softype, Inc.
** P3 Zenith Central | Luzon Avenue | Cebu Business Park, Cebu City | Philippines
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
**                      
**@Author      :  Renato Cada Jr
**@Dated       :  25 February 2020	
**@Description :  This is script is for invoce transaction print
**
**@NApiVersion 2.1
**@NScriptType UserEventScript
**@NModuleScope SameAccount
*****************************************************************************************/
define(['N/record', 'N/url', 'N/search'],

function(record, url, search) {
   
 
    function beforeLoad(scriptContext) {
    	const currentRecord = scriptContext.newRecord;
    	
    	const approvalStatus = currentRecord.getValue({fieldId: 'approvalstatus'});
    	log.debug('approvalStatus',approvalStatus);
    	
    	
    	try{
	    		const currentUser = currentRecord.getValue({fieldId: 'nluser'});
	        	log.debug('currentUser',currentUser);
	        	
	        	const loadEmployee = record.load({
	        		type: 'employee',
	        		id: currentUser
	        	})	
	        	
	    	//get the admin role Id
	        const userRole = currentRecord.getValue({fieldId: 'nlrole'});
	        log.debug('userRole',userRole);
	        let thisUser = isNaN(userRole)? '0':  userRole;
	        log.debug('thisUser',thisUser);
	        
	       //call view function when status is 1 and role is admin or status is 1 only
	    	return approvalStatus!=1 && thisUser!=3? true: view();
    	}catch(e){
    		log.error('error message',e.message);
    	}
    	
    	function view(){
    		// Getting the URL to open the suitelet.
    		var outputUrl = url.resolveScript
    		({
    			scriptId: 'customscript_softype_rsk_st_billpayprint',
    			deploymentId: 'customdeploy_softype_rsk_st_billpayprint',
    			returnExternalUrl: false
    		});		
    		// Getting the record id.
    		var recordId = currentRecord.id;
    		log.debug('record Id', recordId);
    		
    		// Adding parameters to pass in the suitelet.
    		outputUrl += '&action=GET';
    		outputUrl += '&recordid=' + recordId;
    		
    		// Creating function to redirect to the suitelet.
    		var stringScript = "window.open('"+outputUrl+"','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";
    		
    		var printButton = scriptContext.form.addButton
			({
				id: 'custpage_drprint',
				label: 'Official Print',
				functionName: stringScript
			});
    	}
    }
    
    return {
        beforeLoad: beforeLoad,
    };
    
});
