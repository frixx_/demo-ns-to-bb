/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : After creation of Location record in Netsuite script calls RSK API to create Location Master in RSK System
***************************************************************************************/

define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, record, runtime, log) {
		
		function afterSubmit(context) {
		
			var newRecordDetails = context.newRecord;
			try{
				/* log.debug('runtime',runtime);
				log.debug('runtime.executionContext',runtime.executionContext);
				log.debug('context',context);
				
				log.debug('newRecordDetails', newRecordDetails); */
				
				var scriptObj = runtime.getCurrentScript();
				var createAPI = scriptObj.getParameter({name: 'custscript_create_api_location'});
				var updateAPI = scriptObj.getParameter({name: 'custscript_update_api_location'});
				
				
				/**********************************************************
					*************************API CALL************************
				**********************************************************/
				
				var headers = {
					"Content-Type": "application/json"
				};	//Headers if Any
				
				var response;
				var rskID = newRecordDetails.getValue({fieldId: 'custrecord_location_rskidfield'});
				
				var isInActive = newRecordDetails.getValue({fieldId: 'isinactive'});	
				if(context.type == 'create' || rskID == ""){
					
					var url = createAPI;	//RSK API URL
					var jsonBody = {
						"NsId": newRecordDetails.id,
						"Name": newRecordDetails.getValue({
							fieldId: 'name'
						}),
						"LocationTypeId": 1,
						"IsActive": !(isInActive)
					};	
					
					log.debug("jsonBody - Create", jsonBody);			
					
					// If the API is https compliant
					response = http.request({
						method: https.Method.POST,
						url: url,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					
					SetRSKID(newRecordDetails, response, true);
					
				}else if(context.type == 'edit'){
					
					var url = updateAPI;	//RSK API URL
					var jsonBody = {
						"NsId": newRecordDetails.id,
						"Id" : Number(newRecordDetails.getValue({
							fieldId: 'custrecord_location_rskidfield'
						})),
						"Name": newRecordDetails.getValue({
							fieldId: 'name'
						}),
						"LocationTypeId": 1,
						"IsActive": !(isInActive)
					};	
					
					log.debug("jsonBody - Edit", jsonBody);
					
					// If the API is https compliant
					response = http.request({
						method: https.Method.PUT,
						url: url,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					SetRSKID(newRecordDetails, response, true);
				}
			}
			catch(err){
				log.debug("Error", err);
				SetFlag(newRecordDetails, 400, "Server Error");
				return;
			}
		}
		
		function SetRSKID(newRecordDetails, response, flag){
			
			log.debug("response",response);
			var data = JSON.parse(response.body); 
			var dataCode = response.code; 
			log.debug("data",data);
			// log.debug("data.Id",data.Id);
			log.debug("dataCode",dataCode); 
			
			if(dataCode == "200"){
				
				/* newRecordDetails.setValue({
					fieldId: "custrecord_location_flagfield",	//Flag Checkbox Field
					value: false,				
					ignoreFieldChange: true
					});
					
					newRecordDetails.setValue({
					fieldId: "custrecord_location_rskidfield",
					value: data.Id,				
					ignoreFieldChange: true
				}); */
				
				return;
				
			}else{
				SetFlag(newRecordDetails, dataCode, data.message);
			}
		}
		
		function SetFlag(newRecordDetails, dataCode, message){
			
			var rec = record.load({type : newRecordDetails.type, id : newRecordDetails.id});
			
			rec.setValue({
				fieldId: "custrecord_location_flagfield",	//Flag Checkbox Field
				value: true,				
				ignoreFieldChange: true
			});
			rec.setValue({
				fieldId: "custrecord_location_remark",	//Flag Remark Field
				value: dataCode + " : " + message,				
				ignoreFieldChange: true
			});
			rec.save();
		}
		
		return {
			afterSubmit: afterSubmit
		};
	});	