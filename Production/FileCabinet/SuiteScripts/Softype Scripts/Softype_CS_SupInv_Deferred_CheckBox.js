/**

 * @NApiVersion 2.0

 * @NScriptType ClientScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 11/10/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : ClientScript for Checking Deferred Checkbox On the basis of Custom Record 'Match Tax Codes'.  

 **************************************************************************************************************************************/
define(['N/currentRecord','N/search','N/runtime','N/record'],

function(currentRecord,search,runtime,record) {
    var orFieldChanged=false;
    function pageInit(scriptContext) {
        //alert('HIIII')
    }

    function saveRec(scriptContext){
        var currentRecordObj = currentRecord.get();
        /*if (scriptContext.fieldId == 'custpage_pageid') {
            var pageId = currentRecordObj.getValue({
                fieldId : 'custpage_pageid'
            });
            pageId = parseInt(pageId.split('_')[1]);
            pId=pageId;
            reloadPage();
        }
        if (scriptContext.fieldId == 'custpage_customer_name') {
            reloadPage();
        }

        if (scriptContext.fieldId == 'custpage_job') {
            reload();
        }*/
        var itemLineCOunt=currentRecordObj.getLineCount('item')
        var expenseLineCOunt=currentRecordObj.getLineCount('expense')
        //alert('item '+itemLineCOunt)
        //alert('expense '+expenseLineCOunt)


        //if (scriptContext.fieldId == 'taxcode') {
            if (itemLineCOunt>0) {
                for(var i=0;i<itemLineCOunt;i++){
                    currentRecordObj.selectLine({sublistId: 'item',line: i});
                    var taxCode=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'taxcode'});
                    //alert('sublist tax code'+taxCode)
                    var actualTaxCodePresent=getActualTaxCode(taxCode)
                    //alert('function returned code '+actualTaxCodePresent)
                    if (actualTaxCodePresent!='NOT') {
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_deferred_line',value:true});
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_actual_tax_code',value:actualTaxCodePresent}); 
                    }else{
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_deferred_line',value:false});
                    }
                    currentRecordObj.commitLine({sublistId: 'item'});
                }
            }

            if (expenseLineCOunt>0) {
                for(var i=0;i<expenseLineCOunt;i++){
                    currentRecordObj.selectLine({sublistId: 'expense',line: i});
                    var taxCode=currentRecordObj.getCurrentSublistValue({sublistId:'expense',fieldId:'taxcode'});
                    //alert('sublist tax code'+taxCode)
                    var actualTaxCodePresent=getActualTaxCode(taxCode)
                    //alert('function returned code '+actualTaxCodePresent)
                    if (actualTaxCodePresent!='NOT') {
                        currentRecordObj.setCurrentSublistValue({sublistId:'expense',fieldId:'custcol_deferred_line',value:true});
                        currentRecordObj.setCurrentSublistValue({sublistId:'expense',fieldId:'custcol_actual_tax_code',value:actualTaxCodePresent}); 
                    }else{
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_deferred_line',value:false});
                    }
                    currentRecordObj.commitLine({sublistId: 'expense'});
                }
            }

          return true;  
        //}
    }

    function getActualTaxCode(taxcode){
            var filterForTax=new Array();
            var columnForTax=new Array();
            //log.emergency('Inside Function', 'getJobs');

            filterForTax.push(search.createFilter({
                name: 'custrecord_deferred_tax_codes',
                operator: search.Operator.ANYOF,
                values: taxcode
            }));

            columnForTax.push(search.createColumn('custrecord_actual_tax_codes')); 

            var searchedMatchedTaxCode = search.create({
                'type': 'customrecord_match_tax_code',
                'filters': filterForTax,
                'columns': columnForTax
            }).run().getRange(0, 1000);

            if (searchedMatchedTaxCode.length>0) {
                var code=searchedMatchedTaxCode[0].getValue('custrecord_actual_tax_codes')
                return code;
            }else{
                return 'NOT';
            }

        }

    return {
        pageInit: pageInit,
        saveRecord: saveRec
    };
});