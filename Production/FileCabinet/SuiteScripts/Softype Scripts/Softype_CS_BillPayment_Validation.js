/**

 * @NApiVersion 2.0

 * @NScriptType ClientScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 30/09/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : ClientScript for Not Letting User Save the Vendor Payment Record if invoices of different tax code is selected.  

 **************************************************************************************************************************************/
define(['N/currentRecord','N/search','N/runtime','N/record'],

function(currentRecord,search,runtime,record) {
    var orFieldChanged=false;
    function pageInit(scriptContext) {
        //alert('HIIII')
        var currentRecordObj = scriptContext.currentRecord;
        var linecount = currentRecordObj.getLineCount({sublistId:'apply'});
        if (currentRecordObj.getValue('customform')) {
            //log.debug('Before Submit','FORM IS 115')
            if(linecount > 0){
                for(var i = 0; i < linecount; i++){
                    //var wTaxCodeSearchObj=search.load('customsearch62')
                    var checked = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'apply',line:i});
                    //alert('checked '+checked)
                    if(checked){
                        if (i==0) {
                            var dueDate = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'duedate',line:i});
                            //alert('dueDate '+dueDate)
                            currentRecordObj.setValue('trandate',dueDate)
                            break;
                        }
                    }
                }
            }
        }
    }

    function fieldChanged(scriptContext){
        /*var currentRecordObj = currentRecord.get();
        if (scriptContext.fieldId == 'custpage_pageid') {
            var pageId = currentRecordObj.getValue({
                fieldId : 'custpage_pageid'
            });
            pageId = parseInt(pageId.split('_')[1]);
            pId=pageId;
            reloadPage();
        }
        if (scriptContext.fieldId == 'custpage_customer_name') {
            reloadPage();
        }

        if (scriptContext.fieldId == 'custpage_job') {
            reload();
        }*/
        if (scriptContext.fieldId == 'custbody_ub_payout_or_num') {
            orFieldChanged=true;
        }
    }

    function saveRecord(context){
        var currentRecordObj = context.currentRecord; //.get();
        var totalSelected = [];
        var allBillsSelected=[];
        var linecount = currentRecordObj.getLineCount({sublistId:'apply'});
        //alert('linecount : '+linecount)
        var ubPayoutForm = runtime.getCurrentScript().getParameter('custscript_ub_payout_form');
        var WTaxSavedSearch = runtime.getCurrentScript().getParameter('custscript_wtax_code');
        //alert(ubPayoutForm)
        if (ubPayoutForm!=null && ubPayoutForm!='' && currentRecordObj.getValue('customform')==ubPayoutForm) {
            if(linecount > 0){
                for(var i = 0; i < linecount; i++){
                    var wTaxCodeSearchObj=search.load(WTaxSavedSearch)
                    var checked = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'apply',line:i});
                    var tranType = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'trantype',line:i});
                    var bills=currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'internalid',line:i});
                    //alert(bills)
                    if(checked && tranType=='VendBill'){
                        wTaxCodeSearchObj.filters.push(search.createFilter({
                            name: 'internalid',
                            operator: 'is',
                            values: bills
                        }));
                        var wTaxDetails=wTaxCodeSearchObj.run().getRange({start:0,end:1000});
                        //alert('wTaxDetails : '+wTaxDetails)
                        var getId = wTaxDetails[0].getValue('custbody_wtax_code_name')
                        //alert(getId)
                        if (getId=='') {
                            getId='custom'
                        }
                        totalSelected.push(getId);
                        allBillsSelected.push(bills);
                    }
                }
                if(totalSelected.length >  1){
                    //var allEqual=totalSelected.every((val, i, arr) => val === arr[0]);
                    //alert(totalSelected)
                    var allEqual=!!totalSelected.reduce(function(a, b){ return (a === b) ? a : NaN; });
                    if (!allEqual) {
                        alert ('Can Not Submit Vendor Bills With Different Tax Codes');
                        return false;
                    }
                }else if(totalSelected.length == 0){
                    alert('Please select Vendor Bills');
                    return false;
                }
                var orField=currentRecordObj.getValue('custbody_ub_payout_or_num')
                var orDateField=currentRecordObj.getValue('custbody_ub_payout_or_date')
                var refField=currentRecordObj.getValue('custbody_ub_payout_ref')
                if (allBillsSelected.length >=  1 && orField!='' && orFieldChanged) {
                    if (refField=='') {
                        alert('Ref# Field Is Empty')
                    }else{
                        for(var i=0;i<allBillsSelected.length;i++){
                            record.submitFields({
                                type: 'vendorbill',
                                id: allBillsSelected[i],
                                values: {
                                    'custbody_ub_payout_or_num': orField,
                                    'custbody_ub_payout_or_date': orDateField
                                }
                            });
                        }
                    }
                }
            }
        }
        
        return true;
    }


    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        saveRecord: saveRecord
    };
});