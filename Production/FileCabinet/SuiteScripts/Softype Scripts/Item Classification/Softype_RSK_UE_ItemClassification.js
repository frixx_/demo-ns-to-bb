/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : After creation of category record in Netsuite script calls RSK API to create Category Master in RSK System
***************************************************************************************/


define(['N/http', 'N/https', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, record, runtime, log) {
		
		function beforeSubmit(context) {
			
			if(context.type == "delete")
				return;
		
			var newRecordDetails = context.newRecord;
			var assetClass = newRecordDetails.getValue({fieldId: 'custrecord_classification_asset_class'});	
			
			log.debug("AssetClass", assetClass);
				
			if(assetClass == "" || assetClass == null){
				throw "Please add Asset Class";
				return;
			}
		}
		
		function afterSubmit(context) {
						
			if(context.type == "delete")
				return;
		
			var newRecordDetails = context.newRecord;
			try{				
				var name = newRecordDetails.getValue({fieldId: 'name'});
				var assetClass = newRecordDetails.getValue({fieldId: 'custrecord_classification_asset_class'});	
				
				if(assetClass == ""){
					throw "Please add Asset Class";
					return;
				}
				
				var itemCategoryRec = record.create({type: "customrecord_rskcategory"});
				itemCategoryRec.setValue({
					fieldId: "name",
					value: name
				});
				itemCategoryRec.setValue({
					fieldId: "custrecord_asset_class",
					value: assetClass
				});
				itemCategoryRec.setValue({
					fieldId: "custrecord_categoryflag",	//Flag Checkbox Field
					value: true,				
					ignoreFieldChange: true
				});
				itemCategoryRec.setValue({
					fieldId: "custrecord_categoryremarks",	//Flag Remark Field
					value: "Will be updated by Sync API",				
					ignoreFieldChange: true
				});
				var rec = itemCategoryRec.save();
				
				log.debug("Category Record", rec);
				
			/* 	var id = record.submitFields({
					type: "customrecord_cseg_itemclassifica",
					id: newRecordDetails.id,
					values: {
						"custrecord_classification_asset_class": assetClass
					},
					options: {
						enableSourcing: false,
						ignoreMandatoryFields : true
					}
				});
				 */
			}catch(err){
				log.debug("Error", err);
				return;
			}
		}
		
		return {
			beforeSubmit : beforeSubmit,
			afterSubmit : afterSubmit
		};
	});	