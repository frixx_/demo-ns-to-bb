/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  16th September, 2019
 ** @Version     :  2.x
 ** @Description :  User Event Script for updating the 'CANVASSING CHECK'  and 'CANVAASING LINK' on the Purchase Requisition.

 ***************************************************************************************/
define(['N/record', 'N/runtime'],

    function(record, runtime) {


        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {string} scriptContext.type - Trigger type
         * @param {Form} scriptContext.form - Current form
         * @Since 2015.2
         */
        function beforeLoad(scriptContext) {


        }


        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */

        function beforeSubmit(scriptContext) {

        }

        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */
        function afterSubmit(scriptContext) {



            var id = scriptContext.newRecord.id;
            var type = scriptContext.newRecord.type;

            var customRecItemValueArr = [];


            log.audit('scriptContext.type', scriptContext.type);

            if (scriptContext.type == 'create') {


                /** 
                	Loading Custom Record to get the unique PR Numbers' and 
                	getting the line count of that PRs'.
                **/

                var objRecord = record.load({
                    type: type,
                    id: id,
                    isDynamic: true,
                });

                var pr_number = objRecord.getValue({
                    fieldId: 'custrecord_pr_number'

                });

                var canvassingLink = objRecord.getValue({
                    fieldId: 'name'

                });

                log.audit('Canvassing Link ', canvassingLink);

                var prLineCount = objRecord.getLineCount({
                    sublistId: 'recmachcustrecord_link_canvassing'
                });


                for (var c = 0; c < prLineCount; c++) {

                    var customRecItemValue = objRecord.getSublistValue({
                        sublistId: 'recmachcustrecord_link_canvassing',
                        fieldId: 'custrecord_item_canvas',
                        line: c
                    });

                    customRecItemValueArr.push(customRecItemValue);

                }



                /** 
                	Looping through the unique PR Number to update the Canvassing Check  
                	on the Purchase Requisition so that it disappears from the saved search
                	'Purchase Requisition Canvassing' also from the Suitelet.
                	
                **/
				
                for (var pr = 0; pr < pr_number.length; pr++) {


                    var prObjRecord = record.load({
                        type: record.Type.PURCHASE_REQUISITION,
                        id: pr_number[pr],
                        //isDynamic: true,
                    });

                    prObjRecord.setValue({
                        fieldId: 'custbody_canvassing_link',
                        value: id

                    });

                    var lineCount = prObjRecord.getLineCount({
                        sublistId: 'item'
                    });

                    var ITEM_JSON = [];

                    for (var l = 0; l < lineCount; l++) {


                        var itemValue = prObjRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'item',
                            line: l
                        });

                        ITEM_JSON.push(itemValue);

                    }


                    for (var i = 0; i < ITEM_JSON.length; i++) {

                        if (customRecItemValueArr.indexOf(ITEM_JSON[i]) > -1) {

                            prObjRecord.setSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_canvassing_check',
                                line: i,
                                value: true
                            });

                        }
                    }

                    var updatedPRInternalId = prObjRecord.save({
                        ignoreMandatoryFields: true
                    });
					
                    log.audit("Updated PR Internal Id - Canvassing Check and Canvassing Link", updatedPRInternalId);

                }


            }

            var scriptObj = runtime.getCurrentScript();
            log.audit("Remaining Governance Units", scriptObj.getRemainingUsage());
        }



        return {

            // beforeLoad: beforeLoad,
            // beforeSubmit: beforeSubmit,
               afterSubmit: afterSubmit
        };

    });