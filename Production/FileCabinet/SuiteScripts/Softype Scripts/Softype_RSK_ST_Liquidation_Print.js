/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
/***************************************************************************************
	** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  03 March, 2020
 ** @Version     :  2.x
 ** @Description :  Suitelet for Liquidation Print.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/ui/serverWidget', 'N/record', 'N/render', 'N/xml', 'N/format', 'N/runtime',],

    function(serverWidget, record, render, xml, format, runtime) {


        function onRequest(context) {


            var recId = context.request.parameters.recordId;
   

            var objRecord = record.load({
                type: 'customrecord_supplier_inv_broker',
                id: recId,
                isDynamic: true
            });

            var name = objRecord.getValue({
                fieldId: 'name'

            });
            if (name == null)
                name == '';


            var recordId = objRecord.getValue({
                fieldId: 'recordid'

            });

            var writeCheckLink = objRecord.getText({
                fieldId: 'custrecord_write_chk_link'

            });
            if (writeCheckLink == null)
                writeCheckLink == '';


           /*  var BrokerEmployee = objRecord.getValue({
                fieldId: 'custrecord_broker'

            }); */
			
            var BrokerEmployee = objRecord.getText({
                fieldId: 'custrecord_broker'

            });
            if (BrokerEmployee == null)
                BrokerEmployee == '';


            var date = objRecord.getText({
                fieldId: 'custrecord_date'

            });
            if (date == null)
                date == '';


            var department = objRecord.getText({
                fieldId: 'custrecord_department'

            });
            if (department == null)
                department == '';


            var division = objRecord.getText({
                fieldId: 'custrecord_liq_div'

            });
            if (division == null)
                division == '';


            var location = objRecord.getText({
                fieldId: 'custrecord_loc_broker'

            });
            if (location == null)
                location == '';


            var approvalStatus = objRecord.getText({
                fieldId: 'custrecord_approval_sts'

            });

            var invoiceStatus = objRecord.getValue({
                fieldId: 'custrecord_invoice_status'

            });
			if(invoiceStatus == false){
				
				invoiceStatus = 'No'
			}else{
				
				invoiceStatus = 'Yes'
			}

            var LCNumber = objRecord.getText({
                fieldId: 'custrecord_lc_number'

            });
            if (LCNumber == null)
                LCNumber == '';


            var billOfLading = objRecord.getText({
                fieldId: 'custrecord_bill_of_lading'

            });
            if (billOfLading == null)
                billOfLading == '';


            var subsidiary = objRecord.getText({
                fieldId: 'custrecord_subsidiary'

            });
            if (subsidiary == null)
                subsidiary == '';


            var ShipmentNumber = objRecord.getText({
                fieldId: 'custrecord_shipment_number'

            });
            if (ShipmentNumber == null)
                ShipmentNumber == '';


            var grossTotalPostWHT = objRecord.getValue({
                fieldId: 'custrecord_totalamount'

            });
            if (grossTotalPostWHT == null)
                grossTotalPostWHT == '';


            var writeCheckAmount = objRecord.getValue({
                fieldId: 'custrecord_write_check_amt'

            });
            if (writeCheckAmount == null)
                writeCheckAmount == '';
			
			
			var isInactive = objRecord.getValue({
                fieldId: 'isinactive'

            });
			
			if(isInactive == false){
				
				isInactive = 'Yes'
			}else{
				
				isInactive = 'No'
			}
			
            




            var form = serverWidget.createForm({
                title: 'Print'
            });

            var htmlvar = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
            htmlvar += '<pdf>\n';




            // htmlvar += '<body  font-size="9" width="10in" height="6in">';
            htmlvar += '<body font-size="9" width="27.94cm" height="21.59cm">';

            htmlvar += '<table border="1px" width="100%"  margin-bottom="20px">';
            htmlvar += '<tr border-bottom="1px">';
            htmlvar += '<td width="15%" border-right="1px"><b>Name</b></td>';

            htmlvar += '<td width="15%" font-size="8" border-right="1px">' + xml.escape({
                xmlText: name
            }) + '</td> ';
            htmlvar += '<td width="15%" border-right="1px"><b>Division </b></td> ';
            htmlvar += '<td width="15%" border-right="1px">' + xml.escape({
                xmlText: division
            }) + '</td>';
            htmlvar += '<td width="15%" border-right="1px"><b>Gross Total Post WHT</b></td> ';
            htmlvar += '<td width="15%" align="right">' + numberWithCommas(grossTotalPostWHT)+ '</td> ';

            htmlvar += '</tr>';




            htmlvar += '<tr border-bottom="1px">';
            htmlvar += '<td width="15%" border-right="1px"><b>ID </b></td>';
            htmlvar += '<td width="15%" border-right="1px">' + recordId + '</td> ';
            htmlvar += '<td width="15%" border-right="1px"><b>Location</b></td> ';
            htmlvar += '<td width="15%" border-right="1px"><p align="left">' + xml.escape({
                xmlText: location
            }) + '</p></td>';
            htmlvar += '<td width="15%" border-right="1px"><b>Write Check Amount</b></td> ';
            htmlvar += '<td width="15%" align="right">' +numberWithCommas( writeCheckAmount )+ '</td> ';

            htmlvar += '</tr>';



            htmlvar += '<tr border-bottom="1px">';
            htmlvar += '<td width="15%" border-right="1px"><b>Inactive</b></td>';

            htmlvar += '<td width="15%" border-right="1px">'+isInactive+'</td> ';
            htmlvar += '<td width="15%" border-right="1px"><p align="left"><b>Approval Status</b></p> </td> ';
            htmlvar += '<td width="15%" border-right="1px">' + approvalStatus + '</td>';
            htmlvar += '<td width="15%" border-right="1px"></td> ';
            htmlvar += '<td width="15%"></td> ';
            htmlvar += '</tr>';



            htmlvar += '<tr border-bottom="1px">';
            htmlvar += '<td width="15%" border-right="1px"><b>Write Check</b></td>';
            htmlvar += '<td width="15%" border-right="1px"> ' + xml.escape({
                xmlText: writeCheckLink
            }) + '</td> ';
            htmlvar += '<td width="15%" border-right="1px"><b>Invoice Status</b></td> ';
            htmlvar += '<td width="15%" border-right="1px">' + invoiceStatus + '</td>';
            htmlvar += '<td width="15%" border-right="1px"></td> ';
            htmlvar += '<td width="15%"></td> ';
            htmlvar += '</tr>';



            htmlvar += '<tr border-bottom="1px">';
            htmlvar += '<td width="15%" border-right="1px"><b>Broker/Employee</b></td>';
            htmlvar += '<td width="15%" border-right="1px">' + xml.escape({
                xmlText: BrokerEmployee
            }) + '</td> ';
            htmlvar += '<td width="15%" border-right="1px"><b>LC Number</b></td> ';
            htmlvar += '<td width="15%" border-right="1px">' + xml.escape({
                xmlText: LCNumber
            }) + '</td>';
            htmlvar += '<td width="15%" border-right="1px"></td> ';
            htmlvar += '<td width="15%"></td> ';
            htmlvar += '</tr>';



            htmlvar += '<tr border-bottom="1px">';
            htmlvar += '<td width="15%" border-right="1px"><b>Date</b></td>';

            htmlvar += '<td width="15%" border-right="1px">' + date + ' </td> ';
            htmlvar += '<td width="15%" border-right="1px"><b>Bill of Lading</b></td> ';
            htmlvar += '<td width="15%" border-right="1px">' + xml.escape({
                xmlText: billOfLading
            }) + '</td>';
            htmlvar += '<td width="15%" border-right="1px"></td> ';
            htmlvar += '<td width="15%"></td> ';

            htmlvar += '</tr>';


            htmlvar += '<tr border-bottom="1px">';
            htmlvar += '<td width="15%" border-right="1px"><b>Subsidiary </b></td>';
            htmlvar += '<td width="15%" border-right="1px">' + xml.escape({
                xmlText: subsidiary
            }) + '</td> ';
            htmlvar += '<td width="15%" border-right="1px"><p align="left"><b>Shipment Number </b></p></td> ';
            htmlvar += '<td width="15%" border-right="1px"> ' + xml.escape({
                xmlText: ShipmentNumber
            }) + '</td>';
            htmlvar += '<td width="15%" border-right="1px"></td> ';
            htmlvar += '<td width="15%" ></td> ';
            htmlvar += '</tr>';




            htmlvar += '<tr >';
            htmlvar += '<td width="15%" border-right="1px"><b>Department</b> </td>';
            htmlvar += '<td width="15%" border-right="1px">' + xml.escape({
                xmlText: department
            }) + '</td> ';
            htmlvar += '<td width="15%" border-right="1px"></td> ';
            htmlvar += '<td width="15%" border-right="1px"></td>';

            htmlvar += '</tr>';
            htmlvar += '</table>';




            htmlvar += '<table border="1px">';

            htmlvar += '<tr border-bottom="1px">';
			
			
            htmlvar += '<td border-right="1px"><b>Expense/Item</b></td>';

            htmlvar += '<td border-right="1px"><b>Supplier</b></td> ';
            htmlvar += '<td border-right="1px"><b>Memo</b></td> ';
            htmlvar += '<td border-right="1px"><b>Department</b></td>';
            htmlvar += '<td border-right="1px"><b>Division</b></td> ';
            htmlvar += '<td border-right="1px"><b>WHT Code</b></td> ';
            htmlvar += '<td border-right="1px"><p align="left"><b>WHT Tax Rate</b></p></td>';
            htmlvar += '<td border-right="1px" width="13%"><b>Amount</b></td> ';
            htmlvar += '<td border-right="1px"><p align="left"><b>Tax Amount</b></p></td> ';
            htmlvar += '<td border-right="1px"><p align="left"><b>WHT Amount</b></p></td>';
            htmlvar += '<td border-right="1px"><p align="left"><b>Gross Amount After WHT</b></p></td> ';
            htmlvar += '<td border-right="1px"><b>OR#</b></td>';
            htmlvar += '<td border-right="1px"><p align="left"><b>INV/OR Date</b></p></td> ';
            htmlvar += '<td border-right="1px"><p align="left"><b>Type of Transaction</b></p></td> ';
            htmlvar += '<td><p align="left"><b>Tax Code</b></p></td>';
            htmlvar += '</tr>';



            var lineCount = objRecord.getLineCount({
                sublistId: 'recmachcustrecord_lin_broker_inv'
            });



            for (var l = 0; l < lineCount; l++) {


                var expense = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_expense_account',
                    line: l
                });
                if (expense == null)
                    expense == '';
				
				
				 var item = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_item_broker_inv',
                    line: l
                });
                if (item == null)
                    item == '';


                var quantity = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_qty',
                    line: l
                });
                if (quantity == null)
                    quantity == '';


                var supplier = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_supplier_bro_inv',
                    line: l
                });
                if (supplier == null)
                    supplier == '';


                var memo = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_memo_bro_inv',
                    line: l
                });
                if (memo == null)
                    memo == '';


                var department = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_liquidation_department',
                    line: l
                });
                if (department == null)
                    department == '';


                var division = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_liquidation_division',
                    line: l
                });
                if (division == null)
                    division == '';


                var WHTCode = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wtax_code',
                    line: l
                });
                if (WHTCode == null)
                    WHTCode == '';


                var WHTRate = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wth_tax_rate',
                    line: l
                });
                if (WHTRate == null)
                    WHTRate == '';


                var amount = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_amount',
                    line: l
                });
                if (amount == null)
                    amount == '';


                var taxAmount = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_tax_amount',
                    line: l
                });
                if (taxAmount == null)
                    taxAmount == '';


                var whtAmount = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wht_amount',
                    line: l
                });
                if (whtAmount == null)
                    whtAmount == '';


                var grossAmtAfterWHT = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_gross_amt_after_wht',
                    line: l
                });
                if (grossAmtAfterWHT == null)
                    grossAmtAfterWHT == '';


                var or = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_orref',
                    line: l
                });
                if (or == null)
                    or == '';

                var orDate = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_or_date_bro_inv',
                    line: l
                });
                if (orDate == null)
                    orDate == '';


                var typeOfTransaction = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'cseg_type_of_trans',
                    line: l
                });
                if (orDate == null)
                    orDate == '';


                var taxCode = objRecord.getSublistText({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_tax',
                    line: l
                });
                if (taxCode == null)
                    taxCode == '';



                htmlvar += '<tr border-bottom="1px">';
				
				if(expense != ''){
					htmlvar += '<td border-right="1px"><p align="left">' + xml.escape({
						xmlText: expense
					}) + '</p></td>';
				}
				if(item != ''){
					htmlvar += '<td border-right="1px"><p align="left">' + xml.escape({
						xmlText: item
					}) + '</p></td>';
				}
				
				
                // htmlvar += '<td border-right="1px">' + quantity + '</td> ';
                htmlvar += '<td border-right="1px">' + supplier + '</td> ';
                htmlvar += '<td border-right="1px">' + xml.escape({
                    xmlText: memo
                }) + '</td> ';
                htmlvar += '<td border-right="1px">' + xml.escape({
                    xmlText: department
                }) + '</td>';
                htmlvar += '<td border-right="1px">' + xml.escape({
                    xmlText: division
                }) + '</td> ';
                htmlvar += '<td border-right="1px">' + xml.escape({
                    xmlText: WHTCode
                }) + '</td> ';
                htmlvar += '<td border-right="1px">' + WHTRate + '</td>';
                htmlvar += '<td border-right="1px" width="13%">' + amount + '</td> ';
                htmlvar += '<td border-right="1px">' + taxAmount + '</td> ';
                htmlvar += '<td border-right="1px">' + whtAmount + '</td>';
                htmlvar += '<td border-right="1px">' + grossAmtAfterWHT + '</td> ';
                htmlvar += '<td border-right="1px">' + or + '</td>';
                htmlvar += '<td border-right="1px">' + orDate + '</td> ';
                htmlvar += '<td border-right="1px">' + typeOfTransaction + '</td> ';
                htmlvar += '<td><p align="left">' + xml.escape({
                    xmlText: taxCode
                }) + '</p></td>';
                htmlvar += '</tr>';




            }




            htmlvar += '</table>';




            htmlvar += '</body>\n</pdf>';


            var pdfFile = render.xmlToPdf({
                xmlString: htmlvar
            });

            return context.response.writeFile(pdfFile, true);



            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }




        }



        return {
            onRequest: onRequest
        };
    });