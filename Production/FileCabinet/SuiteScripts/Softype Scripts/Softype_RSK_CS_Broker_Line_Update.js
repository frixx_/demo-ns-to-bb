/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 ** @Author      :  Siddhi Kadam.
 ** @Dated       :  17th December, 2019
 ** @Version     :  2.x
 ** @Description : 1) This script is deployed on Liquidation of Bills custom record for calculating Amount, Tax Amount, Gross Amount, 
				   WHT Amount, Gross Amount After WHT.
				   2) For checking the 'CF Check' checkbox if the custom form is 'RSK Liquidation Against PO' 
            	   so that Generate Supplier Bills (PO) is visible.
				   3) For checking if both item and expense is selected on the same line, if yes then give a pop-up and blank it out.
				   4) To check if the correct item is selected against PO.
				   5) To check if the user has entered the correct quantity from the PO.
				   6) For updating current line item with the detail if the same reference and supplier already exists.
				   7) For calulating total gross amount after WHT.
				   8) For calculating the total RFP Amount of the selected RFP Numbers' on the Liquidation of Bills.
				   9) For calculating the total Check Amount of the selected Check Links' on the Liquidation of Bills.
					

 ***************************************************************************************/
define(['N/currentRecord', 'N/url', 'N/search', 'N/record', 'N/runtime', 'N/ui/dialog'],


    function(currentRecord, url, search, record, runtime, dialog) {


        /**
         * Function to be executed after page is initialized.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
         *
         * @since 2015.2
         */
        function pageInit(scriptContext) {


            var liquidationForm = Number(runtime.getCurrentScript().getParameter("custscript_softype_cs_liquidation_form"));
            var currentRec = currentRecord.get();


            /** For checking the 'CF Check' checkbox if the custom form is 'RSK Liquidation Against PO' 
            	so that Generate Supplier Bills (PO) is visible.
            	
            **/

            var customForm = Number(currentRec.getValue({
                fieldId: 'customform'

            }));

            if (customForm == liquidationForm) {

                currentRec.setValue({

                    fieldId: 'custrecord_cf_check',
                    value: true

                });

            }



        }


        /**
         * Function to be executed when field is changed.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @param {string} scriptContext.sublistId - Sublist name
         * @param {string} scriptContext.fieldId - Field name
         * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
         * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
         *
         * @since 2015.2
         */




        function fieldChanged(scriptContext) {


            var fieldId = scriptContext.fieldId;
            var currentRec = currentRecord.get();

            var currentRecordObj = scriptContext.currentRecord;
            var sublistName = scriptContext.sublistId;

            var PO_ARRAY = [];
            var SELECTED_ITEM_ARRAY = [];
            var SELECTED_EXPENSE_ARRAY = [];




            /** Checking if both item and expense is selected on the same line, if yes then give a pop-up and blank it out. - Start **/
            if (scriptContext.fieldId == 'custrecord_item_broker_inv' || scriptContext.fieldId == 'custrecord_expense_account') {

                var item = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_item_broker_inv'
                });

                var expense = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_expense_account'
                });


                if (item && expense) {

                    netsuiteAlert('You can either enter Item or Expense. ');

                    currentRec.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_item_broker_inv',
                        value: ''

                    });

                    currentRec.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_expense_account',
                        value: ''

                    });

                    return;

                }


            }
            /** Checking if both item and expense is selected on the same line, if yes then give a pop-up and blank it out. - End **/


            /** PO Validations - Start **/

            /** To check if the correct item is selected against PO - Start **/



            if (scriptContext.fieldId == 'custrecord_item_broker_inv' || scriptContext.fieldId == 'custrecord_expense_account') {

                var liquidationForm = Number(runtime.getCurrentScript().getParameter("custscript_softype_cs_liquidation_form"));

                var customForm = Number(currentRec.getValue({
                    fieldId: 'customform'

                }));

                if (customForm != liquidationForm) {

                    return;

                }


                var PONumber = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_po_number'
                });


                var item = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_item_broker_inv'
                });

                var expense = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_expense_account'
                });

                SELECTED_ITEM_ARRAY.push(item);
                SELECTED_EXPENSE_ARRAY.push(expense);


                var objRecord = record.load({
                    type: record.Type.PURCHASE_ORDER,
                    id: PONumber,
                    isDynamic: true,
                });

                if (item) {



                    var lineCount = objRecord.getLineCount({
                        sublistId: 'item'
                    });



                    for (var a = 0; a < lineCount; a++) {
                        var poQuanity;

                        var poItem = objRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'item',
                            line: a
                        });

                        poQuanity = objRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'item',
                            line: a
                        });


                        PO_ARRAY.push(poItem);


                    }
                    var match = findMatch(SELECTED_ITEM_ARRAY, PO_ARRAY);

                    if (match.length <= 0) {


                        netsuiteAlert('You have selected wrong Item. ');

                        currentRec.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_lin_broker_inv',
                            fieldId: 'custrecord_item_broker_inv',
                            value: '',
                            ignoreFieldChange: true

                        });
                        return false;

                    }

                } else if (expense) {

                    var lineCount = Number(objRecord.getLineCount({
                        sublistId: 'expense'
                    }));


                    for (var b = 0; b < lineCount; b++) {


                        var poExpense = objRecord.getSublistValue({
                            sublistId: 'expense',
                            fieldId: 'account',
                            line: b
                        });

                        PO_ARRAY.push(poExpense);


                    }
                    var match = findMatch(SELECTED_EXPENSE_ARRAY, PO_ARRAY);

                    if (match.length <= 0) {


                        netsuiteAlert('You have selected wrong Expense Account. ');

                        currentRec.setCurrentSublistValue({
                            sublistId: 'recmachcustrecord_lin_broker_inv',
                            fieldId: 'custrecord_expense_account',
                            value: '',
                            ignoreFieldChange: true

                        })
                        return false;

                    }




                }



            }
            /** To check if the correct item is selected against PO - End **/


            /** To check if the user has entered the correct quantity from the PO - Start **/

            if (scriptContext.fieldId == 'custrecord_brok_inv_qty') {


                var liquidationForm = Number(runtime.getCurrentScript().getParameter("custscript_softype_cs_liquidation_form"));

                var customForm = Number(currentRec.getValue({
                    fieldId: 'customform'

                }));

                if (customForm != liquidationForm) {

                    return;

                }


                var PONumber = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_po_number'
                });


                var item = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_item_broker_inv'
                });

                var itemQuantity = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_qty'
                });

                var expense = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_expense_account'
                });

                SELECTED_ITEM_ARRAY.push({
                    "item": item,
                    "itemQuantity": itemQuantity

                });

                SELECTED_EXPENSE_ARRAY.push(expense);


                var objRecord = record.load({
                    type: record.Type.PURCHASE_ORDER,
                    id: PONumber,
                    isDynamic: true,
                });

                if (item) {



                    var lineCount = objRecord.getLineCount({
                        sublistId: 'item'
                    });



                    for (var a = 0; a < lineCount; a++) {

                        var poItem = objRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'item',
                            line: a
                        });

                        var poQuantity = objRecord.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            line: a
                        });


                        PO_ARRAY.push({

                            "poItem": poItem,
                            "poQuantity": poQuantity


                        });


                    }

                    var match = JSONMatch(PO_ARRAY, SELECTED_ITEM_ARRAY);



                    if (match) {

                        var iQuantity = SELECTED_ITEM_ARRAY[0].itemQuantity;

                        if (iQuantity > match) {
                            netsuiteAlert('Quantity is more than the PO Quantity. ');


                        }

                    }

                }
            }
            /** To check if the user has entered the correct quantity from the PO - End **/

            /** PO Validations - End **/




            /** VAT START **/

            /** Setting custom rate on the rate and also fixing the decimal. - Start **/
            if (scriptContext.fieldId == 'custrecord_custom_rate') {

                var custom_rate = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_custom_rate'
                });


                currentRec.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_rate',
                    value: custom_rate.toFixed(3)
                });

                var customRate = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_custom_rate',

                }));
                customRate = customRate.toFixed(3)



                currentRec.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_custom_rate',
                    value: customRate,
                    ignoreFieldChange: true
                });


            }
            /** Setting custom rate on the rate and also fixing the decimal. - End **/



            if (scriptContext.fieldId == 'custrecord_incl_vat') {

                var vat_checkbox = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_incl_vat'
                });
                if (vat_checkbox == false) {

                    var custom_rate = currentRec.getCurrentSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_custom_rate'
                    });
                    currentRec.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_brok_inv_rate',
                        value: custom_rate
                    });


                }
            }


            /** Calculating VAT - Start **/
            if (scriptContext.fieldId == 'custrecord_tax_code_rate' || scriptContext.fieldId == 'custrecord_incl_vat') {



                var taxcode = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_tax_code'
                });

                var vat_checkbox = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_incl_vat'
                });

                var quantity = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_qty'
                });

                var tax_rate = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_tax_code_rate'
                });



                var custom_rate = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_custom_rate'
                });


                if (tax_rate != '' && vat_checkbox == true) {

                    //var split_taxrate = tax_rate.replace('%', '');

                    //var rateAfterVAT = custom_rate - (custom_rate / (1 + tax_rate) * tax_rate);
                    var rateAfterVAT = custom_rate - (custom_rate / (1 + tax_rate / 100));
                    rateAfterVAT = Number(custom_rate) - Number(rateAfterVAT)



                    currentRec.setCurrentSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_brok_inv_rate',
                        value: rateAfterVAT.toFixed(3)
                    });


                }

            }
            /** Calculating VAT - End **/

            /** VAT END **/




            /** Updating current line item with the detail if the same reference and supplier already exists - Start **/
            if (fieldId == 'custrecord_supplier_bro_inv') {

                var CURRENT_LINE_ITEM_JSON = [];

                var LINE_ITEM_JSON = [];
                var flag = false;


                var currentReference = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_ref_broker'
                });


                var currentSupplier = currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_supplier_bro_inv'
                });


                CURRENT_LINE_ITEM_JSON.push({


                    "currentReference": currentReference,
                    "currentSupplier": currentSupplier,


                });



                var lineCount = currentRec.getLineCount({
                    sublistId: 'recmachcustrecord_lin_broker_inv'
                });


                for (var a = 0; a < lineCount; a++) {


                    var reference = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_ref_broker',
                        line: a
                    });

                    var supplier = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_supplier_bro_inv',
                        line: a
                    });

                    var currency = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_currency_bro_inv',
                        line: a
                    });

                    var exchangeRate = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_exc_rate',
                        line: a
                    });

                    var invoiceDate = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_inv_date',
                        line: a
                    });

                    var counterDate = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_counter_date',
                        line: a
                    });

                    var dueDate = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_due_date',
                        line: a
                    });

                    var memo = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_memo_bro_inv',
                        line: a
                    });

                    var orRef = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_orref',
                        line: a
                    });

                    var orDate = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_or_date_bro_inv',
                        line: a
                    });

                    var terms = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_brok_inv_terms',
                        line: a
                    });

                    var typeOfTransaction = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'cseg_type_of_trans',
                        line: a
                    });

                    var itemClassification = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'cseg_itemclassifica',
                        line: a
                    });

                    var VATType = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_vat_type_brok_bill',
                        line: a
                    });

                    var tradeNonTrade = currentRec.getSublistValue({
                        sublistId: 'recmachcustrecord_lin_broker_inv',
                        fieldId: 'custrecord_liqu_trade_nontrade',
                        line: a
                    });

                    var index = currentRec.getCurrentSublistIndex({

                        sublistId: 'recmachcustrecord_lin_broker_inv'

                    });



                    if (a != index) {

                        LINE_ITEM_JSON.push({

                            reference: reference,
                            supplier: supplier,
                            currency: currency,
                            exchangeRate: exchangeRate,
                            invoiceDate: invoiceDate,
                            counterDate: counterDate,
                            dueDate: dueDate,
                            memo: memo,
                            orRef: orRef,
                            orDate: orDate,
                            terms: terms,
                            typeOfTransaction: typeOfTransaction,
                            itemClassification: itemClassification,
                            VATType: VATType,
                            tradeNonTrade: tradeNonTrade

                        });



                    }


                }


                for (var b = 0; b < CURRENT_LINE_ITEM_JSON.length; b++) {


                    for (var c = 0; c < LINE_ITEM_JSON.length; c++) {




                        if (CURRENT_LINE_ITEM_JSON[b].currentReference == LINE_ITEM_JSON[c].reference && CURRENT_LINE_ITEM_JSON[b].currentSupplier == LINE_ITEM_JSON[c].supplier) {
                            flag = true;



                            var currency = LINE_ITEM_JSON[c].currency;
                            var exchangeRate = LINE_ITEM_JSON[c].exchangeRate;
                            var invoiceDate = LINE_ITEM_JSON[c].invoiceDate;
                            var counterDate = LINE_ITEM_JSON[c].counterDate;
                            var dueDate = LINE_ITEM_JSON[c].dueDate;
                            var memo = LINE_ITEM_JSON[c].memo;
                            var orRef = LINE_ITEM_JSON[c].orRef;
                            var orDate = LINE_ITEM_JSON[c].orDate;
                            var terms = LINE_ITEM_JSON[c].terms;
                            var typeOfTransaction = LINE_ITEM_JSON[c].typeOfTransaction;
                            var itemClassification = LINE_ITEM_JSON[c].itemClassification;
                            var VATType = LINE_ITEM_JSON[c].VATType;
                            var tradeNonTrade = LINE_ITEM_JSON[c].tradeNonTrade;




                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_currency_bro_inv',
                                value: currency
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_exc_rate',
                                value: exchangeRate

                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_inv_date',
                                value: invoiceDate
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_counter_date',
                                value: counterDate
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_due_date',
                                value: dueDate
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_memo_bro_inv',
                                value: memo
                            });


                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_orref',
                                value: orRef
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_or_date_bro_inv',
                                value: orDate
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_brok_inv_terms',
                                value: terms
                            });


                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'cseg_type_of_trans',
                                value: typeOfTransaction
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'cseg_itemclassifica',
                                value: itemClassification
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_vat_type_brok_bill',
                                value: VATType
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_liqu_trade_nontrade',
                                value: tradeNonTrade
                            });




                        } else {

                            if (flag == true) {

                                break;
                            }


                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_currency_bro_inv',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_exc_rate',
                                value: ''

                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_inv_date',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_counter_date',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_due_date',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_memo_bro_inv',
                                value: ''
                            });


                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_orref',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_or_date_bro_inv',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_brok_inv_terms',
                                value: ''
                            });


                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'cseg_type_of_trans',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'cseg_itemclassifica',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_vat_type_brok_bill',
                                value: ''
                            });

                            currentRec.setCurrentSublistValue({
                                sublistId: 'recmachcustrecord_lin_broker_inv',
                                fieldId: 'custrecord_liqu_trade_nontrade',
                                value: ''
                            });


                            // break;

                        }
                        // if (flag == true) {

                        // break;
                        // }



                    }


                }


            }
            /** Updating current line item with the detail if the same reference and supplier already exists - End **/


            /** For calculating Amount, Tax Amount, Gross Amount, WHT Amount, Gross Amount After WHT - Start **/
            if (fieldId == 'custrecord_brok_inv_rate' || fieldId == 'custrecord_brok_inv_qty') {


                var quantity = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_qty'
                }));

                var rate = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_brok_inv_rate'
                }));

                var amount = quantity * rate;


                currentRec.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_amount',
                    value: amount.toFixed(2)
                });

            }


            if (fieldId == 'custrecord_tax_code_rate' || fieldId == 'custrecord_amount') {


                var taxRate = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_tax_code_rate'
                }));

                var amount = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_amount'
                }));

                var taxAmount = ((amount * taxRate) / 100);


                currentRec.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_tax_amount',
                    value: taxAmount.toFixed(2)
                });

            }

            if (fieldId == 'custrecord_tax_amount') {


                var taxAmount = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_tax_amount'
                }));

                var amount = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_amount'
                }));

                var grossAmount = taxAmount + amount;


                currentRec.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_gross_amount',
                    value: grossAmount.toFixed(2)
                });


            }

            if (fieldId == 'custrecord_wth_tax_rate' || fieldId == 'custrecord_amount') {


                var WHTRate = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wth_tax_rate'
                }));

                var amount = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_amount'
                }));

                var WHTAmount = ((amount * WHTRate) / 100);


                currentRec.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wht_amount',
                    value: WHTAmount.toFixed(2)
                });

            }

            if (fieldId == 'custrecord_wht_amount') {


                var WHTAmount = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_wht_amount'
                }));

                var grossAmount = Number(currentRec.getCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_gross_amount'
                }));

                var grossAmountAfterWHT = grossAmount - WHTAmount;


                currentRec.setCurrentSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_gross_amt_after_wht',
                    value: grossAmountAfterWHT.toFixed(2)
                });

            }
            /** For calculating Amount, Tax Amount, Gross Amount, WHT Amount, Gross Amount After WHT - End **/



        }

        function lineInit(scriptContext) {


            var currentRecordObj = scriptContext.currentRecord;
            var sublistName = scriptContext.sublistId;
            var itemCount = currentRecordObj.getLineCount('recmachcustrecord_lin_broker_inv');
            var line = currentRecordObj.getCurrentSublistIndex({
                sublistId: sublistName
            });


            var override = currentRecordObj.getCurrentSublistValue({
                sublistId: sublistName,
                fieldId: 'custrecord_incl_vat'
            });

            var Line = line - 1;
            if (Line >= 0) {
                var vatinclField = currentRecordObj.getSublistField({
                    sublistId: sublistName,
                    fieldId: 'custrecord_incl_vat',
                    line: Line
                });
                vatinclField.isDisabled = false;
            }

            if (itemCount > line) {


                var vatinclField = currentRecordObj.getSublistField({
                    sublistId: sublistName,
                    fieldId: 'custrecord_incl_vat',
                    line: line
                });
                if (override == 1) {
                    vatinclField.isDisabled = true;
                } else {
                    vatinclField.isDisabled = false;
                }
            }




        }




        function saveRecord(scriptContext) {


            var currentRec = currentRecord.get();

            var totalgrossAmountAfterWHT = 0;

            var totalRFPAmount = 0;
            var RFPNumbers = [];
			var RFPNumbersArray = []
            var checkLinks = [];
			var checkLinksArray = [];
            var totalCheckAmount = 0;

            /** For calulating total gross amount after WHT. - Start **/
            var lineCount = currentRec.getLineCount({
                sublistId: 'recmachcustrecord_lin_broker_inv'
            });


            for (var b = 0; b < lineCount; b++) {


                var grossAmountAfterWHT = currentRec.getSublistValue({
                    sublistId: 'recmachcustrecord_lin_broker_inv',
                    fieldId: 'custrecord_gross_amt_after_wht',
                    line: b
                });

                totalgrossAmountAfterWHT = Number(totalgrossAmountAfterWHT) + Number(grossAmountAfterWHT);



            }

            totalgrossAmountAfterWHT = totalgrossAmountAfterWHT.toFixed(2)

            currentRec.setValue({

                fieldId: 'custrecord_totalamount',
                value: totalgrossAmountAfterWHT


            });

            /** For calulating total gross amount after WHT. - End **/



                /** For calculating the total RFP Amount of the selected RFP Numbers' on the Liquidation of Bills. - Start **/


                RFPNumbers = currentRec.getValue({

                    fieldId: 'custrecord_rfp_number'

                });


                for (var i = 0; i < RFPNumbers.length; i++) {
                    if (RFPNumbers[i] != '') {

                        RFPNumbersArray.push(RFPNumbers[i]);
                    }
                }

                if (RFPNumbersArray.length <= 0) {

                    return true;
                }

                var customrecord_rfpSearchObj = search.create({
                    type: "customrecord_rfp",
                    filters: [
                        ["internalid", "anyof", RFPNumbersArray]
                    ],
                    columns: [



                        search.createColumn({
                            name: "custrecord_rfp_amount",
                            summary: "SUM",
                            label: "Amount"
                        })
                    ]
                });
                var searchResultCount = customrecord_rfpSearchObj.runPaged().count;



                customrecord_rfpSearchObj.run().each(function(result) {


                    totalRFPAmount = result.getValue({
                        "name": "custrecord_rfp_amount",
                        "summary": "SUM"
                    });


                    return true;

                });

                log.debug('totalRFPAmount', totalRFPAmount);
                currentRec.setValue({

                    fieldId: 'custrecord_rfp_amt',
                    value: totalRFPAmount


                });


                /** For calculating the total RFP Amount of the selected RFP Numbers' on the Liquidation of Bills. - End **/



            /** For calculating the total Check Amount of the selected Check Links' on the Liquidation of Bills. - Start **/

            checkLinks = currentRec.getValue({

                fieldId: 'custrecord_write_chk_link'

            });

            for (var i = 0; i < checkLinks.length; i++) {
                if (checkLinks[i] != '') {

                    checkLinksArray.push(checkLinks[i]);
                }
            }

			if(checkLinksArray.length <= 0) {
				
				return true;
			}

            var checkSearchObj = search.create({
                type: "check",
                filters: [
                    ["type", "anyof", "Check"],
                    "AND",
                    ["internalid", "anyof", checkLinksArray],
                    "AND",
                    ["mainline", "is", "T"]
                ],
                columns: [
                    search.createColumn({
                        name: "amount",
                        summary: "SUM",
                        label: "Amount"
                    })
                ]
            });
            var searchResultCount = checkSearchObj.runPaged().count;

            checkSearchObj.run().each(function(result) {

                totalCheckAmount = result.getValue({
                    "name": "amount",
                    "summary": "SUM"
                });

                return true;
            });

			totalCheckAmount = Math.abs(totalCheckAmount);


            log.debug('totalCheckAmount', totalCheckAmount);
            currentRec.setValue({

                fieldId: 'custrecord_write_check_amt',
                value: totalCheckAmount


            });



            /** For calculating the total Check Amount of the selected Check Links' on the Liquidation of Bills. - End **/




            return true;




        }



        /** For message Pop-Up **/

        function netsuiteAlert(message) {
            var options = {
                title: "Alert",
                message: message
            };

            function success(result) {
                console.log("Success with value " + result);
            }

            function failure(reason) {
                console.log("Failure: " + reason);
            }

            dialog.alert(options).then(success).catch(failure);
        }


        /** Returns index of matching elements from two arrays' **/

        function findMatch(array_1_small, array2_large) {

            var ary = new Array();
            for (i = 0; i < array2_large.length; i++) {
                for (z = 0; z < array_1_small.length; z++) {
                    if (array2_large[i] == array_1_small[z]) {
                        ary.push(i);

                    }
                }

            }

            return ary;


        }

        function JSONMatch(c, b) {

            // a=a[0];

            b = b[0];
            var c;
            for (var f = 0; f < c.length; f++) {


                a = c[f]


                for (var key in a) {
                    if (a.hasOwnProperty(key)) {

                        var ele1 = a[key]
                        for (var key2 in b) {
                            if (b.hasOwnProperty(key2)) {
                                var ele2 = b[key2]
                                //console.log(ele1)
                                //console.log(ele2)
                                if (a[key] == b[key2]) {

                                    return a["poQuantity"];
                                }

                            }

                        }

                    }

                }

            }

        }




        return {

            pageInit: pageInit,
            fieldChanged: fieldChanged,
            lineInit: lineInit,
            saveRecord: saveRecord


        }
    });