/**

 * @NApiVersion 2.0

 * @NScriptType ClientScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 03/11/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : ClientScript for Not Letting User Save the Vendor Payment Record if invoices of different tax code is selected.  

 **************************************************************************************************************************************/
define(['N/currentRecord','N/search','N/runtime','N/record'],

function(currentRecord,search,runtime,record) {
    var scriptExecutionFlag=false;
    var flagForWtaxOnLine=false;
    var ubPayoutForm = runtime.getCurrentScript().getParameter('custscript_ub_payout_check_form');
    function pageInit(scriptContext) {
        //alert('HIIII')
        var currentRecordObj = currentRecord.get();
        var form=currentRecordObj.getValue('customform')
        if (ubPayoutForm!='' && ubPayoutForm!=null && form==ubPayoutForm) {
            scriptExecutionFlag=true;
        }

        var payee=currentRecordObj.getValue('entity');
        try{
            var vendorLookup = search.lookupFields({
                type: 'vendor',
                id: payee,
                columns: ['custentity_adv_ub_wtax_code']
            });
            //alert(JSON.stringify(vendorLookup))
            if (vendorLookup.custentity_adv_ub_wtax_code==true) {
                flagForWtaxOnLine=true;
            }
        }catch(e){
            console.log('ERRRORORORORORORORO '+JSON.stringify(e))
        }
    }

    function fieldChanged(scriptContext){
        //alert('field changes')
        if (scriptExecutionFlag!=true) {
            return;
        }
        var currentRecordObj = currentRecord.get();
        /*if (scriptContext.fieldId == 'custpage_pageid') {
            var pageId = currentRecordObj.getValue({
                fieldId : 'custpage_pageid'
            });
            pageId = parseInt(pageId.split('_')[1]);
            pId=pageId;
            reloadPage();
        }*/
        if (scriptContext.fieldId == 'custbody_ref_doc') {
            var refPO=currentRecordObj.getValue('custbody_ref_doc');

            if (refPO.length>0) {
                /*var refFieldString='';
                var itemLineCount=currentRecordObj.getLineCount('item');
                var expenseLineCount=currentRecordObj.getLineCount('expense');
                var wTaxRate;
                if (itemLineCount>0) {
                    wTaxRate=currentRecordObj.getSublistValue({sublistId:"item",fieldId:'custcol_adv_wtax_rate',line:0})
                }
                if (expenseLineCount>0) {
                    wTaxRate=currentRecordObj.getSublistValue({sublistId:"expense",fieldId:'custcol_adv_wtax_rate',line:0})
                }
                wTaxRate=parseFloat(wTaxRate)/100;
                for(var i=0;i<refPO.length;i++){
                    if (refPO[i]==undefined || refPO[i]=='' || refPO[i]==null) {
                        return;
                    }
                    var refNumberLookup = search.lookupFields({
                        type: 'purchaseorder',
                        id: refPO[i],
                        columns: ['tranid','total']
                    });
                    var refNumber=refNumberLookup.tranid;
                    var refAmount=refNumberLookup.total;
                    refAmount=Number(refAmount)*Number(wTaxRate)
                    if (i==refPO.length-1) {
                        if (refNumber!='' && refNumber!=undefined && refNumber!=null) {
                            refFieldString+=refNumber+'='+refAmount
                        }
                    }else{
                        if (refNumber!='' && refNumber!=undefined && refNumber!=null) {
                            refFieldString+=refNumber+'='+refAmount+'|'
                        }
                    }
                }
                currentRecordObj.setValue('custbody_ub_payout_ref',refFieldString);*/
            }else{
                //currentRecordObj.setValue('custbody_ub_payout_ref','');
            }
        }
        if (scriptContext.fieldId == 'custcol_adv_wtax_rate') {
            calculateWTaxCode(currentRecordObj,scriptContext.sublistId)
        }

        if (scriptContext.fieldId == 'custcol_gross_amount') {
            var customAmount=currentRecordObj.getCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'custcol_gross_amount'});
            currentRecordObj.setCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'amount',value:customAmount});
        }
        
        if (scriptContext.fieldId == 'entity') {
            var payee=currentRecordObj.getValue('entity');
            try{
                var vendorLookup = search.lookupFields({
                    type: 'vendor',
                    id: payee,
                    columns: ['custentity_adv_ub_wtax_code']
                });
                //alert(JSON.stringify(vendorLookup))
                if (vendorLookup.custentity_adv_ub_wtax_code==true) {
                    flagForWtaxOnLine=true;
                }
            }catch(e){
                console.log('ERRRORORORORORORORO '+JSON.stringify(e))
            }
        }
        if (scriptContext.fieldId == 'custcol_adv_tax_code') {
            //alert(JSON.stringify(scriptContext))
            var selectedTaxCodeIid=currentRecordObj.getCurrentSublistValue({sublistId:'expense',fieldId:'custcol_adv_tax_code'});
            calculateAndSetTax(currentRecordObj,selectedTaxCodeIid,scriptContext.sublistId);
        }

        if (scriptContext.fieldId == 'amount') {
            var selectedTaxCodeIid=currentRecordObj.getCurrentSublistValue({sublistId:'expense',fieldId:'custcol_adv_tax_code'});
            if (selectedTaxCodeIid!='' && selectedTaxCodeIid!=undefined && selectedTaxCodeIid!=null) {
                calculateAndSetTax(currentRecordObj,selectedTaxCodeIid,scriptContext.sublistId)
            }
        }

        if (scriptContext.fieldId == 'custcol_adv_wtax_amount') {
            var wTaxAmount=currentRecordObj.getCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'custcol_adv_wtax_amount'});
            if (wTaxAmount!=undefined && wTaxAmount!='' && wTaxAmount!=null) {
                var customTotalAmount=currentRecordObj.getCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'custcol_adv_ub_total_amt'});
                currentRecordObj.setCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'amount',value:Number(customTotalAmount)-Number(wTaxAmount),ignoreFieldChange: true,forceSyncSourcing: false});
                currentRecordObj.setCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'grossamt',value:Number(customTotalAmount)-Number(wTaxAmount),ignoreFieldChange: true,forceSyncSourcing: false});
            }else{
                var amount=currentRecordObj.getCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'amount'});
                //currentRecordObj.setCurrentSublistValue({sublistId:scriptContext.sublistId,fieldId:'grossamt',value:amount,ignoreFieldChange: true,forceSyncSourcing: false});
            }
        }
    }

    function calculateAndSetTax(currentRecordObj,selectedTaxCodeIid,sublist){
        //alert(sublist)
        var taxRate=getTaxRate(selectedTaxCodeIid);
        var amount=currentRecordObj.getCurrentSublistValue({sublistId:sublist,fieldId:'custcol_gross_amount'});
        //alert('amount '+amount)
        var taxAmount=amount*taxRate/100
        var taxCalculatedAmount=Number(amount)+Number(taxAmount)
        //alert('taxRate '+taxRate);
        currentRecordObj.setCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_tax_rate',value:taxRate});
        currentRecordObj.setCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_tax_amount',value:taxAmount});
        currentRecordObj.setCurrentSublistValue({sublistId:sublist,fieldId:'amount',value:taxCalculatedAmount,ignoreFieldChange: true,forceSyncSourcing: false});
        var wTaxAmount=currentRecordObj.getCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_wtax_amount'});

        if (wTaxAmount!=undefined && wTaxAmount!='' && wTaxAmount!=null) {
            var customTotalAmount=currentRecordObj.getCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_ub_total_amt'});
            currentRecordObj.setCurrentSublistValue({sublistId:sublist,fieldId:'amount',value:Number(customTotalAmount)-Number(wTaxAmount),ignoreFieldChange: true,forceSyncSourcing: false});
        }else{
            currentRecordObj.setCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_ub_total_amt',value:taxCalculatedAmount,ignoreFieldChange: true,forceSyncSourcing: false});
        }
    }

    function calculateWTaxCode(currentRecordObj,sublist){
        var wTaxCodeRate=currentRecordObj.getCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_wtax_rate'});
        if (wTaxCodeRate!='' && wTaxCodeRate!=undefined && wTaxCodeRate!=null) {
            wTaxCodeRate=parseFloat(wTaxCodeRate)
            var amount=currentRecordObj.getCurrentSublistValue({sublistId:sublist,fieldId:'custcol_gross_amount'});
            var wTaxAmount=amount*wTaxCodeRate/100;
            currentRecordObj.setCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_wtax_amount',value:wTaxAmount});
        }else{
            currentRecordObj.setCurrentSublistValue({sublistId:sublist,fieldId:'custcol_adv_wtax_amount',value:''});
        }
    }

    function getTaxRate(selectedTaxCodeIid){
        //alert(selectedTaxCodeIid)
        var taxRateLookup = search.lookupFields({
            type: 'salestaxitem',
            id: selectedTaxCodeIid,
            columns: ['rate']
        });
        var taxRate= taxRateLookup.rate;
        taxRate=taxRate.replace('%','');
        return taxRate;
    }

    function validateLine(context){
        //alert('Validate Line')
        if (scriptExecutionFlag!=true) {return true;}
        var currentRecordObj = currentRecord.get();
        currentRecordObj.setCurrentSublistValue({sublistId:context.sublistId,fieldId:'taxcode',value:5,ignoreFieldChange:true});
        //currentRecordObj.commitLine({sublistId: 'item'});
        if (!flagForWtaxOnLine) {
            currentRecordObj.setCurrentSublistValue({sublistId:context.sublistId,fieldId:'custcol_4601_witaxapplies',value:false});
        }else{
            currentRecordObj.setCurrentSublistValue({sublistId:context.sublistId,fieldId:'custcol_4601_witaxapplies',value:false});
        }
        return true;
    }

    function lineInit(context){
        //alert('Line INit')
        if (scriptExecutionFlag!=true) {return;}
        var currentRecordObj = currentRecord.get();
        //alert('flagForWtaxOnLine '+flagForWtaxOnLine)
        //if (!flagForWtaxOnLine) {
            //alert('inside Line Init')
            currentRecordObj.setCurrentSublistValue({sublistId:context.sublistId,fieldId:'custcol_4601_witaxapplies',value:false});
        //}
    }

    function saveRecord(context){
        //alert('save Record')
        if (scriptExecutionFlag!=true) {return true;}
        var itemTaxAmount=0;
        var itemWTaxAmount=0;
        var expenseTaxAmount=0;
        var expenseWTaxAmount=0;
        var currentRecordObj = currentRecord.get();
        var totalSelected = [];
        var lineCountExpense = currentRecordObj.getLineCount({sublistId:'expense'});
        var lineCountItem = currentRecordObj.getLineCount({sublistId:'item'});
        if(lineCountExpense > 0){
            expenseTaxAmount=calculateTotalTax('expense',currentRecordObj,lineCountExpense)
            expenseWTaxAmount=calculateTotalWTax('expense',currentRecordObj,lineCountExpense)
            var validationFlag=validateWTaxCode(lineCountExpense,'expense',currentRecordObj)
            /*if (!validationFlag) {
                return false;
            }*/
        }
        if (lineCountItem>0) {
            itemTaxAmount=calculateTotalTax('item',currentRecordObj,lineCountItem)
            itemWTaxAmount=calculateTotalWTax('item',currentRecordObj,lineCountItem)
            
            var validationFlag=validateWTaxCode(lineCountItem,'item',currentRecordObj)
            
        }
        var totalTaxAmount=Number(itemTaxAmount)+Number(expenseTaxAmount)
        var totalWTaxAmount=Number(itemWTaxAmount)+Number(expenseWTaxAmount)
        //alert('totalTaxAmount '+totalTaxAmount)
        currentRecordObj.setValue('custbody_adv_ub_tax_amount',totalTaxAmount);
        //alert('totalWTaxAmount '+totalWTaxAmount)
        currentRecordObj.setValue('custbody_adv_ub_wtax_amount',totalWTaxAmount);
        //var headerAmount=calculateHeaderAmount(currentRecordObj);
        //currentRecordObj.setValue('usertotal',headerAmount)
        if (!validationFlag) {
            return false;
        }
        return true;
    }

    function calculateHeaderAmount(currentRecordObj){
        var totalHeader=currentRecordObj.getValue('usertotal');
        var taxAmountHeader=currentRecordObj.getValue('custbody_adv_ub_tax_amount');
        var wTaxAmountHeader=currentRecordObj.getValue('custbody_adv_ub_wtax_amount');

        return (totalHeader+taxAmountHeader)-wTaxAmountHeader;
    }

    function calculateTotalTax(sublist,currentRecordObj,lineCount){
        var totalTaxAmount=0
        for(var i = 0; i < lineCount; i++){
            var taxAmount = currentRecordObj.getSublistValue({sublistId:sublist,fieldId:'custcol_adv_tax_amount',line:i});
            totalTaxAmount+=Number(taxAmount);
        }
        return totalTaxAmount;
    }

    function calculateTotalWTax(sublist,currentRecordObj,lineCount){
        var totalwTaxAmount=0
        for(var i = 0; i < lineCount; i++){
            var amount = currentRecordObj.getSublistValue({sublistId:sublist,fieldId:'amount',line:i});
            var wTaxRate = currentRecordObj.getSublistValue({sublistId:sublist,fieldId:'custcol_adv_wtax_amount',line:i});
            //alert(wTaxRate)
            //wTaxRate=wTaxRate.replace('%','');

            //var wTaxAmount=amount*wTaxRate/100;
            var wTaxAmount=Number(wTaxRate)
            totalwTaxAmount+=Number(wTaxAmount);
        }
        return totalwTaxAmount;
    }

    function validateWTaxCode(linecount,sublist,currentRecordObj){
        var totalSelected = [];
        var allBillsSelected=[];
        //var linecount = currentRecordObj.getLineCount({sublistId:sublist});
        //alert('linecount : '+linecount+' sublist : '+sublist)
        if (ubPayoutForm!='' && ubPayoutForm!=null && currentRecordObj.getValue('customform')==ubPayoutForm) {
            if(linecount > 0){
                for(var i = 0; i < linecount; i++){
                    //var wTaxCodeSearchObj=search.load('customsearch62')
                    var checked = true;//currentRecordObj.getSublistValue({sublistId:sublist,fieldId:'custcol_4601_witaxapplies',line:i});
                    if (sublist.trim()=='item') {
                        //var getId = currentRecordObj.getSublistValue({sublistId:'apply',fieldId:'CUSTBODY_WTAX_CODE_NAME',line:i});
                        var wTaxCode=currentRecordObj.getSublistValue({sublistId:sublist,fieldId:'custcol_adv_wtax_code',line:i});
                        //alert('wTaxCode '+wTaxCode+' checked : '+checked)
                    }else{
                        var wTaxCode=currentRecordObj.getSublistValue({sublistId:sublist,fieldId:'custcol_adv_wtax_code',line:i});
                        //alert('wTaxCode '+wTaxCode+' checked : '+checked)
                    }//alert(bills)
                    if(checked){
                        var getId=wTaxCode;
                        //alert(getId)
                        if (getId=='') {
                            getId='custom'
                        }
                        totalSelected.push(getId);
                        //allBillsSelected.push(bills);
                    }
                }
                if(totalSelected.length >  1){
                    //var allEqual=totalSelected.every((val, i, arr) => val === arr[0]);
                    //alert(totalSelected)
                    var allEqual=!!totalSelected.reduce(function(a, b){ return (a === b) ? a : NaN; });
                    if (!allEqual) {
                        alert ('Can Not Submit With Different Tax Codes');
                        return false;
                    }
                }
            }
        }
        return true;
    }

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        validateLine: validateLine,
        saveRecord:saveRecord,
        lineInit:lineInit
    };
});