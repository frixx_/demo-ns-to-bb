/**
 * @NApiVersion 2.x
 * @NScriptType restlet
*/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 31 December 2019
	**@Version : 2.0
	**@Description : Creates an Item Receipt, Inventory & Landed Cost JV based on Requests received from RSK System
***************************************************************************************/
define(['N/record', 'N/search', 'N/log', 'N/runtime', 'N/format'], function(record, search, log, runtime, format) {
	
	
	var INVENTORY_JV = 1, LANDED_COST_JV = 2;
	var GRNI_ACCOUNT = 115;
	var APPROVED_STATUS = 2;
	var RSK_JOURNAL_ENTRY = 120;
	
	var checkedItems = new Array();
	return {
		post : function(data){
			
			/* request= {
				"requestId": 21398,
				"date": "01/20/2020",
				"purchaseorder": "29410",
				"exchangerate": "52.0000",
				"itemData": [
					{
						"item": "2299",
						"quantity": "100.0000",
						"brokerage": "0.0000",
						"miscReport": "55258.9700",
						"importFee": "0.0000",
						"insurance": "2132.0000",
						"trucking": "2151.0000",
						"arrastre": "20000.0000",
						"customsDuty": "0.0000",
						"costBankCharge": "387264.7700",
						"costDocumentary": "280.0000",
						"glFee": "200.0000",
						"terminalFee": "200.0000",
						"costStorage": "552589.7000",
						"costDocumentFee": "892.8600",
						"costWeightFee": "200.0000"
					}
				]
			} */
			
			if(typeof data === 'string'){
				data = JSON.parse(data);
			}
			
			log.debug("jsonData",JSON.stringify(data));		
			
			
			// https://tc39.github.io/ecma262/#sec-array.prototype.find
			if (!Array.prototype.find) {
			  Object.defineProperty(Array.prototype, 'find', {
				value: function(predicate) {
				  // 1. Let O be ? ToObject(this value).
				  if (this == null) {
					throw new TypeError('"this" is null or not defined');
				  }

				  var o = Object(this);

				  // 2. Let len be ? ToLength(? Get(O, "length")).
				  var len = o.length >>> 0;

				  // 3. If IsCallable(predicate) is false, throw a TypeError exception.
				  if (typeof predicate !== 'function') {
					throw new TypeError('predicate must be a function');
				  }

				  // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
				  var thisArg = arguments[1];

				  // 5. Let k be 0.
				  var k = 0;

				  // 6. Repeat, while k < len
				  while (k < len) {
					// a. Let Pk be ! ToString(k).
					// b. Let kValue be ? Get(O, Pk).
					// c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
					// d. If testResult is true, return kValue.
					var kValue = o[k];
					if (predicate.call(thisArg, kValue, k, o)) {
					  return kValue;
					}
					// e. Increase k by 1.
					k++;
				  }

				  // 7. Return undefined.
				  return undefined;
				},
				configurable: true,
				writable: true
			  });
			}
			
			var requestId = data.Id || "";
			
			try{
			
				var scriptObj = runtime.getCurrentScript();
				INVENTORY_JV = scriptObj.getParameter({name: 'custscript_ir_inventory_jv'});
				LANDED_COST_JV = scriptObj.getParameter({name: 'custscript_ir_landed_cost_jv'});
				GRNI_ACCOUNT = scriptObj.getParameter({name: 'custscript_ir_grni_account'});
								
				var poid = search.create({
						type: search.Type.PURCHASE_ORDER,
						columns: [{
							name: 'internalid'
						},{
							name: 'subsidiary'
						},{
							name: 'cseg_type_of_trans'
						},{
							name: 'custbody_letterofcreditnum'
						},{
							name: 'custbody_rskponum'
						}],
						filters: [ 
						{
							name: 'internalid',
							operator: 'is',
							values: [data.purchaseorder]
						}]
					}).run().getRange(0,1000);		

				log.debug("poid", poid);	
								
				var newRecord = record.transform({
					fromType: record.Type.PURCHASE_ORDER,
					fromId: Number(data.purchaseorder),
					toType: record.Type.ITEM_RECEIPT,
					isDynamic: false,
				});
				
				newRecord.setValue({
					fieldId: "exchangerate",
					value: data.exchangerate
					});
				
				newRecord.setValue({
					fieldId: "trandate",
					value: parseAndFormatDateString(data.date)
				});		
				
				newRecord.setValue({
					fieldId: "custbody_si_rr_no",
					value: requestId
				});
				
				if(!!(data.BillLadingNo)){
				
					// var objField = newRecord.getField({
						// fieldId: 'custbody_billoflasdingnum'
					// });
				
					// var options = objField.getSelectOptions({
						// filter : data.BillLadingNo,
						// operator : 'is'
					// });
					
					// if(!!(options)){
						// if(options.length != 0){
							newRecord.setText({
								fieldId: "custbody_billoflasdingnum",
								value: data.BillLadingNo
							});
						// }
					// }
				}
				
				if(!!(data.Shipment)){
					// var objField = newRecord.getField({
						// fieldId: 'custbody_ship_num'
					// });
				
					// var options = objField.getSelectOptions({
						// filter : data.Shipment,
						// operator : 'is'
					// });
					
					// if(!!(options)){
						// if(options.length != 0){
							newRecord.setText({
								fieldId: "custbody_ship_num",
								value: data.Shipment
							});
						// }
					// }
				}

				/* newRecord.setValue({
					fieldId: "entity",
					value: data.supplier
				});
				
				newRecord.setText({
					fieldId: "postingperiod",
					value: data.postingperiod
				});				
				
				newRecord.setText({
					fieldId: "subsidiary",
					value: data.subsidiary
				});				
				
				newRecord.setText({
					fieldId: "currency",
					value: data.currency
				});
				
				newRecord.setText({
					fieldId: "createdfrom",
					value: data.createdfrom
				}); */
				
				var itemData = data.itemData;
				
				var landedCostJVData = {};
								
				var totBrokerage = 0, totMiscReport = 0, totImportFee = 0, totInsurance = 0, totTrucking = 0, totArrastre = 0, totCustomsDuty = 0, totCostBankCharge = 0, totCostDocumentary = 0, totGLFee = 0, totTerminalFee = 0, totCostStorage = 0, totCostDocumentFee = 0, totCostWeightFee = 0, totInspectionFee = 0, totCSF = 0, totCIV = 0;
				
				// Get all Lines from Item Sublist
				
				var numLines = newRecord.getLineCount({
					sublistId: 'item'
				});
				
				var lineItemDataIndex = {};
				
				for(var zz = 0; zz < numLines; zz++){
					var item = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'item',
						line: zz
					});
					var quantityremaining = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'quantityremaining',
						line: zz
					});
					var department = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'department',
						line: zz
					});
					var division = newRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'class',
						line: zz
					});
					
					if(lineItemDataIndex[item] == undefined){
						lineItemDataIndex[item] = [{"index": zz, "quantityremaining": quantityremaining, "department": department, "division": division, "flag": true}];
					}
					else{
						var oldData = lineItemDataIndex[item];
						oldData.push({"index": zz, "quantityremaining": quantityremaining, "department": department, "division": division, "flag": true});
						lineItemDataIndex[item] = oldData;
					}
					newRecord.setSublistValue({
						sublistId: 'item',
						fieldId: 'itemreceive',
						line: zz,
						value: false
					});
				}
				
				// log.debug("lineItemDataIndex", lineItemDataIndex);
								
				for(var i = 0; i < itemData.length; i++){
														
					// log.debug("lineItem", lineItem[0].getValue("internalid"));
										
					log.debug("lineItem", itemData[i].item);
					
					var length = lineItemDataIndex[itemData[i].item].length;
					
					// var lineNum = lineItemDataIndex[lineItem[0].getValue("internalid")];
					log.debug("lineNum", lineNum);
					log.debug("lineItemDataIndex", lineItemDataIndex);
					
					var lineNum = 0, quantityremaining = 0;
					
					
					var quantityRSK = itemData[i].quantity;
					var quantityRSKTotal = itemData[i].quantity;
					
					checkedItems.push(itemData[i].item);
					
					
					var total = 0;		
										
					for(var zz = 0; zz < lineItemDataIndex[itemData[i].item].length; zz++){
						
						// data = lineItemDataIndex[itemData[i].item];
						lineNum = lineItemDataIndex[itemData[i].item][zz].index;
						quantityremaining = lineItemDataIndex[itemData[i].item][zz].quantityremaining;
						
						var quantity = 0;
						if(quantityremaining <= quantityRSK){
							quantity = quantityremaining;
						}else if(quantityremaining > quantityRSK){
							quantity = quantityRSK;
						}
						
						quantityRSK = quantityRSK - quantity;
							
						if(quantity > 0){
							// checkedItems.push(lineItem[0].getValue("internalid"));
					
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'itemreceive',
								line: lineNum,
								value: true
							});
												
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'item',
								line: lineNum,
								value: itemData[i].item//lineItem[0].getValue("internalid")
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'quantity',
								line: lineNum,
								value: quantity
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostbrokerage',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].brokerage
							});
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostmiscport',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].miscReport
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedimportfee',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].importFee
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostinsurance',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].insurance
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcosttrucking',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].trucking
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostarrastre',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].arrastre
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcustomsduty',
								line: lineNum,
								value: itemData[i].customsDuty
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostbankcharge',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].costBankCharge
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostdocumentary',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].costDocumentary
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedsglfee',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].glFee
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedterminalfee',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].terminalFee
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcoststorage',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].costStorage
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostdocumentfee',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].costDocumentFee
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostweighfee',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].costWeightFee
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_inspection_fee',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].inspectionFee
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_csf',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].CSF
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_landedcostciv',
								line: lineNum,
								value: (quantity / quantityRSKTotal) * itemData[i].commercialInvoiceValue
							});
							
							newRecord.setSublistValue({
								sublistId: 'item',
								fieldId: 'custcol_weight',
								line: lineNum,
								value: itemData[i].Weight
							});
							
							
							if(itemData[i].brokerage != undefined){
								totBrokerage += parseFloat(((quantity / quantityRSKTotal) * itemData[i].brokerage).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].brokerage).toFixed(2));
							}
							if(itemData[i].miscReport != undefined){
								totMiscReport += parseFloat(((quantity / quantityRSKTotal) * itemData[i].miscReport).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].miscReport).toFixed(2));
							}
							if(itemData[i].importFee != undefined){
								totImportFee += parseFloat(((quantity / quantityRSKTotal) * itemData[i].importFee).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].importFee).toFixed(2));
							}
							if(itemData[i].insurance != undefined){
								totInsurance += parseFloat(((quantity / quantityRSKTotal) * itemData[i].insurance).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].insurance).toFixed(2));
							}
							if(itemData[i].trucking != undefined){
								totTrucking += parseFloat(((quantity / quantityRSKTotal) * itemData[i].trucking).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].trucking).toFixed(2));
							}
							if(itemData[i].arrastre != undefined){
								totArrastre += parseFloat(((quantity / quantityRSKTotal) * itemData[i].arrastre).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].arrastre).toFixed(2));
							}
							if(itemData[i].customsDuty != undefined){
								totCustomsDuty += parseFloat(((quantity / quantityRSKTotal) * itemData[i].customsDuty).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].customsDuty).toFixed(2));
							}
							if(itemData[i].costBankCharge != undefined){
								totCostBankCharge += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costBankCharge).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costBankCharge).toFixed(2));
							}
							if(itemData[i].costDocumentary != undefined){
								totCostDocumentary += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costDocumentary).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costDocumentary).toFixed(2));
							}
							if(itemData[i].glFee != undefined){
								totGLFee += parseFloat(((quantity / quantityRSKTotal) * itemData[i].glFee).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].glFee).toFixed(2));
							}
							if(itemData[i].terminalFee != undefined){
								totTerminalFee += parseFloat(((quantity / quantityRSKTotal) * itemData[i].terminalFee).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].terminalFee).toFixed(2));
							}
							if(itemData[i].costStorage != undefined){
								totCostStorage += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costStorage).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costStorage).toFixed(2));
							}
							if(itemData[i].costDocumentFee != undefined){
								totCostDocumentFee += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costDocumentFee).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costDocumentFee).toFixed(2));
							}
							if(itemData[i].costWeightFee != undefined){
								totCostWeightFee += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costWeightFee).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].costWeightFee).toFixed(2));
							}
							if(itemData[i].inspectionFee != undefined){
								totInspectionFee += parseFloat(((quantity / quantityRSKTotal) * itemData[i].inspectionFee).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].inspectionFee).toFixed(2));
							}
							if(itemData[i].CSF != undefined){
								totCSF += parseFloat(((quantity / quantityRSKTotal) * itemData[i].CSF).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].CSF).toFixed(2));
							}
							if(itemData[i].commercialInvoiceValue != undefined){
								totCIV += parseFloat(((quantity / quantityRSKTotal) * itemData[i].commercialInvoiceValue).toFixed(2));
								total += parseFloat(((quantity / quantityRSKTotal) * itemData[i].commercialInvoiceValue).toFixed(2));
							}
					
						}
					}
					
							
					
					/* newRecord.commitLine({
						sublistId: 'item'
					}); */					
					// landedCostJVData[lineItem[0].getValue("internalid")] = Number(itemData[i].brokerage) + Number(itemData[i].miscReport) + Number(itemData[i].importFee) + Number(itemData[i].insurance) + Number(itemData[i].trucking) + Number(itemData[i].arrastre) + Number(itemData[i].customsDuty) + Number(itemData[i].costBankCharge) + Number(itemData[i].costDocumentary) + Number(itemData[i].glFee) + Number(itemData[i].terminalFee) + Number(itemData[i].costStorage) + Number(itemData[i].costDocumentFee) + Number(itemData[i].costWeightFee);
					
					
					var department = lineItemDataIndex[itemData[i].item][0].department;
					var division = lineItemDataIndex[itemData[i].item][0].division;
					
					// "total": (parseFloat(itemData[i].brokerage) + parseFloat(itemData[i].miscReport) + parseFloat(itemData[i].importFee) + parseFloat(itemData[i].insurance) + parseFloat(itemData[i].trucking) + parseFloat(itemData[i].arrastre) /*+ parseFloat(itemData[i].customsDuty)*/ + parseFloat(itemData[i].costBankCharge) + parseFloat(itemData[i].costDocumentary) + parseFloat(itemData[i].glFee) + parseFloat(itemData[i].terminalFee) + parseFloat(itemData[i].costStorage) + parseFloat(itemData[i].costDocumentFee) + parseFloat(itemData[i].costWeightFee)).toFixed(2),
						
					
					landedCostJVData[itemData[i].item] = {
						"total": (total).toFixed(2),
						"department": department,
						"division": division
					};
					log.audit("landedCostJVData[itemData[i].item] - Before - " + itemData[i].item, landedCostJVData);
				}
				
				var landedCostAccounts = new Array();
				
				landedCostAccounts["brokerage"] = totBrokerage;
				landedCostAccounts["miscReport"] = totMiscReport;
				landedCostAccounts["importFee"] = totImportFee;
				landedCostAccounts["insurance"] = totInsurance;
				landedCostAccounts["trucking"] = totTrucking;
				landedCostAccounts["arrastre"] = totArrastre;
				landedCostAccounts["customsDuty"] = totCustomsDuty;
				landedCostAccounts["costBankCharge"] = totCostBankCharge;
				landedCostAccounts["costDocumentary"] = totCostDocumentary;
				landedCostAccounts["glFee"] = totGLFee;
				landedCostAccounts["terminalFee"] = totTerminalFee;
				landedCostAccounts["costStorage"] = totCostStorage;
				landedCostAccounts["costDocumentFee"] = totCostDocumentFee;
				landedCostAccounts["costWeightFee"] = totCostWeightFee;
				landedCostAccounts["inspectionFee"] = totInspectionFee;
				landedCostAccounts["csf"] = totCSF;
				landedCostAccounts["commercialInvoiceValue"] = totCIV;
				
				var recordID = newRecord.save();
				log.debug("recordid",recordID);
				
				data["subsidiary"] = newRecord.getValue({
										fieldId: 'subsidiary'
									});
				data["currency"] = newRecord.getValue({
										fieldId: 'currency'
									});
				// data["billofLading"] = newRecord.getValue({
										// fieldId: 'custbody_billoflasdingnum'
									// });
				data["division"] = newRecord.getValue({
										fieldId: 'class'
									});
				data["department"] = newRecord.getValue({
										fieldId: 'department'
									});
				data["location"] = newRecord.getValue({
										fieldId: 'location'
									});
				// data["shipmentNumber"] = newRecord.getValue({
										// fieldId: 'custbody_ship_num'
									// });
				data["letterofCreditnum"] = newRecord.getValue({
										fieldId: 'custbody_letterofcreditnum'
									});									
									
				data["createdfrom"] = data.purchaseorder;//poid[0].getValue("internalid");
				data["TypeOfTransaction"] = poid[0].getValue("cseg_type_of_trans");
				// data["letterofCreditnum"] = poid[0].getValue("custbody_letterofcreditnum");
				
				
				// log.debug("data logs", data);
												
				var inventoryJV = CreateInventoryJV(data, data["department"], data["division"], recordID);
				var landedCostJV = CreateLandedCostJV(data, data["department"], data["division"], recordID, landedCostJVData, landedCostAccounts);
				
				log.debug("Response", JSON.stringify({
					RequestId: requestId, 
					NsId : recordID, 
					purchaseorder : Number(poid[0].getValue("custbody_rskponum")), 
					POType: poType, 
					inventoryJV : inventoryJV, 
					landedCostJV : landedCostJV, 
					message: "Success"
				}));
					
				var poType = (search.lookupFields({
					type: "purchaseorder",
					id: data.purchaseorder,
					columns: ['custbody_purchaseordertypelist']
				}))['custbody_purchaseordertypelist'][0].text;
					
				return {RequestId: requestId, NsId : recordID, purchaseorder : Number(poid[0].getValue("custbody_rskponum")), POType: poType, inventoryJV : inventoryJV, landedCostJV : landedCostJV, message: "Success"};
			}catch(err){
				log.debug("Error", err);
				return {message:"Failure", error: err};		
			}
			
			//return "Success";
		}
	}
		
	function parseAndFormatDateString(myDate) {

		var initialFormattedDateString = myDate;
		var parsedDateStringAsRawDateObject = format.parse({
			value: initialFormattedDateString,
			type: format.Type.DATE,

		});

		return parsedDateStringAsRawDateObject;
		
	}
	
	/**********************************************************
	 **********************Journal Entries*********************
	**********************************************************/		
	
	function CreateInventoryJV(data, department, division, recordID){
		// Creation of Inventory JV
		
		log.debug("Inventory JV", data);
		log.debug("Inventory JV Dept - Div", department + " - " + division);
	
		var poRecord = record.load({
							type: record.Type.PURCHASE_ORDER, 
							id: data.createdfrom,
							isDynamic: false,
						});
		
		var lineCount = poRecord.getLineCount({
							sublistId: 'item'
						});
		
		var itemDataArray = new Array();
		
		var GRNI_AMOUNT = 0;
		
		for(var i = 0; i < lineCount;i++){
			var item = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'item',
							line: i
						});
			// log.audit("checkedItems",checkedItems);		
			// log.audit("checkedItems item",item);			
			
			var flag = false;
			
			for(var xx = 0; xx < checkedItems.length; xx++){
				if(checkedItems[xx] == item){
					flag = true;
				}
			}
			
			if(!flag){
				continue;
			}
					
			var amount = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'amount',
							line: i
						});
			var quantity = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'quantity',
							line: i
						});
			var departmentLine = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'department',
							line: i
						});
			var divisionLine = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'class',
							line: i
						});
						
			log.emergency("Item - " + item, departmentLine + " - " + divisionLine);			
			var quantityreceived = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'quantityreceived',
							line: i
						});
			var itemClassification = poRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'cseg_itemclassifica',
				line: i
			});
						
			var inventorySegment = poRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'cseg_inv_account',
				line: i
			});
			
			var account = (search.lookupFields({
				type: search.Type.ITEM,
				id: item,
				columns: ['custitem_inventoryassetaccount']
			}))['custitem_inventoryassetaccount'][0].value;
			
			var itemData = {
				"account" : account,
				"debit" : (amount/quantity)*quantityreceived,
				"credit" : 0,
				"itemClassification" : itemClassification,
				"inventorySegment" : inventorySegment,
				"department" : departmentLine,
				"division" : divisionLine
			}
			
			GRNI_AMOUNT += (amount / quantity) * quantityreceived;
			
			itemDataArray.push(itemData);
		}
		
		itemDataArray.push({
			"account" : GRNI_ACCOUNT,
			"debit" : 0,
			"credit" : GRNI_AMOUNT,
			"itemClassification" : "",
			"inventorySegment" : "",
			"department" : department,
			"division" : division
		});

		var jsonBody = {
						  "exchangerate":data.exchangerate, // Need to confirm
						  "date":data.date,
						  "subsidiary":data.subsidiary,
						  "postingperiod":data.postingperiod,
						  "currency":data.currency,
						  "JVType":INVENTORY_JV,//"Inventory JV",
						  "itemReceiptID": recordID,
						  "billofLading": data.BillLadingNo,
						  "department": department,
						  "division": division,
						  "location": data.location,
						  "shipmentNumber": data.Shipment,
						  "TypeOfTransaction":data.TypeOfTransaction,
						  "letterofCreditnum":data.letterofCreditnum,
						  "itemData": itemDataArray
					   };		
		
		return CreateJV(jsonBody, recordID);
	}
	
	function CreateLandedCostJV(dataMain, department, division, recordID, landedCostJVData, landedCostAccounts){
		// Creation of Landed Cost JV
		
		log.debug("Landed Cost JV", dataMain);
		log.debug("Landed JV Dept - Div", department + " - " + division);
		
		// log.debug("dataMain.createdfrom", dataMain.createdfrom);
		var poRecord = record.load({
							type: record.Type.PURCHASE_ORDER, 
							id: dataMain.createdfrom,
							isDynamic: false,
						});
		
		var lineCount = poRecord.getLineCount({
							sublistId: 'item'
						});
						
		// log.debug("landedCostJVData", landedCostJVData);				
		
		var itemDataArray = new Array();
		
		var GRNI_AMOUNT = 0;
		
		var itemChecked = {};
		
		// Adding Landed Cost Items
		for(var i = 0; i < lineCount;i++){
			var item = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'item',
							line: i
						});
						
			if(itemChecked[item] != undefined)
				continue;
			
			log.debug("landedCostJVData - "+item, landedCostJVData[item]);
			if(!(landedCostJVData[item])){
				continue;
			}
			var debit = parseFloat(landedCostJVData[item].total);
			var amount;
			var itemClassification = poRecord.getSublistValue({
							sublistId: 'item',
							fieldId: 'cseg_itemclassifica',
							line: i
						});
					
			var inventorySegment = poRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'cseg_inv_account',
				line: i
			});
			
			var account = search.lookupFields({
				type: search.Type.ITEM,
				id: item,
				columns: ['custitem_inventoryassetaccount']
			});
			
			var itemData = {
				"account" : account["custitem_inventoryassetaccount"][0].value,
				"debit" : Number(debit),
				"credit" : 0,
				"itemClassification" : itemClassification,
				"inventorySegment" : inventorySegment,
				"department": landedCostJVData[item].department, 
				"division": landedCostJVData[item].division
			};
			
			itemDataArray.push(itemData);
			
			itemChecked[item] = true;
		}
		
		var landedCostCreditBalance = search.create({
			type: "customrecord_landedcostcomponent",
			columns: [
				{name: 'custrecord_freightcomponent'}, 
				{name: 'custrecord_landedcostgl'}
			],
			filters: []
		}).run().getRange(0, 999);
		
		/* for(var j = 0; j < landedCostCreditBalance.length; j++){
			if()
			var itemData = {
				"account" : account,
				"debit" : debit,
				"credit" : 0,
				"itemClassification" : itemClassification
			};
		} */
				
		totBrokerage = landedCostAccounts["brokerage"];
		totMiscReport = landedCostAccounts["miscReport"];
		totImportFee = landedCostAccounts["importFee"];
		totInsurance = landedCostAccounts["insurance"];
		totTrucking = landedCostAccounts["trucking"];
		totArrastre = landedCostAccounts["arrastre"];
		totCustomsDuty = landedCostAccounts["customsDuty"];
		totCostBankCharge = landedCostAccounts["costBankCharge"];
		totCostDocumentary = landedCostAccounts["costDocumentary"];
		totGLFee = landedCostAccounts["glFee"];
		totTerminalFee = landedCostAccounts["terminalFee"];
		totCostStorage = landedCostAccounts["costStorage"];
		totCostDocumentFee = landedCostAccounts["costDocumentFee"];
		totCostWeightFee = landedCostAccounts["costWeightFee"];
		totInspectionFee = landedCostAccounts["inspectionFee"];
		totCSF = landedCostAccounts["csf"];
		totCIV = landedCostAccounts["commercialInvoiceValue"];
		
		if(totBrokerage != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostbrokerage";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totBrokerage,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
			
		}
		if(totMiscReport != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostmiscport";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totMiscReport,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
			
		}
		if(totImportFee != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedimportfee";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totImportFee,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
			
		}
		if(totInsurance != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostinsurance";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totInsurance,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totTrucking != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcosttrucking";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totTrucking,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totArrastre != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostarrastre";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
						
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totArrastre,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCustomsDuty != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcustomsduty";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCustomsDuty,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCostBankCharge != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostbankcharge";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCostBankCharge,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCostDocumentary != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostdocumentary";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCostDocumentary,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totGLFee != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedsglfee";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totGLFee,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totTerminalFee != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedterminalfee";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totTerminalFee,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCostStorage != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcoststorage";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCostStorage,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCostDocumentFee != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostdocumentfee";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCostDocumentFee,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCostWeightFee != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostweighfee";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCostWeightFee,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totInspectionFee != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_inspection_fee";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totInspectionFee,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCSF != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_csf";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCSF,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		if(totCIV != 0){
			
			var dataC = landedCostCreditBalance.find(function(p) {
				return p.getValue("custrecord_freightcomponent")=="custcol_landedcostciv";
			});
			
			var account = dataC.getValue("custrecord_landedcostgl");
			
			var itemData = {
				"account" : account,
				"debit" : 0,
				"credit" : totCIV,//balance.balance,
				"itemClassification" : "",
				"inventorySegment" : inventorySegment,
				"department": department, 
				"division": division
			};
			
			itemDataArray.push(itemData);
		}
		
		
		
		// log.debug("itemDataArray", itemDataArray);
	
		var jsonBody = {
						  "currency":dataMain.currency,
						  "exchangerate":dataMain.exchangerate, // Need to confirm
						  "date":dataMain.date,
						  "subsidiary":dataMain.subsidiary,
						  "postingperiod":dataMain.postingperiod,
						  "JVType":LANDED_COST_JV,//"Landed Cost JV",
						  "TypeOfTransaction":dataMain.TypeOfTransaction,
						  "letterofCreditnum":dataMain.letterofCreditnum,
						  "billofLading": dataMain.BillLadingNo,
						  "division": division,
						  "department": department,
						  "location": dataMain.location,
						  "shipmentNumber": dataMain.Shipment,
						  "itemReceiptID": recordID,
						  "itemData": itemDataArray
					   };
						
		return CreateJV(jsonBody, recordID);
		
	}
	
	function CreateJV(data, recordID){
		try{
			var newRecord = record.create({
								type: "journalentry",
								isDynamic: false
							});
			log.debug("CreateJV - data "+ data.JVType, data);
			// log.debug("JV - recordID", recordID);
			// log.debug("data.JVType", data.JVType);
			
			newRecord.setValue({
				fieldId: "customform",
				value: RSK_JOURNAL_ENTRY//120
			});
			
			newRecord.setValue({
				fieldId: "subsidiary",
				value: data.subsidiary
			});
			
			newRecord.setValue({
				fieldId: "approvalstatus",
				value: APPROVED_STATUS
			});
						
			newRecord.setValue({
				fieldId: "trandate",
				value: parseAndFormatDateString(data.date)
			});
			
			newRecord.setValue({
				fieldId: "custbody_createdfrom",
				value: Number(recordID)
			});
			
			newRecord.setText({
				fieldId: "postingperiod",
				value: data.postingperiod
			});				
			
			newRecord.setValue({
				fieldId: "custbody_journaltypes",
				value: data.JVType//"Inventory JV" || "Landed Cost JV" 
			});
			
			if(data.JVType == INVENTORY_JV){
				newRecord.setValue({
					fieldId: "currency",
					value: data.currency
				});
			}
			newRecord.setValue({
				fieldId: "exchangerate",
				value: data.exchangerate
			});
			
			if(!!(data.billofLading)){
				newRecord.setText({
					fieldId: "custbody_billoflasdingnum",
					value: data.billofLading
				});
			}
			/* newRecord.setValue({
				fieldId: "class",
				value: data.division
			});
			newRecord.setValue({
				fieldId: "department",
				value: data.department
			}); */
			
			log.debug("Location", data.location);
			
			if(!!(data.location)){
				newRecord.setValue({
					fieldId: "location",
					value: Number(data.location)
				});
			}
			if(!!(data.shipmentNumber)){
				newRecord.setText({
					fieldId: "custbody_ship_num",
					value: data.shipmentNumber
				});
			}
			if(!!(data.letterofCreditnum)){
				newRecord.setValue({
					fieldId: "custbody_letterofcreditnum",
					value: data.letterofCreditnum
				});
			}
			if(!!(data.TypeOfTransaction)){
				newRecord.setValue({
					fieldId: "cseg_type_of_trans",
					value: Number(data.TypeOfTransaction)
				});
			}
			var itemData = data.itemData;
			
			log.debug("Item Data", itemData);
			
			
			for(var i = 0; i < itemData.length; i++){
				var lineNum = i;
				
				log.debug("itemData", itemData[i]);
				
				// newRecord.selectNewLine('line');
				// newRecord.setCurrentSublistValue('line','account',itemData[i].account);
				// newRecord.setCurrentSublistValue('line','debit', itemData[i].debit);
				// newRecord.setCurrentSublistValue('line','credit', itemData[i].credit);
				// newRecord.setCurrentSublistValue('line','cseg_itemclassifica',1);//Number(itemData[i].itemClassification));
				
				newRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'account',
					line: i,
					value: itemData[i].account
				});
				newRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'debit',
					line: i,
					value: (itemData[i].debit).toFixed(2)
				});
				newRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'credit',
					line: i,
					value: (itemData[i].credit).toFixed(2)
				});
				/* newRecord.setValue({
					sublistId: 'line',
					fieldId: "location",
					line: i,
					value: Number(data.location)
				}); */
				newRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'department',
					line: i,
					value: Number(itemData[i].department)
				});
				newRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'class',
					line: i,
					value: Number(itemData[i].division)
				});
				newRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'cseg_itemclassifica',
					line: i,
					value: itemData[i].itemClassification
				});
				newRecord.setSublistValue({
					sublistId: 'line',
					fieldId: 'cseg_inv_account',
					line: i,
					value: itemData[i].inventorySegment
				});						
				  
				// newRecord.commitLine('line');
			}
			
			var recordid = newRecord.save({
					enableSourcing: false,
					ignoreMandatoryFields : true
				});
			// log.debug("recordid JV",recordid);
			
			var id = record.submitFields({
				type: "journalentry",
				id: recordid,
				values: {
					custbody_createdfrom: recordID
				},
				options: {
					enableSourcing: false,
					ignoreMandatoryFields : true
				}
			});

			
			return {recordid : recordid, message: "Success"};
		}catch(err){
			log.debug("Error",err);
			return {message:"Failure", error: err};		
		}
	}
});