/**
	*@NApiVersion 2.x
	*@NScriptType UserEventScript
**/

/***************************************************************************************
	** Copyright (c) 1998-2019 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
	Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
	Information&quot;).
	**You shall not disclose such Confidential Information and shall use it only in accordance with
	the terms of the license agreement you entered into with Softype.
	**
	**@Author : Amol 
	**@Dated : 23 September 2019
	**@Version : 2.0
	**@Description : Running balance of Liquidation
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {

		function beforeLoad(context) {

			log.debug("context", context);
			
			if(context.type != "view"){
				return;
			}
			
			var newRecordDetails = context.newRecord;
			var scriptObj = runtime.getCurrentScript();
			// var createAPI = scriptObj.getParameter({name: 'custscript_create_api_item'});
			
			var grossAmount = newRecordDetails.getValue({
				fieldId: "custrecord_totalamount"
			});
			var runningBalance = newRecordDetails.getValue({
				fieldId: "custrecord_run_bal_tv"
			});
			
			var values = {};
			
			values["custrecord_run_bal"] = parseFloat(grossAmount||0) - parseFloat(runningBalance||0);
			
			log.debug("grossAmount", grossAmount);
			log.debug("runningBalance", runningBalance);
			
			var id = record.submitFields({
				type: newRecordDetails.type,//record.Type.PURCHASE_ORDER,
				id: newRecordDetails.id,
				values: values,
				options: {
					enableSourcing: false,
					ignoreMandatoryFields : true
				}
			});
		}
	
		function beforeSubmit(context){	
			var newRecordDetails = context.newRecord;
			
			var grossAmount = newRecordDetails.getValue({
				fieldId: "custrecord_totalamount"
			});
			var checkAmount = newRecordDetails.getValue({
				fieldId: "custrecord_write_check_amt"
			});
					
			var rfpNumber = newRecordDetails.getValue({
				fieldId: "custrecord_rfp_number"
			});
			var rfpAmount = newRecordDetails.getValue({
				fieldId: "custrecord_rfp_amt"
			});
			
			log.debug("grossAmount", grossAmount);
			log.debug("rfpAmount", rfpAmount);
			log.debug("checkAmount", checkAmount);
			
			if(!!(rfpNumber)){
				var flag = false;
				if(grossAmount > rfpAmount){
					throw "Gross amount can not be greater than RFP Amount";
					flag = true;
				}
				
				if(grossAmount > checkAmount){
					throw "Gross amount can not be greater than Check Amount";
					flag = true;
				}
				
				if(flag){
					return;
				}			
			}
		}
		return {
			beforeLoad: beforeLoad,
			beforeSubmit: beforeSubmit
		};
	});		