/***************************************************************************************  
	** Copyright (c) 1998-2018 Softype, Inc.
	** P3 Zenith Central | Luzon Avenue | Cebu Business Park, Cebu City | Philippines
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
	**                      
	**@Author      :  Care P
	**@Revised	   :  Renato C
 	**@Dated       :  31st October 2019
	**@Version     :  1.x
	**@Description :  Suitelet for Debit Memo 
***************************************************************************************/


/**
	* @param {nlobjRequest} request Request object
	* @param {nlobjResponse} response Response object
	* @returns {Void} Any output is written via response object
*/

function printto(request, response) {
	
	var RecId = request.getParameter('recordId');
	nlapiLogExecution('Debug', 'RecId', RecId);
	
	var vcRec = nlapiLoadRecord('vendorcredit', RecId); // Load Vendor Credit
	//Reason Custom Fields
	var returnedItems = vcRec.getFieldValue('custbody_return_item_reason');
	nlapiLogExecution('debug', 'returnedItems', returnedItems);		 
	var discountPriceAdj = vcRec.getFieldValue('custbody_disc_adj_reason');
	nlapiLogExecution('debug', 'discountPriceAdj', discountPriceAdj);
	var others = vcRec.getFieldValue('custbody_other_reason');
	nlapiLogExecution('debug', 'others', others);
	var otherReason = vcRec.getFieldValue('custbody_other_reason_text');
	nlapiLogExecution('debug', 'othotherReason', otherReason);
	
	
	var transNo = vcRec.getFieldValue('transactionnumber');
	var referenceNo = vcRec.getFieldValue('tranid');
	var supplier = vcRec.getFieldValue('entity');
	var supplierRec = nlapiLoadRecord('vendor', supplier);
	var businessName = supplierRec.getFieldValue('legalname');
	var tin = supplierRec.getFieldValue('vatregnumber');
	var supplierName = vcRec.getFieldText('entity');
	var supplierAddr = vcRec.getFieldValue('billaddress');
	var date = vcRec.getFieldValue('trandate');
	var memo = vcRec.getFieldValue('memo');
	var amountTotal = vcRec.getFieldValue('usertotal');
	var grossAmountTotal = vcRec.getFieldValue('custbody_dm_gross_amt');
	var supplierInvNum = vcRec.getFieldValue('createdfrom');
	var currency = vcRec.getFieldText('currency');
	if(currency==="Philippine Peso"){
		currency = "Php";
	}
	nlapiLogExecution('debug','currency', currency);
	var supplierInvNumDisplay = vcRec.getFieldText('createdfrom');
	// ===================================================================
	var preparedBy = vcRec.getFieldValue('custbody_prepared_by');
	nlapiLogExecution('debug', 'preparedBy', preparedBy);
	if(preparedBy){
		try{
			var empRec = nlapiLoadRecord('employee', preparedBy);
			var empName = empRec.getFieldValue('entityid');
			}catch(e){
			var empRec = nlapiLoadRecord('partner', preparedBy);
			var empName = empRec.getFieldValue('entityid');
		}
	}
	// =====================================================================
	var dateAndTimeCreated = vcRec.getFieldValue('custbody_date_time_create');
	var poNum = vcRec.getFieldValue('custbody_ref_doc');
	if(poNum){
		var poNumRec = nlapiLoadRecord('purchaseorder', poNum);
		var poNumDisplay = poNumRec.getFieldValue('tranid');
		} else {
		alert("Reference PO field is empty");
	}
	var subsidiary = vcRec.getFieldValue('subsidiary');
	var subRec = nlapiLoadRecord('subsidiary', subsidiary);
	var subAddress = subRec.getFieldValue('mainaddress_text');
	var subTelephone = subRec.getFieldValue('custrecord_subsidiary_tel');
	// var amountInWords = vcRec.getFieldValue('custbody_cashsale_totalword');
	var amountInWords = number2text(grossAmountTotal);//vcRec.getFieldValue('custbody_cashsale_totalword');
	var particularsField = "";
	var newSuppInvNum = "";
	
	
	//Concatenate Values needed to fill in the Particulars field
	particularsField += (memo == null? "" : nlapiEscapeXML(memo));
	particularsField += " " + (poNumDisplay == null? "" : nlapiEscapeXML(poNumDisplay));
	particularsField += " " + (newSuppInvNum == null? "" : nlapiEscapeXML(newSuppInvNum));
	
	var htmlvar = '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
	var image = 'https://5066453.app.netsuite.com/core/media/media.nl?id=840&c=5066453&h=dbffae9c40aa036377e7&fcts=20200305211923&whence=';
	htmlvar += '<pdf>\n';
	htmlvar += '<head>\n';
	htmlvar += '<macrolist>';
	htmlvar += '<macro id="nlheader">';
	htmlvar += '</macro>';
	htmlvar += '<macro id="nlfooter">';
	htmlvar += '</macro>';
	htmlvar += '</macrolist>';
	htmlvar += '</head>';
	
	
	htmlvar += '<body header="nlheader" header-height="10%" padding="0.1in 0.1in 0.1in 0in" height="175mm" font-size="10" width="227mm">';
	//transction number
	htmlvar += '<table style="width:87%; padding: 0px; 0px; 0px; 0px; font-size:10pt;">';
	htmlvar += '<tr>';
	htmlvar += '<td align="right">' + (transNo == null? "" : nlapiEscapeXML(transNo)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	//referrence number 
	htmlvar += '<table style="width: 100%; margin-top: 42px; border:0">';
	htmlvar += '<tr height="15px">';
	htmlvar += '<td align="right" style="padding-top:5px;">' + (referenceNo == null? "" : nlapiEscapeXML(referenceNo)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	//supplier name and date
	htmlvar += '<table style="width: 100%; margin-top: 2px; padding-left:11px; border:0; font-size:9;">';
	htmlvar += '<tr height="15px">';
	htmlvar += '<td style="width: 150px; padding-bottom: -1px;">&nbsp;</td>';
	htmlvar += '<td align="left" style="width: 170px; padding-bottom: -1px;">' + (supplierName == null? "" : nlapiEscapeXML(supplierName)) + '</td>';
	htmlvar += '<td style="width: 230px; padding-bottom: -1px; border: 0; padding-right:2px;" align="center"> <img src="' + nlapiEscapeXML(image) + '" alt="" height="10" width="10" /></td>';
	htmlvar += '<td style="width: 50px; padding-bottom: -1px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + (date == null? "" : nlapiEscapeXML(date)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	//business number and tin
	htmlvar += '<table style="width: 100%; margin-top: 1px; padding-left:10px; border:0; font-size:9;">';
	htmlvar += '<tr height="15px">';
	htmlvar += '<td style="width: 115px; padding-bottom: 3px;">&nbsp;</td>';
	htmlvar += '<td style="width: 215px; padding-bottom: 3px;">' + (businessName == null? "" : nlapiEscapeXML(businessName)) + '</td>';
	htmlvar += '<td style="width: 230px; padding-bottom: 3px;">&nbsp;</td>';
	htmlvar += '<td style="width: 55px;  padding-bottom: 3px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + (tin == null? "" : nlapiEscapeXML(tin)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	//supplier address
	htmlvar += '<table style="width: 100%; margin-top: 0px; padding-left:6px;; font-size:9;">';
	htmlvar += '<tr height="15px">';
	htmlvar += '<td style="width: 47px; padding-bottom: -1px; padding-top: -1px;">&nbsp;</td>';
	htmlvar += '<td style="border:0; padding-bottom: -1px; padding-top: -1px;">' + (supplierAddr == null? "" : nlapiEscapeXML(supplierAddr)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	//amount in words
	htmlvar += '<table style="width: 100%; margin-top: 0px; padding-left:10px; font-size:9; border:0;">';
	htmlvar += '<tr height="15px">';
	htmlvar += '<td style="width: 335px; padding-bottom: -1px;">&nbsp;</td>';
	htmlvar += '<td style="width: 200px; padding-bottom: -1px;">' + (amountInWords == null? "" : nlapiEscapeXML(amountInWords)) + '</td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	//amount total
	htmlvar += '<table style="width: 100%; margin-top: 0px; padding-left:10px; font-size:9; border:0;">';
	htmlvar += '<tr height="15px">';
	htmlvar += '<td style="width: 275px; padding-bottom: -1px;">&nbsp;</td>';
	htmlvar += '<td style="width: 80px; padding-bottom: -1px;">' +(nlapiEscapeXML((grossAmountTotal||(amountTotal||"")).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) +'</td>';
	htmlvar += '<td style="width: 170px; padding-bottom: -1px;">&nbsp;</td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	
	//Reason Custom fields --> start
	htmlvar += '<table style="width: 100%; margin-top:2px; font-size:9pt; border:0; padding-left:13px;">';
	htmlvar += '<tr height="10" border="0" padding-top="3px" margin-left="10px">';
	if(returnedItems=='T'){
		htmlvar += '<td align="right" style="width: 70px; border:0;"> <img src="' + nlapiEscapeXML(image) + '" alt="" height="10" width="10" /> </td>';
		}else{
		htmlvar += '<td align="right" style="width: 70px; border:0;"></td>';
	}
	htmlvar += '<td style="width: 300px; border:0;">&nbsp;</td>';
	if(others=='T'){
		htmlvar += '<td align="center" style="width: 25px; border:0;"><img src="' + nlapiEscapeXML(image) + '" alt="" height="10" width="10"/></td>';
		htmlvar += '<td style="width: 310px; border:0;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + (otherReason == null? "" : nlapiEscapeXML(otherReason)) + '</td>';
		}else{
		htmlvar += '<td style="width: 25px; border:0; "></td>';
		htmlvar += '<td style="width: 310px; border:0;"></td>';
	}
	htmlvar += '</tr>';
	htmlvar += '</table>';
	
	htmlvar += '<table style="width: 100%; margin-top:4px; font-size:9pt; border:0; padding-left:13px;">';
	htmlvar += '<tr height="10" border="0" padding-top="3px">';
	if(discountPriceAdj=='T'){
		htmlvar += '<td align="right" style="width: 70px; border:0; "> <img src="' + nlapiEscapeXML(image) + '" alt="" height="10" width="10" /> </td>';
	}
	else{
		htmlvar += '<td align="right" style="width: 70px; border:0;"></td>';
	}
	htmlvar += '<td style="width: 300px; border:0;">&nbsp;</td>';
	htmlvar += '<td style="width: 20px; border:0;"></td>';
	htmlvar += '<td style="width: 310px; border:0;"></td>';
	htmlvar += '</tr>';
	htmlvar += '</table>';
	//<--- end
	
	//for wider access of GL impact search variable
	var debitMemoGlImpactResult = nlapiSearchRecord('vendorcredit', null, 
		[
			(new nlobjSearchFilter('internalid', null, 'is', RecId))
			], [ 
			(new nlobjSearchColumn('account')),
			(new nlobjSearchColumn('creditamount')),
			(new nlobjSearchColumn('debitamount')).setSort(false)
		]);
		
		var length = debitMemoGlImpactResult.length;
		
		nlapiLogExecution('debug',' GL length', length);
		
		//Concatenate Values needed to fill in the Explanation field
		var lineCountItem = vcRec.getLineItemCount('item');
		for (var b = 1; b <= lineCountItem; b++) {
			var itemId = vcRec.getLineItemValue('item', 'item', b);
			if(itemId != 8){
				var item = vcRec.getLineItemValue('item', 'item', b);
				var description = vcRec.getLineItemValue('item', 'description', b);	
				var quantity = vcRec.getLineItemValue('item', 'quantity', b);
				var rate = vcRec.getLineItemValue('item', 'rate', b);
				// particularsField += " " + (item == null? "" : nlapiEscapeXML(item));
				particularsField += " " + (description == null? "" : nlapiEscapeXML(description));
				
				var itemType = nlapiLookupField('item', item, 'type');;
				
				if(itemType != "OthCharge"){
					particularsField += " " + (quantity == null? "" : nlapiEscapeXML(quantity));
					particularsField += " " + (rate == null? "" : nlapiEscapeXML(rate));
				}
			}
		}
		
		htmlvar += '<table style="width: 88%; margin-top:26px; margin-left: 80px; font-size:9pt; border:0; padding-top:1.5px;" height="47">';
		htmlvar += '<tr  border="0">';
		//htmlvar += '<td style="width: 50px;">&nbsp;</td>';
		htmlvar += '<td style="width: 150px;">' + (particularsField == null? "" : nlapiEscapeXML(particularsField.toUpperCase())) + '</td>';
		//htmlvar += '<td style="width: 380px;">&nbsp;</td>';
		htmlvar += '</tr>';
		htmlvar += '</table>';
		
		htmlvar += '<table style="width: 100%; margin-top: 2px; margin-left: 15px; font-size:8pt; border:0; height:85;">';
		htmlvar += '<tr border="0">';
		htmlvar += '<td style="width: 60px; border: 0;">&nbsp;</td>';
		
		
		//Debit and Credit Arrays and Total Amount Variables	
		var debitArray = [];
		var creditArray = [];
		var totalDebit = 0;
		var totalCredit = 0;
		
		
		if (debitMemoGlImpactResult) {
			htmlvar += '<td style="width: 320px; border:0;">';	
			for(var c = 0; c < debitMemoGlImpactResult.length; c++){
				var accountId = debitMemoGlImpactResult[c].getValue('account');
				var accountRec = nlapiLoadRecord('account', accountId);
				var accountName = accountRec.getFieldValue('acctname');
				var creditAmount = debitMemoGlImpactResult[c].getValue('creditamount');
				var debitAmount = debitMemoGlImpactResult[c].getValue('debitamount');
				//nlapiLogExecution('DEBUG', 'debitAmount', debitAmount);
				//nlapiLogExecution('DEBUG', 'creditAmount', creditAmount);
				//Push Debit and Credit Array
				
				debitArray.push({
					'account': accountName,
					'debitAmount': debitAmount
				});
				creditArray.push({
					'account': accountName,
					'creditAmount': creditAmount
				});
				
				//indent the amount credit only
				
				//Input Tax doesn't have a value don't display it
				if(accountName=="Input Tax"){
					if(creditAmount==0){
						continue;
					}
					
				}
				if(creditAmount!=0){
					htmlvar += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ (accountName == null? "" : nlapiEscapeXML(accountName)) +' <br/>';
					}else{
					htmlvar += ''+ (accountName == null? "" : nlapiEscapeXML(accountName)) +' <br/>';
				}
				//nlapiLogExecution('DEBUG', 'accountName', accountName);
				
			}
			htmlvar += '</td>';
			
			//For Debit Accounts
			htmlvar += '<td align="right" style="width:175px; border:0;">';
			for(var d = 0; d < debitArray.length; d++){
				var debitAccount = debitArray[d].account;
				var amountDebit = debitArray[d].debitAmount;
				totalDebit += parseFloat(amountDebit);
				
				if(debitAccount=="Input Tax"){
					if(amountDebit==0){
						continue;
					}
					
				}
				if(amountDebit!=0)
				{
					htmlvar += '' +(amountDebit == null? "" : nlapiEscapeXML(amountDebit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) +'<br/>';
					//nlapiLogExecution('DEBUG', 'amountDebit', amountDebit);
				}else{
					htmlvar += '<br/>';
				
			}
		}		
		htmlvar += '</td>';
		
		//For Credit Accounts
		htmlvar += '<td align="left" style="width: 37.5.5px; border:0;">';
		for(var e = 0; e < creditArray.length; e++){
			var creditAccount = creditArray[e].account;
			var amountCredit = creditArray[e].creditAmount;
			totalCredit += parseFloat(amountCredit);
			
			if(creditAccount=="Input Tax"){
				if(amountCredit==0){
					continue;
				}
				
			}
			if(amountCredit!=0)
			{
				htmlvar += ''+ (amountCredit == null? "" : nlapiEscapeXML(amountCredit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) +'<br/>';
				//nlapiLogExecution('DEBUG', 'amountCredit', amountCredit);
				}else{
				htmlvar += '<br/>';
			}
			
		}
}
htmlvar += '</td>';

// end if(debitMemoGlImpactResult) condition
htmlvar += '<td style="width: 64.8px;">&nbsp;</td>';
htmlvar += '</tr>';
htmlvar += '</table>';

//currency and amount total
htmlvar += '<table align="center" style="width: 80%; margin-top:-8px; font-size:9pt; border:0; margin-left: 10px;">';
htmlvar += '<tr padding-top="1.5px" border="0" align="right">';
htmlvar += '<td align="center" style="border-right:0; border:0; width: 305px;"></td>';
htmlvar += '<td align="center" style="border:0; border-left:0; width: 5px;">'+ (currency == null? "" : nlapiEscapeXML(currency)) +'</td>';
htmlvar += '<td align="right" style="border-bottom:0; border:0; width: 237px;">'+ (nlapiEscapeXML((grossAmountTotal||(amountTotal||"")).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))) +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
htmlvar += '</tr>';
htmlvar += '</table>';

//confirmed by
htmlvar += '<table style="width: 100%; margin-top: 0px;">';
htmlvar += '<tr>';
htmlvar += '<td style="width: 330px;"></td>'; //Confirmed by
htmlvar += '<td style="width: 120px;"></td>';
htmlvar += '</tr>';
htmlvar += '<tr>';
htmlvar += '<td align="left" style="width: 100px; border:0; padding-top: 22.5px;">'+ (empName == null? "" : nlapiEscapeXML(empName.toUpperCase())) + '<br/>' + (dateAndTimeCreated == null? "" : nlapiEscapeXML(dateAndTimeCreated)) + '</td>';
htmlvar += '<td style="width: 120px; padding-top: 22.5px;"></td>';
htmlvar += '</tr>';
htmlvar += '</table>';

htmlvar += '</body>';
htmlvar += '</pdf>';
var file = nlapiXMLToPDF(htmlvar);
response.setContentType('PDF', 'Print.pdf', 'inline');
response.write(file.getValue());

}


function number2text(value) {
    var fraction = Math.round(frac(value)*100);
    var f_text  = "";
	
    if(fraction > 0) {
		// f_text = convert_number(fraction)+" Cents"; //Changes as Moses req 
		f_text = convert_number(fraction)+" Centavos";
		return numToWords(value)+' Pesos and '+f_text+" Only";
	}
    return numToWords(value)+' Pesos'+f_text+" Only";
}

function frac(f) {
    return f % 1;
}

function convert_number(number) {
    if (number < 0) {
        console.log("Number Must be greater than zero = " + number);
        return "";
	}
    if (number > 100000000000000000000) {
        console.log("Number is out of range = " + number);
        return "";
	}
    if (!is_numeric(number)) {
        console.log("Not a number = " + number);
        return "";
	}
	
    var quintillion = Math.floor(number / 1000000000000000000); /* quintillion */
    number -= quintillion * 1000000000000000000;
    var quar = Math.floor(number / 1000000000000000); /* quadrillion */
    number -= quar * 1000000000000000;
    var trin = Math.floor(number / 1000000000000); /* trillion */
    number -= trin * 1000000000000;
    var Gn = Math.floor(number / 1000000000); /* billion */
    number -= Gn * 1000000000;
    var million = Math.floor(number / 1000000); /* million */
    number -= million * 1000000;
    var Hn = Math.floor(number / 1000); /* thousand */
    number -= Hn * 1000;
    var Dn = Math.floor(number / 100); /* Tens (deca) */
    number = number % 100; /* Ones */
    var tn = Math.floor(number / 10);
    var one = Math.floor(number % 10);
    var res = "";
	
    if (quintillion > 0) {
	res += (convert_number(quintillion) + " quintillion");
	}
    if (quar > 0) {
	res += (convert_number(quar) + " quadrillion");
	}
    if (trin > 0) {
	res += (convert_number(trin) + " trillion");
	}
    if (Gn > 0) {
	res += (convert_number(Gn) + " billion");
	}
    if (million > 0) {
	res += (((res == "") ? "" : " ") + convert_number(million) + " million");
	}
    if (Hn > 0) {
	res += (((res == "") ? "" : " ") + convert_number(Hn) + " Thousand");
	}
	
    if (Dn) {
	res += (((res == "") ? "" : " ") + convert_number(Dn) + " hundred");
	}
	
	
    var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen");
    var tens = Array("", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety");
	
    if (tn > 0 || one > 0) {
	if (!(res == "")) {
	res += " and ";
	}
	if (tn < 2) {
	res += ones[tn * 10 + one];
	} else {
	
	res += tens[tn];
	if (one > 0) {
	res += ("-" + ones[one]);
	}
	}
	}
	
    if (res == "") {
	console.log("Empty = " + number);
	res = "";
	}
    return res;
	}
	function is_numeric(mixed_var) {
    return (typeof mixed_var === 'number' || typeof mixed_var === 'string') && mixed_var !== '' && !isNaN(mixed_var);
	}
	
	
	function numToWords(number) {
	
    //Validates the number input and makes it a string
    if (typeof number === 'string') {
	number = parseInt(number, 10);
	}
    if (typeof number === 'number' && isFinite(number)) {
	number = number.toString(10);
	} else {
	return 'This is not a valid number';
	}
	
    //Creates an array with the number's digits and
    //adds the necessary amount of 0 to make it fully
    //divisible by 3
    var digits = number.split('');
    while (digits.length % 3 !== 0) {
	digits.unshift('0');
	}
	
	
    //Groups the digits in groups of three
    var digitsGroup = [];
    var numberOfGroups = digits.length / 3;
    for (var i = 0; i < numberOfGroups; i++) {
	digitsGroup[i] = digits.splice(0, 3);
	}
    // console.log(digitsGroup); //debug
	
    //Change the group's numerical values to text
    var digitsGroupLen = digitsGroup.length;
    var numTxt = [
	[null, 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'], //hundreds
	[null, 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'], //tens
	[null, 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'] //ones
	];
    var tenthsDifferent = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
	
    // j maps the groups in the digitsGroup
    // k maps the element's position in the group to the numTxt equivalent
    // k values: 0 = hundreds, 1 = tens, 2 = ones
    for (var j = 0; j < digitsGroupLen; j++) {
	for (var k = 0; k < 3; k++) {
	var currentValue = digitsGroup[j][k];
	digitsGroup[j][k] = numTxt[k][currentValue];
	if (k === 0 && currentValue !== '0') { // !==0 avoids creating a string "null hundred"
	digitsGroup[j][k] += ' Hundred ';
	} else if (k === 1 && currentValue === '1') { //Changes the value in the tens place and erases the value in the ones place
	digitsGroup[j][k] = tenthsDifferent[digitsGroup[j][2]];
	digitsGroup[j][2] = 0; //Sets to null. Because it sets the next k to be evaluated, setting this to null doesn't work.
	}
	}
	}
	
    // console.log(digitsGroup); //debug
	
    //Adds '-' for gramar, cleans all null values, joins the group's elements into a string
    for (var l = 0; l < digitsGroupLen; l++) {
	if (digitsGroup[l][1] && digitsGroup[l][2]) {
	digitsGroup[l][1] += ' ';
	}
	digitsGroup[l].filter(function (e) {return e !== null});
	digitsGroup[l] = digitsGroup[l].join('');
	}
	
    // console.log(digitsGroup); //debug
	
    //Adds thousand, millions, billion and etc to the respective string.
    var posfix = [null, 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'Quintillion', 'Sextillion'];
    if (digitsGroupLen > 1) {
	var posfixRange = posfix.splice(0, digitsGroupLen).reverse();
	for (var m = 0; m < digitsGroupLen - 1; m++) { //'-1' prevents adding a null posfix to the last group
	if (digitsGroup[m]) {
	digitsGroup[m] += ' ' + posfixRange[m];
	}
	}
	}
	
    // console.log(digitsGroup); //debug
	
    //Joins all the string into one and returns it
    return digitsGroup.join(' ');
	}		