/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author  : Farhan Shaikh
 **@Dated   : 26/09/2019   DD/MM/YYYY
 **@Version :         1.0
 **@Description :     Workflow Action script 
 ***************************************************************************************/
define(['N/record','N/search',"N/log","N/url","N/redirect"],    function(record,search,log,url,redirect){
    
    function setUbCsvFields(context){
        var currentRecordId = context.newRecord.id;
        log.audit('currentRecordId',currentRecordId);
        var currRecObj = context.newRecord;

        var lineCountItem=currRecObj.getLineCount('item');
        log.debug('lineCountItem',lineCountItem);

        var lineCountExpense=currRecObj.getLineCount('expense');
        log.debug('lineCountExpense',lineCountExpense);

        if (lineCountItem>0) {
            var wTaxCode=currRecObj.getSublistValue({
                sublistId:'item',
                fieldId:'custcol_adv_wtax_code',
                line:0
            });
            log.debug('wTaxCode',wTaxCode)
        }

        if(lineCountExpense>0){
            var wTaxCode=currRecObj.getSublistValue({
                sublistId:'expense',
                fieldId:'custcol_adv_wtax_code',
                line:0
            });
            log.debug('wTaxCode',wTaxCode)
        }

        /*var wTaxDetails = search.lookupFields({
            type: 'customrecord_4601_witaxcode',
            id: wTaxCode,
            columns: ['custrecord_4601_wtc_name','custrecord_4601_wtc_description']
        });*/
        /*var WTaxName = wTaxDetails.custrecord_4601_wtc_name;
        var wTaxDescription = wTaxDetails.custrecord_4601_wtc_description;*/
        if (wTaxCode!='' && wTaxCode!=undefined && wTaxCode!=null) {
            var wTaxDetails = search.lookupFields({
                type: 'customrecord_wtax_record',
                id: wTaxCode,
                columns: ['name','custrecord_wtax_desc']
            });
            log.audit('wTaxDetails',wTaxDetails)
            
            var WTaxName = wTaxDetails.name;
            var wTaxDescription = wTaxDetails.custrecord_wtax_desc;
        }

        if (WTaxName!='' && WTaxName!=undefined && WTaxName!=null) {
            var firstMonth=currRecObj.getValue('custbody_ub_payout_first_month');
            var lastMonth=currRecObj.getValue('custbody_ub_payout_last_month');
            var quarter=currRecObj.getValue('custbody_ub_payout_month_quarter');
            log.audit('firstMonth',firstMonth)
            log.audit('lastMonth',lastMonth)
            log.audit('quarter',quarter)

            var expenseTaxBaseAmount=0;
            var itemTaxBaseAmount=0;
            var taxAmount=currRecObj.getValue('custbody_adv_ub_wtax_amount');
            log.debug('taxAmount',taxAmount)
            if (lineCountItem>0) {
                itemTaxBaseAmount=calculateTaxBaseAmount('item',currRecObj,lineCountItem)
            }

            if (lineCountExpense>0) {
                expenseTaxBaseAmount=calculateTaxBaseAmount('expense',currRecObj,lineCountExpense)
            }
            var taxBaseAmount=Number(itemTaxBaseAmount)+Number(expenseTaxBaseAmount)
    
            var stringToSetInATCField='';
            stringToSetInATCField+='`'+WTaxName+'|'+'taxable='+taxBaseAmount+'|wtax='+taxAmount+'|';
            if (firstMonth) {
                stringToSetInATCField+=firstMonth+'|';
            }
    
            if (lastMonth) {
                stringToSetInATCField+=lastMonth+'|';
            }
    
            if (quarter) {
                stringToSetInATCField+=quarter+'|';
            }
    
            /*var searchObj=search.load('customsearch61_2');
    
            searchObj.filters.push(search.createFilter({
                name: 'custrecord_4601_wtc_name',
                operator: 'is',
                values: wTaxCode
            }));
            var taxDescription=searchObj.run().getRange({start:0,end:1});
            log.debug('taxDescription ',taxDescription);
    
            var description=taxDescription[0].getValue('custrecord_4601_wtc_description')*/
    
            stringToSetInATCField+=wTaxDescription;
            log.audit('stringToSetInATCField',stringToSetInATCField);
            currRecObj.setValue('custbody_ub_payout_atc_code',stringToSetInATCField)
            
        }
        var refPO=currRecObj.getValue('custbody_ref_doc');
        var refRFP=currRecObj.getValue('custbody_ref_rfp_num');
        var refFieldString='';
        var totalAmt;
        var wTaxRate;
        var itemLineCount=currRecObj.getLineCount('item');
        var expenseLineCount=currRecObj.getLineCount('expense');
        if (itemLineCount>0) {
            wTaxRate=currRecObj.getSublistValue({sublistId:"item",fieldId:'custcol_adv_wtax_rate',line:0})
            totalAmt=currRecObj.getSublistValue({sublistId:"item",fieldId:'grossamt',line:0})
        }
        if (expenseLineCount>0) {
            wTaxRate=currRecObj.getSublistValue({sublistId:"expense",fieldId:'custcol_adv_wtax_rate',line:0})
            totalAmt=currRecObj.getSublistValue({sublistId:"expense",fieldId:'grossamt',line:0})
        }
        if (refPO.length>0) {
            if (wTaxRate!=null && wTaxRate!=undefined && wTaxRate!='') {
                wTaxRate=parseFloat(wTaxRate)/100;
                //for(var i=0;i<refPO.length;i++){
                    /*if (refPO[i]==undefined || refPO[i]=='' || refPO[i]==null) {
                        return;
                    }*/
                    var refNumberLookup = search.lookupFields({
                        type: 'purchaseorder',
                        id: refPO,
                        columns: ['tranid','total','taxtotal']
                    });
                    var refNumber=refNumberLookup.tranid;
                    var refAmount=refNumberLookup.total;
                    var refTaxmount=refNumberLookup.taxtotal;
                    var refTaxExcludedAmount=Number(refAmount)+Number(refTaxmount)
                    refTaxExcludedAmount=Number(refTaxExcludedAmount)*Number(wTaxRate)
                    //if (i==refPO.length-1) {
                        if (refNumber!='' && refNumber!=undefined && refNumber!=null) {
                            refFieldString+=refNumber+'='+(Number(refAmount)-refTaxExcludedAmount)
                        }
                    /*}else{
                        if (refNumber!='' && refNumber!=undefined && refNumber!=null) {
                            refFieldString+=refNumber+'='+(Number(refAmount)-refTaxExcludedAmount)+'|'
                        }
                    }*/
                //}
            }else{
                var refNumberLookup = search.lookupFields({
                    type: 'purchaseorder',
                    id: refPO,
                    columns: ['tranid','total','taxtotal']
                });
                var refNumber=refNumberLookup.tranid;
                if (refNumber!='' && refNumber!=undefined && refNumber!=null) {
                    refFieldString+=refNumber+'='+(Number(totalAmt))
                }
            }
            currRecObj.setValue('custbody_ub_payout_ref',refFieldString);
        }else if(refRFP!='' && refRFP!=null && refRFP!=undefined){
            var rfpNum=currRecObj.getValue('custbody_ref_rfp_num');
            var rfpNumText=currRecObj.getText('custbody_ref_rfp_num');
            /*var rfpAmountLookup = search.lookupFields({
                type: 'customrecord_rfp',
                id: rfpNum,
                columns: ['custrecord_rfp_amount']
            });
            log.audit('rfpAmountLookup',rfpAmountLookup)
            
            var rfpAmount = rfpAmountLookup.custrecord_rfp_amount;*/
            refFieldString+=rfpNumText+'='+(Number(totalAmt)) //rfpAmount
            currRecObj.setValue('custbody_ub_payout_ref',refFieldString);
        }else{
            //currRecObj.setValue('custbody_ub_payout_ref','');
        }
    }

    function calculateTaxBaseAmount(sublist,currRecObj,lineCount){
        var totalTaxBaseAmount=0;

        for(var i=0;i<lineCount;i++){
            var wTaxCode=currRecObj.getSublistValue({
                sublistId:sublist,
                fieldId:'custcol_adv_wtax_code',
                line:i
            })

            if (wTaxCode!=undefined && wTaxCode!='' && wTaxCode!=null) {
                var taxBaseAmount=currRecObj.getSublistValue({
                    sublistId:sublist,
                    fieldId:'custcol_gross_amount',
                    line:i
                })
                log.debug('taxBaseAmount',taxBaseAmount);
                totalTaxBaseAmount+=taxBaseAmount;
            }

        }

        return totalTaxBaseAmount;
    }

    return {
        onAction: setUbCsvFields
    };
});