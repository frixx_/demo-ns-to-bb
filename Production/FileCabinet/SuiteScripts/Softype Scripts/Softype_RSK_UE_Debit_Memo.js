/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Siddhi Kadam
 **@Dated       :  27th February, 2020
 **@Version     :  2.x
 **@Description :  This script is deployed on Debit Memo (Supplier Credit) to calculate WHTax Amount.
 ***************************************************************************************/
define(['N/log', 'N/error', 'N/record', 'N/url', 'N/runtime', 'N/search'],

    function(log, error, record, url, runtime, search) {

        function beforeSubmit(scriptContext) {


            var currentRecordObj = scriptContext.newRecord;
            var recordId = scriptContext.newRecord.id;
            var recordType = scriptContext.newRecord.type;
            var whTaxAmountTotalItem = 0;

            var form = Number(runtime.getCurrentScript().getParameter("custscript_form_id_debit_memo"));
			
			
			 log.debug('Debit Memo Record Id - Start', recordId);



            var customForm = Number(currentRecordObj.getValue({
                fieldId: 'customform'
            }));
			
			log.debug('customForm', customForm);


            if (customForm != form) {

                return;
            }


            var createdFrom = Number(currentRecordObj.getValue({
                fieldId: 'createdfrom'
            }));
            log.debug('createdFrom', createdFrom);



            var whTaxAmountTotalExpense = 0;




            if (scriptContext.type == 'delete')
                return;



            var lineItemCount = currentRecordObj.getLineCount({
                sublistId: 'item'

            });


            lineItemCountLastLine = lineItemCount - 1


            if (lineItemCount) {

                for (var i = 0; i < lineItemCount; i++) {



                    var amount = Number(currentRecordObj.getSublistValue({

                        sublistId: 'item',
                        fieldId: 'amount',
                        line: i

                    }));


                    currentRecordObj.setSublistValue({

                        sublistId: 'item',
                        fieldId: 'custcol_4601_witaxbaseamount',
                        line: i,
                        value: amount

                    });

                    var WHTBaseAmount = currentRecordObj.getSublistValue({

                        sublistId: 'item',
                        fieldId: 'custcol_4601_witaxbaseamount',
                        line: i


                    });


                    var WHTRate = Number(currentRecordObj.getSublistValue({

                        sublistId: 'item',
                        fieldId: 'custcol_4601_witaxrate',
                        line: i

                    }));


					log.debug('WHTRate', WHTRate);

                    var WHTaxAmount = Number(WHTBaseAmount * WHTRate) / 100;
                    WHTaxAmount = -Math.abs(WHTaxAmount);




                    currentRecordObj.setSublistValue({

                        sublistId: 'item',
                        fieldId: 'custcol_4601_witaxamount',
                        line: i,
                        value: WHTaxAmount

                    });


					log.debug('WHTaxAmount', WHTaxAmount);

                    var whTaxAmount = Number(currentRecordObj.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_4601_witaxamount',
                        line: i

                    }));

                    whTaxAmountTotalItem = whTaxAmountTotalItem + whTaxAmount;




                }

				log.debug('whTaxAmountTotalItem', whTaxAmountTotalItem);

                currentRecordObj.setSublistValue({

                    sublistId: 'item',
                    fieldId: 'rate',
                    line: lineItemCountLastLine,
                    value: whTaxAmountTotalItem

                });

                currentRecordObj.setSublistValue({

                    sublistId: 'item',
                    fieldId: 'custcol_4601_witaxbaseamount',
                    line: lineItemCountLastLine,
                    value: ''

                });


            }

            var applyCount = currentRecordObj.getLineCount({
                sublistId: 'apply'

            });

           

            for (var j = 0; j < applyCount; j++) {


                var doc = Number(currentRecordObj.getSublistValue({

                    sublistId: 'apply',
                    fieldId: 'doc',
                    line: j,

                }));

                log.debug('doc', doc);

                if (doc == createdFrom) {
					
					log.debug('Inside If doc == createdFrom','doc == createdFrom');


                    var checkedOrUnchecked = currentRecordObj.getSublistValue({

                        sublistId: 'apply',
                        fieldId: 'apply',
                        line: j,


                    });
					
					/** Because if it is not checked then it shouldn't get applied. **/

                    if (checkedOrUnchecked == true) {


                        currentRecordObj.setSublistValue({

                            sublistId: 'apply',
                            fieldId: 'total',
                            line: j,
                            value: whTaxAmountTotalItem

                        });




                    }


                }




            }


			 log.debug('Debit Memo Record Id - End', recordId);

        }




        return {
            beforeSubmit: beforeSubmit

        };
    });