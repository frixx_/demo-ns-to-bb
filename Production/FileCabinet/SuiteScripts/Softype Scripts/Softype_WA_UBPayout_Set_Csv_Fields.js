/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author  : Farhan Shaikh
 **@Dated   : 26/09/2019   DD/MM/YYYY
 **@Version :         1.0
 **@Description :     Workflow Action script 
 ***************************************************************************************/
define(['N/record','N/search',"N/log","N/url","N/redirect","N/runtime"],    function(record,search,log,url,redirect,runtime){
    var jsonArray=[];
    var taxBaseAmount=0;
    var taxAmount=0;
    var wTaxCode;
    var wTaxForCsv = runtime.getCurrentScript().getParameter('custscript_wtax_for_csv');
    var wTaxDescription = runtime.getCurrentScript().getParameter('custscript_wtax_description');
    function setUbCsvFields(context){
        var currentRecordId = context.newRecord.id;
        log.audit('currentRecordId',currentRecordId);
        var billRecObj = context.newRecord;
        var lineCount=billRecObj.getLineCount('apply');
        log.debug('lineCount',lineCount);
        var flagForWTaxCode=true;

        for(var i=0;i<lineCount;i++){
            var sublistVendorBillValue = billRecObj.getSublistValue({
             sublistId: 'apply',
             fieldId: 'apply',
             line: i
            });
            //log.debug('sublistVendorBillValue',sublistVendorBillValue);
            
            var sublistTranType = billRecObj.getSublistValue({
             sublistId: 'apply',
             fieldId: 'trantype',
             line: i
            });
            //log.debug('sublistTranType',sublistTranType);
            if (sublistVendorBillValue==true && sublistTranType=='VendBill') {
                log.debug('Selected Vendor Bill Index',i);
                var sublistRefNum = billRecObj.getSublistValue({
                 sublistId: 'apply',
                 fieldId: 'refnum',
                 line: i
                });

                var sublistTotal = billRecObj.getSublistValue({
                 sublistId: 'apply',
                 fieldId: 'total',
                 line: i
                });

                if (sublistRefNum!='') {
                    var sublistJson={'refnum':sublistRefNum,'total':sublistTotal}
                    jsonArray.push(sublistJson);
                    //log.debug('sublistJson',sublistJson);
                }

                if (flagForWTaxCode) {
                    wTaxCode = billRecObj.getSublistValue({
                     sublistId: 'apply',
                     fieldId: 'CUSTBODY_WTAX_CODE_NAME',
                     line: i
                    });
                    log.emergency('wTaxCode',wTaxCode)
                    flagForWTaxCode=false;
                }

                var suplierInvoiceId = billRecObj.getSublistValue({
                 sublistId: 'apply',
                 fieldId: 'internalid',
                 line: i
                });

                var searchObj=search.load(wTaxForCsv); //customsearch_wa_supinv_wtax_ubpayout_csv

                searchObj.filters.push(search.createFilter({
                    name: 'internalid',
                    operator: 'is',
                    values: suplierInvoiceId
                }));

                var invoiceTaxDetails=searchObj.run().getRange({start:0,end:1000});

                log.debug('invoiceTaxDetails Record ',invoiceTaxDetails);


                for(var j=0;j<invoiceTaxDetails.length;j++){
                    var individualTaxBaseAmount=invoiceTaxDetails[j].getValue('custcol_4601_witaxbaseamount')
                    log.debug('individualTaxBaseAmount ',individualTaxBaseAmount);
    
                    taxBaseAmount+=Number(individualTaxBaseAmount);
    
                    var individualTaxAmount=invoiceTaxDetails[j].getValue('custcol_4601_witaxamount')
                    log.debug('individualTaxAmount ',individualTaxAmount);
    
                    individualTaxAmount=Math.abs(individualTaxAmount)
    
                    taxAmount+=Number(individualTaxAmount);
                }
                log.emergency('invoiceTaxDetails','Tax Code : '+wTaxCode+'Base Amount : '+individualTaxBaseAmount+' Tax Amount : '+individualTaxAmount)
            }
        }
        var stringToSetInRefField=createStringToSetInRef();
        log.audit('stringToSetInRefField',stringToSetInRefField);

        billRecObj.setValue('custbody_ub_payout_ref',stringToSetInRefField);

        if (wTaxCode!='' && wTaxCode!=undefined && wTaxCode!=null) {
            var firstMonth=billRecObj.getValue('custbody_ub_payout_first_month');
            var lastMonth=billRecObj.getValue('custbody_ub_payout_last_month');
            var quarter=billRecObj.getValue('custbody_ub_payout_month_quarter');
            log.audit('firstMonth',firstMonth)
            log.audit('lastMonth',lastMonth)
            log.audit('quarter',quarter)
    
            var stringToSetInATCField='';
            stringToSetInATCField+='`'+wTaxCode+'|'+'taxable='+taxBaseAmount+'|wtax='+taxAmount+'|';
            if (firstMonth) {
                stringToSetInATCField+=firstMonth+'|';
            }
    
            if (lastMonth) {
                stringToSetInATCField+=lastMonth+'|';
            }
    
            if (quarter) {
                stringToSetInATCField+=quarter+'|';
            }
    
            var searchObj=search.load(wTaxDescription); //customsearch_wa_wtax_descri_ubpayout_csv
    
            searchObj.filters.push(search.createFilter({
                name: 'custrecord_4601_wtc_name',
                operator: 'is',
                values: wTaxCode
            }));
            var taxDescription=searchObj.run().getRange({start:0,end:1});
            log.debug('taxDescription ',taxDescription);
    
            var description=taxDescription[0].getValue('custrecord_4601_wtc_description')
    
            stringToSetInATCField+=description;
            log.audit('stringToSetInATCField',stringToSetInATCField);
            billRecObj.setValue('custbody_ub_payout_atc_code',stringToSetInATCField)
        }
    }

    function createStringToSetInRef(){
        var str='';
        for(var i=0;i<jsonArray.length;i++){
            if (i!=jsonArray.length-1) {
                str+=jsonArray[i].refnum+'='+jsonArray[i].total+'|'
            }else{
                str+=jsonArray[i].refnum+'='+jsonArray[i].total
            }
        }
        return str;
    }

    return {
        onAction: setUbCsvFields
    };
});