/**

 * @NApiVersion 2.0

 * @NScriptType ClientScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 30/09/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : ClientScript for Not Letting User Save the Vendor Payment Record if invoices of different tax code is selected.  

 **************************************************************************************************************************************/
define(['N/currentRecord','N/search','N/runtime','N/record'],

function(currentRecord,search,runtime,record) {
    function saveRecord(context){
        //if (scriptExecutionFlag!=true) {return;}
        var currentRecordObj = currentRecord.get();
        var wTaxCode=currentRecordObj.getValue('custpage_defaultwitaxfield');
        if(wTaxCode!='' && wTaxCode!=undefined && wTaxCode!=null){
            currentRecordObj.setValue('custentity_adv_ub_wtax_code',true);
        }else{
            currentRecordObj.setValue('custentity_adv_ub_wtax_code',false);
        }
        //alert(wTaxCode)
        return true;
    }


    return {
        saveRecord: saveRecord
    };
});

