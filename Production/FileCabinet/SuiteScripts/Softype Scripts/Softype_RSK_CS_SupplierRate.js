/**
 *@NApiVersion 2.x
 *@NScriptType ClientScript
 */
/***************************************************************************************
** Copyright (c) 1998-2020 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Saroja Iyer
**@Dated : 07 February 2020
**@Version : 2.0
**@Description : Client Script deployed on Supplier Invoice for rounding off rate upto 3 decimal places.
***************************************************************************************************************/
define(['N/ui/dialog', 'N/record', 'N/search', 'N/runtime', 'N/url', 'N/https', 'N/log'],

    function(dialog, record, search, runtime, url, https) {
		
        //This function is to round off the rate upto 3 decimals
        function fieldchanged(context) {
			
			var newRecord = context.currentRecord;
            var sublistName = context.sublistId;
            var fieldId = context.fieldId;

				//This condition is for the change of the discount fields
				if(sublistName=='item' && fieldId == 'rate'){
					
					var rate =  newRecord.getCurrentSublistValue({
						sublistId:'item',fieldId:'rate'
					});
					if(rate){
						rate = rate.toFixed(3);

						newRecord.setCurrentSublistValue({
							sublistId:'item',fieldId:'rate',value:rate,ignoreFieldChange: true
						});
					}					
					
			}

		}  		

        return {
            fieldChanged: fieldchanged
        };
    });