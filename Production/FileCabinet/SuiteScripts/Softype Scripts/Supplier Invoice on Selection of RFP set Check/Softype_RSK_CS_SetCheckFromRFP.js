/**
	* @NApiVersion 2.x
	* @NScriptType ClientScript
	* @NModuleScope SameAccount
*/
/***************************************************************************************  
	** Copyright (c) 1998-2018 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
	**                      
	**@Author      :  Amol Jagkar
	**@Dated       :  28 January 2020	
	**@Version     :  2.x
	**@Description :  Set Check based on RFP
***************************************************************************************/
define(['N/currentRecord', 'N/record', 'N/url', 'N/search', 'N/https', 'N/format'],
	
    function(currentRecord, record, url, search, https, format) {
		
		/**
         * Validation function to be executed when sublist line is committed.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @param {string} scriptContext.sublistId - Sublist name
         *
         * @returns {boolean} Return true if sublist line is valid
         *
         * @since 2015.2
         */
		
		function fieldChanged(scriptContext){
			console.log("Field ID "+ scriptContext.fieldId);
			
			if(scriptContext.fieldId === "custbody_ref_rfp_num"){
				var objRecord = currentRecord.get();
				
				var requestForPayment = objRecord.getValue({
					fieldId: scriptContext.fieldId
				});
				
				var RFPRecord = record.load({type : "customrecord_rfp", id : requestForPayment});
			 
				console.log("Check "+ RFPRecord.getValue({fieldId : "custrecord_writecheck"}));

				objRecord.setValue({
					fieldId: "custbody_writechecklink",
					value: RFPRecord.getValue({fieldId : "custrecord_writecheck"})
				});
				
				// console.log("value "+ value);
			}else if(scriptContext.fieldId === "custrecord_rfp_number"){
				var objRecord = currentRecord.get();
				
				var requestForPayment = objRecord.getValue({
					fieldId: scriptContext.fieldId
				});
				
				console.log("requestForPayment "+ requestForPayment);
				if(requestForPayment.length != 0){
				
					var checkList = new Array();
					var rfpType = "";
				
					for(var i = 0; i < requestForPayment.length; i++){
						var RFPRecord = record.load({type : "customrecord_rfp", id : requestForPayment[i]});

						console.log("Check "+ RFPRecord.getValue({fieldId : "custrecord_writecheck"}));
						if(!!(RFPRecord.getValue({fieldId : "custrecord_writecheck"}))){
							
							if(rfpType != "" && rfpType != RFPRecord.getValue({fieldId : "custrecord_rfptype"})){
								
								alert("Please choose RFP's with same RFP Type");
								return;
							}
						
							checkList.push(RFPRecord.getValue({fieldId : "custrecord_writecheck"}));
							rfpType = RFPRecord.getValue({fieldId : "custrecord_rfptype"});
						}/* else{
							alert("No Checks are attached to " + RFPRecord.getValue({fieldId : "name"}));
						} */
					}
					
					objRecord.setValue({
						fieldId: "custrecord_write_chk_link",
						value: checkList
					});
					
					objRecord.setValue({
						fieldId: "custrecord_rfp_type",
						value: rfpType
					})
                }
			}
			
			return true;
		}
		
		return {
			fieldChanged: fieldChanged
		};
		
	});	