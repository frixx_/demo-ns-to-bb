/**
*@NApiVersion 2.x
*@NScriptType UserEventScript
**/

/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : On selection of Loan account on Supplier Invoice, create Loan JV & Vendor bill and Knock off bill with the JV created
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {
			
		var IMPORT_FORM = 139;
		var AUTOMATED_JV_FORM = 130;			
		var STANDARD_BILL_PAYMENT = 45;
		var LC_JV_JOURNAL = 5;
		
		var STATUS_APPROVED = 2;
				
		function beforeSubmit(context) {
		
			if(context.type == 'delete')
				return;
			
			var newRecordDetails = context.newRecord;
			// log.debug('newRecordDetails', newRecordDetails);
				
			var scriptObj = runtime.getCurrentScript();
			IMPORT_FORM = scriptObj.getParameter({name: 'custscript_si_loan_lc_jv_import_form'});
			AUTOMATED_JV_FORM = scriptObj.getParameter({name: 'custscript_si_loan_lc_jv_automated_jv'});
			STANDARD_BILL_PAYMENT = scriptObj.getParameter({name: 'custscript_si_loan_lc_jv_std_bill_paymnt'});
			LC_JV_JOURNAL = scriptObj.getParameter({name: 'custscript_si_loan_lc_jv_type'});
						
			var customform = newRecordDetails.getValue({
								 fieldId: 'customform'
							 });
							 
			var jvCreated = newRecordDetails.getValue({
								 fieldId: 'custbody_lc_jv_check'
							 });
			var loanAccount = newRecordDetails.getValue({
								 fieldId: 'custbody_loan_account'
							 });
			
			var department = newRecordDetails.getValue({
								fieldId: 'department'
							});		
			var division = newRecordDetails.getValue({
								fieldId: 'class'
							});	
			var status = newRecordDetails.getValue({
							 fieldId: 'approvalstatus'
						 });
						 
			log.debug('status', status);
						 						 
			if(status != STATUS_APPROVED){
				return;
			}
							 
			log.debug('jvCreated', jvCreated);
			log.debug('loanAccount', loanAccount);
			log.debug('customform', customform == IMPORT_FORM);							 
							 
			if(!jvCreated && !!(loanAccount) && customform == IMPORT_FORM){
				
				var recordid = CreateJV(newRecordDetails, department, division, newRecordDetails.id);
				log.debug('JV recordid', recordid);	
				
				newRecordDetails.setValue({
					fieldId: "custbody_lc_jv_link",
					value: recordid,				
					ignoreFieldChange: true
				});
				newRecordDetails.setValue({
					fieldId: "custbody_lc_jv_check",
					value: true,				
					ignoreFieldChange: true
				});
			}
		}
		
		function CreateJV(newRecordDetails, department, division, billID){
			
			var newJVRecord = record.create({
				type: "journalentry",
				isDynamic: false
			});	
			
			log.debug('newRecordDetails.subsidiary', newRecordDetails.getValue({
														 fieldId: 'subsidiary'
													 }));
			log.debug('Bill ID', billID);
			
			newJVRecord.setValue({
				fieldId: "subsidiary",
				value: 	newRecordDetails.getValue({
							fieldId: 'subsidiary'
						})
			});
			
			newJVRecord.setValue({
				fieldId: "customform",
				value: AUTOMATED_JV_FORM
			});
			
			newJVRecord.setValue({
				fieldId: "custbody_journaltypes",
				value: LC_JV_JOURNAL
			});			
			
			newJVRecord.setValue({
				fieldId: "approvalstatus",
				value: STATUS_APPROVED
			});			
			
			newJVRecord.setValue({
				fieldId: "custbody_createdfrombill",
				value: billID || newRecordDetails.id
			});
			
			newJVRecord.setValue({
				fieldId: "currency",
				value: newRecordDetails.getValue({
										fieldId: 'currency'
									})
			});
			newJVRecord.setValue({
				fieldId: "exchangerate",
				value: newRecordDetails.getValue({
										fieldId: 'exchangerate'
									})
			});
			newJVRecord.setValue({
				fieldId: "custbody_billoflasdingnum",
				value: newRecordDetails.getValue({
										fieldId: 'custbody_billoflasdingnum'
									})
			});
			/* newJVRecord.setValue({
				fieldId: "department",
				value: newRecordDetails.getValue({
										fieldId: 'department'
									})
			});
			newJVRecord.setValue({
				fieldId: "class",
				value: newRecordDetails.getValue({
										fieldId: 'class'
									})
			}); */
			newJVRecord.setValue({
				fieldId: "location",
				value: newRecordDetails.getValue({
										fieldId: 'location'
									})
			});
			newJVRecord.setValue({
				fieldId: "custbody_ship_num",
				value: newRecordDetails.getValue({
										fieldId: 'custbody_ship_num'
									})
			});
			newJVRecord.setValue({
				fieldId: "custbody_letterofcreditnum",
				value: newRecordDetails.getValue({
										fieldId: 'custbody_letterofcreditnum'
									})
			});
			newJVRecord.setValue({
				fieldId: "cseg_type_of_trans",
				value: newRecordDetails.getValue({
										fieldId: 'cseg_type_of_trans'
									})
			});
			newJVRecord.setValue({
				fieldId: "custbody_bill_rfptype",
				value: newRecordDetails.getValue({
							 fieldId: 'custbody_bill_rfptype'
						 })
			});
			
			/* newJVRecord.setText({
				fieldId: "currency",
				value: newRecordDetails.currency
			}); */
			
			newJVRecord.setValue({
				fieldId: "custbody_createdfrombill",
				value: billID
			});
			
			var debitPayee = newRecordDetails.getValue({
								 fieldId: 'entity'
							 });
			var debitAccount = newRecordDetails.getValue({
								 fieldId: 'account'
							 });
			var debitAmount = newRecordDetails.getValue({
								 fieldId: 'usertotal'
							 });
			
			log.debug('debitPayee',debitPayee);
			log.debug('debitAccount',debitAccount);
			log.debug('debitAmount',debitAmount);
			
			//Debit - Supplier Invoice
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'account',
				line: 0,
				value: debitAccount
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'entity',
				line: 0,
				value: Number(debitPayee)
			});							
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'debit',
				line: 0,
				value: debitAmount
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'credit',
				line: 0,
				value: 0
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'department',
				line: 0,
				value: department
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'class',
				line: 0,
				value: division
			});
			
			var creditAccount = newRecordDetails.getValue({
								 fieldId: 'custbody_loan_account'
							 });
			var creditAmount = debitAmount; 
			var creditPayee = (search.lookupFields({
				type: "customrecord_lc_details_record",
				id: newRecordDetails.getValue({
								 fieldId: 'custbody_letterofcreditnum'
							 }),
				columns: ['custrecord_lc_bank']
			}))["custrecord_lc_bank"][0].value;
			 /*var creditAmount = recordCheck.getValue({
								 fieldId: 'usertotal'
							 }); */
			
			
			log.debug('creditAccount',creditAccount);
			log.debug('creditAmount',creditAmount);
			log.debug('creditPayee',creditPayee);
			
			//Credit - Write Check
			
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'entity',
				line: 1,
				value: Number(creditPayee)
			});	
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'account',
				line: 1,
				value: creditAccount
			});				
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'debit',
				line: 1,
				value: 0
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'credit',
				line: 1,
				value: creditAmount
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'department',
				line: 1,
				value: department
			});
			newJVRecord.setSublistValue({
				sublistId: 'line',
				fieldId: 'class',
				line: 1,
				value: division
			});
			
			var recordid = newJVRecord.save();
			log.debug("recordid JV",recordid);
			
			return recordid;
		}

		function afterSubmit(context) {
			if(context.type == 'delete')
				return;
				
			var scriptObj = runtime.getCurrentScript();
			IMPORT_FORM = scriptObj.getParameter({name: 'custscript_si_loan_lc_jv_import_form'});
			AUTOMATED_JV_FORM = scriptObj.getParameter({name: 'custscript_si_loan_lc_jv_automated_jv'});
			STANDARD_BILL_PAYMENT = scriptObj.getParameter({name: 'custscript_si_loan_lc_jv_std_bill_paymnt'});
			
				
			var newRecordDetails = context.newRecord;
			// log.debug('newRecordDetails.id', newRecordDetails.id);
			
			var billID = newRecordDetails.id;			
			log.debug("billId", billID);
			
			var newRecordDetails = record.load({type:"vendorbill",id:newRecordDetails.id});
			
			// log.debug('newRecordDetails', newRecordDetails);
			
			var customform = newRecordDetails.getValue({
				fieldId: 'customform'
			});		
			
			var status = newRecordDetails.getValue({
							 fieldId: 'status'
						 });
						 
			var location = newRecordDetails.getValue({
								fieldId: 'location'
							});		
			var department = newRecordDetails.getValue({
								fieldId: 'department'
							});		
			var division = newRecordDetails.getValue({
								fieldId: 'class'
							});		
			// log.debug('status', status);
			
			var doc = newRecordDetails.getValue({
				fieldId: 'custbody_lc_jv_link'
			});
						 
			log.debug('doc', doc);
			
			if(doc == null || doc == undefined){
				return;
			}
						
						var loanAccount = newRecordDetails.getValue({
								 fieldId: 'custbody_loan_account'
							 });
			log.debug('loanAccount', loanAccount);
					
			try{
			
				if(status === "Open" && (loanAccount != "") && (customform == IMPORT_FORM)){
				
					var billRecord = record.transform({
						fromType: record.Type.VENDOR_BILL,
						fromId: newRecordDetails.id,
						toType: record.Type.VENDOR_PAYMENT,
						isDynamic: true
					});
					
					billRecord.setValue({
						fieldId: 'customform',
						value: STANDARD_BILL_PAYMENT
					});
					
					billRecord.setValue({
						fieldId: "currency",
						value: newRecordDetails.getValue({
												fieldId: 'currency'
											})
					});
					billRecord.setValue({
						fieldId: "exchangerate",
						value: newRecordDetails.getValue({
												fieldId: 'exchangerate'
											})
					});
					billRecord.setValue({
						fieldId: "cseg_type_of_trans",
						value: newRecordDetails.getValue({
												fieldId: 'cseg_type_of_trans'
											})
					});
					billRecord.setValue({
						fieldId: 'location',
						value: location
					});
					
					billRecord.setValue({
						fieldId: 'department',
						value: department
					});
					
					billRecord.setValue({
						fieldId: 'class',
						value: division
					});
					
					
					log.debug('Bill Creation', billRecord.getValue({
													fieldId: "currency"
												}));
					
					var numLines = billRecord.getLineCount({
						sublistId: 'apply'
					});
					log.debug("Numlines", numLines);
										
					for(var i = 0; i < numLines; i++){
						
						var lineNum = billRecord.selectLine({sublistId: 'apply', line: i});
						
						var docFromLine = billRecord.getSublistValue({
							sublistId: 'apply',
							fieldId: 'doc',
							line: i
						});
						
						log.debug("Doc "+i, docFromLine);
												
						if(docFromLine === doc){
							log.debug("Found JV");
							billRecord.setCurrentSublistValue({
								sublistId: 'apply',
								fieldId: 'apply',
								line: i,
								value: true
							});
							billRecord.commitLine({sublistId: 'apply'});
						}else if(Number(docFromLine) == Number(billID)){
							log.debug("Found Invoice");
							billRecord.setCurrentSublistValue({
								sublistId: 'apply',
								fieldId: 'apply',
								line: i,
								value: true
							});
							billRecord.setCurrentSublistValue({
								sublistId: 'apply',
								fieldId: 'department',
								line: i,
								value: department
							});
							billRecord.setCurrentSublistValue({
								sublistId: 'apply',
								fieldId: 'class',
								line: i,
								value: division
							});
							billRecord.commitLine({sublistId: 'apply'});
						}
						
					}

					
					var recordid = billRecord.save({
						enableSourcing: true,
						ignoreMandatoryFields: true
					});
					
					log.debug("billRecord", recordid);
				}			
			}catch(err){
				log.debug("Error", err);
			}		
		}
	
		return {
			beforeSubmit: beforeSubmit,
			afterSubmit: afterSubmit
		};
	});