/**
*@NApiVersion 2.x
*@NScriptType UserEventScript
**/

/***************************************************************************************
** Copyright (c) 1998-2019 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West,
Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. (&quot;Confidential
Information&quot;).
**You shall not disclose such Confidential Information and shall use it only in accordance with
the terms of the license agreement you entered into with Softype.
**
**@Author : Amol 
**@Dated : 23 September 2019
**@Version : 2.0
**@Description : Call RSK API for Supplier Master
***************************************************************************************/


define(['N/http', 'N/https', 'N/search', 'N/record', 'N/runtime', 'N/log'],
	function(http, https, search, record, runtime, log) {
	
		var subsidiaries = new Array();
			
		function afterSubmit(context) {
			
			/* log.debug('runtime',runtime);
			log.debug('runtime.executionContext',runtime.executionContext);
			log.debug('context',context); */
			
			var newRecordDetails = context.newRecord;
			// log.debug('newRecordDetails', newRecordDetails);
			
			var subsidiary = newRecordDetails.getValue({
								fieldId: 'subsidiary'
							});
			try{
				var scriptObj = runtime.getCurrentScript();
				var createAPI = scriptObj.getParameter({name: 'custscript_create_api_supplier'});
				var updateAPI = scriptObj.getParameter({name: 'custscript_update_api_supplier'});
							
				/**********************************************************
				 *************************API CALL************************
				**********************************************************/

				var headers = {
						"Content-Type": "application/json"
				};	//Headers if Any
				var response;
				
				var rskID = newRecordDetails.getValue({fieldId: 'custentity_rsksuppleirid'});
				
				subsidiary = newRecordDetails.getSublistValue({sublistId: "submachine", fieldId: 'subsidiary', line: 0});
				
				log.debug("subsidiary", subsidiary);
				var subsidiaryName = (search.lookupFields({
					type: "subsidiary",
					id: subsidiary,
					columns: ['name']
				}))['name'];
				
				var isInActive = newRecordDetails.getValue({fieldId: 'isinactive'});				
				if(context.type == 'create' || rskID == ""){
				
					var jsonBody = {
						"NsId":newRecordDetails.id,
						"Name":	newRecordDetails.getValue({
									fieldId: 'companyname'
								}),
						"CompanyId": 1, /* newRecordDetails.getValue({
														fieldId: 'entityid'
													}), */
						"Subsidiary" : subsidiaryName,
						"Address" : newRecordDetails.getSublistValue({
										sublistId: 'addressbook',
										fieldId: 'addressbookaddress_text',
										line: 0
									}),
						"LegalName" : newRecordDetails.getValue({
											fieldId: 'legalname'
										}),
						"Tin" : newRecordDetails.getValue({
											fieldId: 'vatregnumber'
										}),
						"Terms" : newRecordDetails.getValue({
											fieldId: 'terms'
										}),
						"WithHoldingTax" : Number(newRecordDetails.getValue({
											fieldId: 'custentity_4601_defaultwitaxcode'
										})),
						"IsActive":!(isInActive)
					};
							
							
					log.debug("jsonBody - Create", jsonBody);		
									
					// If the API is https compliant
					response = http.request({
						method: https.Method.POST,
						url: createAPI,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					
					SetRSKID(newRecordDetails, response, true);
				}else if(context.type == 'edit'){
					
					var jsonBody = 	{
						"NsId":newRecordDetails.id,
						"Id":Number(newRecordDetails.getValue({
														fieldId: 'custentity_rsksuppleirid'
													})),
						"Name":newRecordDetails.getValue({
														fieldId: 'companyname'
													}),
						"CompanyId": 1, /* newRecordDetails.getValue({
														fieldId: 'entityid'
													}), */
						"Subsidiary" : subsidiaryName,
						"Address" : newRecordDetails.getSublistValue({
										sublistId: 'addressbook',
										fieldId: 'addressbookaddress_text',
										line: 0
									}),
						"LegalName" : newRecordDetails.getValue({
											fieldId: 'legalname'
										}),
						"Tin" : newRecordDetails.getValue({
											fieldId: 'vatregnumber'
										}),
						"Terms" : newRecordDetails.getValue({
											fieldId: 'terms'
										}),
						"WithHoldingTax" : Number(newRecordDetails.getValue({
											fieldId: 'custentity_4601_defaultwitaxcode'
										})),
						"IsActive":!(isInActive)
					};
							
					log.debug("jsonBody - Edit", jsonBody);		
							
					response = http.request({
						method: https.Method.PUT,
						url: updateAPI,
						body: JSON.stringify(jsonBody),
						headers: headers
					});
					
					SetRSKID(newRecordDetails, response, true);
				}
			}catch(err){
				log.debug("Error", err);
				SetFlag(newRecordDetails, 400, "Server Error");
				return;
			}
		}
		
		function SetRSKID(newRecordDetails, response, flag){
			
			var data = response.body; 
			var dataCode = response.code; 
			log.debug("data",data);
			log.debug("dataCode",dataCode);
			
			if(dataCode == "200"){
				if(typeof data === "string")
					data = JSON.parse(data);
					
				/* newRecordDetails.setValue({
						fieldId: "custentity_itemmasterflag",	//Flag Checkbox Field
						value: false,				
						ignoreFieldChange: true
					});
					
				if(flag){
					newRecordDetails.setValue({
						fieldId: "custentity_rsksuppleirid",
						value: data.Id,				
						ignoreFieldChange: true
					});
				} */
				
				
				//return;
			}else{
				SetFlag(newRecordDetails, dataCode, data.message);
			}
			
			var subsidiary = newRecordDetails.getValue({
								fieldId: 'subsidiary'
							});
			log.debug("subsidiary", subsidiary);
						
			
			GetSubsidiaries(subsidiary);
				
			var rec = record.load({
				type: "vendor",
				id: newRecordDetails.id
			});
			
			var subsidiaryCount = rec.getLineCount({
				sublistId: 'submachine'
			});
			
			//log.debug("subsidiaries", subsidiaries);
			if(subsidiaryCount == subsidiaries.length){
				return;
			}
			
			var counter = -1;
			var currentSubsidiaries = new Array();
			
			for(var j = 0; j < subsidiaryCount; j++){
				currentSubsidiaries.push(rec.getSublistValue({
					fieldId: "subsidiary",
					sublistId: "submachine",
					line: j
				}));
				counter++;
			}
			
			
			for(var i = 0; i < subsidiaries.length; i++){
				var flag = true;
				for(var k = 0; k < currentSubsidiaries.length; k++){
					if(subsidiaries[i] == currentSubsidiaries[k]){
						flag = false;
					}
				}
				if(flag){
					counter++;
					var subsidiary = rec.setSublistValue({
						fieldId: "subsidiary",
						sublistId: "submachine",
						value: subsidiaries[i],
						line: counter
					});
				}
			
			}
			
			rec.save();
		}
		
		function GetSubsidiaries(subsidiary){
		
			// log.debug("calling GetSubsidiaries");
			var subsidiarySearchObj = search.create({
				   type: "subsidiary",
				   filters:
				   [
					  ["parent","equalto", subsidiary]
				   ],
				   columns:
				   [
					  search.createColumn({name: "internalid", label: "Internal ID"})
				   ]
				}).run().getRange(0,1000);
			//log.debug("subsidiarySearchObj", JSON.stringify(subsidiarySearchObj));
				
			for(var z = 0; z < subsidiarySearchObj.length; z++){
				//log.debug("Adding Subsidiary", subsidiarySearchObj[z].getValue("internalid"));
				subsidiaries.push(subsidiarySearchObj[z].getValue("internalid"));
				GetSubsidiaries(subsidiarySearchObj[z].getValue("internalid"));
			}
			
		}
		
		function SetFlag(newRecordDetails, dataCode, message){
			
			var rec = record.load({type : newRecordDetails.type, id : newRecordDetails.id});

			rec.setValue({
				fieldId: "custentity_itemmasterflag",	//Flag Checkbox Field
				value: true,				
				ignoreFieldChange: true
			});
			rec.setValue({
				fieldId: "custentity_itemmaster_remakrs",	//Flag Remark Field
				value: dataCode + " : " + message,				
				ignoreFieldChange: true
			});
			rec.save();
			
		}
		
		return {
			afterSubmit: afterSubmit
		};
	});