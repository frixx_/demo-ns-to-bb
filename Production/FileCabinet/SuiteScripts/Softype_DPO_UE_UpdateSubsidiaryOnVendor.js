/* **************************************************************************************  
	 ** Copyright (c) 1998-2014 Softype, Inc.                                 
	 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
	 ** All Rights Reserved.                                                    
	 **                                                                         
	 ** This software is the confidential and proprietary information of          
	 ** Softype, Inc. ("Confidential Information"). You shall not               
	 ** disclose such Confidential Information and shall use it only in          
	 ** accordance with the terms of the license agreement you entered into    
	 ** with Softype.                       
	 ** @author:  Nihal Mulani
	 ** @version: 1.0
	 ** Description:
	 ***************************************************************************************/
	var merchantCategoryId = 1;
	function afterSubmit_AddSubsidiary(type)
	{
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		
		  var context = nlapiGetContext().getExecutionContext();
	      nlapiLogExecution("DEBUG","context",context);
       
	  	if((type=='create' || type== 'CREATE'|| type=='edit' || type== 'EDIT') && (context == 'userinterface' || context == 'USERINTERFACE'|| context == 'csv' || context == 'CSV'))	  

		{
			var All_Sub = new Array();
			var columns = new Array();
			columns.push(new nlobjSearchColumn('name'));
          //Added by Sarah on 26th May 2017
          var filter = new Array();
              filter.push(new nlobjSearchFilter('iselimination',null,'is','F'));
          filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));
			var srchSubsidary = nlapiSearchRecord('subsidiary', null, filter, columns);
			for (var i = 0; i < srchSubsidary.length; i++) 
			{
				var ID = srchSubsidary[i].getId();
				All_Sub.push(ID);
			}
	
			var current_Merchant = nlapiLoadRecord(recType,recId);
			// added by sachin
			var categoryId = current_Merchant.getFieldValue('category');
            var subsidiary = Number(current_Merchant.getFieldValue('subsidiary'));
			//---------------//
			// if(categoryId == merchantCategoryId)
            {
						
				for (var j = 0; j < All_Sub.length; j++) 
				{
                    if(subsidiary !=  Number(All_Sub[j])){
                      
                      current_Merchant.selectNewLineItem('submachine'); 
                      current_Merchant.setCurrentLineItemValue('submachine', 'subsidiary', All_Sub[j]); 
                      current_Merchant.commitLineItem('submachine');
                      
                    }
					
				}

				var submit_vendor= nlapiSubmitRecord(current_Merchant,true);

			}
			
			
		//	nlapiLogExecution("DEBUG","Vendor ID",submit_vendor)
		}
	
	}