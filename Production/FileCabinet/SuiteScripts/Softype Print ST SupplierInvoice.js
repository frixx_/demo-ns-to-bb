/**
	* @NApiVersion 2.x
	* @NScriptType Suitelet
	* @NModuleScope SameAccount
*/

/***************************************************************************************
	** Copyright (c) 1998-2018 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	**
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
	** the license agreement you entered into with Softype.
	**
	** @Author     :  Meena
	** @Dated       :  11 Dec 2019
	** @Version     :  2.0
	** @Description :  Suitelet to print the data from Bill Payment record after the userevent redirects here.
	
***************************************************************************************/

/**
	* @param {nlobjRequest} request Request object
	* @param {nlobjResponse} response Response object
	* @returns {Void} Any output is written via response object
*/
define(['N/format','N/runtime','N/record', 'N/xml', 'N/render', 'N/http', 'N/format', 'N/search'],
	function(format,runtime,record, xml, render, http, format, search) {
		function onRequest(context) {
			/* var ITEM = "INPUT:UNDEF-PH"; */
			var ITEM = "Withholding Tax Payable - Expanded (WE_PH)";
			
			if (context.request.method == 'GET') {
				// Getting the parameters from Userevent.
				var action = context.request.parameters.action;
				var recordid = context.request.parameters.recordid;
				
				var loadrec = record.load({
					type: 'vendorbill',
					id: recordid
				});
				
				var subsidiary = loadrec.getText({
					fieldId: 'subsidiary'
				});
				var transactionNumber = loadrec.getText({
					fieldId: 'transactionnumber'
				});
				var vendorName = loadrec.getText({
					fieldId: 'entity'
				});
				
				
				var invoiceNo = loadrec.getText({
					fieldId: 'tranid'
				});
				var invoiceDate = loadrec.getText({
					fieldId: 'trandate'
				});
				var referencePo = loadrec.getText({
					fieldId: 'custbody_print_ref_po'
				});
				var description = loadrec.getText({
					fieldId: 'memo'
				});
				var createdDate = loadrec.getText({
					fieldId: 'createddate'
				});
				
				createdDate = (createdDate.split(" "))[0];
				//log.audit('createdDate',new Date(createdDate.formatDate("d/m/yy")));
				
				// createDate = formatDate(createdDate);
				
				log.debug('createDate',createdDate);
				//createdDate = new Date(createdDate).formatDate("d/m/yy");
				
				var terms = loadrec.getText({
					fieldId: 'terms'
				});
				var rRDate = loadrec.getText({
					fieldId: 'custbody_print_rr_date'
				});
				var rRNumber = loadrec.getText({
					fieldId: 'custbody_si_rr_no'
				});
				var payableAmount = loadrec.getText({
					fieldId: 'usertotal'
				});
				var atc = loadrec.getText({
					fieldId: 'custbody_print_wtax_code'
				});
				var dueDate = loadrec.getText({
					fieldId: 'duedate'
				});
				var counterDate = loadrec.getText({
					fieldId: 'custbody_counter_date'
				});
				var counterReceiptNo = loadrec.getText({
					fieldId: 'custbody_ctr_rpt_num'
				});
				var preparedBy = loadrec.getText({
					fieldId: 'custbody_prepared_by'
				});
				var approvedBy = loadrec.getText({
					fieldId: 'custbody_req_appr_by'
				});
				var dateAndTimeCreated = loadrec.getText({
					fieldId: 'custbody_date_time_create'
				});
				var postingPeriod = loadrec.getText({
					fieldId: 'postingperiod'
				});
				var taxCode = loadrec.getText({
					fieldId: 'custbody_print_tax_code'
				});
				var trade = loadrec.getText({
					fieldId: 'custbody_tradenontrade'
				});
				
				var userRole = runtime.getCurrentUser();
				log.audit('userRole',userRole.roleId);
				
				
				
				
				// Creating the htmlvar file to displayed in the suitelet.
				htmlvar = '';
				htmlvar += '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
				htmlvar += '<pdf>\n'
				htmlvar += '<head>';
				
				
				
				
				htmlvar += '<macrolist>';
				
				htmlvar += '<macro id="nlheader" height ="auto">';
				htmlvar += '<table align="center">';
				htmlvar += '<tr>';
				htmlvar += '<td colspan = "6" align="center" padding-bottom="1" font-size="18px"><b>' + xml.escape(subsidiary.toUpperCase()) + '</b></td>';
				htmlvar += '</tr>';
				htmlvar += '<tr>';
				htmlvar += '<td colspan = "6" align="center" font-size="18px"><b>ACCOUNTS PAYABLE VOUCHER</b></td>';
				htmlvar += '</tr>';
				htmlvar += '</table>';
				
				htmlvar += '<table cellspacing="0" width="235px" float="left">';
				htmlvar += '<tr>';
				htmlvar += '<td width="100px" style="font-size:10px"><p align="left">APV No:</p></td>';
				htmlvar += '<td width="180px" align="left" style="font-size:10px">' + xml.escape(transactionNumber) + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td align="left" width="100px" style="font-size:10px"><p align="left">APV Date:</p></td>';
				htmlvar += '<td width="180px" style="font-size:10px">' + xml.escape(createdDate) + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="100px" style="font-size:10px"><p align="left">Vendor Name:</p></td>';
				htmlvar += '<td width="200px" style="font-size:10px"><p align="left">' + xml.escape(vendorName) + '</p></td></tr>';
				
				htmlvar += '<tr>';
				htmlvar += '<td width="100px" align="left" style="font-size:10px"><p align="left">Invoice No:</p></td>';
				htmlvar += '<td width="200px" align="left" style="font-size:10px; word-wrap: break-word;">' + xml.escape(invoiceNo) + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="100px" align="left" style="font-size:10px"><p align="left">Invoice Date:</p></td>';
				htmlvar += '<td width="180px" align="left" style="font-size:10px">' + xml.escape(invoiceDate) + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="130px" align="left" style="font-size:10px"><p align="left">Payable Amount:</p></td>';
				htmlvar += '<td width="170px" align="left" style="font-size:10px">' + xml.escape(payableAmount) + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="130px" align="left" style="font-size:10px"><p align="left">Posting Period:</p></td>';
				htmlvar += '<td width="180px" align="left" style="font-size:10px">' + xml.escape(postingPeriod) + '</td></tr>';
				htmlvar += '</table>';
				
				
				htmlvar += '<table float="center" width="265px">';
				htmlvar += '<tr>';
				htmlvar += '<td width="50px" align="left" style="font-size:10px">Terms:&nbsp;</td>';
				
				if(terms.length > 23){
					terms = terms.concat("...");
				}
				htmlvar += '<td width="210px" align="left" style="font-size:10px"><p align="left">' + xml.escape(terms == '' ? '' : terms.substring(0, 25)) + '</p></td></tr>';
				
				
				
				var poNumberArray = [];
				var poAmountArray = '';
				var amt;
				
				
				var customform = loadrec.getValue({
					fieldId: 'customform'
				});
				
				if (customform == 126) {
					
					var getReferencePo = loadrec.getValue({
						fieldId: 'custbody_print_ref_po'
					});
					
					for (var i = 0; i < getReferencePo.length; i++) {
						
						
						var poNumber = search.lookupFields({
							type: 'purchaseorder',
							id: getReferencePo[i],
							columns: ['tranid', 'custbody_po_total_php']
						});
						poNumberArray.push(poNumber.tranid);
						if (poAmountArray != '') {
							amt = poNumber.custbody_po_total_php;
							amt = amt.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
							poAmountArray = poAmountArray + '; ' + amt;
	                        } else {
							var amt = poNumber.custbody_po_total_php;
							amt = amt.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
							poAmountArray = amt;
						}
						
					}
					
					htmlvar += '<tr>';
					htmlvar += '<td width="100px" align="left" style="font-size:10px">PO No:&nbsp;</td>';
					htmlvar += '<td width="100px" align="left" style="font-size:10px">' + poNumberArray.toString() + '</td></tr>';
					htmlvar += '<tr>';
					htmlvar += '<td width="110px" align="left" style="font-size:10px">PO Amount:&nbsp;</td>';
					htmlvar += '<td width="100px" align="left" style="font-size:10px">' + xml.escape(poAmountArray) + '</td>';
					htmlvar += '</tr>';
					
				}
				
				
				htmlvar += '<tr>';
				htmlvar += '<td width="150px" align="left" style="font-size:10px">Trade/ Non-Trade:&nbsp;</td>';
				htmlvar += '<td width="180px" align="left" style="font-size:10px">' + trade + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="160px" align="left" style="font-size:10px">Tax Code:&nbsp;</td>';
				
				htmlvar += '<td width="220px" align="left" style="font-size:10px"><p align="left">' + xml.escape(taxCode) + '</p></td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="100px" align="left" style="font-size:10px"><p align="left">Description:</p>&nbsp;</td>';
				
				if(description.length > 100){
					description = description.concat("...");
				}
				
				htmlvar += '<td width="230px" align="left" style="font-size:10px;"><p align="left">' + xml.escape(description == '' ? '' : description.substring(0, 100)) + '</p></td>';
				htmlvar += '</tr>';
				htmlvar += '</table>';
				
				htmlvar += '<table float="right" width="206px">';
				htmlvar += '<tr>';
				htmlvar += '<td width="250px" align="left" style="font-size:10px"><p align="left">DUE Date:</p></td>';
				htmlvar += '<td width="200px" style="font-size:10px">' + xml.escape(dueDate) + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="250px" align="left" style="font-size:10px"><p align="left">CR No:</p></td>';
				htmlvar += '<td width="250px" style="font-size:10px">' + xml.escape(counterReceiptNo) + '</td></tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="250px" align="left" style="font-size:10px"><p align="left">Counter Date:</p></td>';
				htmlvar += '<td width="250px" style="font-size:10px">' + xml.escape(counterDate) + '</td>';
				htmlvar += '</tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="250px" align="left" style="font-size:10px"><p align="left">RR No.:</p></td>';
				htmlvar += '<td width="250px" style="font-size:10px">' + rRNumber + '</td>';
				htmlvar += '</tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="250px" align="left" style="font-size:10px"><p align="left">RR Date:</p></td>';
				htmlvar += '<td width="250px" style="font-size:10px"><p align="left">' + xml.escape(rRDate) + '</p></td>';
				htmlvar += '</tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="250px" align="left" style="font-size:10px"><p align="left">ATC:</p></td>';
				htmlvar += '<td width="300px" align="left" style="font-size:10px"><p align="left">' + (atc == undefined ? '' : atc) + '</p></td></tr>';
				htmlvar += '</table>';
				
				
				htmlvar += '</macro>';
				
				htmlvar += '<macro id="nlfooter">';
				
				htmlvar += '</macro>';
				htmlvar += '</macrolist>';
				
				htmlvar += '</head>';
				
				htmlvar += '<body header="nlheader"  header-height="43%"   footer="nlfooter" footer-height="" padding="0.0in 0.3in 0.5in 0.3in" width="21.50cm" height="14.00cm">';
				
				htmlvar += '<table width="100%" border="1">';
				htmlvar += '<tr>';
				htmlvar += '<th border-right="1" width="4%" border-bottom="1" align="center" style="font-size:10px"></th>';
				htmlvar += '<th border-right="1" width="37%" border-bottom="1" align="center" style="font-size:10px"><b>Account Code</b></th>';
				htmlvar += '<th border-right="1" width="17%" border-bottom="1" align="center" style="font-size:10px"><b>Department</b></th>';
				htmlvar += '<th border-right="1" width="15%" border-bottom="1" align="center" style="font-size:10px"><b>Division</b></th>';
				
				htmlvar += '<th border-right="1" width="13%" border-bottom="1" align="center" style="font-size:10px"><b>Dr Amount</b></th>';
				htmlvar += '<th align="center" width="13%" border-bottom="1" style="font-size:10px"><b>Cr Amount</b></th>';
				htmlvar += '</tr>';
				
				
				
				
				var GL_search = search.create({
					type: "transaction",
					filters: [
						["internalid", "anyof", recordid]
					],
					columns: [
						search.createColumn({
							name: "account",
							summary: "GROUP",
							label: "Account"
						}),
						search.createColumn({
							name: "creditamount",
							summary: "SUM",
							label: "Amount (Credit)"
						}),
						search.createColumn({
							name: "debitamount",
							summary: "SUM",
							label: "Amount (Debit)"
						}),
						search.createColumn({
							name: "department",
							summary: "GROUP",
							label: "Department"
						}),
						search.createColumn({
							name: "class",
							summary: "GROUP",
							label: "Division"
						}),
						search.createColumn({
							name: "line.cseg_rsk_expenseseg",
							summary: "GROUP",
							label: "RSK Expense Seg"
						})
					]
				}).run().getRange(0, 1000);;
				
				
				var number = 0;
				var debitAmountTotal = 0;
				var creditAmountTotal = 0;
				var srNumArray = [];
				for (var y = 0; y < GL_search.length; y++) {
					
					var accountName = GL_search[y].getText({
						"name": "account",
						"summary": search.Summary.GROUP
					});
					
					var accountCode = accountName.split(" ");
					var code = accountCode[0];
					var accountStatement = accountName.split(":");
					accountStatement = accountStatement[accountStatement.length - 1];
					var account = code + accountStatement;
					
					
					var debitAmount = GL_search[y].getValue({
						"name": "debitamount",
						"summary": search.Summary.SUM
					});
					var creditAmount = GL_search[y].getValue({
						"name": "creditamount",
						"summary": search.Summary.SUM
					});
					
					
					var department = GL_search[y].getText({
						"name": "department",
						"summary": search.Summary.GROUP
					});
					
					
					
					var division = GL_search[y].getText({
						"name": "class",
						"summary": search.Summary.GROUP
					});
					
					
					
					var rskSegment = GL_search[y].getText({
						"name": "line.cseg_rsk_expenseseg",
						"summary": search.Summary.GROUP
					});
					
					
					if (debitAmount != '' || creditAmount != '') {
						if (debitAmount) {
							number = number + 1;
							
							var numItem = loadrec.getLineCount({
								sublistId: 'item'
							});
							
							
							htmlvar += '<tr>';
							htmlvar += '<td border-right="1" style="font-size:10px">' + number + '</td>';
							htmlvar += '<td border-right="1" style="font-size:11px"><p align="left">' + xml.escape(rskSegment == "- None -" ? account : rskSegment) + '</p></td>';
							htmlvar += '<td border-right="1" style="font-size:11px"><p align="left">' + xml.escape(department) + '</p></td>';
							htmlvar += '<td border-right="1" style="font-size:10px"><p align="left">' + xml.escape(division) + '</p></td>';
							htmlvar += '<td border-right="1" style="font-size:10px" align="right">' + xml.escape(debitAmount).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</td>';
							htmlvar += '</tr>';
							
							debitAmount = parseFloat(debitAmount);
							debitAmountTotal = debitAmountTotal + debitAmount;
							
						}
					}
				}
				
				
				
				for (var z = 0; z < GL_search.length; z++) {
					
					var accountName = GL_search[z].getText({
						"name": "account",
						"summary": search.Summary.GROUP
					});
					
					var accountCode = accountName.split(" ");
					var code = accountCode[0];
					var accountStatement = accountName.split(":");
					accountStatement = accountStatement[accountStatement.length - 1];
					var account = code + accountStatement;
					
					
					var debitAmount = GL_search[z].getValue({
						"name": "debitamount",
						"summary": search.Summary.SUM
					});
					var creditAmount = GL_search[z].getValue({
						"name": "creditamount",
						"summary": search.Summary.SUM
					});
					
					
					var department = GL_search[z].getText({
						"name": "department",
						"summary": search.Summary.GROUP
					});
					
					
					var division = GL_search[z].getText({
						"name": "class",
						"summary": search.Summary.GROUP
					});
					
					var rskSegment = GL_search[z].getText({
						"name": "line.cseg_rsk_expenseseg",
						"summary": search.Summary.GROUP
					});
					
					if (debitAmount != '' || creditAmount != '') {
						if (creditAmount) {
							number = number + 1;
							
							htmlvar += '<tr>';
							
							htmlvar += '<td border-right="1" style="font-size:10px">' + number + '</td>';
							htmlvar += '<td border-right="1" style="font-size:11px"><p align="left">' + xml.escape(rskSegment == "- None -" ? account : rskSegment) + '</p></td>';
							htmlvar += '<td border-right="1" style="font-size:11px"><p align="left">' + xml.escape(department) + '</p></td>';
							htmlvar += '<td border-right="1" style="font-size:10px"><p align="left">' + xml.escape(division) + '</p></td>';
							
							htmlvar += '<td border-right="1" align="right"></td>';
							htmlvar += '<td align="right" style="font-size:10px">' + xml.escape(creditAmount).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</td>';
							htmlvar += '</tr>';
							
							creditAmount = parseFloat(creditAmount);
							creditAmountTotal = creditAmountTotal + creditAmount;
							
							
						}
					}
					
				}
				
				htmlvar += '<tr>';
				htmlvar += '<td colspan="3" align="left"  style="font-size:10px" border-top="1">TOTAL</td>';
				htmlvar += '<td border-top="1" style="font-size:10px"></td>';
				
				htmlvar += '<td border-left="1" border-top="1" align="right" style="font-size:10px"><b>' + debitAmountTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</b></td>';
				
				htmlvar += '<td border-top="1" border-left="1" align="right" style="font-size:10px"><b>' + creditAmountTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</b></td>';
				htmlvar += '</tr>';
				htmlvar += '</table>';
				
				htmlvar += '<table style="page-break-inside: avoid" width="100%">';
				htmlvar += '<tr>';
				htmlvar += '<td colspan = "2"  padding-top="5" align="center" font-size="10px">--- NOTHING FOLLOWS ---</td>';
				htmlvar += '</tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="500px" align="left" padding-left="15" style="font-size:10px">Prepared By:</td>';
				
				htmlvar += '<td width="300px" style="font-size:10px">Approved By:</td>';
				
				htmlvar += '</tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="500px" padding-top="1" padding-left="15">_____________________</td>';
				
				htmlvar += '<td width="300px" padding-top="1">_____________________</td>';
				htmlvar += '</tr>';
				
				htmlvar += '<tr>';
				htmlvar += '<td width="300px" align="left" padding-left="10" style="font-size:10px"><p align="center">' + preparedBy.toUpperCase() + '</p></td>';
				
				htmlvar += '<td width="200px" align="center" padding-left="3" style="font-size:10px">' + approvedBy.toUpperCase() + '</td>';
				htmlvar += '</tr>';
				htmlvar += '<tr>';
				htmlvar += '<td width="300px" align="left" padding-left="10" style="font-size:10px"><p align="center">' + xml.escape(dateAndTimeCreated) + '</p></td></tr>';
				htmlvar += '</table>';
				
				htmlvar += '</body>';
				htmlvar += '</pdf>';
				
				var file = render.xmlToPdf({
					xmlString: htmlvar
				});
				
				context.response.writeFile(file, true);
				return;
			}
		}
		
		function formatDate(data) {
			// return format.format({value: data, type: format.Type.DATE})  
			
			var dd = data.getDate();
			var mm = data.getMonth() + 1; //January is 0!
			
			var yyyy = data.getFullYear();
			if (dd < 10) {
				dd = '0' + dd;
			} 
			if (mm < 10) {
				mm = '0' + mm;
			} 
			return yyyy + '-' + mm + '-' + dd;
		} 
		
		/* function formatDate(dateVal)
		{
			dateVal = new Date(dateVal);
			var dd = dateVal.getDate();
			var mm = dateVal.getMonth() + 1;
			var yy = dateVal.getFullYear();
			var returnDate = format.parse({
				value: mm + '/' + dd + '/' + yy,
				type: format.Type.TEXT
			});
			return returnDate;
		} 
		 */
		
		return {
			onRequest: onRequest
		};
	});
	
		