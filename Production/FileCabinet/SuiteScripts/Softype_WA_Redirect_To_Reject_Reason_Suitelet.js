/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author  : Farhan Shaikh
 **@Dated   : 05/11/2019   DD/MM/YYYY
 **@Version :         1.0
 **@Description :     Workflow Action script To Call Suitelet For Rejection Reason
 ***************************************************************************************/
define(['N/record','N/search',"N/log","N/url","N/redirect"],    function(record,search,log,url,redirect){
    function redirectToSuitelet(context){
        var currentRecordId = context.newRecord.id;
        var currentRecObj = context.newRecord;
        log.debug('currentRecObj',currentRecObj)
        /*var suitletURL = url.resolveScript({
                scriptId: 'customscript_st_tran_rejection_reason',
                deploymentId: 'customdeploy_st_tran_rejection_reason',
                returnExternalUrl: true
        });*/

        redirect.toSuitelet({
            scriptId: 208 ,
            deploymentId: 1,
            parameters: {'recType':context.newRecord.type,'recId':currentRecordId}
        });
    }

    return {
        onAction: redirectToSuitelet
    };
});