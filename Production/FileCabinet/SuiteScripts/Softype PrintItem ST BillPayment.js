/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

/***************************************************************************************
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
  ** @Author     :  Meena
 ** @Dated       :  11 Dec 2019
 ** @Version     :  2.0
 ** @Description :  Suitelet to print the data from Bill Payment record after the userevent redirects here.

 ***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/record', 'N/xml', 'N/render', 'N/http', 'N/format', 'N/search'],
    function(record, xml, render, http, format, search) {
        function onRequest(context) {
            var ITEM = "Withholding Tax Payable - Expanded (WE_PH)";
            log.debug('item',ITEM);

            if (context.request.method == 'GET') {
                // Getting the parameters from Userevent.
                var action = context.request.parameters.action;
                var recordid = context.request.parameters.recordid;
                var loadrec = record.load({
                    type: 'vendorpayment',
                    id: recordid
                    //isDynamic: true
                });




                var subsidiary = loadrec.getText({
                    fieldId: 'subsidiary'
                });
                var supplierName = loadrec.getText({
                    fieldId: 'custbody_alternate_payee'
                });
                var pcvNo = loadrec.getValue({
                    fieldId: 'transactionnumber'
                });
                var address = loadrec.getValue({
                    fieldId: 'address'
                });
                var date = loadrec.getText({
                    fieldId: 'custbody_trans_date'
                });
                var amountInWords = loadrec.getValue({
                    fieldId: 'custbody_cashsale_totalword'
                });
                var totalAmount = loadrec.getValue({
                    fieldId: 'total'
                });



                var apvNumber = loadrec.getValue({
                    fieldId: 'custbody_apv_number'
                });
                var poNumber = loadrec.getValue({
                    fieldId: 'custbody_print_ref_po'
                });
                var rskNumber = loadrec.getValue({
                    fieldId: 'custbody_rskponum'
                });
                var bankAccount = loadrec.getText({
                    fieldId: 'account'
                });
                var checkNo = loadrec.getValue({
                    fieldId: 'tranid'
                });
                var datePrint = loadrec.getValue({
                    fieldId: 'custbody_nameon_checkprint'
                });

                var preparedBy = loadrec.getText({
                    fieldId: 'custbody_prepared_by'
                });
                var dateAndTimeCreated = loadrec.getText({
                    fieldId: 'custbody_date_time_create'
                });
                var terms = loadrec.getValue({
                    fieldId: 'terms'
                });
				
				var supplierId = loadrec.getValue({
					fieldId: 'entity'
				})
				
				log.audit('legalName',legalName);
				
				 var loadVendorRec = record.load({
                    type: 'vendor',
                    id: supplierId
                 });
				 
				 log.audit('loadVendorRec',loadVendorRec);
				 
				 var legalName = loadVendorRec.getText({
					 fieldId: 'legalname'
				 });
				 
				 log.audit('legalName',legalName);

                /* var currency = '₱'; */
                /* log.debug('currency',currency); */

                // Creating the htmlvar file to displayed in the suitelet.
                htmlvar = '';
                htmlvar += '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
                htmlvar += '<pdf>\n'
                htmlvar += '<head>';
                htmlvar += '<macrolist>';
                htmlvar += '<macro id="nlheader">';
                htmlvar += '<table align="center">';
                htmlvar += '<tr>';
                htmlvar += '<td colspan="4" align="center" padding-bottom="15" style="font-family: Arial, Helvetica, sans-serif; font-size:20px"><b>' + xml.escape(subsidiary) + '- CHECK VOUCHER</b></td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="150px" padding-top="5" align="left" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><b>PAID TO:&nbsp;</b></td>';
                htmlvar += '<td width="400px" padding-top="5" padding-left="5px" style="font-family: Arial, Helvetica, sans-serif, serif; font-size:10px"><b>' + xml.escape(supplierName == '' ? legalName : supplierName) + '</b></td>';
                htmlvar += '<td width="150px" padding-top="5" align="left" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><b>PCV NO:&nbsp;</b></td>';
                htmlvar += '<td width="200px" padding-top="5" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><b>' + xml.escape(pcvNo) + '</b></td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="100px" padding-top="5" style="font-family: Arial, Helvetica, sans-serif; font-size:10px;"><b>ADDRESS:&nbsp;</b></td>';
                htmlvar += '<td width="400px" padding-top="5" padding-left="5px" padding-right="30px" style="font-family: Arial, Helvetica, sans-serif; font-size:10px;"><b>' + xml.escape(address) + '</b></td>';
                htmlvar += '<td padding-top="5" padding-left="22px" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><b>DATE:</b></td>';
                htmlvar += '<td padding-top="5" padding-right="20px" style="font-family: Arial, Helvetica, sans-serif; font-size:10px;">' + xml.escape(date) + '</td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td></td>';
                htmlvar += '<td padding-top="8" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><b>Received from ' + subsidiary + '&nbsp;the amount of&nbsp;' + xml.escape(amountInWords) + '&nbsp;(' + xml.escape(totalAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + ')&nbsp;in full payment of amount described below:</b></td></tr>';
                htmlvar += '</table>';
                htmlvar += '</macro>';

                htmlvar += '<macro id="nlfooter">';
                htmlvar += '<table align="center">';
                htmlvar += '<tr>';
                htmlvar += '<td colspan = "4" align="center" font-family="font-family: Arial, Helvetica, sans-serif; font-size:10px">--- Nothing Follows ---</td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="100px" align="center" padding-top="14" padding-left="6" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">Printed By:</td>';
                htmlvar += '<td width="100px" align="center" padding-top="14" padding-left="17" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">Checked By:</td>';
                htmlvar += '<td width="100px" align="center" padding-top="14" padding-left="38" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">Approved By:</td>';
                htmlvar += '<td width="100px" align="center" padding-top="14" padding-left="47" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">Received By:</td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="200px" padding-top="12" padding-left="5">_______________</td>';
                htmlvar += '<td width="200px" padding-top="12" padding-left="19">_______________</td>';
                htmlvar += '<td width="200px" padding-top="12" padding-left="41">_______________</td>';
                htmlvar += '<td width="200px" padding-top="12" padding-left="47">_______________</td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="200px" align="center" padding-top="7" padding-left="8" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><p align="center">' + xml.escape(preparedBy.toUpperCase()) + '</p></td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="200px" align="center" padding-top="7" padding-left="3" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">' + xml.escape(dateAndTimeCreated) + '</td>';
                htmlvar += '</tr>';
                htmlvar += '</table>';
				
				



                htmlvar += '</macro>';
                htmlvar += '</macrolist>';
				
				htmlvar += '</head>';


                htmlvar += '<body header="nlheader" header-height="30%" footer="nlfooter" footer-height="20%" width="21.50cm" height="16.50cm" padding="0.3in 0.5in 0.5in 0.5in">';

                htmlvar += '<table width="100%" border="1" align="center">';
                htmlvar += '<tr>';
                htmlvar += '<th width="55%" border-right="1" border-bottom="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:12px"><b>APV No. / DESCRIPTION</b></th>';
                htmlvar += '<th width="15%" border-right="1" border-bottom="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:12px"><b>PO NO.</b></th>';
                htmlvar += '<th width="15%" border-right="1" border-bottom="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:12px"><b>TERMS</b></th>';
                htmlvar += '<th width="15%" border-bottom="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:12px"><b>AMOUNT</b></th>';
                htmlvar += '</tr>';



                var numLines = loadrec.getLineCount({
                    sublistId: 'apply'
                });

                //log.debug('numLines:', numLines);



                for (var i = 0; i < numLines; i++) { //1

                    var internalId = loadrec.getSublistValue({
                        sublistId: 'apply',
                        fieldId: 'internalid',
                        line: i
                    });
                    //log.debug('internalId', internalId);
				

                    var loadLineItem = record.load({
                        type: 'vendorbill',
                        id: internalId,
                        isDynamic: true
                    });



                    //supplier invoice item list 

                    var numItem = loadLineItem.getLineCount({
                        sublistId: 'item'
                    });
                    //log.debug('numItem', numItem);



                    //purchase order internal id
						
                    var numPurchaseOrder = loadLineItem.getLineCount({
                        sublistId: 'purchaseorders'
                    });
					log.debug('numPurchaseOrder',numPurchaseOrder);
					if(numPurchaseOrder){
                   // log.debug('numPurchaseOrder', numPurchaseOrder);
					

                    var purchaseInternalId = loadLineItem.getSublistValue({
                        sublistId: 'purchaseorders',
                        fieldId: 'id',
                        line: i
                    });
					

                    //log.debug('purchaseInternalId', purchaseInternalId);

                    var purchaseOrderLoad = record.load({
                        type: 'purchaseorder',
                        id: purchaseInternalId,
                        isDynamic: true
                    });
					
					log.debug('purchaseOrderLoad', purchaseOrderLoad);

					var refPo = purchaseOrderLoad.getValue({
                            fieldId: 'tranid'
                        });
						
						log.debug('refPo', refPo);
						
                    
                    var purchaseNumItem = purchaseOrderLoad.getLineCount({
                        sublistId: 'item'
                    });

                   // log.debug('purchaseNumItem', purchaseNumItem);

                    for (var k = 0; k < purchaseNumItem; k++) { //2

                        /* var lineItem = numItem.selectLine({
                            sublistId: 'item',
                            line: k
                        }); */


                        var itemName = purchaseOrderLoad.getSublistText({
                            sublistId: 'item',
                            fieldId: 'item',
                            line: k
                        });
                       // log.debug('itemDetails:', itemName);

                        if (itemName == ITEM)
                            continue;


                        var itemRate = purchaseOrderLoad.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'rate',
                            line: k
                        });

                        itemRate = itemRate.toFixed(2);

                        //log.debug('itemRate', itemRate);

                        var itemQuantity = purchaseOrderLoad.getSublistText({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            line: k
                        });

                        //log.debug('itemQuantity', itemQuantity);

                        var itemAmount = purchaseOrderLoad.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'amount',
                            line: k
                        });

                        //log.debug('itemAmount', itemAmount);

                        var linkedOrder = purchaseOrderLoad.getSublistText({
                            sublistId: 'item',
                            fieldId: 'linkedorder',
                            line: k
                        });
                       // log.debug('linkedOrder', linkedOrder);


                        var custbodyApvNumber = loadLineItem.getValue({
                            fieldId: 'custbody_apv_number'
                        });

                        //log.debug('custbodyApvNumber', custbodyApvNumber);

                        //if(refPo != '' && refPo != null){		
                        
                        //}
						
						

                       // log.debug('refPo', JSON.stringify(refPo));

                        var terms = loadLineItem.getText({
                            fieldId: 'terms'
                        });

                        //log.debug('terms', terms);




                        htmlvar += '<tr>';
                        htmlvar += '<td border-right="1" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">' + (custbodyApvNumber == null ? ' ' : custbodyApvNumber) + ',&nbsp;' + (refPo == null ? ' ' : refPo)+ ',&nbsp; RS '+(linkedOrder == null ? '' : linkedOrder) + ',&nbsp;&nbsp;&nbsp;&nbsp;' + (itemQuantity == ITEM ? ' ' : itemQuantity) + '&nbsp;' + (itemName == ITEM ? ' ' : itemName) + '&nbsp;@' + (itemRate == ITEM ? ' ' : itemRate) + '</td>';

                        htmlvar += '<td border-right="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"></td>';
                        htmlvar += '<td border-right="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">' + xml.escape(terms == undefined ? ' ' : terms) + '</td>'; //terms
                        htmlvar += '<td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">' + xml.escape(itemAmount == undefined ? '0' : itemAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + '</td>';
                        htmlvar += '</tr>';


                    }
                }
				
				else if(numPurchaseOrder == 0){
					
					 var supplierNumItem = loadLineItem.getLineCount({
                        sublistId: 'item'
                     });
					 log.debug('supplierNumItem',supplierNumItem);
						if(supplierNumItem){
						for(var i=0; i<supplierNumItem; i++){
						 var itemName = loadLineItem.getSublistText({
								sublistId: 'item',
								fieldId: 'item',
								line: i
							});
							log.debug('@@@itemDetails:', itemName);

                        if (itemName == ITEM)
                            continue;


                        var itemRate = loadLineItem.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'rate',
                            line: i
                        });

                        itemRate = itemRate.toFixed(2);

                        log.debug('@@@itemRate', itemRate);

                        var itemQuantity = loadLineItem.getSublistText({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            line: i
                        });

                        log.debug('@@@itemQuantity', itemQuantity);

                        var itemAmount = loadLineItem.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'amount',
                            line: i
                        });

                        log.debug('@@@itemAmount', itemAmount);

                        var linkedOrder = loadLineItem.getSublistText({
                            sublistId: 'item',
                            fieldId: 'linkedorder',
                            line: i
                        });
                        log.debug('@@@linkedOrder', linkedOrder);

						htmlvar += '<tr>';
                        htmlvar += '<td border-right="1" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">' + (custbodyApvNumber == null || undefined ? ' ' : custbodyApvNumber) + ',&nbsp;' + (refPo == null || undefined ? ' ' : refPo)+ ',&nbsp; RS '+(linkedOrder == null || undefined ? '' : linkedOrder) + ',&nbsp;&nbsp;&nbsp;&nbsp;' + (itemQuantity == ITEM ? ' ' : itemQuantity) + '&nbsp;' + (itemName == ITEM ? ' ' : itemName) + '&nbsp;@' + (itemRate == ITEM ? ' ' : itemRate) + '</td>';
                        htmlvar += '<td border-right="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"></td>';
                        htmlvar += '<td border-right="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"></td>'; //terms
                        htmlvar += '<td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size:10px">' + xml.escape(itemAmount == undefined ? '0' : itemAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + '</td>';
                        htmlvar += '</tr>';
					
					
					
					}
					}
						/* 	else if(supplierNumItem == 0){
						
						 var supplierNumExpense = loadLineItem.getLineCount({
                        sublistId: 'expense'
                     });
					 log.debug('supplierNumExpense',supplierNumExpense);
						for(var j=0; j<supplierNumExpense; j++){
						 var itemName = loadLineItem.getSublistText({
								sublistId: 'expense',
								fieldId: 'item',
								line: j
							});
							log.debug('@@@itemDetails:', itemName);

                        if (itemName == ITEM)
                            continue;


                        var itemRate = loadLineItem.getSublistValue({
                            sublistId: 'expense',
                            fieldId: 'rate',
                            line: j
                        });
						if(itemRate){
                        itemRate = itemRate.toFixed(2);

                        log.debug('@@@itemRate', itemRate);
						}
                        var itemQuantity = loadLineItem.getSublistText({
                            sublistId: 'expense',
                            fieldId: 'quantity',
                            line: j
                        });

                        log.debug('@@@itemQuantity', itemQuantity);

                        var itemAmount = loadLineItem.getSublistValue({
                            sublistId: 'expense',
                            fieldId: 'amount',
                            line: j
                        });

                        log.debug('@@@itemAmount', itemAmount);

                        var linkedOrder = loadLineItem.getSublistText({
                            sublistId: 'expense',
                            fieldId: 'linkedorder',
                            line: j
                        });
                        log.debug('@@@linkedOrder', linkedOrder);

						htmlvar += '<tr>';
                        htmlvar += '<td border-right="1" style="font-family: Times New Roman, Times, serif; font-size:12px">' + (custbodyApvNumber == null ? ' ' : custbodyApvNumber) + ',&nbsp;' + (refPo == null ? ' ' : refPo)+ ',&nbsp; RS '+(linkedOrder == null ? '' : linkedOrder) + ',&nbsp;&nbsp;&nbsp;&nbsp;' + (itemQuantity == ITEM ? ' ' : itemQuantity) + '&nbsp;' + (itemName == ITEM ? ' ' : itemName) + '&nbsp;@' + (itemRate == ITEM ? ' ' : itemRate) + '</td>';
                        htmlvar += '<td border-right="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"></td>';
                        htmlvar += '<td border-right="1" align="center" style="font-family: Times New Roman, Times, serif; font-size:10px"></td>'; //terms
                        htmlvar += '<td align="right" style="font-family: Times New Roman, Times, serif; font-size:10px">' + xml.escape(itemAmount == undefined ? '0' : itemAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + '</td>';
                        htmlvar += '</tr>';
					}
					
				} */
				}
				}
                htmlvar += '</table>';
                htmlvar += '<table width="100%" align="center" border-bottom="1" border-left="1" border-right="1">';
                htmlvar += '<tr>';
                htmlvar += '<td border-bottom="1" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><b>PAYMENT INFORMATION</b></td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td padding-left="3px" style="font-family: Arial, Helvetica, sans-serif; font-size:10px"><b>Check No.:&nbsp; ' + xml.escape(bankAccount) + '&nbsp;&nbsp;&nbsp;' + xml.escape(checkNo) + '&nbsp;&nbsp;' + xml.escape(datePrint == undefined ? ' ' : datePrint) + '&nbsp;&nbsp;PHP' + xml.escape(totalAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')) + '</b></td>';
                htmlvar += '</tr>';

                htmlvar += '</table>';


                htmlvar += '</body>';
                htmlvar += '</pdf>';




                var file = render.xmlToPdf({
                    xmlString: htmlvar
                });

                context.response.writeFile(file, true);
                return;
            }
        }


        return {
            onRequest: onRequest
        };
    });