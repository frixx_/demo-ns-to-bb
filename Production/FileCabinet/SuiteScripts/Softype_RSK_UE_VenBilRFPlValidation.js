/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
 /***************************************************************************************  
 ** Copyright (c) 1998-2020 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      : Aayushi Chaplot
 **@Dated       : 9-01-2020
 **@Version     : 2.x
 **@Description : Validating a RFT field of supplier Invoice where value is set to the RFT Field and once its set user cant change after saving
 ***************************************************************************************/
 

define(['N/ui/serverWidget', 'N/search','N/record'],function(ui, search,record){
	function beforeLoad(context){
		try{
			//taking value of RFT Field
			log.debug('context=',context);
			var currentRec = context.newRecord;
			var getval = currentRec.getValue({
				fieldId : 'custbody_ref_rfp_num'
			});
			//for make copy ..
			if(context.type == 'copy'){
				log.debug('context.type=',context.type);
				currentRec.setValue({
					fieldId:'custbody_ref_rfp_num',
					value:''
				});				//value is blank becuase on script, Inline(whose value can change) applied on RFT field on make copy its giveing RFT value that we dont want 
				return;
				//on make copy, dont want rest of of the code should run

			}
			log.debug('getval=',getval);
			if(getval){
				// document.getElementById('inpt_custbody_ref_rfp_num6').disabled = true;

				var form = context.form;
				var getRFPField = form.getField({
					id:'custbody_ref_rfp_num'
				});
				//RFP field will always update on the basis of Supplier Invoice
				getRFPField.updateDisplayType({
					displayType : ui.FieldDisplayType.INLINE
				});
				//Inline: so that, cant change the value of Field
				log.debug('getRFPRield=',getRFPField);
			}
		}
		catch(exception){
			log.debug('exception', exception);
		}
	}
	function beforeSubmit(context){
		try{
			var currentRec = context.newRecord;
			var getRFP = currentRec.getValue({
				fieldId:'custbody_ref_rfp_num'
			});
			var getRFP = currentRec.getValue({
				fieldId:'custbody_ref_rfp_num'
			});

			if(getRFP != ''){

				var rec = record.load({
					type:'customrecord_rfp',
					id:getRFP
				});
				var getRFPAmount = rec.getValue({
					fieldId:'custrecord_rfp_amount'
				});
				
				log.debug("Total RFP amount", getRFPAmount);
				
				// Get Remaining amount from RFP after deducting Gross amount from attached supplier invoices
				var getSupplierInvoices = rec.getValue({
					fieldId:'custrecord_rfp_supplierbill'
				});
				
				log.debug("Supplier Invoices attached to RFP", getSupplierInvoices);
				
				var amountAlreadyAttached = 0;
				
				for(var z = 0; z < getSupplierInvoices.length; z++){
				
					var invoice = record.load({
						type:'vendorbill',
						id:getSupplierInvoices[z]
					});
					
					log.debug("Checking invoice", getSupplierInvoices[z]);
					
					var venBillTotLines = currentRec.getLineCount({
						sublistId: 'item'
					});
					for(var i = 0; i < venBillTotLines; i++){
				
						var grossAmount = invoice.getSublistValue({
							sublistId: 'item',
							fieldId: 'grossamt',
							line: i
						});
						
						log.debug("Checking invoice " + getSupplierInvoices[z], grossAmount);
						
						amountAlreadyAttached += grossAmount;
					}
					
				}
				
				log.debug("Amount already attached to RFP", amountAlreadyAttached);
				
				var availableAmount = getRFPAmount - amountAlreadyAttached;
				
				log.debug("Available Amount", availableAmount);
				
				// Getting gross amount from current Supplier Invoice
				var venBillTotLines = currentRec.getLineCount({
					sublistId: 'item'
				});
				
				var amountToBePaid = 0;
				
				for(var i = 0; i < venBillTotLines; i++){
				
					var grossAmount = currentRec.getSublistValue({
						sublistId: 'item',
						fieldId: 'grossamt',
						line: i
					});
					
					amountToBePaid += grossAmount;
				}
							
				log.debug("Amount to be Paid", amountToBePaid);
				
				if(availableAmount < amountToBePaid){
					// throw ' Exception :- Please choose another RFP.';
				}

				/* var getSupplierInvoice = rec.getValue({
					fieldId:'custrecord_rfp_supplierbill'
				});

				log.debug('getSupplier=',getSupplierInvoice);

				if(getSupplierInvoice !='')
				{
					throw 'Exception :- please choose another RFP';
				} */
			}
		}catch(err){
			log.debug("Error", err);
		}
	}
	return{
		beforeLoad:beforeLoad,
		beforeSubmit:beforeSubmit	
	} 


});