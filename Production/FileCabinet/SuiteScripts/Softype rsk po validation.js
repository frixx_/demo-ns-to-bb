/**
 *@NApiVersion 2.x
 *@NScriptType ClientScript
 */
define(['N/currentRecord', 'N/search'],
    function(currentRecord, search) {

        function validateField(context) {
            return check(context);
        }
		
		function check(context){
			var currentRec = context.currentRecord;

             if (context.fieldId == 'entity' || context.fieldId == 'location' || context.sublistId == 'item' && context.fieldId == 'item') {
                var supplierId = currentRec.getValue({
                    fieldId: 'entity'
                });

                console.log('supplierId' +supplierId);

                if (supplierId != '') {
                    var supplierRskFieldId = (search.lookupFields({
                        type: search.Type.VENDOR,
                        id: supplierId,
                        columns: ['custentity_rsksuppleirid']
                    }))['custentity_rsksuppleirid'];


                    console.log('supplierRskFieldId' + supplierRskFieldId);

                    if (supplierRskFieldId == '') {
                        alert('Please select vendor with RSK ID');
                        return false;
						
					
                    }
                }
            // }
            // if (context.fieldId == 'location') {
                var locationId = currentRec.getValue({
                    fieldId: 'location'
                });

                if (locationId != '') {
                    var locationRskFieldId = (search.lookupFields({
                        type: search.Type.LOCATION,
                        id: locationId,
                        columns: ['custrecord_location_rskidfield']
                    }))['custrecord_location_rskidfield'];

                    if (locationRskFieldId == '') {
                        alert('Please select another Location with RSK ID');

                        return false;
                    }
                }
           //  }
           //  if (context.sublistId == 'item' && context.fieldId == 'item') {
                var itemId = currentRec.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item'
                });
                if (itemId != '') {
                    var itemRskFieldId = (search.lookupFields({
                        type: "noninventoryitem",
                        id: itemId,
                        columns: ['custitem_rskitemid']
                    }))['custitem_rskitemid'];

                    if (itemRskFieldId == '') {
                        alert('Please select another Item with RSK ID');
                        return false;
                    }
                }
            }
            return true;
		}


        return {
            validateField: validateField
        };
    });