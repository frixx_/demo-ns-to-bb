/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Meena Nag
 **@Dated       :  20th January, 2020
 **@Version     :  2.x
 **@Description :  To create custom print button on Supplier Invoice.
 ***************************************************************************************/



/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */
 
 define(['N/runtime','N/search','N/record','N/ui/serverWidget','N/url'],
	function(runtime,search,record,serverWidget,url){
		function beforeLoad(scriptContext){
			
		var currentRecord = scriptContext.newRecord; 
			log.debug('currentRecord:',currentRecord);
			if(scriptContext.type == 'view') {
			var form = scriptContext.form;
			var recordType = currentRecord.type;
			var recordid = currentRecord.id;
			log.debug('recordType:',recordType);
			
			var statusApproved = currentRecord.getValue('approvalstatus');
			
			var ADMINISTRATOR = 3;
			
			var userObj = runtime.getCurrentUser();
		
			var outputUrl = url.resolveScript
		({
			scriptId: 'customscript_print_st_supplierinvoice',
			deploymentId: 'customdeploy_print_st_supplierinvoice',
			returnExternalUrl: false
		});
		// Adding parameters to pass in the suitelet.
		outputUrl += '&action=GET';
		outputUrl += '&recordid=' + recordid;

		// Creating function to redirect to the suitelet.
		var stringScript = "window.open('"+outputUrl+"','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";
			if(statusApproved == 1){
			var printLabel = form.addButton({
				id: 'custpage_printId',
				label: 'APV Print',
				functionName: stringScript
			});
			}
			
			else if(statusApproved == 2 && userObj.role == ADMINISTRATOR){
			var printLabel = form.addButton({
				id: 'custpage_printId',
				label: 'APV Print',
				functionName: stringScript
			});
			}
			 
		
			}
		
		
		}
		return{
		beforeLoad: beforeLoad
		};
		});