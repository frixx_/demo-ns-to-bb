/***************************************************************************************  
** Copyright (c) 1998-2018 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  
**                        
** @author:  Rajendran C Achari
** @version: 1.0
** Description: To add subsidiary on creation of Customer.
***************************************************************************************/

	function afterSubmit_AddSubsidiary(type)
	{
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		nlapiLogExecution("DEBUG","Start");
       
       var context = nlapiGetContext().getExecutionContext();
       nlapiLogExecution("DEBUG","context",context);
       
      
       
	if((type=='create' || type== 'CREATE'|| type=='edit' || type== 'EDIT') && (context == 'userinterface' || context == 'USERINTERFACE'|| context == 'csvimport' || context == 'CSVIMPORT'))	  
		{
		
			var current_Customer = nlapiLoadRecord(recType,recId);	
			var customFormId = current_Customer.getFieldValue('customform');
			nlapiLogExecution("DEBUG","customFormId",customFormId);
			
			
			var All_Sub = new Array();
			var columns = new Array();
			columns.push(new nlobjSearchColumn('name'));
			var filter = new Array();
          filter.push(new nlobjSearchFilter('iselimination',null,'is','F'));
          filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));
          //filter.push(new nlobjSearchFilter('custrecord_subsidiary_customer',null,'is','T'));
			var srchSubsidary = nlapiSearchRecord('subsidiary', null, filter, columns);
			nlapiLogExecution('audit','srchSubsidary length',srchSubsidary.length);
			if(srchSubsidary){
				
			for (var i = 0; i < srchSubsidary.length; i++) 
				{
				var ID = srchSubsidary[i].getId();
				All_Sub.push(ID);
				}
			}
			nlapiLogExecution('audit','All_Sub',All_Sub);	


					var customerAlreadyAsignedSubsidiary = [];
			
			
					var lineNum = current_Customer.getLineItemCount('submachine');
					nlapiLogExecution('audit','Linecount',lineNum);
					
				for(var k = 1; k <= lineNum; k++){
					
					var subsidiaryValue = current_Customer.getLineItemValue('submachine', 'subsidiary', k);	
					customerAlreadyAsignedSubsidiary.push(subsidiaryValue);
					
				}
				nlapiLogExecution('audit','customerAlreadyAsignedSubsidiary',customerAlreadyAsignedSubsidiary);
				
					
				for (var j = 0; j < All_Sub.length; j++) 
				{
					
					//to check wheather subsidiary to be added already exist in customer
					if(customerAlreadyAsignedSubsidiary.indexOf(All_Sub[j]) == -1){
						nlapiLogExecution('audit','new subsidiary');
						current_Customer.selectNewLineItem('submachine'); 
						current_Customer.setCurrentLineItemValue('submachine', 'subsidiary', All_Sub[j]); 
						current_Customer.commitLineItem('submachine');
					}
					
				}

				var submit_vendor = nlapiSubmitRecord(current_Customer,true);

			
		}
	
	}	